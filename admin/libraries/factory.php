<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

/**
 * ZJ_Donation Component - Factory Library
 * @package		ZJ_Donation
 * @subpackage	Library
 */
class ZJ_DonationFactory {
	
	/**
	 * Get credits footer string.
	 */
	function getFooter() {
		return '<p class="zj_copyright"><span class="zj_title">ZJ Donation v1.0.0</span> copyright (C) 2011 by Joomseller Solutions - <a href="http://www.joomseller.com" target="_blank">http://www.joomseller.com</a></p>';
	}

	/**
	 * Get model of component
	 */
	function &getModel($type, $prefix = 'ZJ_DonationModel', $config = array()) {
		static $instance;

		if(!isset($instance[$type]) || !is_object($instance[$type])) {
			$instance[$type] = JModel::getInstance($type, $prefix, $config);
		}

		return $instance[$type];
	}

	/**
	 * Get component configuration
	 */
	function getConfig() {
		static $instance;

		if(!is_object($instance)) {
			$config_model	= &ZJ_DonationFactory::getModel('configs');
			$instance		= $config_model->getData();
		}

		return $instance;
	}
}
