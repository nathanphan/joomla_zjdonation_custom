<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');

/**
 * ZJ_Donation -  Base Controller
 * @package		ZJ_Donation
 * @subpackage	Controller
 */
class ZJ_DonationController extends JController {
	/**
	 * Display.
	 */
	function display() {
		// set a default view if none exists
		if (!JRequest::getCmd('view')) {
			JRequest::setVar('view', 'dashboard');
		}
		
		parent::display();
	}

	/**
	 * Overloaded getModel method.
	 */
	function getModel($name = '', $prefix = '', $config = array()) {
		if(empty($name)) {
			$r = null;
			if(!preg_match('/ZJ_DonationController(.*)/i', get_class($this), $r)) {
				JError::raiseError(500, "JController::getName() : Cannot get or parse class name.");
			}

			$name = strtolower($r[1]);
		}

		return parent::getModel($name, $prefix, $config);
	}
}
