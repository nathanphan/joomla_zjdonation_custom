INSERT INTO `#__zj_donation_recurring_types` (`id`, `title`, `description`, `type`, `value`, `published`, `ordering`, `checked_out`, `checked_out_time`) VALUES
(1, 'Daily', 'Daily', 'day', 1, 1, 1, 0, '0000-00-00 00:00:00'),
(2, 'Weekly', 'Weekly', 'week', 1, 1, 2, 0, '0000-00-00 00:00:00'),
(3, 'Monthly', 'Monthly donation', 'month', 1, 1, 3, 0, '0000-00-00 00:00:00'),
(4, 'Quarterly', 'Quarterly donation', 'month', 3, 1, 4, 0, '0000-00-00 00:00:00'),
(5, 'Annually', 'Annually Donation', 'year', 1, 1, 5, 0, '0000-00-00 00:00:00');