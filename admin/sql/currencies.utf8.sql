INSERT INTO `#__zj_donation_currencies` (`id`, `title`, `code`, `sign`, `position`, `description`, `published`, `ordering`, `checked_out`, `checked_out_time`) VALUES
(1, 'US Dollar', 'USD', '$', 0, '', 1, 1, 0, '0000-00-00 00:00:00'),
(2, 'Euro', 'EUR', '&euro;', 0, '', 1, 2, 0, '0000-00-00 00:00:00'),
(3, 'Pound', 'GBP', '£', 0, '', 1, 3, 0, '0000-00-00 00:00:00');