<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.filesystem.folder' );

function com_install() {

	$db			= JFactory::getDBO();
	
	//Setup default currency
	$sql	= 'SELECT count(*) FROM #__zj_donation_currencies';
	$db->setQuery($sql);
	$total	= $db->loadResult();
	if (!$total) {
		$configSql = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_zj_donation'.DS.'sql'.DS.'currencies.utf8.sql' ;
		$sql = JFile::read($configSql) ;
		$queries = $db->splitSql($sql);
		if (count($queries)) {
			foreach ($queries as $query) {
			if ($query != '' && $query{0} != '#') {
					$db->setQuery($query);
					$db->query();
				}
			}
		}
	}

	//Setup default countries
	$sql	= 'SELECT count(*) FROM #__zj_donation_countries';
	$db->setQuery($sql);
	$total	= $db->loadResult();
	if (!$total) {
		$configSql = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_zj_donation'.DS.'sql'.DS.'countries.utf8.sql' ;
		$sql = JFile::read($configSql) ;
		$queries = $db->splitSql($sql);
		if (count($queries)) {
			foreach ($queries as $query) {
			if ($query != '' && $query{0} != '#') {
					$db->setQuery($query);
					$db->query();
				}
			}
		}
	}

	//Setup default configs
	$sql	= 'SELECT count(*) FROM #__zj_donation_configs';
	$db->setQuery($sql);
	$total	= $db->loadResult();
	if (!$total) {
		$configSql = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_zj_donation'.DS.'sql'.DS.'configs.utf8.sql' ;
		$sql = JFile::read($configSql) ;
		$queries = $db->splitSql($sql);
		if (count($queries)) {
			foreach ($queries as $query) {
			if ($query != '' && $query{0} != '#') {
					$db->setQuery($query);
					$db->query();
				}
			}
		}
	}

	//Setup default recurring type
	$sql	= 'SELECT count(*) FROM #__zj_donation_recurring_types';
	$db->setQuery($sql);
	$total	= $db->loadResult();
	if (!$total) {
		$configSql = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_zj_donation'.DS.'sql'.DS.'recurring.utf8.sql' ;
		$sql = JFile::read($configSql) ;
		$queries = $db->splitSql($sql);
		if (count($queries)) {
			foreach ($queries as $query) {
			if ($query != '' && $query{0} != '#') {
					$db->setQuery($query);
					$db->query();
				}
			}
		}
	}
		
	$sql	= "ALTER TABLE `#__zj_donation_donates` ADD `access` TINYINT NOT NULL DEFAULT '1' AFTER `subscribe_id`";
	$db->setQuery($sql);
	$db->query();
	
	$sql	= "ALTER TABLE  `#__zj_donation_campaigns` ADD  `country_id` INT NOT NULL AFTER  `amounts`";
	$db->setQuery($sql);
	$db->query();
}
?>