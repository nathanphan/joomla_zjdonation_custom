<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
?>

<form action="index.php" method="post" name="adminForm">
	<table class="adminform">
		<tr>
			<td align="left" width="100%">
				<?php echo JText::_('Filter'); ?>:
				<input type="text" class="text_area" name="search" id="search" value="<?php echo $this->lists['search']; ?>" onchange="document.adminForm.submit();" />
				<button onclick="this.form.submit();"><?php echo JText::_('Go'); ?></button>
				<button onclick="this.form.getElementById('search').value=''; this.form.getElementById('filter_state').value = '0'; this.form.submit();">
					<?php echo JText::_('Reset'); ?>
				</button>
			</td>
			<td nowrap="nowrap">
				<?php
				echo $this->lists['campaign'];
				echo $this->lists['state'];
				?>
			</td>
		</tr>
	</table>
	<div id="editcell">
		<table class="adminlist">
			<thead>
				<tr>
					<th width="5">
						<?php echo JText::_('NUM'); ?>
					</th>
					<th width="20">
						<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($this->rows); ?>);" />
					</th>
					<th class="title">
						<?php echo JHTML::_('grid.sort', JText::_('First Name'), 'a.first_name', $this->lists['order_Dir'], $this->lists['order']); ?>
					</th>
					<th class="title">
						<?php echo JHTML::_('grid.sort', JText::_('Last Name'), 'a.last_name', $this->lists['order_Dir'], $this->lists['order']); ?>
					</th>
					<th class="title">
						<?php echo JHTML::_('grid.sort', JText::_('Email'), 'a.email', $this->lists['order_Dir'], $this->lists['order']); ?>
					</th>
					<th class="title">
						<?php echo JHTML::_('grid.sort', JText::_('Campaign'), 'c.title', $this->lists['order_Dir'], $this->lists['order']); ?>
					</th>
					<th class="title">
						<?php echo JHTML::_('grid.sort', JText::_('Donation Type'), 'r.title', $this->lists['order_Dir'], $this->lists['order']); ?>
					</th>
					<th class="title">
						<?php echo JHTML::_('grid.sort', JText::_('Recurring Times'), 'a.recurring_times', $this->lists['order_Dir'], $this->lists['order']); ?>
					</th>
					<th class="title">
						<?php echo JHTML::_('grid.sort', JText::_('Amount'), 'a.amount', $this->lists['order_Dir'], $this->lists['order']); ?>
					</th>
					<th class="title">
						<?php echo JHTML::_('grid.sort', JText::_('Payment Method'), 'a.payment_method', $this->lists['order_Dir'], $this->lists['order']); ?>
					</th>
					<th class="title">
						<?php echo JHTML::_('grid.sort', JText::_('Transaction ID'), 'a.transaction_id', $this->lists['order_Dir'], $this->lists['order']); ?>
					</th>
					<th class="title">
						<?php echo JHTML::_('grid.sort', JText::_('Donated'), 'a.donated', $this->lists['order_Dir'], $this->lists['order']); ?>
					</th>
					<th width="5%" nowrap="nowrap">
						<?php echo JHTML::_('grid.sort', JText::_('Published'), 'a.published', $this->lists['order_Dir'], $this->lists['order']); ?>
					</th>
					<th class="title">
						<?php echo JHTML::_('grid.sort', JText::_('Date'), 'a.created_date', $this->lists['order_Dir'], $this->lists['order']); ?>
					</th>
					<th width="1%" nowrap="nowrap">
						<?php echo JHTML::_('grid.sort', JText::_('ID'), 'a.id', $this->lists['order_Dir'], $this->lists['order']); ?>
					</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="15">
						<?php echo $this->pagination->getListFooter(); ?>
					</td>
				</tr>
			</tfoot>
			<tbody>
			<?php
				$k = 0;
				for ($i = 0, $n = count($this->rows); $i < $n; $i++) {
					$row =& $this->rows[$i];

					$row->checked_out	= 0;

					$link		= JRoute::_('index.php?option=com_zj_donation&controller=donates&task=view&cid[]=' . $row->id);
					$link_campaign	= JRoute::_('index.php?option=com_zj_donation&controller=campaigns&task=edit&cid[]=' . $row->campaign_id);
					
					$checked	= JHTML::_('grid.checkedout', $row, $i);
					$published	= JHTML::_('grid.published', $row, $i);
				?>
				<tr class="row<?php echo $k; ?>">
					<td>
						<?php echo $this->pagination->getRowOffset($i); ?>
					</td>
					<td>
						<?php echo $checked; ?>
					</td>
					<td>
						<span class="editlinktip hasTip" title="<?php echo JText::_('View Donation'); ?>::<?php echo $this->escape($row->first_name); ?>">
							<a href="<?php echo $link; ?>">
								<?php echo $this->escape($row->first_name); ?>
							</a>
						</span>
					</td>
					<td align="center">
						<?php echo $row->last_name; ?>
					</td>
					<td align="center">
						<?php echo $row->email; ?>
					</td>
					<td>
						<span class="editlinktip hasTip" title="<?php echo JText::_('Edit Campaign'); ?>::<?php echo $this->escape($row->campaign); ?>">
							<a href="<?php echo $link_campaign; ?>">
								<?php echo $this->escape($row->campaign); ?>
							</a>
						</span>
					</td>
					<td align="center">
						<?php echo $row->recurring ? $row->recurring_title : JText::_('One Time'); ?>
					</td>
					<td align="center">
						<?php echo ($row->recurring) ? (int)$row->donated_times.'/'.(int)$row->recurring_times : '0'; ?>
					</td>
					<td align="center">
						<?php echo $row->amount; ?>
					</td>
					<td align="center">
						<?php echo $row->payment_method; ?>
					</td>
					<td align="center">
						<?php echo $row->transaction_id; ?>
					</td>
					<td align="center">
						<?php echo JHTML::_('zj_donation.icon', $row->donated, JText::_('Donated'), JText::_('Failed')); ?>
					</td>
					<td align="center">
						<?php echo $published; ?>
					</td>
					<td align="center" nowrap="nowrap">
						<?php echo JHTML::_('date', $row->created_date, JText::_('DATE_FORMAT_LC4')); ?>
					</td>
					<td align="center">
						<?php echo $row->id; ?>
					</td>
				</tr>
				<?php
					$k = 1 - $k;
				}
				?>
			</tbody>
		</table>
	</div>

	<input type="hidden" name="option" value="com_zj_donation" />
	<input type="hidden" name="view" value="donates" />
	<input type="hidden" name="controller" value="donates" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="campaign_id" value="<?php echo $this->campaign_id; ?>" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
	<?php echo JHTML::_('form.token'); ?>
</form>

<?php
echo ZJ_DonationFactory::getFooter();