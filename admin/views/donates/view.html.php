<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

/**
 * ZJ_Donation Component - Donates View.
 * @package		ZJ_Donation
 * @subpackage	View
 */
class ZJ_DonationViewDonates extends JView {
	/**
	 * Display.
	 */
	function display($tpl = null) {
		global $mainframe, $option;

		

		// get data from model
		$rows		= &$this->get('Data');
		$total		= &$this->get('Total');
		$pagination	= &$this->get('Pagination');

		// build lists html
		$lists		= $this->_buildLists();
		$ordering	= $lists['order'] == 'a.created_date';

		$this->addToolBar();

		$filter_campaign_id = $mainframe->getUserStateFromRequest($option . '.donates.filter_campaign_id', 'filter_campaign_id', 0, 'int');

		$this->assignRef('user',		JFactory::getUser());
		$this->assignRef('lists',		$lists);
		$this->assignRef('rows',		$rows);
		$this->assignRef('campaign_id',	$filter_campaign_id);
		$this->assignRef('ordering',	$ordering);
		$this->assignRef('pagination',	$pagination);

		parent::display($tpl);
	}

	function addToolBar() {
		// set page title
		$document = &JFactory::getDocument();
		$document->setTitle(JText::_('Donation Management'));

		// load behavior
		JHTML::_('behavior.tooltip');

		// add submenu
		JSubMenuHelper::addEntry(JText::_('Dashboard'), 'index.php?option=com_zj_donation');
		JSubMenuHelper::addEntry(JText::_('Campaigns'), 'index.php?option=com_zj_donation&view=campaigns');
		JSubMenuHelper::addEntry(JText::_('Fields'), 'index.php?option=com_zj_donation&view=fields');
		JSubMenuHelper::addEntry(JText::_('Donates'), 'index.php?option=com_zj_donation&view=donates', true);
		JSubMenuHelper::addEntry(JText::_('Currencies'), 'index.php?option=com_zj_donation&view=currencies');
		JSubMenuHelper::addEntry(JText::_('Recurring'), 'index.php?option=com_zj_donation&view=recurringtypes');
		JSubMenuHelper::addEntry(JText::_('Configuration'), 'index.php?option=com_zj_donation&view=configs');


		// create the toolbar
		JToolBarHelper::title(JText::_('Donation Management'), 'donate.png');
		JToolBarHelper::publishList();
		JToolBarHelper::unpublishList();
		JToolBarHelper::deleteList();
		JToolBarHelper::editListX('edit', JText::_('View Details'));
	}

	/**
	 * Build list html
	 */
	function _buildLists() {
		global $mainframe, $option;

		// get variables from request
		$filter_state		= $mainframe->getUserStateFromRequest($option . '.donates.filter_state', 'filter_state', '', 'word');
		$filter_order		= $mainframe->getUserStateFromRequest($option . '.donates.filter_order', 'filter_order', 'a.created_date', 'cmd' );
		$filter_order_Dir	= $mainframe->getUserStateFromRequest($option . '.donates.filter_order_Dir', 'filter_order_Dir', '', 'word' );
		$search				= $mainframe->getUserStateFromRequest($option . '.donates.search', 'search', '', 'string');
		$search				= JString::trim(JString::strtolower($search));
		$filter_campaign_id		= $mainframe->getUserStateFromRequest($option . '.donates.filter_campaign_id', 'filter_campaign_id', 0, 'int');

		// state filter
		$lists['state']		= JHTML::_('grid.state', $filter_state);

		// table ordering
		$lists['order_Dir']	= $filter_order_Dir;
		$lists['order']		= $filter_order;

		// search filter
		$lists['search']	= $search;

		// category filter
		$lists['campaign']	= JHTML::_('zj_donation.campaign', 'filter_campaign_id', $filter_campaign_id, "onchange='document.adminForm.submit();'");

		return $lists;
	}
}
