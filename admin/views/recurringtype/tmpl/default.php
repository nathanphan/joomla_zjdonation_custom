<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

$db			= &JFactory::getDBO();
$nullDate 	= $db->getNullDate();
?>

<script type="text/javascript">
	function submitbutton(task) {
		var form = document.adminForm;
		
		if (task == 'cancel') {
			submitform(task);
		} else if (form.title.value == '') {
			alert('<?php echo JText::_('Please add a title!'); ?>');
			form.title.focus();
		} else {
			submitform(task);
		}
	}
</script>

<form action="index.php" method="post" name="adminForm" id="adminForm">
	<table class="admintable">
		<tr>
			<td class="key" width="30%">
				<label for="title">
					<?php echo JText::_('Title'); ?>:
				</label>
			</td>
			<td>
				<input type="text" class="inputbox" name="title" id="title" value="<?php echo $this->row->title; ?>" size="40" maxlength="255" />
			</td>
		</tr>
		<tr>
			<td class="key">
				<label for="title">
					<?php echo JText::_('Value'); ?>:
				</label>
			</td>
			<td>
				<input type="text" class="inputbox" name="value" id="value" value="<?php echo $this->row->value; ?>" size="40" maxlength="255" />
			</td>
		</tr>
		<tr>
			<td class="key">
				<label for="title">
					<?php echo JText::_('type'); ?>:
				</label>
			</td>
			<td>
				<?php echo $this->lists['recurringtype'];?>
			</td>
		</tr>
		<tr>
			<td class="key">
				<label for="published">
					<?php echo JText::_('Published'); ?>:
				</label>
			</td>
			<td>
				<?php echo $this->lists['published']; ?>
			</td>
		</tr>
		<tr>
			<td class="key">
				<label for="ordering">
					<?php echo JText::_('Ordering'); ?>:
				</label>
			</td>
			<td>
				<?php echo $this->lists['ordering']; ?>
			</td>
		</tr>
		<tr>
			<td class="key">
				<label for="description">
					<?php echo JText::_('Description'); ?>:
				</label>
			</td>
			<td>
				<textarea class="inputbox" cols="50" rows="10" name="description" id="description"><?php echo $this->row->description;?></textarea>
			</td>
		</tr>
	</table>
	
	<input type="hidden" name="option" value="com_zj_donation" />
	<input type="hidden" name="controller" value="recurringtype" />
	<input type="hidden" name="cid[]" value="<?php echo $this->row->id; ?>" />
	<input type="hidden" name="task" value="" />
	<?php echo JHTML::_('form.token'); ?>
</form>

<?php
echo ZJ_DonationFactory::getFooter();