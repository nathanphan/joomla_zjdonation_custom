<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

/**
 * ZJ_Donation Component - RecurringType View.
 * @package		ZJ_Donation
 * @subpackage	View
 */
class ZJ_DonationViewRecurringType extends JView {
	/**
	 * Display.
	 */
	function display($tpl = null) {
		global $mainframe;
		
		$editor = &JFactory::getEditor();
		$user	= &JFactory::getUser();
		$model	= &$this->getModel();
		
		// fail if checked out not by 'me'
		if ($model->isCheckedOut($user->get('id'))) {
			$msg = JText::sprintf('DESCBEINGEDITTED', JText::_('The Recurring Type'), $row->title);
			$mainframe->redirect('index.php?option=com_zj_donation&view=recurringtypes', $msg);
		}
		// do checkout
		$model->checkout();

		// hide mainmenu
		JRequest::setVar('hidemainmenu', 1);

		// get data from model
		$row	= &$this->get('Data');
		$isNew	= $row->id < 1;
		
		// set page title
		$text		= $isNew ? JText::_('New') : JText::_('Edit');
		$document	= &JFactory::getDocument();
		$document->setTitle(JText::_('Recurring Type') . ' ' . $text);
		
		// load behavior
		jimport('joomla.html.pane');
		JHTML::_('behavior.tooltip');
		JHTML::_('behavior.modal');

		// create the toolbar
		JToolBarHelper::title(JText::_('Recurring Type') . ': <small><small>[ ' . $text . ' ]</small></small>', 'recurring.png');
		JToolBarHelper::apply();
		JToolBarHelper::save();
		if ($isNew) {
			JToolBarHelper::cancel();
		} else {
			JToolBarHelper::cancel('cancel', JText::_('Close'));
		}
		
		// edit or create?
		if (!$isNew) {
			$model->checkout($user->get('id'));
		} else {
			// initialise new record
			$row->published	= 1;
			$row->order		= 0;
		}
		
		// build lists
		$lists				= $this->_buildLists($row);

		$this->assignRef('user',			$user);
		$this->assignRef('pane',			$pane);
		$this->assignRef('lists',			$lists);
		$this->assignRef('row',				$row);
		
		parent::display($tpl);
	}

	/**
	 * Build lists html.
	 */
	function _buildLists($row) {
		// build the html select list for ordering
		$query = 'SELECT ordering AS value, title AS text'
			. ' FROM #__zj_donation_recurring_types'
			. ' ORDER BY ordering'
		;
		$lists['ordering']		= JHTML::_('list.specificordering', $row, $row->id, $query);

		// build published list
		$lists['published']		= JHTML::_('select.booleanlist', 'published', 'class="inputbox"', $row->published);

		// Build recurring type list
		$lists['recurringtype']	= JHTML::_('zj_donation.recurringtype', 'type', $row->type, '', 1, '- ' . JText::_('Select a Recurring Type') . ' -', $row->type);

		return $lists;
	}
}
