<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
?>
<form action="index.php" method="post" name="adminForm">
<table>
<tr>
	<td align="left" width="100%">
		<?php echo JText::_('Filter'); ?>:
		<input type="text" class="text_area" name="search" id="search" value="<?php echo $this->lists['search']; ?>" onchange="document.adminForm.submit();" />
		<button onclick="this.form.submit();"><?php echo JText::_('Go'); ?></button>
		<button onclick="this.form.getElementById('search').value=''; this.form.getElementById('filter_state').value = '0'; this.form.submit();">
			<?php echo JText::_('Reset'); ?>
		</button>
	</td>
	<td nowrap="nowrap">
		<?php
		echo $this->lists['state'];
		?>
	</td>
</tr>
</table>
<div id="editcell">
	<table class="adminlist">
	<thead>
		<tr>
			<th width="5">
				<?php echo JText::_( '#' ); ?>
			</th>
			<th width="20">
				<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $this->items ); ?>);" />
			</th>
			<th style="text-align: left;">
				<?php echo JHTML::_('grid.sort',  'Name', 'a.name', $this->lists['order_Dir'], $this->lists['order']); ?>
			</th>
			<th style="text-align: left;">
				<?php echo JHTML::_('grid.sort',  'Title', 'a.title', $this->lists['order_Dir'], $this->lists['order']); ?>
			</th>
			<th style="text-align: left;">
				<?php echo JHTML::_('grid.sort',  'Field Type', 'a.field_type', $this->lists['order_Dir'], $this->lists['order'] ); ?>
			</th>
			<th class="title">
				<?php echo JHTML::_('grid.sort',  'Require', 'a.required', $this->lists['order_Dir'], $this->lists['order'] ); ?>
			</th>
			<th class="title">
				<?php echo JHTML::_('grid.sort',  'Published', 'a.published', $this->lists['order_Dir'], $this->lists['order'] ); ?>
			</th>
			<!--th class="title">
				<?php echo JHTML::_('grid.sort',  'Core', 'a.core', $this->lists['order_Dir'], $this->lists['order'] ); ?>
			</th-->
			<th width="8%" nowrap="nowrap">
				<?php echo JHTML::_('grid.sort',  'Order', 'a.ordering', $this->lists['order_Dir'], $this->lists['order'] ); ?>
				<?php echo JHTML::_('grid.order',  $this->items , 'filesave.png', 'save_order' ); ?>
			</th>						
			<th width="1%" nowrap="nowrap">
				<?php echo JHTML::_('grid.sort',  'ID', 'a.id', $this->lists['order_Dir'], $this->lists['order'] ); ?>
			</th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="9">
				<?php echo $this->pagination->getListFooter(); ?>
			</td>
		</tr>
	</tfoot>
	<tbody>
	<?php
	$k = 0;
	$ordering = ($this->lists['order'] == 'a.ordering');
	for ($i=0, $n=count( $this->items ); $i < $n; $i++)
	{
		$row = &$this->items[$i];
		$link		= JRoute::_( 'index.php?option=com_zj_donation&controller=fields&task=edit&cid[]='. $row->id );
		$checked	= JHTML::_('grid.id',   $i, $row->id );
		$published	= JHTML::_('grid.published', $row, $i, 'tick.png', 'publish_x.png', '' );
		$required	= JHTML::_('zj_donation.required', $row, $i);

		?>
		<tr class="<?php echo "row$k"; ?>">
			<td>
				<?php echo $this->pagination->getRowOffset( $i ); ?>
			</td>
			<td>
				<?php echo $checked; ?>
			</td>
			<td>
				<a href="<?php echo $link; ?>">
					<?php echo $row->name; ?>
				</a>
			</td>	
			<td>
				<a href="<?php echo $link; ?>">
					<?php echo JText::_($row->title); ?>
				</a>
			</td>
			<td>
				<?php
					$fieldTypes = array(
								0 => Jtext::_('Textbox'),
								1 => Jtext::_('Textarea'),
								2 => Jtext::_('Dropdown'),
								3 => Jtext::_('Checkbox List') ,
								4 => Jtext::_('Radio List') ,
								5 => Jtext::_('Date Time') 	
								) ;
					echo $fieldTypes[$row->field_type] ;								
			 	?>
			</td>						
			<td align="center">
				<?php echo $required; ?>
			</td>
			<td align="center">
				<?php echo $published ; ?>
			</td>
			<td class="order">
				<span><?php echo $this->pagination->orderUpIcon( $i, true,'orderup', 'Move Up', $ordering ); ?></span>
				<span><?php echo $this->pagination->orderDownIcon( $i, $n, true, 'orderdown', 'Move Down', $ordering ); ?></span>
				<?php $disabled = $ordering ?  '' : 'disabled="disabled"'; ?>
				<input type="text" name="order[]" size="5" value="<?php echo $row->ordering;?>" <?php echo $disabled ?> class="text_area" style="text-align: center" />
			</td>	
			<td align="center">			
				<?php echo $row->id; ?>
			</td>
		</tr>
		<?php
		$k = 1 - $k;
	}
	?>
	</tbody>
	</table>
	</div>
	<input type="hidden" name="option" value="com_zj_donation" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="view" value="fields" />
	<input type="hidden" name="controller" value="fields" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>