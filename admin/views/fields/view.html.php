<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

/**
 * ZJ_Donation Component - Fields View.
 * @package		ZJ_Donation
 * @subpackage	View
 */
class ZJ_DonationViewFields extends JView {
	function display($tpl = null)
	{		
		global $mainframe, $option;
		$user		= JFactory::getUser();
		$items		= &$this->get( 'Data');
		$total		= &$this->get('Total');
		$pagination = &$this->get( 'Pagination' );

		// build lists html
		$lists		= $this->_buildLists();
		$ordering	= $lists['order'] == 'a.ordering';

		$this->addToolBar();
		
		$this->assignRef('user',		$user);
		$this->assignRef('lists',		$lists);
		$this->assignRef('items',		$items);
		$this->assignRef('pagination',	$pagination);
		parent::display($tpl);
	}

	function addToolBar() {
		// load behavior
		JHTML::_('behavior.tooltip');

		// set page title
		$document = &JFactory::getDocument();
		$document->setTitle(JText::_('Field Management'));

		// add submenu
		JSubMenuHelper::addEntry(JText::_('Dashboard'), 'index.php?option=com_zj_donation');
		JSubMenuHelper::addEntry(JText::_('Campaigns'), 'index.php?option=com_zj_donation&view=campaigns');
		JSubMenuHelper::addEntry(JText::_('Fields'), 'index.php?option=com_zj_donation&view=fields', true);
		JSubMenuHelper::addEntry(JText::_('Donates'), 'index.php?option=com_zj_donation&view=donates');
		JSubMenuHelper::addEntry(JText::_('Currencies'), 'index.php?option=com_zj_donation&view=currencies');
		JSubMenuHelper::addEntry(JText::_('Recurring'), 'index.php?option=com_zj_donation&view=recurringtypes');
		JSubMenuHelper::addEntry(JText::_('Configuration'), 'index.php?option=com_zj_donation&view=configs');


		// create the toolbar
		JToolBarHelper::title(JText::_('Field Management'), 'field.png');
		JToolBarHelper::publishList();
		JToolBarHelper::unpublishList();
		JToolBarHelper::deleteList();
		JToolBarHelper::editListX();
		JToolBarHelper::addNewX();
	}

	/**
	 * Build list html
	 */
	function _buildLists() {
		global $mainframe, $option;

		// get variables from request
		$filter_state		= $mainframe->getUserStateFromRequest($option . '.currencies.filter_state', 'filter_state', '', 'word');
		$filter_order		= $mainframe->getUserStateFromRequest($option . '.currencies.filter_order', 'filter_order', 'a.ordering', 'cmd' );
		$filter_order_Dir	= $mainframe->getUserStateFromRequest($option . '.currencies.filter_order_Dir', 'filter_order_Dir', '', 'word' );
		$search				= $mainframe->getUserStateFromRequest($option . '.currencies.search', 'search', '', 'string');
		$search				= JString::trim(JString::strtolower($search));

		// state filter
		$lists['state']		= JHTML::_('grid.state', $filter_state);

		// table ordering
		$lists['order_Dir']	= $filter_order_Dir;
		$lists['order']		= $filter_order;

		// search filter
		$lists['search']	= $search;

		return $lists;
	}
}