<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

$images		= json_decode($this->row->gallery);
?>
<table class="adminlist zj_table">
	<thead>
		<tr class="title">
			<th width="1%">

			</th>
			<th>
				<?php echo JText::_('Title'); ?>
			</th>
			<th>
				<?php echo JText::_('Image Name'); ?>
			</th>
			<th width="5%">
				<?php echo JText::_('Published'); ?>
			</th>
			<th width="5%">
				<?php echo JText::_('Remove'); ?>
			</th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="6" style="text-align: left;">
				<a href="javascript:void(0);" onclick="zj_addImage()">
					<div class="zj_addbutton">
						<?php echo JText::_('Add more image'); ?>
					</div>
				</a>
			</td>
		</tr>
	</tfoot>
	<tbody id="zj_image_sortable">
	<?php
	for ($i = 0, $n = count(@$images->title); $i < $n; $i++) {
		?>
		<tr id="zj_image_row-<?php echo $i; ?>">
			<td align="center" valign="top">
				<div class="zj_image_sortablehandler"></div>
			</td>
			<td align="center" valign="top">
				<input type="text" class="text_area" name="gallery[title][]" value="<?php echo $images->title[$i]; ?>" size="35" />
			</td>
			<td nowrap="nowrap" align="center">
				<input type="text" style="background-color: #ffffff; float: left;" id="zj_image_name-<?php echo $i; ?>" value="<?php echo $images->name[$i]; ?>" size="35" disabled="disabled" />
				<input type="hidden" name="gallery[name][]" id="zj_image-<?php echo $i; ?>" value="<?php echo $images->name[$i]; ?>" />
				<div class="button2-left hasTip" id="zj_image_select-<?php echo $i; ?>" title="<img src='<?php echo $this->live_site; ?>media/zj_donation/galleries/<?php echo $images->name[$i]; ?>' width='200' alt='images' />">
					<div class="blank">
						<a class="modal" title="" href="<?php echo $this->admin_live_site; ?>index.php?option=com_zj_donation&view=imagehandler&tmpl=component&task=selectgallery&image_id=<?php echo $i; ?>" rel="{handler: 'iframe', size: {x: 650, y: 400}}">
							<?php echo JText::_('Image'); ?>
						</a>
					</div>
				</div>
			</td>
			<td align="center" valign="top">
				<input type="checkbox" class="inputbox" value="1" onclick="zj_setImagePublish(<?php echo $i; ?>, this.checked ? 1 : 0);" <?php echo $images->published[$i] != 0 ? 'checked="checked"' : ''; ?> />
				<input type="hidden" name="gallery[published][]" id="zj_image_published-<?php echo $i; ?>" value="<?php echo $images->published[$i]; ?>" />
			</td>
			<td align="center" valign="top">
				<a href="javascript:void(0)" onclick="zj_removeImage(<?php echo $i; ?>);">
					<img src="<?php echo $this->admin_live_site; ?>components/com_zj_donation/assets/images/delete.png" alt="remove" />
				</a>
			</td>
		</tr>
		<?php
	}
	?>
	</tbody>
</table>