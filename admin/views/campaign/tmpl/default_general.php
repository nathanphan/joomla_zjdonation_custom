<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
?>

<fieldset class="adminform">
	<legend><?php echo JText::_('Details'); ?></legend>
	<table class="admintable" width="100%">
		<tr>
			<td nowrap="nowrap" class="key">
				<label for="title">
					<?php echo JText::_('Title'); ?>:
				</label>
			</td>
			<td>
				<input type="text" class="inputbox" name="title" id="title" value="<?php echo $this->row->title; ?>" size="40" maxlength="255" />
			</td>
			<td nowrap="nowrap" class="key">
				<label for="published">
					<?php echo JText::_('Published'); ?>:
				</label>
			</td>
			<td>
				<?php echo $this->lists['published']; ?>
			</td>
		</tr>
		<tr>
			<td nowrap="nowrap" class="key">
				<label for="alias">
					<?php echo JText::_('Alias'); ?>:
				</label>
			</td>
			<td>
				<input type="text" class="inputbox" name="alias" id="alias" value="<?php echo $this->row->alias; ?>" size="40" maxlength="255" />
			</td>
			<td nowrap="nowrap" class="key">
				<label for="feature">
					<?php echo JText::_('Feature'); ?>:
				</label>
			</td>
			<td>
				<?php echo $this->lists['feature']; ?>
			</td>
		</tr>
		<tr>
			<td nowrap="nowrap" class="key">
				<label for="ordering">
					<?php echo JText::_('Ordering'); ?>:
				</label>
			</td>
			<td>
				<?php echo $this->lists['ordering']; ?>
			</td>
			<td nowrap="nowrap" class="key">
				<label for="recurring">
					<?php echo JText::_('Recurring'); ?>:
				</label>
			</td>
			<td>
				<?php echo $this->lists['recurring']; ?>
			</td>
		</tr>
		
		<tr>
			<td nowrap="nowrap" class="key">
				<label for="country_id">
					<?php echo JText::_('Country'); ?>:
				</label>
			</td>
			<td>
				<?php echo $this->country_box; ?>
			</td>
			<td >
				
			</td>
			<td>
				
			</td>
		</tr>
	</table>
</fieldset>