<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

$images		= json_decode($this->row->gallery);
$zj_image	= count(@$images->title);
$document	= &JFactory::getDocument();

$js = "
var zj_image	= $zj_image;
window.addEvent('domready', function() {
	zj_reloadJs();
});
";

$document->addScriptDeclaration($js);
?>

<script type="text/javascript">
	function submitbutton(task) {
		var form = document.adminForm;
		
		if (task == 'cancel') {
			submitform(task);
		} else if (form.title.value == '') {
			alert('<?php echo JText::_('Please add a title!'); ?>');
			form.title.focus();
		} else {
			submitform(task);
		}
	}
</script>

<form action="index.php" method="post" name="adminForm" id="adminForm">
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
			<td valign="top">
				<?php
				//General pane
				echo $this->loadTemplate('general');
				echo $this->tabs->startPane('campaign_pane');

				//Description pane
				echo $this->tabs->startPanel(JText::_('Description'), 'description_panel');
				echo $this->loadTemplate('description');
				echo $this->tabs->endPanel();

				//Addition description pane
				echo $this->tabs->startPanel(JText::_('Addition Information'), 'addition_panel');
				echo $this->loadTemplate('addition');
				echo $this->tabs->endPanel();

				//Extra Fields pane
				echo $this->tabs->startPanel(JText::_('Extra Fields'), 'extrafields_panel');
				echo $this->loadTemplate('fields');
				echo $this->tabs->endPanel();

				//Message Pane
				echo $this->tabs->startPanel(JText::_('Messages'), 'messages_panel');
				echo $this->loadTemplate('messages');
				echo $this->tabs->endPanel();

				//Galleries Pane
				echo $this->tabs->startPanel(JText::_('Gallery'), 'galery_panel');
				echo $this->loadTemplate('galleries');
				echo $this->tabs->endPanel();
				echo $this->tabs->endPane();
				?>
			</td>
			<td valign="top" width="320px" style="padding: 7px 0 0 5px">
				<?php echo $this->loadTemplate('right');?>
			</td>
		</tr>
	</table>
	
	<input type="hidden" name="option" value="com_zj_donation" />
	<input type="hidden" name="controller" value="campaign" />
	<input type="hidden" name="cid[]" value="<?php echo $this->row->id; ?>" />
	<input type="hidden" name="task" value="" />
	<?php echo JHTML::_('form.token'); ?>
</form>

<?php
echo ZJ_DonationFactory::getFooter();