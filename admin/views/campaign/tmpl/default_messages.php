<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

$messages	= json_decode($this->row->messages);
$config		= ZJ_DonationFactory::getConfig();
?>
<fieldset class="adminform">
	<legend><?php echo JText::_('E-mail Configuration'); ?></legend>
	<table class="admintable" cellspacing="1">
		<tr>
			<td class="key" valign="top">
				<?php echo JText::_('Notification E-mail'); ?>
			</td>
			<td>
				<input type="text" class="text_area" name="messages[notify_email]" id="notify_email" value="<?php echo ($this->row->id) ? $messages->notify_email : $config->get('notify_email'); ?>" size="70" />
			</td>
		</tr>
		<tr>
			<td class="key" valign="top">
				<?php echo JText::_('New Donation Placed'); ?>
			</td>
			<td>
				<?php
				echo JHTML::_('select.booleanlist', 'messages[notify_new_donation]', 'notify_new_donation', ($this->row->id) ? $messages->notify_new_donation : $config->get('notify_new_donation'));
				?>
			</td>
		</tr>
		<tr>
			<td class="key" valign="top">
				<?php echo JText::_('Donation Actived'); ?>
			</td>
			<td>
				<?php
				echo JHTML::_('select.booleanlist', 'messages[notify_donation_actived]', 'notify_donation_actived', ($this->row->id) ? $messages->notify_donation_actived : $config->get('notify_donation_actived'));
				?>
			</td>
		</tr>
	</table>
</fieldset>

<fieldset class="adminform">
	<legend><?php echo JText::_('Email To Donor'); ?></legend>
	<table class="admintable" cellspacing="1">
		<tr>
			<td class="key" valign="top">
				<?php echo JText::_('Thank You E-mail Subject'); ?>
			</td>
			<td>
				<input type="text" class="text_area" name="messages[email_thanks_subject]" id="email_thanks_subject" value="<?php echo ($this->row->id) ? $messages->email_thanks_subject : $config->get('email_thanks_subject'); ?>" size="100" />
			</td>
		</tr>
		<tr>
			<td class="key" valign="top">
				<?php echo JText::_('Thank You E-mail Body'); ?>
			</td>
			<td>
				<textarea class="text_area" name="messages[email_thanks_body]" id="email_thanks_body" rows="8" cols="70"><?php echo ($this->row->id) ? $messages->email_thanks_body : $config->get('email_thanks_body'); ?></textarea>
			</td>
		</tr>
		<tr>
			<td class="key" valign="top">
				<?php echo JText::_('Confirm E-mail Subject'); ?>
			</td>
			<td>
				<input type="text" class="text_area" name="messages[email_donation_actived_subject]" id="email_donation_actived_subject" value="<?php echo ($this->row->id) ? $messages->email_donation_actived_subject : $config->get('email_donation_actived_subject'); ?>" size="100" />
			</td>
		</tr>
		<tr>
			<td class="key" valign="top">
				<?php echo JText::_('Confirm E-mail Body'); ?>
			</td>
			<td>
				<textarea class="text_area" name="messages[email_donation_actived_body]" id="email_donation_actived_body" rows="8" cols="70"><?php echo ($this->row->id) ? $messages->email_donation_actived_body : $config->get('email_donation_actived_body'); ?></textarea>
			</td>
		</tr>
	</table>
</fieldset>

<fieldset class="adminform">
	<legend><?php echo JText::_('Administrator Nofify'); ?></legend>
	<table class="admintable" cellspacing="1">
		<tr>
			<td class="key" valign="top">
				<?php echo JText::_('New Donation E-mail Subject'); ?>
			</td>
			<td>
				<input type="text" class="text_area" name="messages[email_admin_donation_created_subject]" id="email_admin_order_created_subject" value="<?php echo ($this->row->id) ? $messages->email_admin_donation_created_subject : $config->get('email_admin_donation_created_subject'); ?>" size="100" />
			</td>
		</tr>
		<tr>
			<td class="key" valign="top">
				<?php echo JText::_('New Donation E-mail Body'); ?>
			</td>
			<td>
				<textarea class="text_area" name="messages[email_admin_donation_created_body]" id="email_admin_donation_created_body" rows="8" cols="70"><?php echo ($this->row->id) ? $messages->email_admin_donation_created_body : $config->get('email_admin_donation_created_body'); ?></textarea>
			</td>
		</tr>

		<tr>
			<td class="key" valign="top">
				<?php echo JText::_('Donation Actived E-mail Subject'); ?>
			</td>
			<td>
				<input type="text" class="text_area" name="messages[email_admin_donation_actived_subject]" id="email_admin_donation_actived_subject" value="<?php echo ($this->row->id) ? $messages->email_admin_donation_actived_subject : $config->get('email_admin_donation_actived_subject'); ?>" size="100" />
			</td>
		</tr>
		<tr>
			<td class="key" valign="top">
				<?php echo JText::_('Donation Actived E-mail Body'); ?>
			</td>
			<td>
				<textarea class="text_area" name="messages[email_admin_donation_actived_body]" id="email_admin_donation_actived_body" rows="8" cols="70"><?php echo ($this->row->id) ? $messages->email_admin_donation_actived_body : $config->get('email_admin_donation_actived_body'); ?></textarea>
			</td>
		</tr>
	</table>
</fieldset>