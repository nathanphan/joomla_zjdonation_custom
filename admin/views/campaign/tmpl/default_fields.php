<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

$extra	= json_decode($this->row->fields);
?>

<table class="adminlist zj_table">
	<thead>
		<tr class="title">
			<th width="1%">
			</th>
			<th>
				<?php echo JText::_('Title'); ?>
			</th>
			<th>
				<?php echo JText::_('Field Name'); ?>
			</th>
			<th>
				<?php echo JText::_('Field Type'); ?>
			</th>
			<th>
				<?php echo JText::_('Required'); ?>
			</th>
			<th width="5%">
				<?php echo JText::_('Visible'); ?>
			</th>
		</tr>
	</thead>
	<tbody id="zj_sortable">
	<?php
	if (!count($extra)) {
		for ($i = 0, $n = count($this->fields); $i < $n; $i++) {
			$field	= $this->fields[$i];
			?>
			<tr id="zj_field_row-<?php echo $i; ?>">
				<td align="center" valign="top">
					<div class="zj_sortablehandler"></div>
				</td>
				<td align="center" valign="top">
					<?php echo $field->title;?>
				</td>
				<td align="center" valign="top">
					<?php echo $field->name;?>
				</td>
				<td align="center" valign="top">
					<?php echo JHTML::_('zj_donation.fieldtype', $field->field_type);?>
				</td>
				<td align="center" valign="top">
					<?php echo JHTML::_('zj_donation.icon', $field->required);?>
				</td>
				<td align="center" valign="top">
					<input type="hidden" name="fields[id][]" value="<?php echo $field->id;?>"/>
					<input type="hidden" name="fields[index][]" value="<?php echo $i;?>"/>
					<input type="hidden" name="fields[published][]" id="zj_field_published-<?php echo $i; ?>" value="<?php echo $field->published; ?>" />
					<input type="checkbox" class="inputbox" value="1" onclick="zj_setFieldPublish(<?php echo $i; ?>, this.checked ? 1 : 0);" <?php echo $field->published != 0 ? 'checked="checked"' : ''; ?> />
				</td>
			</tr>
			<?php
		}
	} else {
		for ($i = 0, $n = count($this->fields); $i < $n; $i++) {
			$index	= isset($extra->index[$i]) ? $extra->index[$i] : $i;
			$field	= $this->fields[$index];
			?>
			<tr id="zj_field_row-<?php echo $i; ?>">
				<td align="center" valign="top">
					<div class="zj_sortablehandler"></div>
				</td>
				<td align="center" valign="top">
					<?php echo $field->title;?>
				</td>
				<td align="center" valign="top">
					<?php echo $field->name;?>
				</td>
				<td align="center" valign="top">
					<?php echo JHTML::_('zj_donation.fieldtype', $field->field_type);?>
				</td>
				<td align="center" valign="top">
					<?php echo JHTML::_('zj_donation.icon', $field->required);?>
				</td>
				<td align="center" valign="top">
					<input type="hidden" name="fields[id][]" value="<?php echo isset($extra->id[$i]) ? $extra->id[$i] : $this->fields[$i]->id;?>"/>
					<input type="hidden" name="fields[index][]" value="<?php echo isset($extra->index[$i]) ? $extra->index[$i] : $i;?>"/>
					<input type="hidden" name="fields[published][]" id="zj_field_published-<?php echo $i; ?>" value="<?php echo $extra->published[$i]; ?>" />
					<input type="checkbox" class="inputbox" value="1" onclick="zj_setFieldPublish(<?php echo $i; ?>, this.checked ? 1 : 0);" <?php echo $extra->published[$i] != 0 ? 'checked="checked"' : ''; ?> />
				</td>
			</tr>
			<?php
		}
	}
	?>
	</tbody>
</table>
