<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

$db			= &JFactory::getDBO();
$nullDate 	= $db->getNullDate();
?>

<table width="100%" style="border: 1px dashed silver; padding: 5px; margin-bottom: 10px;">
<?php if ($this->row->id) { ?>
	<tr>
		<td>
			<strong><?php echo JText::_('Campaign ID'); ?>:</strong>
		</td>
		<td>
			<?php echo $this->row->id; ?>
		</td>
	</tr>
<?php } ?>
	<tr>
		<td>
			<strong><?php echo JText::_('Goal'); ?>:</strong>
		</td>
		<td>
			<?php echo $this->row->goal; ?>
		</td>
	</tr>
	<tr>
		<td>
			<strong><?php echo JText::_('Donated Amount'); ?>:</strong>
		</td>
		<td>
			<?php echo (@$this->row->donated_amount) ? $this->row->donated_amount : '0.00'; ?>
		</td>
	</tr>
	<tr>
		<td>
			<strong><?php echo JText::_('Donors'); ?>:</strong>
		</td>
		<td>
			<?php echo (int)@$this->row->donors; ?>
		</td>
	</tr>
	<tr>
		<td>
			<strong><?php echo JText::_('Author'); ?>:</strong>
		</td>
		<td>
			<?php echo $this->row->id ? $this->row->author : $this->user->username; ?>
		</td>
	</tr>
	<tr>
		<td>
			<strong><?php echo JText::_('State'); ?>:</strong>
		</td>
		<td>
			<?php echo $this->row->published > 0 ? JText::_('Published') : JText::_('Unpublished'); ?>
		</td>
	</tr>
	<tr>
		<td>
			<strong><?php echo JText::_('Created'); ?>:</strong>
		</td>
		<td>
			<?php
			if ( $this->row->created_date == $nullDate ) {
				echo JText::_('New campaign');
			} else {
				echo JHTML::_('date', $this->row->created_date, JText::_('DATE_FORMAT_LC2'));
			}
			?>
		</td>
	</tr>
</table>
<?php
echo $this->pane->startPane('info_pane');
echo $this->pane->startPanel(JText::_('Information'), 'information');
?>
<table width="100%" class="paramlist admintable">
	<tr>
		<td class="paramlist_key" width="20%">
			<label for="author">
				<?php echo JText::_('Author'); ?>:
			</label>
		</td>
		<td class="paramlist_value">
			<input type="text" class="inputbox" name="created_by" id="author" value="<?php echo $this->row->created_by; ?>" size="20" />
		</td>
	</tr>
	<tr>
		<td class="paramlist_key">
			<label for="created_date">
				<?php echo JText::_('Date'); ?>:
			</label>
		</td>
		<td class="paramlist_value">
			<?php echo JHTML::_('calendar', $this->row->created_date, 'created_date', 'created_date', '%Y-%m-%d', array('class'=>'inputbox', 'size'=>'25', 'maxlength'=>'19')); ?>
		</td>
	</tr>
	<tr>
		<td class="paramlist_key">
			<label for="goal">
				<?php echo JText::_('Goal'); ?>:
			</label>
		</td>
		<td class="paramlist_value">
			<input type="text" class="inputbox" name="goal" id="goal" value="<?php echo $this->row->goal; ?>" size="20" />
		</td>
	</tr>
	<tr>
		<td class="paramlist_key">
			<label for="published_up">
				<?php echo JText::_('Start Date'); ?>:
			</label>
		</td>
		<td class="paramlist_value">
			<?php echo JHTML::_('calendar', $this->row->published_up, 'published_up', 'published_up', '%Y-%m-%d', array('class'=>'inputbox', 'size'=>'25', 'maxlength'=>'19')); ?>
		</td>
	</tr>
	<tr>
		<td class="paramlist_key">
			<label for="published_down">
				<?php echo JText::_('End Date'); ?>:
			</label>
		</td>
		<td class="paramlist_value">
			<?php echo JHTML::_('calendar', $this->row->published_down, 'published_down', 'published_down', '%Y-%m-%d', array('class'=>'inputbox', 'size'=>'25', 'maxlength'=>'19')); ?>
		</td>
	</tr>
	<tr>
		<td class="paramlist_key">
			<label for="amounts">
				<?php echo JText::_('Default Amounts'); ?>:
			</label>
		</td>
		<td class="paramlist_value">
			<input type="text" size="40" class="inputbox" name="amounts" id="amounts" class="inputbox" value="<?php echo str_replace(' ', '', $this->row->amounts);?>" />
			<br/>
			<em><?php echo JText::_('Separate by comma ","');?></em>
		</td>
	</tr>
	<tr>
		<td class="paramlist_key">
			<label for="custom_amount">
				<?php echo JText::_('Custom Amount'); ?>:
			</label>
		</td>
		<td class="paramlist_value">
			<?php echo $this->lists['custom_amount'];?>
		</td>
	</tr>
	<tr>
		<td class="paramlist_key">
			<?php echo JText::_('Image'); ?>
		</td>
		<td class="paramlist_value" nowrap="nowrap" width="70%">
			<input type="text" style="background-color: #ffffff; float: left;" id="zj_campaign_image_name" value="<?php echo $this->row->image; ?>" size="35" disabled="disabled" />
			<input type="hidden" name="image" id="zj_campaign_image" value="<?php echo $this->row->image; ?>" />
			<div class="button2-left hasTip" id="zj_campaign_image_select" title="<img src='<?php echo $this->live_site; ?>/media/zj_donation/images/<?php echo $this->row->image; ?>' width='200' alt='images' />">
				<div class="blank">
					<a class="modal" title="" href="<?php echo $this->admin_live_site; ?>index.php?option=com_zj_donation&view=imagehandler&tmpl=component&task=selectimage" rel="{handler: 'iframe', size: {x: 650, y: 400}}">
						<?php echo JText::_('Image'); ?>
					</a>
				</div>
			</div>
		</td>
	</tr>
</table>
<?php
echo $this->pane->endPanel();
echo $this->pane->startPanel(JText::_('Metadata Information'), 'metadata_info');
?>
<table width="100%">
	<tr>
		<td>
			<label for="meta_keywords">
				<?php echo JText::_('Meta keywords'); ?>:
			</label>
			<br />
			<textarea class="inputbox" name="meta_keywords" id="meta_keywords" rows="5" cols="40"><?php echo $this->row->meta_keywords; ?></textarea>
		</td>
	</tr>
	<tr>
		<td>
			<label for="meta_description">
				<?php echo JText::_('Meta description'); ?>:
			</label>
			<br />
			<textarea class="inputbox" name="meta_description" id="meta_description" rows="5" cols="40"><?php echo $this->row->meta_description; ?></textarea>
		</td>
	</tr>
</table>
<?php
echo $this->pane->endPanel();
echo $this->pane->endPane();
?>
