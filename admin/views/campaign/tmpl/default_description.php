<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
?>

<table class="adminform">
	<tr>
		<td>
			<?php
			echo $this->editor->display('description', $this->row->description, '100%', '400', '50', '20', array('pagebreak', 'readmore'));
			?>
		</td>
	</tr>
</table>
