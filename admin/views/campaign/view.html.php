<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

/**
 * ZJ_Donation Component - Campaign View.
 * @package		ZJ_Donation
 * @subpackage	View
 */
class ZJ_DonationViewCampaign extends JView {
	/**
	 * Display.
	 */
	function display($tpl = null) {
		global $mainframe;
		
		$editor = &JFactory::getEditor();
		$user	= &JFactory::getUser();
		$model	= &$this->getModel();
		
		// fail if checked out not by 'me'
		if ($model->isCheckedOut($user->get('id'))) {
			$msg = JText::sprintf('DESCBEINGEDITTED', JText::_('The campaign'), $row->title);
			$mainframe->redirect('index.php?option=com_zj_donation&view=campaigns', $msg);
		}
		// do checkout
		$model->checkout();

		// hide mainmenu
		JRequest::setVar('hidemainmenu', 1);

		// get data from model
		$row	= &$this->get('Data');
		$isNew	= $row->id < 1;

		$model_fields	= ZJ_DonationFactory::getModel('fields');
		$fields			= $model_fields->getData();
		
		// set page title
		$text		= $isNew ? JText::_('New') : JText::_('Edit');
		$document	= &JFactory::getDocument();
		$document->setTitle(JText::_('Campaign') . ' ' . $text);
		
		// load behavior
		jimport('joomla.html.pane');
		JHTML::_('behavior.tooltip');
		JHTML::_('behavior.modal');
		
		$pane 	= &JPane::getInstance('sliders', array('allowAllClose' => 1));
		$tabs	= &JPane::getInstance();

		// create the toolbar
		JToolBarHelper::title(JText::_('Campaign') . ': <small><small>[ ' . $text . ' ]</small></small>', 'campaign.png');
		JToolBarHelper::apply();
		JToolBarHelper::save();
		if ($isNew) {
			JToolBarHelper::cancel();
		} else {
			JToolBarHelper::cancel('cancel', JText::_('Close'));
		}
		
		// edit or create?
		if (!$isNew) {
			$model->checkout($user->get('id'));
		} else {
			// initialise new record
			$row->published		= 1;
			$row->ordering		= 0;
			$row->created_by	= $user->get('id');
		}
				
		// clean the campaign data
		JFilterOutput::objectHTMLSafe($row, ENT_QUOTES, array('description', 'addition', 'messages', 'fields', 'gallery'));
		
		// build lists
		$lists				= $this->_buildLists($row);

		// get administrator live site
		$admin_live_site	= &JURI::base();
		$live_site			= $mainframe->getSiteURL();
		
		//get all Country
		$country_model =& JModel::getInstance('country','ZJ_DonationModel');

		$country_list = $country_model->getList();
//		initalize dropdown box.
		$countrySelectOpt = array (JHTML::_ ( 'select.option', '', '-- Select --' ) );
		foreach ( $country_list as $r ) {
			$countrySelectOpt [] = JHTML::_ ( 'select.option', $r->id, $r->name );
		}
		$country_box = JHTML::_ ( 'select.genericlist', $countrySelectOpt, 'country_id', '', 'value', 'text',$row->country_id );
		$this->assignRef ( 'country_box', $country_box );

		$this->assignRef('live_site',		$live_site);
		$this->assignRef('admin_live_site', $admin_live_site);
		$this->assignRef('user',			$user);
		$this->assignRef('pane',			$pane);
		$this->assignRef('tabs',			$tabs);
		$this->assignRef('editor',			$editor);
		$this->assignRef('lists',			$lists);
		$this->assignRef('row',				$row);
		$this->assignRef('fields',			$fields);
		
		parent::display($tpl);
	}

	/**
	 * Build lists html.
	 */
	function _buildLists($row) {
		// build the html select list for ordering
		$query = 'SELECT ordering AS value, title AS text'
			. ' FROM #__zj_donation_campaigns'
			. ' ORDER BY ordering'
		;
		$lists['ordering']		= JHTML::_('list.specificordering', $row, $row->id, $query);

		// build published list
		$lists['published']		= JHTML::_('select.booleanlist', 'published', 'class="inputbox"', $row->published);

		// build feature list
		$lists['feature']		= JHTML::_('select.booleanlist', 'feature', 'class="inputbox"', $row->feature);

		// build recurring list
		$lists['recurring']		= JHTML::_('select.booleanlist', 'recurring', 'class="inputbox"', $row->recurring);

		// build custom_amount list
		$lists['custom_amount']		= JHTML::_('select.booleanlist', 'custom_amount', 'class="inputbox"', $row->custom_amount);

		return $lists;
	}
}
