<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

/**
 * ZJ_Donation Component - Dashboard View
 * @package		ZJ_Donation
 * @subpackage	View
 */
class ZJ_DonationViewDashboard extends JView {
	/**
	 * Display
	 */
	function display($tpl = null) {

		// load behavior
		jimport('joomla.html.pane');
		$pane = &JPane::getInstance('sliders', array('allowAllClose' => 1));

		$this->addToolBar();
		// assign vairiable to template
		$xml	= $this->get('XmlParser');

		$this->assignRef('xml',		$xml);
		$this->assignRef('pane',	$pane);

		parent::display($tpl);
	}

	function addToolBar() {
		// set page title
		$document = &JFactory::getDocument();
		$document->setTitle(JText::_('Joomseller - Donation'));
		JHTML::_('behavior.tooltip');

		// add submenu
		JSubMenuHelper::addEntry(JText::_('Dashboard'), 'index.php?option=com_zj_donation', true);
		JSubMenuHelper::addEntry(JText::_('Campaigns'), 'index.php?option=com_zj_donation&view=campaigns');
		JSubMenuHelper::addEntry(JText::_('Fields'), 'index.php?option=com_zj_donation&view=fields');
		JSubMenuHelper::addEntry(JText::_('Donates'), 'index.php?option=com_zj_donation&view=donates');
		JSubMenuHelper::addEntry(JText::_('Currencies'), 'index.php?option=com_zj_donation&view=currencies');
		JSubMenuHelper::addEntry(JText::_('Recurring'), 'index.php?option=com_zj_donation&view=recurringtypes');
		JSubMenuHelper::addEntry(JText::_('Configuration'), 'index.php?option=com_zj_donation&view=configs');


		//create the toolbar
		JToolBarHelper::title(JText::_('Joomseller - Donation'), 'dashboard.png');
	}

	/**
	 * Dipslay icon button.
	 */
	function quickiconButton($link, $image, $text, $modal = 0) {
		?>
	<div style="float: left;">
		<div class="icon">
			<?php
			if($modal) {
				JHTML::_('behavior.modal');
			?>
			<a href="<?php echo JRoute::_($link . '&tmpl=component'); ?>" style="cursor: pointer;" class="modal" rel="{handler:'iframe', size: {x: 650, y: 400}}">
			<?php } else { ?>
			<a href="<?php echo JRoute::_($link); ?>">
			<?php
			}
			echo JHTML::_('image', 'administrator/components/com_zj_donation/assets/images/' . $image, $text);
			?>
				<span>
					<?php echo $text; ?>
				</span>
			</a>
		</div>
	</div>
<?php
	}
}
