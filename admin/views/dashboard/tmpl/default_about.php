<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

?>

<table class="adminlist">
	<tr>
		<td valign="middle" style="width:30%;"><strong><?php echo JText::_('COM_ZJ_DONATION_INSTALLED_VERSION')?></strong></td>
		<td><strong><?php echo $this->xml->version;?></strong></td>
	</tr>

	<tr>
		<td valign="middle"><strong><?php echo JText::_('COM_ZJ_DONATION_COPYRIGHT')?></strong></td>
		<td><?php echo $this->xml->copyright;?></td>
	</tr>


	<tr>
		<td valign="middle"><strong><?php echo JText::_('COM_ZJ_DONATION_LICENSE')?></strong></td>
		<td><?php echo $this->xml->license;?></td>
	</tr>

	<tr>
		<td valign="middle"><strong><?php echo JText::_('COM_ZJ_DONATION_CREDITS')?></strong></td>
		<td>
			<?php
				if( isset( $this->xml->zjmember ) && !empty($this->xml->zjmember) ){
			?>
			<ul style="margin: 0; padding-left: 15px;">
				<?php
					foreach( $this->xml->zjmember as $k=>$v ){
						echo '<li>'.'<strong>'.$v['data'].'</strong>'.' ('.$v['name'].')'.'</li>';
					}
				?>
			</ul>
			<?php } ?>
		</td>
	</tr>
	<tr>
		<td colspan="2"><a href="<?php echo $this->xml->jed_link;?>" target="_blank"><strong><?php echo JText::_('COM_ZJ_DONATION_JED_NOTIFICATION');?></strong></a></td>
	</tr>
</table>