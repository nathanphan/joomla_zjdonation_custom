<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

?>

<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
		<td valign="top">
			<table class="adminlist">
				<tr>
					<td>
						<div id="cpanel">
							<?php
								$this->quickiconButton('index.php?option=com_zj_donation&amp;view=campaigns', 'icon-48-campaign.png', JText::_('Campaigns'));
								$this->quickiconButton('index.php?option=com_zj_donation&amp;view=fields', 'icon-48-field.png', JText::_('Fields'));
								$this->quickiconButton('index.php?option=com_zj_donation&amp;view=donates', 'icon-48-donate.png', JText::_('Donates'));
								$this->quickiconButton('index.php?option=com_zj_donation&amp;view=currencies', 'icon-48-currency.png', JText::_('Currencies'));
								$this->quickiconButton('index.php?option=com_zj_donation&amp;view=recurringtypes', 'icon-48-recurring.png', JText::_('Recurring'));
								$this->quickiconButton('index.php?option=com_zj_donation&amp;view=configs', 'icon-48-config.png', JText::_('Configuration'));

							?>
						</div>
					</td>
				</tr>
			</table>
		</td>
		<td valign="top" width="40%" style="padding: 0 0 0 5px">
			<?php
			echo $this->pane->startPane('statistics_pane');

			echo $this->pane->startPanel(JText::_('COM_ZJ_DONATION_ABOUT_ZJ_DONATION'), 'about_panel');
			echo $this->loadTemplate('about');
			echo $this->pane->endPanel();
			
			echo $this->pane->endPane();
			?>
		</td>
	</tr>
</table>

<?php
echo ZJ_DonationFactory::getFooter();