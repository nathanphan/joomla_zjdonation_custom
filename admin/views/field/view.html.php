<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

/**
 * ZJ_Donation Component - Field View.
 * @package		ZJ_Donation
 * @subpackage	View
 */
class ZJ_DonationViewField extends JView {
	function display($tpl = null) {
		global $mainframe, $option;
		$user	= JFactory::getUser();
		
		$model	= &$this->getModel();
		// get data from model
		$row 	= $this->get('Data');
		
		$isNew	= $row->id < 1;
		
		// fail if checked out not by 'me'
		if ($model->isCheckedOut($user->get('id'))) {
			$msg = JText::sprintf('DESCBEINGEDITTED', JText::_('The field'), $row->title);
			$mainframe->redirect('index.php?option=com_zj_realty&view=fields', $msg);
		}

		// set page title
		$text		= $isNew ? JText::_('New') : JText::_('Edit');
		$document	= &JFactory::getDocument();
		$document->setTitle(JText::_('Field') . ' ' . $text);
		
		// TODO: load behavior here...
		jimport('joomla.html.pane');
		JHTML::_('behavior.tooltip');

		// create the toolbar
		JToolBarHelper::title(JText::_('Field') . ': <small><small>[ ' . $text . ' ]</small></small>', 'field.png');
		JToolBarHelper::apply();
		JToolBarHelper::save();
		if ($isNew) {
			JToolBarHelper::cancel();
		} else {
			JToolBarHelper::cancel('cancel', JText::_('Close'));
		}
		
		// edit or create?
		if (!$isNew) {
			$model->checkout($user->get('id'));
		}

		// build lists
		$lists				= $this->_buildLists($row);
				
		$this->assignRef('row',		$row);
		$this->assignRef('lists',	$lists );
		parent::display($tpl);				
	}

	/**
	 * Build lists html.
	 */
	function _buildLists($row) {
		// build the html select list for ordering
		$query = 'SELECT ordering AS value, title AS text'
			. ' FROM #__zj_donation_fields'
			. ' ORDER BY ordering'
		;
		$lists['ordering']		= JHTML::_('list.specificordering', $row, $row->id, $query);

		// build published list
		$lists['published']		= JHTML::_('select.booleanlist', 'published', 'class="inputbox"', $row->published);

		// build required list
		$lists['required'] 		= JHTML::_('select.booleanlist', 'required', ' class="inputbox" ', $row->required) ;

		// build field type
		$fieldTypes = array(
							0 => 'Textbox' ,
							1 => 'Textarea' ,
							2 => 'Dropdown' ,
							3 => 'Checkbox List' ,
							4 => 'Radio List' ,
							5 => 'Date Time'
					);
		$options 	= array() ;
		$options[] 	= JHTML::_('select.option', -1, JText::_('Field Type')) ;
		foreach ($fieldTypes As $value => $text) {
			$options[]	= JHTML::_('select.option', $value, $text ) ;
		}
		$lists['field_type'] 	= JHTML::_('select.genericlist', $options, 'field_type',' class="inputbox" ', 'value', 'text', $row->field_type);

		return $lists;
	}
}