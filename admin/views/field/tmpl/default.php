<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

?>
<script language="javascript" type="text/javascript">
	function submitbutton(pressbutton) {
		var form = document.adminForm;
		if (pressbutton == 'cancel') {
			submitform( pressbutton );
			return;				
		} else {
			if (form.name.value == "") {
				alert("<?php echo JText::_('Please enter the name for this custom field'); ?>");
				form.name.focus();
				return ;
			}
			if (form.title.value == "") {
				alert("<?php echo JText::_('Please enter title for this custom field'); ?>");
				form.title.focus();
				return ; 
			}
			if (form.field_type.value == -1) {
				alert("<?php echo JText::_('Please choose field type for this custom field') ; ?>");
				return ; 
			}			
			submitform( pressbutton );
		}
	}
</script>
<form action="index.php" method="post" name="adminForm" id="adminForm">
<div class="col width-55" style="float:left">			
	<table class="admintable">
		<tr>
			<td class="key" width="30%">
				<?php echo  JText::_('Name'); ?>				
			</td>
			<td>
				<input class="inputbox" type="text" name="name" id="name" size="50" maxlength="250" value="<?php echo $this->row->name;?>" onchange="checkFieldName();" />
			</td>
		</tr>
		<tr>
			<td class="key" width="30%">
				<?php echo  JText::_('Title'); ?>				
			</td>
			<td>
				<input class="inputbox" type="text" name="title" id="title" size="50" maxlength="250" value="<?php echo JText::_($this->row->title);?>" />
			</td>
		</tr>
		<tr>
			<td class="key" width="30%">
				<?php echo JText::_('Ordering'); ?>:
			</td>
			<td colspan="3">
				<?php echo $this->lists['ordering']; ?>
			</td>
		</tr>
		<tr>
			<td class="key">
				<?php echo JText::_('Field Type'); ?>
			</td>
			<td>
				<?php echo $this->lists['field_type']; ?>
			</td>
		</tr>
		<tr>
			<td class="key">
				<?php echo  JText::_('Description'); ?>
			</td>
			<td>
				<textarea class="inputbox" rows="5" cols="50" name="description"><?php echo $this->row->description;?></textarea>
			</td>
		</tr>
		<tr>
			<td class="key">
				<?php echo JText::_('Required'); ?>
			</td>
			<td>
				<?php echo $this->lists['required']; ?>
			</td>
		</tr>
		<tr>
			<td class="key">
				<?php echo JText::_('Values'); ?>
			</td>
			<td>
				<textarea rows="5" cols="50" name="values"><?php echo JText::_($this->row->values); ?></textarea>
			</td>
		</tr>
		<tr>
			<td class="key">
				<?php echo JText::_('Default Values'); ?>
			</td>
			<td>
				<textarea rows="5" cols="50" name="default_values"><?php echo JText::_($this->row->default_values); ?></textarea>
			</td>
		</tr>		
		<tr>
			<td class="key" width="30%">
				<?php echo  JText::_('Rows'); ?>
			</td>
			<td>
				<input class="inputbox" type="text" name="rows" id="rows" size="10" maxlength="250" value="<?php echo $this->row->rows;?>" />
			</td>
		</tr>
		<tr>
			<td class="key" width="30%">
				<?php echo  JText::_('Cols'); ?>
			</td>
			<td>
				<input class="inputbox" type="text" name="cols" id="cols" size="10" maxlength="250" value="<?php echo $this->row->cols;?>" />
			</td>
		</tr>
		<tr>
			<td class="key" width="30%">
				<?php echo  JText::_('Size'); ?>
			</td>
			<td>
				<input class="inputbox" type="text" name="size" id="size" size="10" maxlength="250" value="<?php echo $this->row->size;?>" />
			</td>
		</tr>
		<tr>
			<td class="key" width="30%">
				<?php echo  JText::_('Css class'); ?>
			</td>
			<td>
				<input class="inputbox" type="text" name="css_class" id="css_class" size="10" maxlength="250" value="<?php echo $this->row->css_class;?>" />
			</td>
		</tr>		
		<tr>
			<td class="key">
				<?php echo JText::_('Published'); ?>
			</td>
			<td>
				<?php echo $this->lists['published']; ?>
			</td>
		</tr>
	</table>			
</div>		
<div class="clr"></div>
	<input type="hidden" name="option" value="com_zj_donation" />
	<input type="hidden" name="cid[]" value="<?php echo $this->row->id; ?>" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="controller" value="field" />
	<?php echo JHTML::_( 'form.token' ); ?>	
	<script type="text/javascript" language="javascript">
		function checkFieldName() {
			var form = document.adminForm ;
			var name = form.name.value ;
			var oldValue = name ;			
			name = name.replace('zj_','');			
			name.replace(/[^a-zA-Z0-9]+/g,'');			
			form.name.value='zj_' + name;
			if (oldValue != form.name.value) {
				alert('<?php echo JText::_('Field name automatically changed to follow naming standard'); ?>');
			}			
		}
	</script>
	
</form>