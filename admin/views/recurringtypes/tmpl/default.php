<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
?>

<form action="index.php" method="post" name="adminForm">
	<table class="adminform">
		<tr>
			<td align="left" width="100%">
				<?php echo JText::_('Filter'); ?>:
				<input type="text" class="text_area" name="search" id="search" value="<?php echo $this->lists['search']; ?>" onchange="document.adminForm.submit();" />
				<button onclick="this.form.submit();"><?php echo JText::_('Go'); ?></button>
				<button onclick="this.form.getElementById('search').value=''; this.form.getElementById('filter_state').value = '0'; this.form.submit();">
					<?php echo JText::_('Reset'); ?>
				</button>
			</td>
			<td nowrap="nowrap">
				<?php
				echo $this->lists['state'];
				?>
			</td>
		</tr>
	</table>
	<div id="editcell">
		<table class="adminlist">
			<thead>
				<tr>
					<th width="5">
						<?php echo JText::_('NUM'); ?>
					</th>
					<th width="20">
						<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($this->rows); ?>);" />
					</th>
					<th class="title">
						<?php echo JHTML::_('grid.sort', JText::_('Title'), 'a.title', $this->lists['order_Dir'], $this->lists['order']); ?>
					</th>
					<th class="title">
						<?php echo JHTML::_('grid.sort', JText::_('Value'), 'a.value', $this->lists['order_Dir'], $this->lists['order']); ?>
					</th>
					<th class="title">
						<?php echo JHTML::_('grid.sort', JText::_('Type'), 'a.type', $this->lists['order_Dir'], $this->lists['order']); ?>
					</th>
					<th width="5%" nowrap="nowrap">
						<?php echo JHTML::_('grid.sort', JText::_('Published'), 'a.published', $this->lists['order_Dir'], $this->lists['order']); ?>
					</th>
					<th width="10%" nowrap="nowrap">
						<?php echo JHTML::_('grid.sort', JText::_('Order'), 'a.ordering', $this->lists['order_Dir'], $this->lists['order'] ); ?>
						<?php if ($this->ordering) echo JHTML::_('grid.order', $this->rows ); ?>
					</th>
					<th width="1%" nowrap="nowrap">
						<?php echo JHTML::_('grid.sort', JText::_('ID'), 'a.id', $this->lists['order_Dir'], $this->lists['order']); ?>
					</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="8">
						<?php echo $this->pagination->getListFooter(); ?>
					</td>
				</tr>
			</tfoot>
			<tbody>
			<?php
				$k = 0;
				for ($i = 0, $n = count($this->rows); $i < $n; $i++) {
					$row =& $this->rows[$i];

					$link		= JRoute::_('index.php?option=com_zj_donation&controller=recurringtypes&task=edit&cid[]=' . $row->id);
					$checked	= JHTML::_('grid.checkedout', $row, $i);
					$published	= JHTML::_('grid.published', $row, $i);
				?>
				<tr class="row<?php echo $k; ?>">
					<td>
						<?php echo $this->pagination->getRowOffset($i); ?>
					</td>
					<td>
						<?php echo $checked; ?>
					</td>
					<td>
						<?php
						if (JTable::isCheckedOut($this->user->get('id'), $row->checked_out)) {
							echo $this->escape($row->title);
						} else {
						?>
						<span class="editlinktip hasTip" title="<?php echo JText::_('Edit Campaign'); ?>::<?php echo $this->escape($row->title); ?>">
							<a href="<?php echo $link; ?>">
								<?php echo $this->escape($row->title); ?>
							</a>
						</span>
						<?php
						}
						?>
					</td>
					<td>
						<?php echo $row->value; ?>
					</td>
					<td>
						<?php echo $row->type; ?>
					</td>
					<td align="center">
						<?php echo $published; ?>
					</td>
					<td class="order" align="center">
						<span>
							<?php echo $this->pagination->orderUpIcon($i, true,'orderup', 'Move Up', $this->ordering); ?>
						</span>
						<span>
							<?php echo $this->pagination->orderDownIcon($i, $n, true, 'orderdown', 'Move Down', $this->ordering); ?>
						</span>
						<?php $disabled = $this->ordering ?  '' : 'disabled="disabled"'; ?>
						<input type="text" class="text_area" name="order[]" value="<?php echo $row->ordering;?>" size="5" <?php echo $disabled; ?> style="text-align: center" />
					</td>
					<td align="center">
						<?php echo $row->id; ?>
					</td>
				</tr>
				<?php
					$k = 1 - $k;
				}
				?>
			</tbody>
		</table>
	</div>
	
	<input type="hidden" name="option" value="com_zj_donation" />
	<input type="hidden" name="view" value="recurringtypes" />
	<input type="hidden" name="controller" value="recurringtypes" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
	<?php echo JHTML::_('form.token'); ?>
</form>

<?php
echo ZJ_DonationFactory::getFooter();