<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

/**
 * ZJ_Donation Component - Recurring Types View.
 * @package		ZJ_Donation
 * @subpackage	View
 */
class ZJ_DonationViewCurrency extends JView {
	
	/**
	 * Display.
	 */
	function display($tpl = null) {
		global $mainframe;
		
		$editor = &JFactory::getEditor();
		$user	= &JFactory::getUser();
		$model	= &$this->getModel();

		// fail if checked out not by 'me'
		if ($model->isCheckedOut($user->get('id'))) {
			$msg = JText::sprintf('DESCBEINGEDITTED', JText::_('The Currency'), $row->title);
			$mainframe->redirect('index.php?option=com_zj_donation&view=currencies', $msg);
		}
		// do checkout
		$model->checkout();

		// hide mainmenu
		JRequest::setVar('hidemainmenu', 1);

		// get data from model
		$row	= &$this->get('Data');
		$isNew	= $row->id < 1;

		// set page title
		$text		= $isNew ? JText::_('New') : JText::_('Edit');
		$document	= &JFactory::getDocument();
		$document->setTitle(JText::_('Currency') . ' ' . $text);

		// load behavior
		jimport('joomla.html.pane');
		JHTML::_('behavior.tooltip');
		JHTML::_('behavior.modal');

		// create the toolbar
		JToolBarHelper::title(JText::_('Currency') . ': <small><small>[ ' . $text . ' ]</small></small>', 'currency.png');
		JToolBarHelper::apply();
		JToolBarHelper::save();
		if ($isNew) {
			JToolBarHelper::cancel();
		} else {
			JToolBarHelper::cancel('cancel', JText::_('Close'));
		}

		// edit or create?
		if (!$isNew) {
			$model->checkout($user->get('id'));
		} else {
			// initialise new record
			$row->published	= 1;
			$row->order		= 0;
		}

		// clean the currency data
		JFilterOutput::objectHTMLSafe($row, ENT_QUOTES, 'description');

		// build lists
		$lists				= $this->_buildLists($row);
		
		$this->assignRef('user',		$user);
		$this->assignRef('pane',		$pane);
		$this->assignRef('editor',		$editor);
		$this->assignRef('lists',		$lists);
		$this->assignRef('row',			$row);
		
		parent::display($tpl);
	}

	/**
	 * Build lists html.
	 */
	function _buildLists($row) {
		// build the html select list for ordering
		$query = 'SELECT ordering AS value, title AS text'
			. ' FROM #__zj_donation_currencies'
			. ' ORDER BY ordering'
		;
		$lists['ordering']		= JHTML::_('list.specificordering', $row, $row->id, $query);

		// build published list
		$lists['published']		= JHTML::_('select.booleanlist', 'published', 'class="inputbox"', $row->published);

		// build position list
		$lists['position']		= JHTML::_('select.booleanlist', 'position', 'class="inputbox"', $row->position, JText::_('Right'), JText::_('Left'));

		return $lists;
	}
}
