<?php
/**
 * @version		$Id: $
 * @author		Joomseller!
 * @package		Joomla!
 * @subpackage	Adwords Manager
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, See LICENSE.txt
 */

// Check to ensure this file is included in Joomla!
defined( '_JEXEC' ) or die( 'Restricted access' );

$db			=& JFactory::getDBO();
$nullDate 	= $db->getNullDate();
?>

<script type="text/javascript">
	function submitbutton(task) {
		var form = document.adminForm;

		if (task == 'cancel') {
			submitform(task);
		} else if (form.title.value == '') {
			alert('<?php echo JText::_('Please add a title!'); ?>');
			form.title.focus();
		} else if (form.code.value == '') {
			alert('<?php echo JText::_('Please add a currency code'); ?>');
			form.code.focus();
		} else {
			submitform(task);
		}
	}
</script>

<form action="index.php" method="post" name="adminForm" id="adminForm">
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
			<td valign="top">
				<table class="adminform">
					<tr>
						<td nowrap="nowrap">
							<label for="title">
								<?php echo JText::_('Title'); ?>:
							</label>
						</td>
						<td>
							<input type="text" class="inputbox" name="title" id="title" value="<?php echo $this->row->title; ?>" size="40" maxlength="100" />
						</td>
						<td>
							<label for="published">
								<?php echo JText::_('Published') . ':'; ?>
							</label>
						</td>
						<td>
							<?php echo $this->lists['published']; ?>
						</td>
					</tr>
					<tr>
						<td>
							<label for="code">
								<?php echo JText::_('Code'); ?>:
							</label>
						</td>
						<td>
							<input type="text" class="inputbox" name="code" id="code" value="<?php echo $this->row->code; ?>" size="10" maxlength="20" />
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<label for="sign">
								<?php echo JText::_('Sign'); ?>:
							</label>
							&nbsp;&nbsp;
							<input type="text" class="inputbox" name="sign" id="sign" value="<?php echo $this->row->sign; ?>" size="10" maxlength="20" />
						</td>
						<td>
							<label for="position">
								<?php echo JText::_('Position'); ?>:
							</label>
						</td>
						<td>
							<?php echo $this->lists['position']; ?>
						</td>
					</tr>
					<tr>
						<td>
							<label for="ordering">
								<?php echo JText::_('Ordering'); ?>:
							</label>
						</td>
						<td colspan="3">
							<?php echo $this->lists['ordering']; ?>
						</td>
					</tr>
				</table>

				<table class="adminform">
					<tr>
						<td>
							<?php
							echo $this->editor->display('description', $this->row->description, '100%', '400', '50', '20', array('pagebreak', 'readmore'));
							?>
						</td>
					</tr>
				</table>
			</td>
			<td valign="top" width="320px" style="padding: 7px 0 0 5px">
				<table width="100%" style="border: 1px dashed silver; padding: 5px; margin-bottom: 10px;">
				<?php if ($this->row->id) { ?>
					<tr>
						<td>
							<strong><?php echo JText::_('Currency ID'); ?>:</strong>
						</td>
						<td>
							<?php echo $this->row->id; ?>
						</td>
					</tr>
				<?php } ?>
					<tr>
						<td>
							<strong><?php echo JText::_('State'); ?>:</strong>
						</td>
						<td>
							<?php echo $this->row->published > 0 ? JText::_('Published') : JText::_('Unpublished'); ?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	<input type="hidden" name="option" value="com_zj_donation" />
	<input type="hidden" name="controller" value="currency" />
	<input type="hidden" name="cid[]" value="<?php echo $this->row->id; ?>" />
	<input type="hidden" name="task" value="" />
	<?php echo JHTML::_('form.token'); ?>
</form>

<?php
echo ZJ_DonationFactory::getFooter();