<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

/**
 * ZJ_Donation Component - Donate View.
 * @package		ZJ_Donation
 * @subpackage	View
 */
class ZJ_DonationViewDonate extends JView {
	/**
	 * Display.
	 */
	function display($tpl = null) {
		global $mainframe;

		$editor = &JFactory::getEditor();
		$user	= &JFactory::getUser();
		$model	= &$this->getModel();

		// hide mainmenu
		JRequest::setVar('hidemainmenu', 1);

		// get data from model
		$fields	= &$this->get('Fields');
		$row	= &$this->get('Data');
		$isNew	= $row->id < 1;

		if ($isNew) {
			$mainframe->redirect('index.php?option=com_zj_donation&view=donates', JText::_('Donation not found.'));
			return false;
		}

		//Get campaign
		$campaign_model		= ZJ_DonationFactory::getModel('campaign');
		$campaign_model->setId($row->campaign_id);
		$campaign			= $campaign_model->getData();

		// build lists
		$lists				= $this->_buildLists($row);
		$admin_live_site	= JURI::base(true) . '/';
		$live_site			= $mainframe->getSiteURL();

		$this->addToolBar();

		$this->assignRef('live_site',		$live_site);
		$this->assignRef('admin_live_site',	$admin_live_site);
		$this->assignRef('user',			$user);
		$this->assignRef('lists',			$lists);
		$this->assignRef('fields',			$fields);
		$this->assignRef('campaign',		$campaign);
		$this->assignRef('row',				$row);

		parent::display($tpl);
	}

	/**
	 * Build lists html.
	 */
	function _buildLists($row) {

		// build published list
		$lists['published']		= JHTML::_('select.booleanlist', 'published', 'class="inputbox"', $row->published);

		// build the parent list
		$lists['campaign']		= JHTML::_('zj_donation.campaign', 'campaign_id', (int) $row->campaign_id, '', 1, '- ' . JText::_('Select a Campaign') . ' -', $row->id);

		return $lists;
	}

	function addToolBar() {
		// set page title
		$document = &JFactory::getDocument();
		$document->setTitle(JText::_('Donation Details'));

		// load behavior
		JHTML::_('behavior.tooltip');

		// add submenu
		JSubMenuHelper::addEntry(JText::_('Dashboard'), 'index.php?option=com_zj_donation');
		JSubMenuHelper::addEntry(JText::_('Campaigns'), 'index.php?option=com_zj_donation&view=campaigns');
		JSubMenuHelper::addEntry(JText::_('Fields'), 'index.php?option=com_zj_donation&view=fields');
		JSubMenuHelper::addEntry(JText::_('Donates'), 'index.php?option=com_zj_donation&view=donates', true);
		JSubMenuHelper::addEntry(JText::_('Currencies'), 'index.php?option=com_zj_donation&view=currencies');
		JSubMenuHelper::addEntry(JText::_('Recurring'), 'index.php?option=com_zj_donation&view=recurringtypes');
		JSubMenuHelper::addEntry(JText::_('Configuration'), 'index.php?option=com_zj_donation&view=configs');

		
		// create the toolbar
		JToolBarHelper::title(JText::_('Donation Details'), 'donate.png');
		JToolBarHelper::publishList();
		JToolBarHelper::unpublishList();
		JToolBarHelper::deleteList();
		JToolBarHelper::spacer();
		JToolBarHelper::back(JText::_('Back'), 'index.php?option=com_zj_donation&view=donates');
	}
}
