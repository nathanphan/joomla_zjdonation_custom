<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

?>
<fieldset class="adminform">
	<legend><?php echo JText::_('Campaign Information'); ?></legend>
	<table class="adminlist">
		<thead>
			<tr class="title">
				<th width="1">
					<?php echo JText::_('ID'); ?>
				</th>
				<th class="title" width="40%">
					<?php echo JText::_('Campaign Title'); ?>
				</th>
				<th class="title" width="20%">
					<?php echo JText::_('Goal'); ?>
				</th>
				<th class="title" width="20%">
					<?php echo JText::_('Donated Amount'); ?>
				</th>
				<th class="title">
					<?php echo JText::_('Donors'); ?>
				</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$campaign_link = 'index.php?option=com_zj_donation&view=campaign&cid[]=' . $this->campaign->id;
			?>
			<tr>
				<td>
					<?php echo $this->campaign->id; ?>
				</td>
				<td>
					<a href="<?php echo $campaign_link ; ?>">
						<?php echo $this->campaign->title; ?>
					</a>
				</td>
				<td align="center">
					<?php echo $this->campaign->goal; ?>
				</td>
				<td align="center">
					<?php echo $this->campaign->donated_amount; ?>
				</td>
				<td align="center">
					<?php echo $this->campaign->donors; ?>
				</td>
			</tr>
		</tbody>
	</table>
</fieldset>