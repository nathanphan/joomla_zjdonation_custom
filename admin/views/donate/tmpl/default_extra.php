<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

if (count($this->fields)) {
	?>
	<fieldset class="adminform">
		<legend><?php echo JText::_('Addition Information'); ?></legend>
		<table class="admintable" cellpadding="1">
			<tbody>
				<?php
				for ($i = 0, $n = count($this->fields); $i < $n; $i++) {
					$field	= $this->fields[$i];
					?>
					<tr>
						<td class="key">
							<?php echo $field->title; ?>
						</td>
						<td>
							<?php echo $field->field_value; ?>
						</td>
					</tr>
					<?php
				}
				?>
			</tbody>
		</table>
	</fieldset>
	<?php
}
?>