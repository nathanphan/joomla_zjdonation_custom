<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

?>
<fieldset class="adminform">
	<legend><?php echo JText::_('Donor Information'); ?></legend>
	<table class="admintable" cellpadding="1">
		<tbody>
			<tr>
				<td class="key">
					<?php echo JText::_('First Name'); ?>
				</td>
				<td>
					<?php echo $this->row->first_name; ?>
				</td>
			</tr>
			<tr>
				<td class="key">
					<?php echo JText::_('Last Name'); ?>
				</td>
				<td>
					<?php echo $this->row->last_name; ?>
				</td>
			</tr>
			<tr>
				<td class="key">
					<?php echo JText::_('E-mail'); ?>
				</td>
				<td>
					<?php echo $this->row->email; ?>
				</td>
			</tr>
			<tr>
				<td class="key">
					<?php echo JText::_('Organization'); ?>
				</td>
				<td>
					<?php echo $this->row->organization; ?>
				</td>
			</tr>
			<tr>
				<td class="key">
					<?php echo JText::_('Website'); ?>
				</td>
				<td>
					<?php echo $this->row->website; ?>
				</td>
			</tr>
			<tr>
				<td class="key">
					<?php echo JText::_('Address'); ?>
				</td>
				<td>
					<?php echo $this->row->address; ?>
				</td>
			</tr>
			<tr>
				<td class="key">
					<?php echo JText::_('City'); ?>
				</td>
				<td>
					<?php echo $this->row->city; ?>
				</td>
			</tr>
			<tr>
				<td class="key">
					<?php echo JText::_('State'); ?>
				</td>
				<td>
					<?php echo $this->row->state; ?>
				</td>
			</tr>
			<tr>
				<td class="key">
					<?php echo JText::_('Zip'); ?>
				</td>
				<td>
					<?php echo $this->row->zip; ?>
				</td>
			</tr>
			<tr>
				<td class="key">
					<?php echo JText::_('Country'); ?>
				</td>
				<td>
					<?php echo $this->row->country; ?>
				</td>
			</tr>
			<tr>
				<td class="key">
					<?php echo JText::_('Phone Number'); ?>
				</td>
				<td>
					<?php echo $this->row->phone; ?>
				</td>
			</tr>
		</tbody>
	</table>
</fieldset>