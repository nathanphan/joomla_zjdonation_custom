<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

$db			= &JFactory::getDBO();
$nullDate 	= $db->getNullDate();

$user_link	= 'index.php?option=com_users&view=user&task=edit&cid[]=' . $this->row->user_id;
?>
<fieldset class="adminform">
	<legend><?php echo JText::_('Donation Information'); ?></legend>
	<table class="admintable" cellpadding="1">
		<tbody>
			<tr>
				<td class="key">
					<?php echo JText::_('ID'); ?>
				</td>
				<td>
					<?php echo $this->row->id; ?>
				</td>
			</tr>
			<tr>
				<td class="key">
					<?php echo JText::_('User Name'); ?>
				</td>
				<td>
					<?php if ($this->row->user_id) { ?>
					<a href="<?php echo $user_link; ?>">
						<?php echo $this->row->username; ?>
					</a>
					<?php } ?>
				</td>
			</tr>
			<tr>
				<td class="key">
					<?php echo JText::_('Donation Amount'); ?>
				</td>
				<td>
					<?php echo $this->row->amount; ?>
				</td>
			</tr>
			<tr>
				<td class="key">
					<?php echo JText::_('Peyment Method'); ?>
				</td>
				<td>
					<?php echo $this->row->payment_method; ?>
				</td>
			</tr>
			<tr>
				<td class="key">
					<?php echo JText::_('Transaction ID'); ?>
				</td>
				<td>
					<?php echo $this->row->transaction_id; ?>
				</td>
			</tr>
			<tr>
				<td class="key">
					<?php echo JText::_('Published'); ?>
				</td>
				<td style="color: red;">
					<?php echo $this->row->published ? JText::_('Yes') : JText::_('No'); ?>
				</td>
			</tr>
			<tr>
				<td class="key">
					<?php echo JText::_('Date Created'); ?>
				</td>
				<td>
					<?php echo JHTML::_('date', $this->row->created_date, JText::_('DATE_FORMAT_LC2')); ?>
				</td>
			</tr>
			<tr>
				<td class="key">
					<?php echo JText::_('Payment Date'); ?>
				</td>
				<td>
					<?php echo $this->row->payment_date == $nullDate ? JText::_('Never') : JHTML::_('date', $this->row->payment_date, JText::_('DATE_FORMAT_LC2')); ?>
				</td>
			</tr>
			<tr>
				<td class="key">
					<?php echo JText::_('Donation Type'); ?>
				</td>
				<td>
					<?php echo $this->row->recurring ? $this->row->recurring_title : JText::_('One Time'); ?>
				</td>
			</tr>
			<tr>
				<td class="key">
					<?php echo JText::_('Recurring Times'); ?>
				</td>
				<td>
					<?php echo ($this->row->recurring) ? (int)$this->row->donated_times.'/'.(int)$this->row->recurring_times : '0'; ?>
				</td>
			</tr>
			<tr>
				<td class="key">
					<?php echo JText::_('IP Address'); ?>
				</td>
				<td>
					<?php echo (int)$this->row->ip_address; ?>
				</td>
			</tr>
		</tbody>
	</table>
</fieldset>