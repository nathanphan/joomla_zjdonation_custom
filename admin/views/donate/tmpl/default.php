<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
?>

<form name="adminForm" action="index.php" method="post">
	<input type="hidden" name="option" value="com_zj_donation" />
	<input type="hidden" name="view" value="donate" />
	<input type="hidden" name="controller" value="donates" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="cid[]" value="<?php echo $this->row->id; ?>" />
	<input type="hidden" name="boxchecked" value="1" />
	<?php echo JHTML::_('form.token'); ?>
</form>

<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
		<td valign="top" width="45%">
			<?php echo $this->loadTemplate('donor'); ?>
			<?php echo $this->loadTemplate('extra'); ?>
		</td>
		<td valign="top" width="320px" style="padding: 7px 0 0 5px">
			<?php echo $this->loadTemplate('donation'); ?>
			<?php echo $this->loadTemplate('campaign');?>
		</td>
	</tr>
</table>

<?php
echo ZJ_DonationFactory::getFooter();