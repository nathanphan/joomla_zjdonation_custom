<?php
/**
 * @version		$Id: view.html.php 1 2009-10-05 06:23:31 PM Pham Minh Tuan $
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2009 by Zerosoft. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 * This file may not be redistributed in whole or significant part.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');


/**
 * ZJ_Donation Component - Image Handler View
 * @package		ZJ_Donation
 * @subpackage	View
 * @since		1.0
 */
class ZJ_DonationViewImageHandler extends JView {
	
	/**
	 * Display
	 *
	 */
	function display($tpl = null) {
		global $mainframe, $option;
		
		// get vars
		$config		= &ZJ_DonationFactory::getConfig();
		$task		= JRequest::getVar('task');
		$search		= $mainframe->getUserStateFromRequest($option . '.imagehandler.search', 'search', '', 'string');
		$search		= JString::trim(JString::strtolower($search));
		
		$image_id	= JRequest::getInt('image_id');
		$allowed_ext = $config->get('image_allowed_ext');

		// set variables
		if ($task == 'selectgallery') {
			$folder			= 'galleries';
			$task_upload 	= 'galleryup';
		} else {
			$task			= 'selectimage';
			$folder			= 'images';
			$task_upload	= 'imageup';
		}
		
		JRequest::setVar('folder', $folder);
		
		// Do now allow cache
		JResponse::allowCache(false);
		
		// get images
		$images		= $this->get('Data');
		$pagination	=& $this->get('Pagination');
		
		jimport('joomla.client.helper');
		$ftp =& JClientHelper::setCredentialsFromRequest('ftp');
		
		$show_choose_image = true;
		if (!(count($images) > 0 || $search)) {
			JError::raiseNotice('100', JText::_('No images available'));
			$show_choose_image = false;
		}
		
		$site_url = $mainframe->getSiteURL();
		
		$this->assignRef('images',		$images);
		$this->assignRef('folder',		$folder);
		$this->assignRef('task',		$task);
		$this->assignRef('task_upload',	$task_upload);
		$this->assignRef('search',		$search);
		$this->assignRef('state',		$this->get('state'));
		$this->assignRef('pagination',	$pagination);
		$this->assignRef('site_url',	$site_url);
		$this->assignRef('ftp',			$ftp);
		$this->assignRef('config',	$config);
		$this->assignRef('show_choose_image', $show_choose_image);
		$this->assignRef('image_id',	$image_id);
		
		parent::display($tpl);
	}

	/**
	 * Set image
	 * 
	 * @access	private
	 * @since	1.0
	 */
	function _setImage($index = 0) {
		if (isset($this->images[$index])) {
			$this->_tmp_img =& $this->images[$index];
		} else {
			$this->_tmp_img = new JObject;
		}
	}
	
}

?>