<?php
/**
 * @version		$Id: default.php 1 2009-10-05 06:23:31 PM Pham Minh Tuan $
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2009 by Zerosoft. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 * This file may not be redistributed in whole or significant part.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

?>

<form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
	<?php echo $this->loadTemplate('upload'); ?>
	<?php if ($this->show_choose_image) {?>
	<div class="imghead">
		<?php echo JText::_('SEARCH') . ' '; ?>
		<input type="text" name="search" id="search" value="<?php echo $this->search; ?>" class="text_area" onchange="document.adminForm.submit();" />
		<button onclick="document.adminForm.submit();"><?php echo JText::_('Go'); ?></button>
		<button onclick="document.adminForm.getElementById('search').value='';document.adminForm.submit();"><?php echo JText::_('Reset'); ?></button>
	
	</div>
	
	<div class="imglist">
			<?php
			for ($i = 0, $n = count($this->images); $i < $n; $i++) :
				$this->_setImage($i);
				echo $this->loadTemplate('image');
			endfor;
			?>
	</div>
	
	<div class="clear"></div>
			
	<div class="pnav"><?php echo $this->pagination->getListFooter(); ?></div>
	<?php
	}
	?>
	<input type="hidden" name="option" value="com_zj_donation" />
	<input type="hidden" name="controller" value="imagehandler" />
	<input type="hidden" name="view" value="imagehandler" />
	<input type="hidden" name="tmpl" value="component" />
	<input type="hidden" name="task" value="<?php echo $this->task; ?>" />
	<input type="hidden" name="image_id" value="<?php echo $this->image_id; ?>" />
	
	<?php echo JHTML::_('form.token'); ?>
</form>

<?php echo ZJ_DonationFactory::getFooter(); ?>