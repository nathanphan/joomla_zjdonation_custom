<?php
/**
* @version		$Id: default.php 1 Jul 20, 2009 \" $
* @package		Joomla!
* @copyright	Copyright (C) 2008 - 2009 Zerosoft. All rights reserved.
* @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

?>

<form method="post" action="<?php echo $this->request_url; ?>" enctype="multipart/form-data" name="adminForm">

<table class="noshow">
  	<tr>
		<td width="50%" valign="top">
		
				<?php if($this->ftp): ?>
				<fieldset class="adminform">
					<legend><?php echo JText::_('FTP Title'); ?></legend>

					<?php echo JText::_('FTP Dscription'); ?>
					
					<?php if (JError::isError($this->ftp)): ?>
						<p><?php echo JText::_($this->ftp->message); ?></p>
					<?php endif; ?>

					<table class="adminform nospace">
						<tbody>
							<tr>
								<td width="120">
									<label for="username"><?php echo JText::_('Username'); ?>:</label>
								</td>
								<td>
									<input type="text" id="username" name="username" class="input_box" size="70" value="" />
								</td>
							</tr>
							<tr>
								<td width="120">
									<label for="password"><?php echo JText::_('Password'); ?>:</label>
								</td>
								<td>
									<input type="password" id="password" name="password" class="input_box" size="70" value="" />
								</td>
							</tr>
						</tbody>
					</table>
				</fieldset>
			<?php endif; ?>

			<fieldset class="adminform">
			<legend><?php echo JText::_('Select image to upload'); ?></legend>
			<table class="admintable" cellspacing="1">
				<tbody>
					<tr>
	          			<td>
 							<input class="inputbox" name="userfile" id="userfile" type="file" />
							<br /><br />
							<input class="button" type="submit" value="<?php echo JText::_('Upload') ?>" name="adminForm" />
    			       	</td>
      				</tr>
				</tbody>
			</table>
			</fieldset>

		</td>
        <td width="50%" valign="top">

			<fieldset class="adminform">
			<legend><?php echo JText::_('Attention'); ?></legend>
			<table class="admintable" cellspacing="1">
				<tbody>
					<tr>
	          			<td>
 							<b><?php
 							echo JText::_('Target directory').':'; ?></b>
							<?php
							if ($this->task == 'img') {
								echo "/media/zj_windextra/images/";
								$this->task = 'imageup';
							} else {
								echo "/media/zj_windextra/gallery/";
								$this->task = 'galleryup';
							}

							?><br />
							<b><?php echo JText::_('Image filesize').':'; ?></b> <?php echo '1000'; ?> kb<br />
							<?php echo JText::_('Allowed extensions: jpg, png, gif'); ?>
    			       	</td>
      				</tr>
				</tbody>
			</table>
			</fieldset>

		</td>
	</tr>
</table>


<?php echo JHTML::_( 'form.token' ); ?>
<input type="hidden" name="option" value="<?php echo $this->option; ?>" />
<input type="hidden" name="controller" value="imagehandler" />
<input type="hidden" name="view" value="imagehandler" />
<input type="hidden" name="task" value="<?php echo $this->task;?>" />
</form>
