<?php
/**
 * @version		$Id: default_upload.php 1 2009-10-05 06:23:31 PM Pham Minh Tuan $
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2009 by Zerosoft. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 * This file may not be redistributed in whole or significant part.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

?>
<table class="noshow">
  	<tr>
		<td width="50%" valign="top">
			<?php if($this->ftp): ?>
			<fieldset class="adminform">
				<legend><?php echo JText::_('FTP Title'); ?></legend>

				<?php echo JText::_('FTP Dscription'); ?>
				
				<?php if (JError::isError($this->ftp)): ?>
					<p><?php echo JText::_($this->ftp->message); ?></p>
				<?php endif; ?>

				<table class="adminform nospace">
					<tbody>
						<tr>
							<td width="120">
								<label for="username"><?php echo JText::_('Username') . ':'; ?>:</label>
							</td>
							<td>
								<input type="text" id="username" name="username" class="input_box" size="70" value="" />
							</td>
						</tr>
						<tr>
							<td width="120">
								<label for="password"><?php echo JText::_('Password') . ':'; ?>:</label>
							</td>
							<td>
								<input type="password" id="password" name="password" class="input_box" size="70" value="" />
							</td>
						</tr>
					</tbody>
				</table>
			</fieldset>
			<?php endif; ?>

			<fieldset class="adminform">
			<legend><?php echo JText::_('Select image to upload'); ?></legend>
			<table class="admintable" cellspacing="1">
				<tbody>
					<tr>
	          			<td>
 							<input class="inputbox" name="zj_image" id="zj_image" type="file" size="40" />
							<p>
								<span style="color:#AAAAAA;"><?php echo JText::_('Valid file type') . ': ';?><i><?php echo implode(', ', explode(',', str_replace(' ', '', $this->config->get('image_allowed_ext')))); ?></i></span>
							</p>
							<input class="button" type="button" value="<?php echo JText::_('Upload') ?>" onclick="document.adminForm.task.value = '<?php echo $this->task_upload; ?>'; document.adminForm.submit();" />
    			       	</td>
      				</tr>
				</tbody>
			</table>
			</fieldset>
		</td>
        <td width="50%" valign="top">
			<fieldset class="adminform">
			<legend><?php echo JText::_('Attention!'); ?></legend>
			<table class="admintable" cellspacing="1">
				<tbody>
					<tr>
	          			<td>
 							<b><?php echo JText::_('Target directory').':'; ?></b>
							/media/zj_donation/<?php echo $this->folder; ?>/
							<br />
							<b><?php echo JText::_('Image filesize').':'; ?></b> <?php echo @$this->config->get('image_max_size'); ?> kb
							<?php
							if (@$this->config->get('gddisable')) {
								if (imagetypes() & IMG_PNG) {
									echo "<br /><font color='green'>".JText::_('PNG support')."</font>";
								} else {
									echo "<br /><font color='red'>".JText::_('No PNG support')."</font>";
								}
								if (imagetypes() & IMG_JPEG) {
									echo "<br /><font color='green'>".JText::_('JPG support')."</font>";
								} else {
									echo "<br /><font color='red'>".JText::_('No JPG support')."</font>";
								}
								if (imagetypes() & IMG_GIF) {
									echo "<br /><font color='green'>".JText::_('GIF support')."</font>";
								} else {
									echo "<br /><font color='red'>".JText::_('No GIF support')."</font>";
								}
							} else {
								echo "<br /><font color='green'>".JText::_( 'PNG support' )."</font>";
								echo "<br /><font color='green'>".JText::_( 'JPG support' )."</font>";
								echo "<br /><font color='green'>".JText::_( 'GIF support' )."</font>";
							}
							?>
    			       	</td>
      				</tr>
				</tbody>
			</table>
			</fieldset>
		</td>
	</tr>
</table>