<?php
/**
 * @version		$Id: default_image.php 1 2009-10-05 06:23:31 PM Pham Minh Tuan $
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2009 by Zerosoft. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 * This file may not be redistributed in whole or significant part.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
?>

<div class="item">
	<div align="center" class="imgBorder">
		<a title="<?php echo $this->_tmp_img->name; ?>" onclick="window.parent.zj_selectImage('<?php echo $this->image_id; ?>', '<?php echo $this->folder; ?>', '<?php echo $this->_tmp_img->name; ?>');">
			<div class="image">
				<img src="<?php echo $this->site_url; ?>/media/zj_donation/<?php echo $this->folder; ?>/<?php echo $this->_tmp_img->name; ?>"  width="<?php echo $this->_tmp_img->width_60; ?>" height="<?php echo $this->_tmp_img->height_60; ?>" alt="<?php echo $this->_tmp_img->name; ?> - <?php echo $this->_tmp_img->size; ?>" />
			</div>
		</a>
	</div>
<div class="controls">
	<?php echo $this->_tmp_img->size; ?> -
	<a class="delete-item" title="Remove" href="index.php?option=com_zj_donation&amp;task=delete&amp;controller=imagehandler&amp;tmpl=component&amp;folder=<?php echo $this->folder; ?>&amp;rm[]=<?php echo $this->_tmp_img->name; ?>">
		<img src="<?php echo $this->site_url; ?>administrator/images/publish_x.png" width="16" height="16" border="0" alt="<?php echo JText::_('Delete'); ?>" />
	</a>
</div>
<div class="imageinfo">
	<?php echo $this->escape(substr($this->_tmp_img->name, 0, 10) . (strlen($this->_tmp_img->name) > 10 ? '...' : '')); ?>
	</div>
</div>