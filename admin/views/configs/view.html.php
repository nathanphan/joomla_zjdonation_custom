<?php
/**
 * @version		$Id$
 * @author		Joomseller
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 * This file may not be redistributed in whole or significant part.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

/**
 * ZJ_Donation Component - Configs View
 * @package		ZJ_Donation
 * @subpackage	View
 */
class ZJ_DonationViewConfigs extends JView {
	
	/**
	 * Display.
	 */
	function display($tpl = null) {
		global $mainframe;

		// get data from model
		$row = $this->get('Data');

		jimport('joomla.html.pane');
		JHTML::_('behavior.switcher');

		// Build the component's submenu
		$contents = '';
		$tmplpath = dirname(__FILE__).DS.'tmpl';
		ob_start();
		require_once($tmplpath.DS.'navigation.php');
		$contents = ob_get_contents();
		ob_end_clean();

		// Set document data
		$document = &JFactory::getDocument();
		$document->setBuffer($contents, 'modules', 'submenu');

		// set page title
		$document	= &JFactory::getDocument();
		$document->setTitle(JText::_('ZJ Donation Configuration'));
		
		//
		JToolBarHelper::title(JText::_('ZJ Donation Configuration'), 'config.png');
		JToolBarHelper::apply();
		JToolBarHelper::save();
		JToolBarHelper::spacer();
		JToolBarHelper::back(JText::_('Back'), 'index.php?option=com_zj_donation');

		$tabs	= &JPane::getInstance();
		$editor	= &JFactory::getEditor();

		// build the community component list
		$community_components	= $this->get('CommunityComponents');
		$community_components	= array_merge(array(array('value' => '0', 'text' => '- ' . JText::_('None') . ' -')), $community_components);
		$lists['community_components'] = JHTML::_('select.genericlist', $community_components, 'config[community_integrate]', 'class="inputbox" size="1"', 'value', 'text', $row->get('community_integrate'));

		// build the purchase user type list
		$lists['donate_user_types'] = JHTML::_('select.genericlist', $this->get('DonateUserTypes'), 'config[donate_permission]', 'class="inputbox" size="1"', 'value', 'text', $row->get('donate_permission'));

		$this->assignRef('row', $row);
		$this->assignRef('lists', $lists);
		$this->assignRef('tabs', $tabs);
		$this->assignRef('editor', $editor);

		parent::display($tpl);
	}
}
