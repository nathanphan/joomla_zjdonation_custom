<?php
/**
 * @version		$Id$
 * @author		Joomseller
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 * This file may not be redistributed in whole or significant part.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
?>

<div id="page-email">
	<table class="noshow">
		<tr>
			<td width="100%">
				<fieldset class="adminform">
					<legend><?php echo JText::_('E-mail Configuration'); ?></legend>
					<table class="admintable" cellspacing="1">
						<tr>
							<td class="key" valign="top">
								<?php echo JText::_('Admin E-mail'); ?>
							</td>
							<td>
								<input type="text" class="text_area" name="config[admin_email]" id="admin_email" value="<?php echo $this->row->get('admin_email'); ?>" size="70" />
							</td>
						</tr>
						<tr>
							<td class="key" valign="top">
								<?php echo JText::_('Admin E-mail Name'); ?>
							</td>
							<td>
								<input type="text" class="text_area" name="config[admin_email_name]" id="admin_email_name" value="<?php echo $this->row->get('admin_email_name'); ?>" size="70" />
							</td>
						</tr>
						<tr>
							<td class="key" valign="top">
								<?php echo JText::_('Admin Notify E-mail'); ?>
							</td>
							<td>
								<input type="text" class="text_area" name="config[notify_email]" id="notify_email" value="<?php echo $this->row->get('notify_email'); ?>" size="70" />
							</td>
						</tr>
						<tr>
							<td class="key" valign="top">
								<?php echo JText::_('New Donation Placed'); ?>
							</td>
							<td>
								<?php
								echo JHTML::_('select.booleanlist', 'config[notify_new_donation]', '', $this->row->get('notify_new_donation'));
								?>
							</td>
						</tr>
						<tr>
							<td class="key" valign="top">
								<?php echo JText::_('Donation Actived'); ?>
							</td>
							<td>
								<?php
								echo JHTML::_('select.booleanlist', 'config[notify_donation_actived]', '', $this->row->get('notify_donation_actived'));
								?>
							</td>
						</tr>
					</table>
				</fieldset>
				<?php
				echo $this->tabs->startPane('send_email');
				echo $this->tabs->startPanel(JText::_('E-mail To Donor'), 'user_notify');
				?>
				<div>
					<fieldset class="adminform">
						<legend><?php echo JText::_('Thank You E-mail Message'); ?></legend>
						<table class="admintable" cellspacing="1">
							<tr>
								<td class="key" valign="top">
									<?php echo JText::_('E-mail Subject'); ?>
								</td>
								<td>
									<input type="text" class="text_area" name="config[email_thanks_subject]" id="email_thanks_subject" value="<?php echo $this->row->get('email_thanks_subject'); ?>" size="100" />
								</td>
							</tr>
							<tr>
								<td class="key" valign="top">
									<?php echo JText::_('E-mail Body'); ?>
								</td>
								<td>
									<textarea class="text_area" name="config[email_thanks_body]" id="email_thanks_body" rows="8" cols="70"><?php echo $this->row->get('email_thanks_body'); ?></textarea>
								</td>
							</tr>
						</table>
					</fieldset>
					<fieldset class="adminform">
						<legend><?php echo JText::_('Donation Actived Notify E-mail Message'); ?></legend>
						<table class="admintable" cellspacing="1">
							<tr>
								<td class="key" valign="top">
									<?php echo JText::_('E-mail Subject'); ?>
								</td>
								<td>
									<input type="text" class="text_area" name="config[email_donation_actived_subject]" id="email_donation_actived_subject" value="<?php echo $this->row->get('email_donation_actived_subject'); ?>" size="100" />
								</td>
							</tr>
							<tr>
								<td class="key" valign="top">
									<?php echo JText::_('E-mail Body'); ?>
								</td>
								<td>
									<textarea class="text_area" name="config[email_donation_actived_body]" id="email_donation_actived_body" rows="8" cols="70"><?php echo $this->row->get('email_donation_actived_body'); ?></textarea>
								</td>
							</tr>
						</table>
					</fieldset>
				</div>
				<?php
				echo $this->tabs->endPanel();
				echo $this->tabs->startPanel(JText::_('Administrator Notify'), 'admin_notify');
				?>
				<div>
					<fieldset class="adminform">
						<legend><?php echo JText::_('New Donation Placed E-mail Message'); ?></legend>
						<table class="admintable" cellspacing="1">
							<tr>
								<td class="key" valign="top">
									<?php echo JText::_('E-mail Subject'); ?>
								</td>
								<td>
									<input type="text" class="text_area" name="config[email_admin_donation_created_subject]" id="email_admin_order_created_subject" value="<?php echo $this->row->get('email_admin_donation_created_subject'); ?>" size="100" />
								</td>
							</tr>
							<tr>
								<td class="key" valign="top">
									<?php echo JText::_('E-mail Body'); ?>
								</td>
								<td>
									<textarea class="text_area" name="config[email_admin_donation_created_body]" id="email_admin_donation_created_body" rows="8" cols="70"><?php echo $this->row->get('email_admin_donation_created_body'); ?></textarea>
								</td>
							</tr>
						</table>
					</fieldset>
					<fieldset class="adminform">
						<legend><?php echo JText::_('Donation Actived E-mail Message'); ?></legend>
						<table class="admintable" cellspacing="1">
							<tr>
								<td class="key" valign="top">
									<?php echo JText::_('E-mail Subject'); ?>
								</td>
								<td>
									<input type="text" class="text_area" name="config[email_admin_donation_actived_subject]" id="email_admin_donation_actived_subject" value="<?php echo $this->row->get('email_admin_donation_actived_subject'); ?>" size="100" />
								</td>
							</tr>
							<tr>
								<td class="key" valign="top">
									<?php echo JText::_('E-mail Body'); ?>
								</td>
								<td>
									<textarea class="text_area" name="config[email_admin_donation_actived_body]" id="email_admin_donation_actived_body" rows="8" cols="70"><?php echo $this->row->get('email_admin_donation_actived_body'); ?></textarea>
								</td>
							</tr>
						</table>
					</fieldset>
				</div>
				<?php
				echo $this->tabs->endPanel();
				echo $this->tabs->endPane();
				?>
			</td>
		</tr>
	</table>
</div>