<?php
/**
 * @version		$Id$
 * @author		Joomseller
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 * This file may not be redistributed in whole or significant part.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
?>

<div id="page-general">
	<table class="noshow">
		<tr>
			<td width="45%" align="left">
				<fieldset class="adminform">
					<legend><?php echo JText::_('Image Configuration'); ?></legend>
					<table class="admintable" cellpadding="1">
						<tr>
							<td class="key">
								<span class="editlinktip hasTip" title="">
									<?php echo JText::_('Allow Extensions'); ?>
								</span>
							</td>
							<td>
								<input type="text" class="text_area" name="config[image_allowed_ext]" id="image_allowed_ext" value="<?php echo $this->row->get('image_allowed_ext'); ?>" size="60" />
							</td>
						</tr>
						<tr>
							<td class="key">
								<span class="editlinktip hasTip" title="">
									<?php echo JText::_('Max Size'); ?>
								</span>
							</td>
							<td>
								<input type="text" class="text_area" name="config[image_max_size]" id="image_max_size" value="<?php echo $this->row->get('image_max_size'); ?>" size="20" /><em>(Byte)</em>
							</td>
						</tr>
					</table>
				</fieldset>
				<fieldset class="adminform">
					<legend><?php echo JText::_('Template'); ?></legend>
					<table class="admintable" cellspacing="1">
						<tr>
							<td class="key">
								<span class="editlinktip hasTip" title="">
									<?php echo JText::_('Default template'); ?>
								</span>
							</td>
							<td>
								<?php
								echo JHTML::_('zj_donation.template', 'config[template]', $this->row->get('template'));
								?>
							</td>
						</tr>
					</table>
				</fieldset>
			</td>
			<td width="45%" align="right">
				<fieldset class="adminform">
					<legend><?php echo JText::_('Currency Configuration'); ?></legend>
					<table class="admintable" cellspacing="1">
						<tr>
							<td class="key">
								<span class="editlinktip hasTip" title="">
									<?php echo JText::_('Default Currency'); ?>
								</span>
							</td>
							<td>
								<?php
								echo JHTML::_('zj_donation.currency', 'config[currency]', (int) $this->row->get('currency'));
								?>
							</td>
						</tr>
						<tr>
							<td class="key" width="185">
								<span class="editlinktip hasTip" title="">
									<?php echo JText::_('Currency Display'); ?>
								</span>
							</td>
							<td>
								<?php
								echo JHTML::_('select.booleanlist', 'config[currency_display]', 'class="inputbox"', $this->row->get('currency_display'), JText::_('Sign'), JText::_('Code'));
								?>
							</td>
						</tr>
					</table>
				</fieldset>
				<fieldset class="adminform">
					<legend><?php echo JText::_('Social sharing'); ?></legend>
					<table class="admintable" cellspacing="1">
						<tr>
							<td class="key">
								<span class="editlinktip hasTip" title="">
									<?php echo JText::_('Social sharing'); ?>
								</span>
							</td>
							<td>
								<?php
								echo JHTML::_('select.booleanlist', 'config[social_share]', 'class="inputbox"', $this->row->get('social_share'));
								?>
							</td>
						</tr>
					</table>
				</fieldset>
			</td>
		</tr>
	</table>
</div>