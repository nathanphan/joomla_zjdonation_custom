<?php
/**
 * @version		$Id$
 * @author		Joomseller
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 * This file may not be redistributed in whole or significant part.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
?>

<div class="submenu-box">
	<div class="submenu-pad">
		<ul id="submenu" class="zj_document_configuration">
			<li>
				<a id="general" class="active">
					<?php echo JText::_('General'); ?>
				</a>
			</li>
			<li>
				<a id="email">
					<?php echo JText::_('E-mail'); ?>
				</a>
			</li>
			<li>
				<a id="message">
					<?php echo JText::_('Message'); ?>
				</a>
			</li>
			<li>
				<a id="permission">
					<?php echo JText::_('Integration & Permission'); ?>
				</a>
			</li>
		</ul>
		<div class="clr"></div>
	</div>
</div>
<div class="clr"></div>