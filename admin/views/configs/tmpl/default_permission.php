<?php
/**
 * @version		$Id$
 * @author		Joomseller
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 * This file may not be redistributed in whole or significant part.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
?>

<div id="page-permission">
	<table class="noshow">
		<tr>
			<td width="65%">
				<fieldset class="adminform">
					<legend><?php echo JText::_('User Field Configuration'); ?></legend>
					<table class="adminlist">
						<thead>
							<tr class="title">
								<th align="center">
									<?php echo JText::_('Field Name'); ?>
								</th>
								<th align="center">
									<?php echo JText::_('Visible'); ?>
								</th>
								<th align="center">
									<?php echo JText::_('Required'); ?>
								</th>
								<th align="center">
									<?php echo JText::_('Jomsocial'); ?>
								</th>
								<th align="center">
									<?php echo JText::_('Community Builder'); ?>
								</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="zj_key">
									<span class="editlinktip hasTip" title="">
										<?php echo JText::_('Organization'); ?>
									</span>
								</td>
								<td align="center">
									<?php
									$checked = $this->row->get('field_organization_visible') != 0 ? 'checked="checked"' : '';
									?>
									<input type="checkbox" class="inputbox" name="config[field_organization_visible]" value="1" <?php echo $checked; ?> />
								</td>
								<td align="center">
									<?php
									$checked = $this->row->get('field_organization_required') != 0 ? 'checked="checked"' : '';
									?>
									<input type="checkbox" class="inputbox" name="config[field_organization_required]" value="1" <?php echo $checked; ?> />
								</td>
								<td align="center">
									<input type="text" class="text_area" name="config[field_organization_js]" value="<?php echo $this->row->get('field_organization_js'); ?>" size="25" />
								</td>
								<td align="center">
									<input type="text" class="text_area" name="config[field_organization_cb]" value="<?php echo $this->row->get('field_organization_cb'); ?>" size="25" />
								</td>
							</tr>
							<tr>
								<td class="zj_key">
									<span class="editlinktip hasTip" title="">
										<?php echo JText::_('Website'); ?>
									</span>
								</td>
								<td align="center">
									<?php
									$checked = $this->row->get('field_website_visible') != 0 ? 'checked="checked"' : '';
									?>
									<input type="checkbox" class="inputbox" name="config[field_website_visible]" value="1" <?php echo $checked; ?> />
								</td>
								<td align="center">
									<?php
									$checked = $this->row->get('field_website_required') != 0 ? 'checked="checked"' : '';
									?>
									<input type="checkbox" class="inputbox" name="config[field_website_required]" value="1" <?php echo $checked; ?> />
								</td>
								<td align="center">
									<input type="text" class="text_area" name="config[field_website_js]" value="<?php echo $this->row->get('field_website_js'); ?>" size="25" />
								</td>
								<td align="center">
									<input type="text" class="text_area" name="config[field_website_cb]" value="<?php echo $this->row->get('field_website_cb'); ?>" size="25" />
								</td>
							</tr>
							<tr>
								<td class="zj_key">
									<span class="editlinktip hasTip" title="">
										<?php echo JText::_('Address'); ?>
									</span>
								</td>
								<td align="center">
									<?php
									$checked = $this->row->get('field_address_visible') != 0 ? 'checked="checked"' : '';
									?>
									<input type="checkbox" class="inputbox" name="config[field_address_visible]" value="1" <?php echo $checked; ?> />
								</td>
								<td align="center">
									<?php
									$checked = $this->row->get('field_address_required') != 0 ? 'checked="checked"' : '';
									?>
									<input type="checkbox" class="inputbox" name="config[field_address_required]" value="1" <?php echo $checked; ?> />
								</td>
								<td align="center">
									<input type="text" class="text_area" name="config[field_address_js]" value="<?php echo $this->row->get('field_address_js'); ?>" size="25" />
								</td>
								<td align="center">
									<input type="text" class="text_area" name="config[field_address_cb]" value="<?php echo $this->row->get('field_address_cb'); ?>" size="25" />
								</td>
							</tr>
							<tr>
								<td class="zj_key">
									<span class="editlinktip hasTip" title="">
										<?php echo JText::_('City'); ?>
									</span>
								</td>
								<td align="center">
									<?php
									$checked = $this->row->get('field_city_visible') != 0 ? 'checked="checked"' : '';
									?>
									<input type="checkbox" class="inputbox" name="config[field_city_visible]" value="1" <?php echo $checked; ?> />
								</td>
								<td align="center">
									<?php
									$checked = $this->row->get('field_city_required') != 0 ? 'checked="checked"' : '';
									?>
									<input type="checkbox" class="inputbox" name="config[field_city_required]" value="1" <?php echo $checked; ?> />
								</td>
								<td align="center">
									<input type="text" class="text_area" name="config[field_city_js]" value="<?php echo $this->row->get('field_city_js'); ?>" size="25" />
								</td>
								<td align="center">
									<input type="text" class="text_area" name="config[field_city_cb]" value="<?php echo $this->row->get('field_city_cb'); ?>" size="25" />
								</td>
							</tr>
							<tr>
								<td class="zj_key">
									<span class="editlinktip hasTip" title="">
										<?php echo JText::_('State'); ?>
									</span>
								</td>
								<td align="center">
									<?php
									$checked = $this->row->get('field_state_visible') != 0 ? 'checked="checked"' : '';
									?>
									<input type="checkbox" class="inputbox" name="config[field_state_visible]" value="1" <?php echo $checked; ?> />
								</td>
								<td align="center">
									<?php
									$checked = $this->row->get('field_state_required') != 0 ? 'checked="checked"' : '';
									?>
									<input type="checkbox" class="inputbox" name="config[field_state_required]" value="1" <?php echo $checked; ?> />
								</td>
								<td align="center">
									<input type="text" class="text_area" name="config[field_state_js]" value="<?php echo $this->row->get('field_state_js'); ?>" size="25" />
								</td>
								<td align="center">
									<input type="text" class="text_area" name="config[field_state_cb]" value="<?php echo $this->row->get('field_state_cb'); ?>" size="25" />
								</td>
							</tr>
							<tr>
								<td class="zj_key">
									<span class="editlinktip hasTip" title="">
										<?php echo JText::_('Zip Code'); ?>
									</span>
								</td>
								<td align="center">
									<?php
									$checked = $this->row->get('field_zip_visible') != 0 ? 'checked="checked"' : '';
									?>
									<input type="checkbox" class="inputbox" name="config[field_zip_visible]" value="1" <?php echo $checked; ?> />
								</td>
								<td align="center">
									<?php
									$checked = $this->row->get('field_zip_required') != 0 ? 'checked="checked"' : '';
									?>
									<input type="checkbox" class="inputbox" name="config[field_zip_required]" value="1" <?php echo $checked; ?> />
								</td>
								<td align="center">
									<input type="text" class="text_area" name="config[field_zip_js]" value="<?php echo $this->row->get('field_zip_js'); ?>" size="25" />
								</td>
								<td align="center">
									<input type="text" class="text_area" name="config[field_zip_cb]" value="<?php echo $this->row->get('field_zip_cb'); ?>" size="25" />
								</td>
							</tr>
							<tr>
								<td class="zj_key">
									<span class="editlinktip hasTip" title="">
										<?php echo JText::_('Country'); ?>
									</span>
								</td>
								<td align="center">
									<?php
									$checked = $this->row->get('field_country_visible') != 0 ? 'checked="checked"' : '';
									?>
									<input type="checkbox" class="inputbox" name="config[field_country_visible]" value="1" <?php echo $checked; ?> />
								</td>
								<td align="center">
									<?php
									$checked = $this->row->get('field_country_required') != 0 ? 'checked="checked"' : '';
									?>
									<input type="checkbox" class="inputbox" name="config[field_country_required]" value="1" <?php echo $checked; ?> />
								</td>
								<td align="center">
									<input type="text" class="text_area" name="config[field_country_js]" value="<?php echo $this->row->get('field_country_js'); ?>" size="25" />
								</td>
								<td align="center">
									<input type="text" class="text_area" name="config[field_country_cb]" value="<?php echo $this->row->get('field_country_cb'); ?>" size="25" />
								</td>
							</tr>
							<tr>
								<td class="zj_key">
									<span class="editlinktip hasTip" title="">
										<?php echo JText::_('Phone'); ?>
									</span>
								</td>
								<td align="center">
									<?php
									$checked = $this->row->get('field_phone_visible')!= 0 ? 'checked="checked"' : '';
									?>
									<input type="checkbox" class="inputbox" name="config[field_phone_visible]" value="1" <?php echo $checked; ?> />
								</td>
								<td align="center">
									<?php
									$checked = $this->row->get('field_phone_required') != 0 ? 'checked="checked"' : '';
									?>
									<input type="checkbox" class="inputbox" name="config[field_phone_required]" value="1" <?php echo $checked; ?> />
								</td>
								<td align="center">
									<input type="text" class="text_area" name="config[field_phone_js]" value="<?php echo $this->row->get('field_phone_js'); ?>" size="25" />
								</td>
								<td align="center">
									<input type="text" class="text_area" name="config[field_phone_cb]" value="<?php echo $this->row->get('field_phone_cb'); ?>" size="25" />
								</td>
							</tr>
						</tbody>
					</table>
				</fieldset>
			</td>
			<td>
				<fieldset class="adminform">
					<legend><?php echo JText::_('Integration Configuration'); ?></legend>
					<table class="admintable" cellspacing="1">
						<tr>
							<td class="key" width="185">
								<span class="editlinktip hasTip" title="">
									<?php echo JText::_('Community'); ?>
								</span>
							</td>
							<td>
								<?php echo $this->lists['community_components']; ?>
							</td>
						</tr>
					</table>
				</fieldset>
				<fieldset class="adminform">
					<legend><?php echo JText::_('Permission Configuration'); ?></legend>
					<table class="admintable">
						<tr>
							<td class="key">
								<span class="editlinktip hasTip" title="">
									<?php echo JText::_('Donate'); ?>
								</span>
							</td>
							<td>
								<?php echo $this->lists['donate_user_types']; ?>
							</td>
						</tr>
					</table>
				</fieldset>
				<fieldset class="adminform">
					<legend><?php echo JText::_('Jomsocial Wall Integration'); ?></legend>
					<table class="admintable">
						<tr>
							<td class="key">
								<span class="editlinktip hasTip" title="<?php echo JText::_('Automatically post on user jomsocial wall when the user donate');?>">
									<?php echo JText::_('Donate'); ?>
								</span>
							</td>
							<td>
								<?php
								echo JHTML::_('select.booleanlist', 'config[js_donate]', 'class="inputbox"', $this->row->get('js_donate'));
								?>
							</td>
						</tr>						
					</table>
				</fieldset>
			</td>
		</tr>
	</table>
</div>