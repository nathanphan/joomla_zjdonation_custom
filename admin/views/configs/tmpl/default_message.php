<?php
/**
 * @version		$Id$
 * @author		Joomseller
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 * This file may not be redistributed in whole or significant part.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
?>

<div id="page-message">
	<table class="noshow">
		<tr>
			<td width="100%">
				<fieldset class="adminform">
					<legend><?php echo JText::_('Thank You Message Page'); ?></legend>
					<table class="admintable" cellspacing="1" width="100%">
						<tr>
							<td class="key" valign="top">
								<?php echo JText::_('Use URL'); ?>
							</td>
							<td>
								<?php
								echo JHTML::_('select.booleanlist', 'config[page_thanks_type]', '', $this->row->get('page_thanks_url'));
								?>
							</td>
						</tr>
						<tr>
							<td class="key" valign="top">
								<?php echo JText::_('Message URL'); ?>
							</td>
							<td>
								<input type="text" class="text_area" name="config[page_thanks_url]" id="page_thanks_url" value="<?php echo $this->row->get('page_thanks_url'); ?>" size="100" />
							</td>
						</tr>
						<tr>
							<td class="key" valign="top">
								<?php echo JText::_('Message Title'); ?>
							</td>
							<td>
								<input type="text" class="text_area" name="config[page_thanks_title]" id="page_thanks_title" value="<?php echo $this->row->get('page_thanks_title'); ?>" size="100" />
							</td>
						</tr>
						<tr>
							<td class="key" valign="top">
								<?php echo JText::_('Message Content'); ?>
							</td>
							<td>
								<?php
								echo $this->editor->display('config[page_thanks_msg]', $this->row->get('page_thanks_msg'), '100%', '300', '50', '20', array('pagebreak', 'readmore'));
								/*
								<textarea class="text_area" name="config[page_thanks_msg]" id="page_thanks_msg" rows="8" cols="70"><?php echo $this->row->get('page_thanks_msg'); ?></textarea>
								*/
								?>
							</td>
						</tr>
					</table>
				</fieldset>
				<fieldset class="adminform">
					<legend><?php echo JText::_('Cancel Message Page'); ?></legend>
					<table class="admintable" cellspacing="1" width="100%">
						<tr>
							<td class="key" valign="top">
								<?php echo JText::_('Use URL'); ?>
							</td>
							<td>
								<?php
								echo JHTML::_('select.booleanlist', 'config[page_cancel_type]', '', $this->row->get('page_cancel_type'));
								?>
							</td>
						</tr>
						<tr>
							<td class="key" valign="top">
								<?php echo JText::_('Message URL'); ?>
							</td>
							<td>
								<input type="text" class="text_area" name="config[page_cancel_url]" id="page_cancel_url" value="<?php echo $this->row->get('page_cancel_url'); ?>" size="100" />
							</td>
						</tr>
						<tr>
							<td class="key" valign="top">
								<?php echo JText::_('Message Title'); ?>
							</td>
							<td>
								<input type="text" class="text_area" name="config[page_cancel_title]" id="page_cancel_title" value="<?php echo $this->row->get('page_cancel_title'); ?>" size="100" />
							</td>
						</tr>
						<tr>
							<td class="key" valign="top">
								<?php echo JText::_('Message Content'); ?>
							</td>
							<td>
								<?php
								echo $this->editor->display('config[page_cancel_msg]', $this->row->get('page_cancel_msg'), '100%', '300', '50', '20', array('pagebreak', 'readmore'));
								?>
							</td>
						</tr>
					</table>
				</fieldset>
				<?php
				/*
				<fieldset class="adminform">
					<legend><?php echo JText::_('Pay Later Page Message'); ?></legend>
					<table class="admintable" cellspacing="1" width="100%">
						<tr>
							<td class="key" valign="top">
								<?php echo JText::_('Use URL'); ?>
							</td>
							<td>
								<?php
								echo JHTML::_('select.booleanlist', 'config[page_paylater_thanks_type]', '', $this->row->get('page_paylater_thanks_type'));
								?>
							</td>
						</tr>
						<tr>
							<td class="key" valign="top">
								<?php echo JText::_('Message URL'); ?>
							</td>
							<td>
								<input type="text" class="text_area" name="config[page_paylater_thanks_url]" id="page_paylater_thanks_url" value="<?php echo $this->row->get('page_paylater_thanks_url'); ?>" size="100" />
							</td>
						</tr>
						<tr>
							<td class="key" valign="top">
								<?php echo JText::_('Message Title'); ?>
							</td>
							<td>
								<input type="text" class="text_area" name="config[page_paylater_thanks_title]" id="page_paylater_thanks_title" value="<?php echo $this->row->get('page_paylater_thanks_title'); ?>" size="100" />
							</td>
						</tr>
						<tr>
							<td class="key" valign="top">
								<?php echo JText::_('Message Content'); ?>
							</td>
							<td>
								<?php
								echo $this->editor->display('config[page_paylater_thanks_msg]', $this->row->get('page_paylater_thanks_msg'), '100%', '300', '50', '20', array('pagebreak', 'readmore'));
								?>
							</td>
						</tr>
					</table>
				</fieldset>
				*/
				?>
			</td>
		</tr>
	</table>
</div>