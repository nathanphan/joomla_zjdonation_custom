<?php
/**
 * @version		$Id$
 * @author		Joomseller
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 * This file may not be redistributed in whole or significant part.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
?>

<script type="text/javascript">
	function submitbutton(task) {
		var form = document.adminForm;

		if (task == 'cancel') {
			submitform(task);
		} else {
			submitform(task);
		}
	}

</script>

<form action="index.php" method="post" name="adminForm" id="adminForm">
	<div id="config-document">
		<?php
		echo $this->loadTemplate('general');
		echo $this->loadTemplate('email');
		echo $this->loadTemplate('message');
		echo $this->loadTemplate('permission');
		?>
	</div>

	<input type="hidden" name="option" value="com_zj_donation" />
	<input type="hidden" name="controller" value="configs" />
	<input type="hidden" name="task" value="" />
	<?php echo JHTML::_('form.token'); ?>
</form>

<?php
echo ZJ_DonationFactory::getFooter();