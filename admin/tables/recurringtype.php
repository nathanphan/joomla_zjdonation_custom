<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * ZJ_Donation Component - Recurring Type Table
 * @package		ZJ_Donation
 * @subpackage	Table
 */
class TableRecurringType extends JTable {
	/** @var int Primary key */
	var $id					= null;
	/** @var string */
	var $title				= null;
	/** @var string */
	var $description		= null;
	/** @var string */
	var $type				= null;
	/** @var int */
	var $value				= null;
	/** @var boolean */
	var $published			= null;
	/** @var int */
	var $ordering			= null;
	/** @var int */
	var $checked_out		= null;
	/** @var time */
	var $checked_out_time	= null;
	
	/**
	 * @param $db A database connector object
	 */
	function __construct(&$db) {
		parent::__construct('#__zj_donation_recurring_types', 'id', $db);
	}
	
	/**
	 * Binds an array to the object.
	 */
	function bind($array, $ignore = '') {
		$result = parent::bind($array);
		// cast properties
		$this->id		= (int) $this->id;
		
		return $result;
	}
	
	/**
	 * Overloaded check function.
	 */
	function check() {
		// check for valid name
		if (trim($this->title) == '') {
			$this->setError(JText::_('Recurring must contain the title.'));
			return false;
		}
		
		return parent::check();
	}
}
