<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * ZJ_Donation Component - Field Type Table
 * @package		ZJ_Donation
 * @subpackage	Table
 */
class TableField extends JTable {
	/** @var int Primary key */
	var $id					= null;
	/** @var string */
	var $name				= null;
	/** @var string */
	var $title				= null;
	/** @var text */
	var $description		= null;
	/** @var int */
	var $field_type			= null;
	/** @var boolean */
	var $required			= null;
	/** @var text */
	var $values				= null;
	/** @var text */
	var $default_values		= null;
	/** @var int */
	var $rows				= null;
	/** @var int */
	var $cols				= null;
	/** @var int */
	var $size				= null;
	/** @var string */
	var $css_class			= null;
	/** @var int */
	var $ordering			= null;
	/** @var boolean */
	var $published			= null;
	/** @var int */
	var $core				= null;
	/** @var int */
	var $checked_out		= null;
	/** @var time */
	var $checked_out_time	= null;
	
	
	/**
	 * @param $db A database connector object
	 * 
	 */
	function __construct(&$db) {
		parent::__construct('#__zj_donation_fields', 'id', $db);
	}
	
	
	/**
	 * Binds an array to the object
	 * 
	 * @access	public
	 * @since	1.0
	 */
	function bind($array, $ignore = '') {
		$result = parent::bind($array);
		// cast properties
		$this->id			= (int) $this->id;
		
		return $result;
	}
	
	/**
	 * Overloaded check function
	 * 
	 * @access	public
	 * @since	1.0
	 */
	function check() {
		if (trim($this->name) == '' || trim($this->title) == '') {
				
			return false;
		}
		return parent::check();
	}
}
?>