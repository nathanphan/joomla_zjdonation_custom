<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * ZJ_Donation Component - Configs Table
 * @package		ZJ_Donation
 * @subpackage	Table
 */
class TableConfigs extends JTable {
	/** @var int Primary key */
	var $id					= null;
	/** @var int */
	var $key				= null;
	/** @var string */
	var $value				= null;
	
	// TODO: add component settings here...
	
	
	/**
	 * @param $db A database connector object.
	 */
	function __construct(&$db) {
		parent::__construct('#__zj_donation_configs', 'id', $db);
	}
	
	/**
	 * Binds an array to the object.
	 * @access	public
	 * @since	1.0
	 */
	function bind($array, $ignore = '') {
		$result = parent::bind($array);
		// cast properties
		$this->id			= 1;
		
		return $result;
	}
}
