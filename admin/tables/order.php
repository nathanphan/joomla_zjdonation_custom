<?php

/**
 * @package		order.php
 * @author 		Nathan
 * @copyright 	2012 NathanPhan	.
 * @license 	GNU/GPL v2 http://www.gnu.org/licenses/gpl-2.0.html
 *
 */

class TableOrder extends JTable {

	function __construct(&$db) {
		parent::__construct('#__zj_donation_order', 'id', $db);

		//Hack to make this act a little like active record
		$this->_db->setQuery('SHOW columns FROM ' . $this->_tbl);
		foreach ($this->_db->loadObjectList() as $k => $column) {
			$field = $column->Field;
			$this->$field = '';
		}
	}
}