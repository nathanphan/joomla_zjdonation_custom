<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * ZJ_Donation Component - Donate Table
 * @package		ZJ_Donation
 * @subpackage	Table
 */
class TableDonate extends JTable {
	/** @var int Primary key */
	var $id					= null;
	/** @var int */
	var $campaign_id		= null;
	/** @var string */
	var $first_name			= null;
	/** @var string */
	var $last_name			= null;
	/** @var string */
	var $organization		= null;
	/** @var string */
	var $website			= null;
	/** @var string */
	var $address			= null;
	/** @var string */
	var $city				= null;
	/** @var string */
	var $state				= null;
	/** @var string */
	var $zip				= null;
	/** @var string */
	var $country			= null;
	/** @var string */
	var $phone				= null;
	/** @var string */
	var $fax				= null;
	/** @var string */
	var $email				= null;
	/** @var string */
	var $comment			= null;
	/** @var datetime */
	var $created_date		= null;
	/** @var datetime */
	var $payment_date		= null;
	/** @var string */
	var $payment_method		= null;
	/** @var string */
	var $token				= null;
	/** @var string */
	var $transaction_id		= null;
	/** @var int */
	var $user_id			= null;
	/** @var int */
	var $receive_id			= null;
	/** @var float */
	var $amount				= null;
	/** @var boolean */
	var $published			= null;
	/** @var boolean */
	var $recurring			= null;
	/** @var int */
	var $recurring_id		= null;
	/** @var int */
	var $recurring_times	= null;
	/** @var boolean */
	var $donated			= null;
	/** @var int */
	var $donated_times		= null;
	/** @var string */
	var $ip_addess			= null;
	/** @var string */
	var $subscribe_id		= null;
	/** @var int */
	var $access				= null;
	/** @var int */
	var $order_id				= null;
	

	/**
	 * @param $db A database connector object.
	 */
	function __construct(&$db) {
		parent::__construct('#__zj_donation_donates', 'id', $db);
	}

	/**
	 * Binds an array to the object.
	 */
	function bind($array, $ignore = '') {
		$result = parent::bind($array);
		// cast properties
		$this->id			= (int) $this->id;
		$this->campaign_id	= (int) $this->campaign_id;
		
		return $result;
	}

	/**
	 * Overloaded check function.
	 */
	function check() {
		
		// check for valid name
		if (trim($this->first_name) == '' || trim($this->last_name) == '') {
			$this->setError(JText::_('Donate must contain the user name.'));
			return false;
		}

		// check for valid campaign
		if ($this->campaign_id == 0) {
			$this->setError(JText::_('Please select a campaign'));
			return false;
		}

		return parent::check();
	}
}
?>