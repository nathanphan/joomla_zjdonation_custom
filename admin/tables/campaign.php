<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * ZJ_Donation Component - Campaign Table
 * @package		ZJ_Donation
 * @subpackage	Table
 */
class TableCampaign extends JTable {
	/** @var int Primary key */
	var $id					= null;
	/** @var string */
	var $title				= null;
	/** @var string */
	var $alias				= null;
	/** @var string */
	var $description		= null;
	/** @var string */
	var $addition			= null;
	/** @var string */
	var $amounts			= null;
	/** @var country id */
	var $country_id 		= null;
	/** @var float */
	var $goal				= null;
	/** @var boolean */
	var $custom_amount		= null;
	/** @var datetime */
	var $published_up		= null;
	/** @var datetime */
	var $published_down		= null;
	/** @var string	*/
	var $messages			= null;
	/** @var string	*/
	var $fields				= null;
	/** @var string	*/
	var $image				= null;
	/** @var string	*/
	var $gallery			= null;
	/** @var boolean */
	var $recurring			= null;
	/** @var string */
	var $payments			= null;
	/** @var boolean */
	var $feature			= null;
	/** @var int */
	var $created_by			= null;
	/** @var time */
	var $created_date		= null;
	/** @var boolean */
	var $published			= null;
	/** @var int */
	var $ordering			= null;
	/** @var string */
	var $meta_keywords		= null;
	/** @var string */
	var $meta_description 	= null;
	/** @var int */
	var $checked_out		= null;
	/** @var time */
	var $checked_out_time	= null;
	
	/**
	 * @param $db A database connector object
	 */
	function __construct(&$db) {
		parent::__construct('#__zj_donation_campaigns', 'id', $db);
	}
	
	/**
	 * Binds an array to the object.
	 */
	function bind($array, $ignore = '') {
		$result = parent::bind($array);
		// cast properties
		$this->id		= (int) $this->id;
		
		return $result;
	}
	
	/**
	 * Overloaded check function.
	 */
	function check() {
		// check for valid name
		if (trim($this->title) == '') {
			$this->setError(JText::_('Campaign must contain the title.'));
			return false;
		}
		// alias
		if (empty($this->alias) || $this->alias == $alias) {
			$this->alias = JFilterOutput::stringURLSafe($this->title);
		}
		
		return parent::check();
	}
}
