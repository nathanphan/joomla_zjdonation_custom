<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

// add general stylesheet and javascript
global $mainframe;
$admin_live_site	= JURI::base(true) . '/';
$live_slite			= $mainframe->getSiteURL();
$document			= &JFactory::getDocument();

$document->addStyleSheet($admin_live_site . '/components/com_zj_donation/assets/css/zj_donation.css');
$document->addScript($admin_live_site . '/components/com_zj_donation/assets/js/zj_donation.js');
$document->addScriptDeclaration("var zj_admin_live_site = '$admin_live_site';");
$document->addScriptDeclaration("var zj_live_site='$live_slite';");

// require libraries
JHTML::addIncludePath(JPATH_COMPONENT.DS.'helpers');
JTable::addIncludePath(JPATH_COMPONENT.DS.'tables');

require_once(JPATH_COMPONENT.DS.'libraries'.DS.'factory.php');
require_once(JPATH_COMPONENT.DS.'libraries'.DS.'image.php');

// require the base controller
require_once(JPATH_COMPONENT.DS.'controller.php');

// require the specific controller
$controller = JRequest::getCmd('controller', JRequest::getCmd('view'));
if($controller) {
	jimport('joomla.filesystem.file');

	$path = JPATH_COMPONENT.DS.'controllers'.DS.$controller.'.php';
	if(JFile::exists($path)) {
		require_once($path);
	} else {
		$controller = '';
	}
}
// create the controller
$classname = 'ZJ_DonationController' . ucfirst($controller);
$controller = new $classname();

// perform the request task
$controller->execute(JRequest::getVar('task', null, 'default', 'cmd'));

// redirect if set by the controller
$controller->redirect();
