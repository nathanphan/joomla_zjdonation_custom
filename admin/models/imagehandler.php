<?php
/**
 * @version		$Id$
 * @author		Joomseller
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 * This file may not be redistributed in whole or significant part.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

/**
 * ZJ_Donation Component - Image Handler Model
 * @package		ZJ_Donation
 * @subpackage	Model
 * @since		1.0
 */
class ZJ_DonationModelImageHandler extends JModel {
	/** @var array Image array */
	var $_data			= null;
	/** @var object Pagination object */
	var $_pagination	= null;
	
	/**
	 * Constructor.
	 */
	function __construct() {
		global $mainframe, $option;
		
		parent::__construct();
		
		$limit		= $mainframe->getUserStateFromRequest('global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
		$limitstart	= $mainframe->getUserStateFromRequest($option . '.imagehandler.limitstart', 'limitstart', 0, 'int');
		$search		= $mainframe->getUserStateFromRequest($option . '.imagehandler.search', 'search', '', 'string');
		$search		= JString::trim(JString::strtolower($search));
		
		$this->setState('limit', $limit);
		$this->setState('limitstart', $limitstart);
		$this->setState('search', $search);
	}
	
	/**
	 * Method to get state of the model.
	 * @access	public
	 * @since	1.0
	 */
	function getState($property = null) {
		static $set;
		
		if (!$set) {
			$folder = JRequest::getWord('folder');
			$this->setState('folder', $folder);
			
			$set = true;
		}
		
		return parent::getState($property);
	}
	
	/**
	 * Method to get images item data.
	 * @access	public
	 * @return	array
	 */
	function getData() {
		$list = $this->_getList();
		$listimg = array();
		
		$limit = $this->getState('limit');
		if ($limit == 0) {
			$limit = $this->getState('total');
			$s = 1;
		} else {
			$s = $this->getState('limitstart') + 1;
		}
		
		for ($i = ($s - 1), $n = $s + $limit, $total = $this->getState('total'); $i < $n; $i++) {
			if ($i + 1 <= $total) {
				$list[$i]->size = $this->_parseSize(filesize($list[$i]->path));
				
				$info = @getimagesize($list[$i]->path);
				$list[$i]->width	= @$info[0];
				$list[$i]->height	= @$info[1];
				
				$dimensions = ZJ_DonationImage::imageResize($list[$i]->width, $list[$i]->height, 60, 60);
				$list[$i]->width_60 = $dimensions[0];
				$list[$i]->height_60 = $dimensions[1];
				
				$listimg[] = $list[$i];
			}
		}
		
		return $listimg;
	}
	
	/**
	 * Method to get a pagination object for the images.
	 * @access	public
	 * @return	integer
	 */
	function getPagination() {
		if (empty($this->_pagination)) {
			jimport('joomla.html.pagination');
			$this->_pagination = new JPagination($this->getState('total'), $this->getState('limitstart'), $this->getState('limit'));
		}
		
		return $this->_pagination;
	}
	
	/**
	 * Method to get images from folder.
	 * @access	public
	 * @return	mixed
	 */
	function _getList() {
		static $list;
		
		// only process the list once par request
		if (is_array($list)) {
			return $list;
		}
		
		// get folder from request
		$folder = $this->getState('folder');
		$search = $this->getState('search');
		
		// initialize variables
		$base_path = JPATH_SITE.DS.'media'.DS.'zj_donation'.DS.$folder;
		
		$images = array();
		
		// get the list of files and folders from the given folder
		$file_list = JFolder::files($base_path, '^(.)*((\.jpg)|(\.gif)|(\.png))$');
		
		if ($file_list !== false) {
			foreach ($file_list as $file) {
				if (is_file($base_path.DS.$file) && substr($file, 0, 1) != '.') {
					if ($search == '') {
						$tmp = new JObject();
						$tmp->name = $file;
						$tmp->path = JPath::clean(($base_path.DS.$file));
						
						$images[] = $tmp;
					} else if (stristr($file, $search)) {
						$tmp = new JObject();
						$tmp->name = $file;
						$tmp->path = JPath::clean(($base_path.DS.$file));
						
						$images[] = $tmp;
					}
				}
			}
		}
		
		$list = $images;
		$this->setState('total', count($list));
		
		return $list;
	}
	
	/**
	 * Method to print size of an image
	 * 
	 * @access	private
	 * @return	string
	 */
	function _parseSize($size) {
		if ($size < 1024) {
			
			return $size . ' bytes';
		} else {
			if ($size > 1024 && $size < 1024 * 1024) {
				return sprintf('%01.2f', $size / 1024.0) . ' Kb';
			} else {
				return sprintf('%01.2f', $size / (1024.0 * 1024)) . ' Mb';
			}
		}
	}
}
