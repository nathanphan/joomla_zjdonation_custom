<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

/**
 * ZJ_Donation Component - Field Model.
 * @package		ZJ_Donation
 * @subpackage	Model
 */
class ZJ_DonationModelField extends JModel {
	/** @var int */
	var $_id				= null;
	/** @var array answer data array */
	var $_data				= null;
	
	
	/**
	 * Constructor
	 *
	 */
	function __construct() {
		parent::__construct();
		
		$array = JRequest::getVar('cid', array(0), '', 'array');
		$this->setId((int)$array[0]);
	}
	
	/**
	 * Method to set the identifier
	 *
	 * @access	public
	 * @param	int $id field identifier
	 */
	function setId($id) {
		$this->_id		= $id;
		$this->_data	= null;
	}
	
	/**
	 * Logic for the field edit screen
	 *
	 * @access public
	 * @return array
	 */
	function getData() {
		if ($this->_loadData()) {
			
		} else {
			$this->_initData();
		}
		
		return $this->_data;
	}
	
	/**
	 * Tests if the field is checked out
	 *
	 * @access public
	 * @param int $uid A user id
	 * @return boolean True if checked out
	 */
	function isCheckedOut($uid = 0) {
		if ($this->_loadData()) {
			if ($uid) {
				return ($this->_data->checked_out && $this->_data->checked_out != $uid);
			} else {
				return $this->_data->checked_out;
			}
		}
		
		return false;
	}
	
	/**
	 * Method to checkin/unlock the item
	 *
	 * @access public
	 * @return boolean True on success
	 */
	function checkin() {
		if ($this->_id) {
			$row = $this->getTable();
			if (!$row->checkin($this->_id)) {
				$this->setError($this->_db->getErrorMsg());
				return false;
			}
			
			return true;
		}
		
		return false;
	}
	
	
	/**
	 * Method to checkout/lock the item
	 *
	 * @access public
	 * @param int $uid User ID of the user checking the item out
	 * @return boolean True on success
	 */
	function checkout($uid = null) {
		if ($this->_id) {
			if (is_null($uid)) {
				$user	=& JFactory::getUser();
				$uid	= $user->get('id');
			}
			
			$row =& $this->getTable();
			if (!$row->checkout($uid, $this->_id)) {
				$this->setError($this->_db->getErrorMsg());
				return false;
			}
			
			return true;
		}
		
		return false;
	}
	
	/**
	 * Method to store the field
	 *
	 * @access public
	 * @param $data
	 * @return boolean True on success
	 */
	function store($data) {
		$row =& $this->getTable();
		
        // bind the form fields to the version table
		if (!$row->bind($data)) {
			$this->setError($this->_db->getErrorMsg());
			
			return false;
		}
		
		// if new item, order last in appropriate group
		if (!$row->id) {
			//$date = &JFactory::getDate();
			//$row->date_created	= $date->toMySQL();
			$row->ordering		= $row->getNextOrder();
		}
		
		// make sure the category is valid
		if (!$row->check()) {
			$this->setError($this->_db->getErrorMsg());

			return false;
		}
		
		// store the category table to the database
		if (!$row->store()) {
			$this->setError($this->_db->getErrorMsg());
			
			return false;
		}
		
		return $row;
	}
	
	
	/**
	 * Method to remove a field
	 *
	 * @access public
	 * @param $cid
	 * @return boolean	True on success
	 */
	function delete($cid = array()) {
		if (count( $cid ))
		{
			$cids =  implode(',', $cid);
			//Delete data from field values table
			$sql = 'DELETE FROM #__zj_donation_field_values WHERE field_id IN ('.$cids.')';
			$this->_db->setQuery($sql);
			if (!$this->_db->query())
				return false ;
			$sql = 'DELETE FROM #__zj_donation_fields WHERE id IN ('.$cids.')';
			$this->_db->setQuery($sql);
			if (!$this->_db->query())
				return false;			
		}
		return true;
	}
	
	
	/**
	 * Method to (un)publish a field
	 *
	 * @access	public
	 * @return	boolean	True on success
	 */
	function publish($cid = array(), $publish = 1) {
		$user	=& JFactory::getUser();
		
		if (count($cid)) {
			JArrayHelper::toInteger($cid);
			$cids = implode(', ', $cid);
			
			$query = 'UPDATE #__zj_donation_fields'
				. ' SET published = ' . (int) $publish
				. ' WHERE id IN (' . $cids . ')'
				. ' AND (checked_out = 0 OR (checked_out = ' . (int) $user->get('id') . ' ))'
			;
			$this->_db->setQuery($query);
			if (!$this->_db->query()) {
				$this->setError($this->_db->getErrorMsg());
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Method to (un)required a field
	 *
	 * @access	public
	 * @return	boolean	True on success
	 */
	function required($cid = array(), $required = 1) {
		$user	=& JFactory::getUser();
		
		if (count($cid)) {
			JArrayHelper::toInteger($cid);
			$cids = implode(', ', $cid);
			
			$query = 'UPDATE #__zj_donation_fields'
				. ' SET required = ' . (int) $required
				. ' WHERE id IN (' . $cids . ')'
				. ' AND (checked_out = 0 OR (checked_out = ' . (int) $user->get('id') . ' ))'
			;
			$this->_db->setQuery($query);
			if (!$this->_db->query()) {
				$this->setError($this->_db->getErrorMsg());
				return false;
			}
		}
		
		return true;
	}
    
	
	
	/**
	 * Method to move a field
	 *
	 * @access	public
	 * @return	boolean	True on success
	 */
	function move($direction) {
		$row =& $this->getTable();
		if (!$row->load($this->_id)) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}
		
		if (!$row->move($direction)) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}
		
		return true;
	}
	
	
	/**
	 * Method to saveorder a field
	 *
	 * @access	public
	 * @return	boolean	True on success
	 */
	function saveorder($cid = array(), $order) {
		$row =& $this->getTable();
		
		// update ordering values
		for ($i = 0, $n = count($cid); $i < $n; $i++) {
			$row->load((int) $cid[$i]);
			
			if ($row->ordering != $order[$i]) {
				$row->ordering = $order[$i];
				if (!$row->store()) {
					$this->setError($this->_db->getErrorMsg());
					return false;
				}
			}
		}
		
		return true;
	}
	
	/**
	 * Method to load field data
	 *
	 * @access private
	 * @return boolean True on success
	 */
	function _loadData() {
		// let load the content if it doesn't already exist
		if (empty($this->_data)) {
			$query = 'SELECT a.*'
				. ' FROM #__zj_donation_fields AS a'
				. ' WHERE a.id = ' . $this->_id
			;
			$this->_db->setQuery($query);
			$this->_data = $this->_db->loadObject();
			
			return (boolean) $this->_data;
		}
		
		return true;
	}
	
	/**
	 * Method to initialise the field data
	 *
	 * @access private
	 * @return boonlean True on success
	 */
	function _initData() {			
		// let load the content if it doesn't already exist
		if (empty($this->_data)) {
			$row	= new stdClass();
			
			$row->id				= null;
			$row->name				= null;
			$row->title				= null;
			$row->field_type		= null;
			$row->description		= null;				
			$row->required			= null;
			$row->values			= null;
			$row->default_values	= null;
			$row->rows				= null;
			$row->cols				= null;
			$row->size				= 25;
			$row->css_class			= 'inputbox';				
			$row->ordering			= 0;				
			$row->published			= 1;
			$row->core				= 0;
			$row->checked_out		= 0;
			$row->checked_out_time	= null;
			
			$this->_data = $row;
			
			return (boolean) $this->_data;
		}	
		
		return true;
	}
}