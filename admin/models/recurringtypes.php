<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

/**
 * ZJ_Donation Component - Recurring Types Model.
 * @package		ZJ_Donation
 * @subpackage	Model
 */
class ZJ_DonationModelRecurringTypes extends JModel {
	/** @var array Recurring Types array */
	var $_data			= null;
	/** @var int Total of Recurring Types */
	var $_total			= null;
	/** @var object Pagination object */
	var $_pagination	= null;
	
	/**
	 * Constructor.
	 */
	function __construct() {
		global $mainframe, $option;

		// call parent constructor
		parent::__construct();

		// get the pagination request variables
		$limit		= $mainframe->getUserStateFromRequest('global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
		$limitstart = $mainframe->getUserStateFromRequest($option . '.recurringtypes.limitstart', 'limitstart', 0, 'int');

		// in case limit has been changed, adjust limitstart accordingly
		$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
		
		$this->setState('limit', $limit);
		$this->setState('limitstart', $limitstart);
	}

	/****************************************************************
	 * GET DATA FOR PRESENTATION.
	 ***************************************************************/

	/**
	 * Get recurringtypes data.
	 */
	function getData() {
		if (empty($this->_data)) {
			$query			= $this->_buildQuery();
			$this->_data	= $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));
		}

		return $this->_data;
	}

	/**
	 * Get recurring types data.
	 */
	function getTotal() {
		if (empty($this->_total)) {
			$query			= $this->_buildQuery();
			$this->_total	= $this->_getListCount($query);
		}

		return $this->_total;
	}

	/**
	 * Get pagination object.
	 */
	function getPagination() {
		if (empty($this->_pagination)) {
			jimport('joomla.html.pagination');
			$this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit'));
		}

		return $this->_pagination;
	}
	
	/**
	 * Build query string.
	 */
	function _buildQuery() {
		// get the WHERE and ORDER BY clauses for the query
		$where		= $this->_buildQueryWhere();
		$orderby	= $this->_buildQueryOrderBy();

		$query = 'SELECT a.*'
			. ' FROM #__zj_donation_recurring_types AS a'
			. $where
			. $orderby
		;

		return $query;
	}

	/**
	 * Build query where string.
	 */
	function _buildQueryWhere() {
		global $mainframe, $option;

		$db				= $this->_db;
		$filter_state	= $mainframe->getUserStateFromRequest($option . '.recurringtypes.filter_state', 'filter_state', '', 'word');
		$search			= $mainframe->getUserStateFromRequest($option . '.recurringtypes.search', 'search', '', 'string');
		$search			= JString::trim(JString::strtolower($search));

		$where = array();

		// filter by state of recurring types
		if ($filter_state) {
			if ($filter_state == 'P') {
				$where[] = 'a.published = 1';
			} else if ($filter_state == 'U') {
				$where[] = 'a.published = 0';
			}
		}

		// filter by the title of recurring types
		if ($search) {
			$where[] = 'LOWER(a.title) LIKE ' . $db->Quote('%' . $db->getEscaped($search, true) . '%', false);
		}

		// build the WHERE clause
		$where = count($where) ? ' WHERE ' . implode(' AND ', $where) : '';

		return $where;
	}

	/**
	 * Build query orderby string.
	 */
	function _buildQueryOrderBy() {
		global $mainframe, $option;

		$filter_order		= $mainframe->getUserStateFromRequest($option . 'recurringtypes.filter_order', 'filter_order', 'a.ordering', 'cmd');
		$filter_order_Dir	= $mainframe->getUserStateFromRequest($option . 'recurringtypes.filter_order_Dir', 'filter_order_Dir', '', 'word');

		if ($filter_order == 'a.ordering') {
			$orderby = ' ORDER BY a.ordering ' . $filter_order_Dir;
		} else {
			$orderby = ' ORDER BY ' . $filter_order . ' ' . $filter_order_Dir . ', a.ordering ';
		}

		return $orderby;
	}
	
	/****************************************************************
	 * MANIPULATE DATA.
	 ***************************************************************/

	/**
	 * Overloaded getTable method.
	 */
	function getTable($name = 'recurringtype', $prefix = 'Table', $options = array()) {
		return parent::getTable($name, $prefix, $optins);
	}

	/**
	 * (Un)Publish recurring types.
	 */
    function publish($cid = array(), $publish = 1) {
		$user			= &JFactory::getUser();
		$affected_rows	= 0;

		if (count($cid)) {
			JArrayHelper::toInteger($cid);
			$cids = implode(', ', $cid);

			$query = 'UPDATE #__zj_donation_recurring_types'
				. ' SET published = ' . (int) $publish
				. ' WHERE id IN(' . $cids . ')'
				. ' AND(checked_out = 0 OR (checked_out = ' . (int) $user->get('id') . '))'
			;
			$this->_db->setQuery($query);

			if (!$this->_db->query()) {
				$this->setError($this->_db->getErrorMsg());
				return false;
			} else {
				$affected_rows = $this->_db->getAffectedRows();
			}
		}

		return $affected_rows;
	}

	/**
	 * Remove recurring types.
	 */
	function delete($cid = array()) {
		$affected_rows = 0;

		if (count($cid)) {
			JArrayHelper::toInteger($cid);
			$cids = implode(', ', $cid);

			$query = 'DELETE FROM #__zj_donation_recurring_types'
				. ' WHERE id IN (' . $cids . ')'
			;
			$this->_db->setQuery($query);

			if (!$this->_db->query()) {
				$this->setError($this->_db->getErrorMsg());
				return false;
			} else {
				$affected_rows = $this->_db->getAffectedRows();
			}
		}

		return $affected_rows;
	}

	/**
	 * Move a recurring type up/down.
	 */
	function move($direction) {
		 $cid	= JRequest::getVar('cid', array(0), 'post', 'array');
		 $id	= $cid[0];
		 $row	= &$this->getTable();

		 if (!$row->load($id)) {
			 $this->setError($this->_db->getErrorMsg());
			 return false;
		 }

		 if (!$row->move($direction)) {
			 $this->setError($this->_db->getErrorMsg());
			 return false;
		 }

		 return true;
	}

	/**
	 * Re-order recurring types.
	 */
	function saveorder($cid = array(), $order = array()) {
		$row = &$this->getTable();

		// update ordering values
		for ($i = 0, $n = count($cid); $i < $n; $i++) {
			$row->load((int) $cid[$i]);

			if ($row->ordering != $order[$i]) {
				$row->ordering = $order[$i];
				if (!$row->store()) {
					$this->setError($this->_db->getErrorMsg());
					return false;
				}
			}
		}

		return true;
	}
}