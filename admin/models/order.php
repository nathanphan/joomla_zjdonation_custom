<?php

/**
 * @package		order.php
 * @author 		Nathan
 * @copyright 	2012 NathanPhan	.
 * @license 	GNU/GPL v2 http://www.gnu.org/licenses/gpl-2.0.html
 *
 */


// no direct access
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.model');
class ZJ_DonationModelOrder extends JModel{
	
	private $_table = '' ;
	private $_totalRows = NULL;
	private $_id = NULL;
	private $_data = NULL;
	private $_pagination = NULL;
	
	public function __construct() {
		parent::__construct ();
		//get table name info 
		$tableObj = $this->getTable( );
		$this->_table = $tableObj->getTableName( );
		
		$this->_setId ( array_shift ( JRequest::getVar ( 'cid', array (0 ), 'default', 'array' ) ) );
		
	
		if ('order' == JRequest::getCMD ( 'view' )) {
			$app = JFactory::getApplication ();
			
			$option = JRequest::getCmd ( 'option' );
	
			$limit = $app->getUserStateFromRequest ( '{$option}.limit', 'limit', 10, 'int' );
			$limitstart = JRequest::getVar ( 'limitstart', 0, '', 'int' );
			$limitstart = ($limit != 0 ? (floor ( $limitstart / $limit ) * $limit) : 0);
			$this->setState ( 'limit', $limit );
			$this->setState ( 'limitstart', $limitstart );
	
			$filter_order = $app->getUserStateFromRequest ( "{$option}.{$this->getName()}.filter_order", 'filter_order', 'name', 'cmd' );
			$filter_order_Dir = $app->getUserStateFromRequest ( "{$option}.{$this->getName()}.filter_order_Dir", 'filter_order_Dir', 'asc', 'word' );
			$this->setState ( 'filter_order', $filter_order );
			$this->setState ( 'filter_order_Dir', $filter_order_Dir );
		}
	}
	
	private function _setId($id) {
		$this->_id = $id;
		$this->_data = NULL;
	}
	
	public function insertId() {
		return $this->_id ? $this->_id : $this->_db->insertid ();
	}
	
	public function getTotal() {
		if (NULL === $this->_totalRows) {
			$this->getData ();
		}
		return $this->_totalRows;
	}
	
	public function getData() {
			
	
		if ('' == JRequest::getVar ( 'task' )) {
			$this->_data = $this->_getList ( $this->_buildQuery () . ' ' . $this->_buildContentOrderBy (), $this->getState ( 'limitstart' ), $this->getState ( 'limit' ) );
			
			$sql = "SELECT FOUND_ROWS();";
			$this->_db->setQuery ( $sql );
			$this->_totalRows = ( int ) $this->_db->loadResult ();
			
		} elseif (0 != $this->_id) {
			
			$this->_db->setQuery ( $this->_buildQuery () . " WHERE id={$this->_id}" );
			$this->_data = $this->_db->loadObject ();
			
		}
		
				 		
		if (! $this->_data) {
			$this->_data = $this->getTable ();
			$this->_data->id = 0;
			
		}
	
		
		
		return $this->_data;
	}
	
	
	
	public function getList() {
		return $this->_getList ( $this->_buildQuery () );
	}
	
	private function _buildQuery() {
		$query = "SELECT SQL_CALC_FOUND_ROWS * FROM {$this->_table}";
		return $query;
	}
	
	private function _buildContentOrderBy() {
		$orderby = '';
		$filter_order = $this->getState ( 'filter_order' );
		$filter_order_Dir = $this->getState ( 'filter_order_Dir' );
	
		/* Error handling is never a bad thing*/
		if (! empty ( $filter_order ) && ! empty ( $filter_order_Dir )) {
			$orderby = ' ORDER BY ' . $filter_order . ' ' . $filter_order_Dir;
		}
	
		return $orderby;
	}
	
	public function store() {
		$row = $this->getTable ();
		$data = JRequest::get ( 'post' );
		
		$data ['id'] = $this->_id;
		if (! $row->bind ( $data )) {
			$this->setError ( $this->_db->getErrorMsg () );
			return false;
		}
	
		if (! $row->check ()) {
			$this->setError ( $this->_db->getErrorMsg () );
			return false;
		}
	
		if (! $row->store ()) {
			$this->setError ( $this->_db->getErrorMsg () );
			return false;
		}
		
		return true;
	
	}
	
	public function delete() {
		$ids = JRequest::getVar ( 'cid', array (0 ), 'default', 'array' );
		$row = $this->getTable ();
	
		foreach ( $ids as $id ) {
			if (! $row->delete ( $id )) {
				$this->setError ( $row->_db->getErrorMsg () );
				return false;
			}
		}
	
		return true;
	}
	
	function getPagination() {
		if (empty ( $this->_pagination )) {
			jimport ( 'joomla.html.pagination' );
			$this->_pagination = new JPagination ( $this->getTotal (), $this->getState ( 'limitstart' ), $this->getState ( 'limit' ) );
		}
		return $this->_pagination;
	}
}