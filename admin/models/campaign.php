<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

/**
 * ZJ_Donation Component - Campaign Model.
 * @package		ZJ_Donation
 * @subpackage	Model
 */
class ZJ_DonationModelCampaign extends JModel {
	/** @var int Id of campaign */
	var $_id				= null;
	/** @var object campaign object */
	var $_data				= null;
	
	/**
	 * Constructor.
	 */
	function __construct() {
		// call parent constructor
		parent::__construct();
		
		$cid = JRequest::getVar('cid', array(0), '', 'array');
		$this->setId((int) $cid[0]);
	}

	/****************************************************************
	 * GET DATA FOR PRESENTATION.
	 ***************************************************************/
	
	/**
	 * Set campaign id.
	 */
	function setId($id) {
		if (!isset($this->_id) || $this->_id != $id) {
			$this->_id		= (int) $id;
			$this->_data	= null;
		}
	}

	/**
	 * Get or init campaign for edit screen.
	 */
	function getData() {
		if ($this->_loadData()) {

		} else {
			$this->_initData();
		}

		return $this->_data;
	}

	/**
	 * Load campaign data.
	 */
	function _loadData() {
		if (empty($this->_data)) {
			$query = 'SELECT a.*, u.username AS author, count(d.id) AS `donors`, sum(d.amount) AS `donated_amount`'
				. ' FROM #__zj_donation_campaigns AS a'
				. ' LEFT JOIN #__users AS u ON a.created_by = u.id'
				. ' LEFT JOIN #__zj_donation_donates AS d ON d.campaign_id = a.id AND d.donated = 1'
				. ' WHERE a.id = ' . $this->_id
				. ' GROUP BY a.id'
			;
			$this->_db->setQuery($query);
			$this->_data = $this->_db->loadObject();

			return (boolean) $this->_data;
		}

		return true;
	}

	/**
	 * Initialize campaign data.
	 */
	function _initData() {
		if (empty($this->_data)) {
			$user	= &JFactory::getUser();
			$date	= &JFactory::getDate();
			$row	= new stdClass();

			$row->id				= 0;
			$row->title				= null;
			$row->alias				= null;
			$row->description		= null;
			$row->addition			= null;
			$row->amounts			= null;
			$row->goal				= '0.00';
			$row->custom_amount		= 1;
			$row->published_up		= '0000-00-00 00:00:00';
			$row->published_down	= '0000-00-00 00:00:00';
			$row->messages			= null;
			$row->fields			= null;
			$row->image				= null;
			$row->gallery			= null;
			$row->recurring			= 1;
			$row->payments			= null;
			$row->feature			= 0;
			$row->created_by		= null;
			$row->created_date		= '0000-00-00 00:00:00';
			$row->published			= 1;
			$row->ordering			= 0;
			$row->meta_keywords		= null;
			$row->meta_description	= null;
			$row->checked_out		= 0;
			$row->checked_out_time	= '0000-00-00 00:00:00';

			$this->_data = $row;

			return (boolean) $this->_data;
		}

		return true;
	}

	/**
	 * Test if campaign is checked out.
	 */
	function isCheckedOut($uid = 0) {
		if ($this->_loadData()) {
			if ($uid) {
				return ($this->_data->checked_out && $this->_data->checked_out != $uid);
			} else {
				return $this->_data->checked_out;
			}
		}

		return false;
	}

	/****************************************************************
	 * MANIPULATE DATA.
	 ***************************************************************/
	
	/**
	 * Checkin/unlock the campaign.
	 */
	function checkin() {
		if ($this->_id) {
			$row = $this->getTable();
			if (!$row->checkin($this->_id)) {
				$this->setError($this->_db->getErrorMsg());
				return false;
			}
			return true;
		}

		return false;
	}

	/**
	 * Checkout/lock campaign.
	 */
	function checkout($uid = null) {
		if ($this->_id) {
			if (is_null($uid)) {
				$user	= &JFactory::getUser();
				$uid	= $user->get('id');
			}

			$row = &$this->getTable();
			if (!$row->checkout($uid, $this->_id)) {
				$this->setError($this->_db->getErrorMsg());
				return false;
			}

			return true;
		}

		return false;
	}
	
	/**
	 * Save campaign to database.
	 */
	function store($data) {
		$row = &$this->getTable();
		
		// bind the form fields to the campaigns table
		if (!$row->bind($data)) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		// if it's new item, order last in appropriate group.
		$date = &JFactory::getDate();

		if (!$row->created_date || $row->created_date == $this->_db->getNullDate()) {
			$row->created_date = $date->toMySQL();
		}

		if (!$row->id) {
			$row->ordering = $row->getNextOrder();
		}

		// make sure the campaign is valid
		if (!$row->check()) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		// store the campaign to the database
		if (!$row->store()) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		// unlock the table
		$row->checkin();

		return $row;
	}
}