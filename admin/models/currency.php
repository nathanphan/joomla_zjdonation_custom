<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

/**
 * ZJ_Donation Component - Currency Model.
 * @package		ZJ_Donation
 * @subpackage	Model
 */
class ZJ_DonationModelCurrency extends JModel {
	/** @var int currency identifier */
	var $_id				= null;
	/** @var array currency data array */
	var $_data				= null;
	
	
	/**
	 * Constructor.
	 */
	function __construct() {
		parent::__construct();
		
		$array = JRequest::getVar('cid', array(0), '', 'array');
		$this->setId((int) $array[0]);
	}
	
	/**
	 * Method to set the currency identifier.
	 * @access	public
	 * @param	int $id currency identifier
	 */
	function setId($id) {
		$this->_id		= $id;
		$this->_data	= null;
	}

	/****************************************************************
	 * Get data for presentation.
	 ***************************************************************/
	
	/**
	 * Logic for the currency edit screen.
	 * @access public
	 * @return array
	 */
	function getData() {
		if ($this->_loadData()) {
			
		} else {
			$this->_initData();
		}
		
		return $this->_data;
	}

	/**
	 * Method to load content currency data.
	 * @access private
	 * @return boolean True on success
	 */
	function _loadData() {
		// let load the content if it doesn't already exist
		if (empty($this->_data)) {
			$query = 'SELECT a.*'
				. ' FROM #__zj_donation_currencies AS a'
				. ' WHERE a.id = ' . $this->_id
			;
			$this->_db->setQuery($query);
			$this->_data = $this->_db->loadObject();

			return (boolean) $this->_data;
		}

		return true;
	}

	/**
	 * Method to initialise the currency data.
	 * @access private
	 * @return boonlean True on success
	 */
	function _initData() {
		// let load the content if it doesn't already exist
		if (empty($this->_data)) {
			$user	= &JFactory::getUser();
			$row	= new stdClass();

			$row->id				= 0;
			$row->title				= null;
			$row->sign				= null;
			$row->code				= null;
			$row->position			= 0;
			$row->description		= null;
			$row->published			= 0;
			$row->checked_out		= 0;
			$row->checked_out_time	= null;
			$row->ordering			= 0;

			$this->_data = $row;

			return (boolean) $this->_data;
		}

		return true;
	}
	
	/**
	 * Tests if the currency is checked out.
	 * @access public
	 * @param int $uid A user id
	 * @return boolean	True if checked out
	 */
	function isCheckedOut($uid = 0) {
		if ($this->_loadData()) {
			if ($uid) {
				return ($this->_data->checked_out && $this->_data->checked_out != $uid);
			} else {
				return $this->_data->checked_out;
			}
		}
		
		return false;
	}

	/****************************************************************
	 * Manipulate data.
	 ***************************************************************/
	
	/**
	 * Method to checkin/unlock the item.
	 * @access public
	 * @return boolean True on success
	 */
	function checkin() {
		if ($this->_id) {
			$row = &$this->getTable();
			
			if (!$row->checkin($this->_id)) {
				$this->setError($this->_db->getErrorMsg());
				
				return false;
			}
			
			return true;
		}
		
		return false;
	}
	
	
	/**
	 * Method to checkout/lock the item.
	 * @access public
	 * @param int $uid User ID of the user checking the item out
	 * @return boolean True on success
	 */
	function checkout($uid = null) {
		if ($this->_id) {
			if (is_null($uid)) {
				$user	= &JFactory::getUser();
				$uid	= $user->get('id');
			}

			// checkout the table
			$row = &$this->getTable();
			
			if (!$row->checkout($uid, $this->_id)) {
				$this->setError($this->_db->getErrorMsg());
				
				return false;
			}
			
			return true;
		}
		
		return false;
	}
	
	/**
	 * Method to store the currency.
	 * @access public
	 * @param $data
	 * @return boolean True on success
	 */
	function store($data) {
		$row = &$this->getTable();
		// bind the form fields to the version table
		if (!$row->bind($data)) {
			$this->setError($this->_db->getErrorMsg());
			
			return false;
		}
		
		if (!$row->id) {
			$row->ordering		= $row->getNextOrder();
		}
		
		// make sure the currency is valid
		if (!$row->check()) {
			$this->setError($this->_db->getErrorMsg());

			return false;
		}
		// store the currency table to the database
		if (!$row->store()) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}
		return $row;
	}
}
