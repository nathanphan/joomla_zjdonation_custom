<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');
jimport('joomla.installer.installer');
/**
 * ZJ_Donation Component - Dashboard Model.
 * @package		ZJ_Donation
 * @subpackage	Model
 */
class ZJ_DonationModelDashboard extends JModel {
	/**
	 * Constructor.
	 */
	function __construct() {
		parent::__construct();
	}

	function getXmlParser(){
		$xmlfiles	= JFolder::files(JPATH_COMPONENT_ADMINISTRATOR, '.xml$', 1, true);
		$installer	= JInstaller::getInstance();
		$xml		= new stdClass();
		if (!empty($xmlfiles)) {
			foreach ($xmlfiles as $file){
				$manifest	= $installer->_isManifest($file);
				if (!is_null($manifest)) {

					// If the root method attribute is set to upgrade, allow file overwrite
					$root =& $manifest->document;
					if ($root->attributes('method') == 'upgrade') {
						$this->_overwrite = true;
					}

					// Set the manifest object and path
					$this->_manifest	= $manifest;
					$this->manifest		= $file;
					$this->source		= dirname($file);
					$xml->version		= $root->getElementByPath('version')->data();
					$xml->copyright		= $root->getElementByPath('copyright')->data();
					$xml->license		= $root->getElementByPath('license')->data();
					$xml->jed_link		= $root->getElementByPath('jedlink')->data();
					foreach ($root->children() as $child){
						if (is_a($child, 'JSimpleXMLElement') && $child->name() == 'members') {
							if( count($child->children()) ){
								$i=0;
								foreach( $child->children() as $c ){
									$xml->zjmember[$i]['name'] = trim($c->_attributes['type']);
									$xml->zjmember[$i]['data'] = trim($c->data());
									$i++;
								}
							}
						}
					}

				}

			}
		}
		return $xml;
	}
}
