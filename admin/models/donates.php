<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

/**
 * ZJ_Donation Component - Donate Model.
 * @package		ZJ_Donation
 * @subpackage	Model
 */
class ZJ_DonationModelDonates extends JModel {
	/** @var array Donates array */
	var $_data			= null;
	/** @var int Total of Donate */
	var $_total			= null;
	/** @var object Pagination object */
	var $_pagination	= null;

	/**
	 * Constructor.
	 */
	function __construct() {
		global $mainframe, $option;

		// call parent constructor
		parent::__construct();

		// get the pagination request variables
		$limit		= $mainframe->getUserStateFromRequest('global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
		$limitstart = $mainframe->getUserStateFromRequest($option . '.donates.limitstart', 'limitstart', 0, 'int');

		// in case limit has been changed, adjust limitstart accordingly
		$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);

		$this->setState('limit', $limit);
		$this->setState('limitstart', $limitstart);
	}

	/****************************************************************
	 * GET DATA FOR PRESENTATION.
	 ***************************************************************/

	/**
	 * Get donates data.
	 */
	function getData() {
		if (empty($this->_data)) {
			$query			= $this->_buildQuery();
			$this->_data	= $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));
		}

		return $this->_data;
	}

	/**
	 * Get donates data.
	 */
	function getTotal() {
		if (empty($this->_total)) {
			$query			= $this->_buildQuery();
			$this->_total	= $this->_getListCount($query);
		}

		return $this->_total;
	}

	/**
	 * Get pagination object.
	 */
	function getPagination() {
		if (empty($this->_pagination)) {
			jimport('joomla.html.pagination');
			$this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit'));
		}

		return $this->_pagination;
	}

	/**
	 * Build query string.
	 */
	function _buildQuery() {
		// get the WHERE and ORDER BY clauses for the query
		$where		= $this->_buildQueryWhere();
		$orderby	= $this->_buildQueryOrderBy();

		$query = 'SELECT a.*, c.title AS `campaign`, c.checked_out AS cchecked_out, r.title AS `recurring_title`'
			. ' FROM #__zj_donation_donates AS a'
			. ' LEFT JOIN #__zj_donation_campaigns AS c ON a.campaign_id = c.id'
			. ' LEFT JOIN #__zj_donation_recurring_types AS r ON r.id = a.recurring_id'
			. $where
			. $orderby
		;

		return $query;
	}

	/**
	 * Build query where string.
	 */
	function _buildQueryWhere() {
		global $mainframe, $option;

		$db				= $this->_db;
		$filter_state	= $mainframe->getUserStateFromRequest($option . '.donates.filter_state', 'filter_state', '', 'word');
		$search			= $mainframe->getUserStateFromRequest($option . '.donates.search', 'search', '', 'string');
		$search			= JString::trim(JString::strtolower($search));
		$filter_campaign_id	= $mainframe->getUserStateFromRequest($option . '.donates.filter_campaign_id', 'filter_campaign_id', 0, 'int');

		$where = array();

		// filter by state of donates
		if ($filter_state) {
			if ($filter_state == 'P') {
				$where[] = 'a.published = 1';
			} else if ($filter_state == 'U') {
				$where[] = 'a.published = 0';
			}
		}

		// filter by the title of donates
		if ($search) {
			$where[] = 'LOWER(a.first_name) LIKE ' . $db->Quote('%' . $db->getEscaped($search, true) . '%', false)
					. ' OR LOWER(a.last_name) LIKE ' . $db->Quote('%' . $db->getEscaped($search, true) . '%', false)
					. ' OR LOWER(a.email) LIKE ' . $db->Quote('%' . $db->getEscaped($search, true) . '%', false)
					;
		}

		// filter by campaign of donates
		if ($filter_campaign_id > 0) {
			$where[] = 'a.campaign_id = ' . (int) $filter_campaign_id;
		}

		// build the WHERE clause
		$where = count($where) ? ' WHERE ' . implode(' AND ', $where) : '';

		return $where;
	}

	/**
	 * Build query orderby string.
	 */
	function _buildQueryOrderBy() {
		global $mainframe, $option;

		$filter_order		= $mainframe->getUserStateFromRequest($option . 'donates.filter_order', 'filter_order', 'a.created_date', 'cmd');
		$filter_order_Dir	= $mainframe->getUserStateFromRequest($option . 'donates.filter_order_Dir', 'filter_order_Dir', '', 'word');

		if ($filter_order == 'a.created_date') {
			$orderby = ' ORDER BY `campaign`, a.created_date ' . $filter_order_Dir;
		} else {
			$orderby = ' ORDER BY ' . $filter_order . ' ' . $filter_order_Dir . ', `campaign`, a.created_date desc';
		}

		return $orderby;
	}

	/****************************************************************
	 * MANIPULATE DATA.
	 ***************************************************************/

	/**
	 * Overloaded getTable method.
	 */
	function getTable($name = 'donate', $prefix = 'Table', $options = array()) {
		return parent::getTable($name, $prefix, $optins);
	}

	/**
	 * (Un)Publish donates.
	 */
    function publish($cid = array(), $publish = 1) {
		$user			= &JFactory::getUser();
		$affected_rows	= 0;

		if (count($cid)) {
			JArrayHelper::toInteger($cid);
			$cids = implode(', ', $cid);

			$query = 'UPDATE #__zj_donation_donates'
				. ' SET published = ' . (int) $publish
				. ' WHERE id IN(' . $cids . ')'
			;
			$this->_db->setQuery($query);

			if (!$this->_db->query()) {
				$this->setError($this->_db->getErrorMsg());
				return false;
			} else {
				$affected_rows = $this->_db->getAffectedRows();
			}
		}

		return $affected_rows;
	}

	/**
	 * Remove donates.
	 */
	function delete($cid = array()) {
		$affected_rows = 0;

		if (count($cid)) {
			JArrayHelper::toInteger($cid);
			$cids = implode(', ', $cid);

			$query = 'DELETE FROM #__zj_donation_donates'
				. ' WHERE id IN (' . $cids . ')'
			;
			$this->_db->setQuery($query);

			if (!$this->_db->query()) {
				$this->setError($this->_db->getErrorMsg());
				return false;
			} else {
				$affected_rows = $this->_db->getAffectedRows();
			}
		}

		return $affected_rows;
	}
}