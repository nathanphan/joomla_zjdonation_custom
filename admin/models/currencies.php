<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

/**
 * ZJ_Donation Component - Currencies Model.
 * @package		ZJ_Donation
 * @subpackage	Model
 */
class ZJ_DonationModelCurrencies extends JModel {
	/** @var array Currency array */
	var $_data			= null;
	/** @var int Currency total */
	var $_total			= null;
	/** @var object Pagination object */
	var $_pagination	= null;
	
	/**
	 * Constructor.
	 */
	function __construct() {
		global $mainframe, $option;
		
		parent::__construct();
		
		// get the pagination request variables
		$limit		= $mainframe->getUserStateFromRequest('global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
		$limitstart	= $mainframe->getUserStateFromRequest($option . '.currencies.limitstart', 'limitstart', 0, 'int');
		
		// in case limit has been changed, adjust limitstart accordingly
		$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
		
		$this->setState('limit', $limit);
		$this->setState('limitstart', $limitstart);
	}

	/****************************************************************
	 * Get data for presentation.
	 ***************************************************************/
	
	/**
	 * Method to get currencies item data.
	 * @access	public
	 * @return	array
	 */
	function getData() {
		// Lets load the data if it doesn't already exist
		if (empty($this->_data)) {
			$query = $this->_buildQuery();
			$this->_data = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));
		}
		
		return $this->_data;
	}
	
	/**
	 * Method to get the total number of currency items.
	 * @access	public
	 * @return	integer
	 */
	function getTotal() {
		// Lets load the total if it doesn't already exist
		if (empty($this->_total)) {
			$query = $this->_buildQuery();
			$this->_total = $this->_getListCount($query);
		}
		
		return $this->_total;
	}
	
	/**
	 * Method to get a pagination object for the currencies.
	 * @access	public
	 * @return	integer
	 */
	function getPagination() {
		// Lets load the pagination if it doesn't already exist
		if (empty($this->_pagination)) {
			jimport('joomla.html.pagination');
			$this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit'));
		}
		
		return $this->_pagination;
	}
	
	/**
	 * Method to get query string.
	 * @access	private
	 * @return	string
	 */
	function _buildQuery() {
		// Get the WHERE and ORDER BY clauses for the query
		$where		= $this->_buildQueryWhere();
		$orderby	= $this->_buildQueryOrderBy();
		
		$query = 'SELECT a.* '
			. ' FROM #__zj_donation_currencies AS a'
			. $where
			. $orderby
		;
		
		return $query;
	}
	
	/**
	 * Method to build WHERE clause for the query.
	 * @access	private
	 * @return	string
	 */
	function _buildQueryWhere() {
		global $mainframe, $option;
		
		$db					= $this->_db;
		$filter_state		= $mainframe->getUserStateFromRequest($option . '.currencies.filter_state', 'filter_state', '', 'word');
		$search				= $mainframe->getUserStateFromRequest($option . '.currencies.search', 'search', '', 'string');
		$search				= JString::strtolower($search);
		
		$where = array();
		
		// filter by state of currencies
		if ($filter_state) {
			if ($filter_state == 'P') {
				$where[] = 'a.published = 1';
			} else if ($filter_state == 'U') {
				$where[] = 'a.published = 0';
			}
		}

		// filter by title of currencies
		if ($search) {
			$where[] = 'LOWER(a.title) LIKE ' . $db->Quote('%' . $db->getEscaped($search, true) . '%', false);
		}

		// build the WHERE string
		$where = (count($where) ? ' WHERE ' . implode(' AND ', $where) : '');
		
		return $where;
	}
	
	/**
	 * Method to get ORDER BY clause for the query.
	 * @access	private
	 * @return	string
	 */
	function _buildQueryOrderBy() {
		global $mainframe, $option;
		
		$filter_order		= $mainframe->getUserStateFromRequest($option . 'currencies.filter_order', 'filter_order', 'a.ordering', 'cmd');
		$filter_order_Dir	= $mainframe->getUserStateFromRequest($option . 'currencies.filter_order_Dir', 'filter_order_Dir', '', 'word');
		
		if ($filter_order == 'a.ordering') {
			$orderby = ' ORDER BY a.ordering ' . $filter_order_Dir;
		} else {
			$orderby = ' ORDER BY ' . $filter_order . ' ' . $filter_order_Dir . ', a.ordering ';
		}
		
		return $orderby;
	}

	/****************************************************************
	 * Manipulate data.
	 ***************************************************************/

	/**
	 * Overwrite getTable method.
	 */
	function getTable($name = 'currency', $prefix = 'Table', $options = array()) {
		return parent::getTable($name, $prefix, $options);
	}

	/**
	 * Method to (un)publish a currency.
	 * @access	public
	 * @return	boolean	True on success
	 */
	function publish($cid = array(), $publish = 1) {
		$user	= &JFactory::getUser();

		if (count($cid)) {
			JArrayHelper::toInteger($cid);
			$cids = implode(', ', $cid);

			$query = 'UPDATE #__zj_donation_currencies'
				. ' SET published = ' . (int) $publish
				. ' WHERE id IN (' . $cids . ')'
				. ' AND (checked_out = 0 OR (checked_out = ' . (int) $user->get('id') . ' ))'
			;
			$this->_db->setQuery($query);

			if (!$this->_db->query()) {
				$this->setError($this->_db->getErrorMsg());

				return false;
			}
		}

		return true;
	}

	/**
	 * Method to remove a currency.
	 * @access public
	 * @param $cid
	 * @return boolean	True on success
	 */
	function delete($cid = array()) {
		if (count($cid)) {
			JArrayHelper::toInteger($cid);
			$cids = implode(', ', $cid);
			$query = 'DELETE FROM #__zj_donation_currencies'
				. ' WHERE id IN (' . $cids . ')';
			;
			$this->_db->setQuery($query);

			if (!$this->_db->query()) {
				$this->setError($this->_db->getErrorMsg());

				return false;
			}
		}

		return true;
	}

	/**
	 * Method to move a currency.
	 * @access	public
	 * @return	boolean	True on success
	 */
	function move($direction) {
		$cid	= JRequest::getVar('cid', array(0), 'post', 'array');
		$id		= $cid[0];
		$row	= &$this->getTable();

		if (!$row->load($id)) {
			$this->setError($this->_db->getErrorMsg());

			return false;
		}

		if (!$row->move($direction)) {
			$this->setError($this->_db->getErrorMsg());

			return false;
		}

		return true;
	}

	/**
	 * Method to move a currency.
	 * @access	public
	 * @return	boolean	True on success
	 */
	function saveorder($cid = array(), $order = array()) {
		$row = &$this->getTable();

		// update ordering values
		for ($i = 0, $n = count($cid); $i < $n; $i++) {
			$row->load((int) $cid[$i]);

			if ($row->ordering != $order[$i]) {
				$row->ordering = $order[$i];
				if (!$row->store()) {
					$this->setError($this->_db->getErrorMsg());
					return false;
				}
			}
		}

		return true;
	}
}
