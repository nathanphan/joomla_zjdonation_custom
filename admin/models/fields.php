<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

/**
 * ZJ_Donation Component - Fields Model.
 * @package		ZJ_Donation
 * @subpackage	Model
 */
class ZJ_DonationModelFields extends JModel {
	/** @var array Category array */
	var $_data			= null;
	/** @var int Total of categories */
	var $_total			= null;
	/** @var object Pagination object */
	var $_pagination	= null;

	/**
	 * Constructor.
	 */
	function __construct() {
		global $mainframe, $option;

		parent::__construct();

		// get the pagination request variables
		$limit		= $mainframe->getUserStateFromRequest('global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
		$limitstart	= $mainframe->getUserStateFromRequest($option . '.fields.limitstart', 'limitstart', 0, 'int');

		// in case limit has been changed, adjust limitstart accordingly
		$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);

		$this->setState('limit', $limit);
		$this->setState('limitstart', $limitstart);
	}

	/******************************************************
	 * Get data for presentation.
	 *****************************************************/

	/**
	 * Method to get mods item data.
	 * @access	public
	 * @return	array
	 */
	function getData() {
		// Lets load the content if it doesn't already exist
		if (empty($this->_data)) {
			$this->_loadData();
		}

		return $this->_data;
	}

	/**
	 * Method to get the total number of mod items.
	 * @access	public
	 * @return	integer
	 */
	function getTotal() {
		// Lets load the content if it doesn't already exist
		if (empty($this->_total)) {
			$this->_loadData();
		}

		return $this->_total;
	}

	/**
	 * Method to get a pagination object for the mods.
	 * @access	public
	 * @return	integer
	 */
	function getPagination() {
		// Lets load the content if it doesn't already exist
		if (empty($this->_pagination)) {
			$this->_loadData();
		}

		return $this->_pagination;
	}
	
	/**
	 * Method to get the data of mod items.
	 * @access	private
	 */
	function _loadData() {
		global $mainframe, $option;

		$db = &$this->getDBO();

		$query	= $this->_buildQuery();
		$db->setQuery($query);
		$rows	= $db->loadObjectList();

		$this->_total = count($rows);

		jimport('joomla.html.pagination');
		$this->_pagination = new JPagination($this->_total, $this->getState('limitstart'), $this->getState('limit'));

		// slice out elements based on limits
		$rows = array_slice($rows, $this->_pagination->limitstart, $this->_pagination->limit);

		$this->_data = $rows;
	}
	
	/**
	 * Build the select clause
	 *
	 * @return string
	 */
	function _buildQuery() {
		// Get the WHERE and ORDER BY clauses for the query
		$where		= $this->_buildContentWhere();
		$orderby	= $this->_buildContentOrderBy();
		$query = 'SELECT a.* FROM #__zj_donation_fields AS a '
			. $where
			. $orderby
		;
		return $query;
	}
	/**
	 * Build order by clause for the select command
	 *
	 * @return string order by clause
	 */
	function _buildContentOrderBy() {
		global $mainframe, $option;
		
		$filter_order		= $mainframe->getUserStateFromRequest($option . '.fields.filter_order', 'filter_order', 'a.ordering', 'cmd');
		$filter_order_Dir	= $mainframe->getUserStateFromRequest($option . '.fields.filter_order_Dir', 'filter_order_Dir', '', 'word');
		
		$limit				= $this->getState('limit');
		$limitstart			= $this->getState('limitstart');
		$levellimit			= 9999;
		
		if ($filter_order == 'a.ordering') {
			$orderby = ' ORDER BY a.ordering ' . $filter_order_Dir;
		} else {
			$orderby = ' ORDER BY ' . $filter_order . ' ' . $filter_order_Dir . ', a.ordering ';
		}
		
		return $orderby;
	}
	/**
	 * Build the where clause
	 *
	 * @return string
	 */
	function _buildContentWhere() {
		global $mainframe, $option;
		$db					=& JFactory::getDBO();
		$filter_state		= $mainframe->getUserStateFromRequest($option . '.fields.filter_state', 'filter_state', '', 'word');
		$search				= $mainframe->getUserStateFromRequest($option . '.fields.search', 'search', '', 'string');
		$search				= JString::strtolower($search);
		$where = array();		
		if ($search) {
			$where[] = 'LOWER(a.title) LIKE '.$db->Quote( '%'.$db->getEscaped( $search, true ).'%', false );
		}
		
		if ($filter_state) {
			if ($filter_state == 'P') {
				$where[] = 'a.published = 1';
			} else {
				$where[] = 'a.published = 0';
			}
		}
		
		$where 		= ( count( $where ) ? ' WHERE '. implode( ' AND ', $where ) : '' );
		return $where;
	}
}