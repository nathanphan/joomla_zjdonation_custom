<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Utility class for creating HTML lists for ZJ_Donation component
 * @package		ZJ_Donation
 * @subpackage	Helper
 */
class JHTMLZJ_Donation {
	/**
	 * Display list of campaign.
	 */
	function campaign($name = 'campaign_id', $active = 0, $javascript = '', $size = 1) {
		$db = &JFactory::getDBO();

		$query = 'SELECT id AS value, title AS text'
			. ' FROM #__zj_donation_campaigns AS a'
			. ' ORDER BY ordering'
		;
		$db->setQuery($query);
		$rows = $db->loadObjectList();

		$list[]			= JHTML::_('select.option', '0', '- ' . JText::_('Select a campaign') . ' -');
		$list			= array_merge($list, $rows);
		return JHTML::_('select.genericlist', $list, $name, 'class="inputbox" size="' . $size . '" ' . $javascript, 'value', 'text', intval($active));
	}

	/**
	 * Display list of recurring type.
	 */
	function recurringtype($name = 'type', $active = 0, $javascript = '', $size = 1) {
		$list	= array(
					array('value'	=> '-1', 'text'		=> '- ' . JText::_('Select a recurring type') . ' -'),
					array('value'	=> 'day', 'text'	=> 'Day'),
					array('value'	=> 'week', 'text'	=> 'Week'),
					array('value'	=> 'month', 'text'	=> 'Month'),
					array('value'	=> 'year', 'text'	=> 'Year')
				);

		return JHTML::_('select.genericlist', $list, $name, 'class="inputbox" size="' . $size . '" ' . $javascript, 'value', 'text', intval($active));
	}

	/**
	 * Display the required grid
	 * @return string
	 */
	function required( &$row, $i, $imgY = 'tick.png', $imgX = 'publish_x.png', $prefix='' ) {
        $img	= $row->required ? $imgY : $imgX;
		$task 	= $row->required ? 'un_required' : 'required';
		$alt 	= $row->required ? JText::_( 'Required' ) : JText::_( 'Not required' );
		$action = $row->required ? JText::_( 'Not Require' ) : JText::_( 'Require' );
		$href = '
		<a href="javascript:void(0);" onclick="return listItemTask(\'cb'. $i .'\',\''. $task .'\')" title="'. $action .'">
		<img src="images/'. $img .'" border="0" alt="'. $alt .'" /></a>'
		;
        return $href;
	}

	/**
	 * Display the feature grid
	 * @return string
	 */
	function feature( &$row, $i, $imgY = 'tick.png', $imgX = 'publish_x.png', $prefix='' ) {
        $img	= $row->feature ? $imgY : $imgX;
		$task 	= $row->feature ? 'unfeature' : 'feature';
		$alt 	= $row->feature ? JText::_( 'Featured Item' ) : JText::_( 'Not Feature Item' );
		$action = $row->feature ? JText::_( 'Not Feature Item' ) : JText::_( 'Featured Item' );
		$href = '
		<a href="javascript:void(0);" onclick="return listItemTask(\'cb'. $i .'\',\''. $task .'\')" title="'. $action .'">
		<img src="images/'. $img .'" border="0" alt="'. $alt .'" /></a>'
		;
        return $href;
	}


	/**
	 * Display (Un) published icon
	 * @return string
	 */
	function icon($value, $text1 = 'Required', $text2 = 'Not require', $imgY = 'tick.png', $imgX = 'publish_x.png') {
		$img	= $value ? $imgY : $imgX;
		$alt 	= $value ? $text1 : $text2;
		$href = '<img src="images/'. $img .'" border="0" alt="'. $alt .'" title="'. $alt .'" />';
		return $href;
	}

	/**
	 * Display a Field Type
	 * @return string
	 */
	function fieldtype($type) {
		/*
			0 => 'Textbox'
			1 => 'Textarea'
			2 => 'Dropdown'
			3 => 'Checkbox List'
			4 => 'Radio List'
			5 => 'Date Time'
		 */
		$fieldtype	= array('Textbox', 'Textarea', 'Dropdown', 'Checkbox List', 'Radio List', 'Date Time');
		return $fieldtype[$type];
	}

	/**
	 * Display the currency list
	 * @param $name Name of the list
	 * @param $active Active list item
	 * @param $javascript Javascript of the list
	 * @param $size Size of the list
	 * @return string
	 */
	function currency($name = 'currency_id', $active = 0, $javascript = '', $size = 1) {
		$db = &JFactory::getDBO();

		$query = 'SELECT DISTINCT a.id AS value, a.title AS text'
			. ' FROM #__zj_donation_currencies AS a'
			. ' ORDER BY a.ordering'
		;
		$db->setQuery($query);
		$rows = $db->loadObjectList();

		$list[]		= JHTML::_('select.option', '0', '- ' . JText::_('Select a currency') . ' -');
		$list		= array_merge($list, $rows);
		return JHTML::_('select.genericlist', $list, $name, 'class="inputbox" size="' . $size . '" ' . $javascript, 'value', 'text', intval($active));
	}

	/**
	 * Display the countries list
	 * @param $name Name of the list
	 * @param $active Active list item
	 * @param $javascript Javascript of the list
	 * @param $size Size of the list
	 * @return string
	 */
	function country($name = 'country_id', $active = '0', $javascript = '', $size = 1) {
		$db = &JFactory::getDBO();

		$query = 'SELECT DISTINCT a.title AS value, a.title AS text'
			. ' FROM #__zj_donation_countries AS a'
			. ' ORDER BY a.title'
		;
		$db->setQuery($query);
		$rows = $db->loadObjectList();

		//$list[]		= JHTML::_('select.option', '', '- ' . JText::_('Select a country') . ' -');
		//$list		= array_merge($list, $rows);
		return JHTML::_('select.genericlist', $rows, $name, 'class="inputbox" size="' . $size . '" ' . $javascript, 'value', 'text', $active);
	}

	/**
	 * Display the countries list
	 * @param $name Name of the list
	 * @param $active Active list item
	 * @param $javascript Javascript of the list
	 * @param $size Size of the list
	 * @return string
	 */
	function template($name = 'template', $active = 'default', $javascript = '', $size = 1) {
		$folders	= JFolder::folders(JPATH_ROOT.DS.'components'.DS.'com_zj_donation'.DS.'templates');
		$options = array ();
		foreach ($folders as $folder) {
			$options[] = JHTML::_('select.option', $folder, $folder);
		}

		return JHTML::_('select.genericlist', $options, $name, 'class="inputbox" size="' . $size . '" ' . $javascript, 'value', 'text', $active);
	}
}
