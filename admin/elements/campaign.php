<?php
/**
 * @version		$Id$
 * @author		Joomseller!
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_zj_donation'.DS.'tables');

class JElementCampaign extends JElement
{
	/**
	 * Element name
	 *
	 * @access	protected
	 * @var		string
	 */
	var	$_name = 'Campaign';

	function fetchElement($name, $value, &$node, $control_name)
	{
		global $mainframe;
		
		$db			=& JFactory::getDBO();
		$doc 		=& JFactory::getDocument();
		$template 	= $mainframe->getTemplate();
		$fieldName	= $control_name.'['.$name.']';
		
		$campaign =& JTable::getInstance('campaign','table');
		if ($value) {
			$campaign->load($value);
		} else {
			$campaign->title = JText::_('Select an Campaign');
		}
		$js = "
		function jSelectCampaign(id, title, object) {
			document.getElementById(object + '_id').value = id;
			document.getElementById(object + '_name').value = title;
			document.getElementById('sbox-window').close();
		}";
		$doc->addScriptDeclaration($js);
		?>
		<script language="javascript" type="text/javascript">
		<!--
		function submitbutton(pressbutton) {
			var form = document.adminForm;
			var type = form.type.value;
		
			if (pressbutton == 'cancelItem') {
				submitform( pressbutton );
				return;
			}
			if ( (type != "separator") && (trim( form.name.value ) == "") ){
				alert( "<?php echo JText::_( 'Item must have a title', true ); ?>" );
			}
			else if( document.getElementById('id_id').value == 0 ){
				alert( "<?php echo JText::_('Please select an Campaign', true ); ?>" );
			} else {
				submitform( pressbutton );
			}
		}
		//-->
		</script>	
		<?php
		$link = 'index.php?option=com_zj_donation&view=campaigns&amp;tmpl=component&amp;object='.$name;

		JHTML::_('behavior.modal', 'a.modal');
		$html = "\n".'<div style="float: left;"><input style="background: #ffffff;" type="text" id="'.$name.'_name" value="'.htmlspecialchars($campaign->title, ENT_QUOTES, 'UTF-8').'" disabled="disabled" /></div>';
//		$html .= "\n &nbsp; <input class=\"inputbox modal-button\" type=\"button\" value=\"".JText::_('Select')."\" />";
		$html .= '<div class="button2-left"><div class="blank"><a class="modal" title="'.JText::_('Select an Campaign').'"  href="'.$link.'" rel="{handler: \'iframe\', size: {x: 850, y: 375}}">'.JText::_('Select').'</a></div></div>'."\n";
		$html .= "\n".'<input type="hidden" id="'.$name.'_id" name="'.$fieldName.'" value="'.(int)$value.'" />';

		return $html;
	}
}
