/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2010 by Zerosoft. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */
 
var zj_image	= 0;

// reload javascript library
function zj_reloadJs() {
	// reload modal
	SqueezeBox.initialize({});

	$$('a.modal').each(function(el) {
		el.addEvent('click', function(e) {
			new Event(e).stop();
			SqueezeBox.fromElement(el);
		});
	});

	// find the un tooltip element
	var els = Array();
	$$('.hasTip').each(function(el) {
		if (el.getProperty('title')) {
			els.include(el);
		}
	});

	new Tips(els, {
		maxTitleChars: 50, fixed: false
	});

	// reload sortable
	new Sortables($('zj_sortable'), {
		'handles':	'.zj_sortablehandler'
	});

	// image sortable
	new Sortables($('zj_image_sortable'), {
		'handles':	'.zj_image_sortablehandler'
	});
}

// set field published status
function zj_setFieldPublish(id, value) {
	if ($('zj_field_published-' + id)) {
		$('zj_field_published-' + id).value = value;
	}
}


// add image for product
function zj_addImage() {
	var id = zj_image++;

	// create the table row
	var tr = new Element('tr', {
		'id': 'zj_image_row-' + id
	}).inject($('zj_image_sortable'));

	// create the sortable handler table cell
	var td_sortable_handler = new Element('td', {
		'align':	'center',
		'valign':	'top'
	}).inject(tr);
	// create the div
	var div_sortable_handler = new Element('div', {
		'class':	'zj_image_sortablehandler'
	}).inject(td_sortable_handler);

	// create the image title table cell
	var td_image_title = new Element('td', {
		'align':	'center',
		'valign':	'top'
	}).inject(tr);
	// create the image title textbox
	var txt_image_title = new Element('input', {
		'type':		'text',
		'class':	'text_area',
		'name':		'gallery[title][]',
		'value':	'',
		'size':		'35'
	}).inject(td_image_title);

	// create the file name table cell
	var td_image_name = new Element('td', {
		'align':	'center',
		'nowrap':	'nowrap',
		'valign':	'top'
	}).inject(tr);
	// create the file name textbox
	var txt_image_name = new Element('input', {
		'type':		'text',
		'styles': {
			'background-color':	'#FFFFFF',
			'float':			'left'
		},
		'id':		'zj_image_name-' + id,
		'value':	'',
		'size':		'35',
		'disabled':	'disabled'
	}).inject(td_image_name);
	// create the file name hidden
	var hidd_image_name = new Element('input', {
		'type':		'hidden',
		'name':		'gallery[name][]',
		'id':		'zj_image-' + id,
		'value':	''
	}).inject(td_image_name);
	// create the select button
	var div_outter = new Element('div', {
		'id':		'zj_image_select-' + id,
		'class':	'button2-left hasTip'
	}).inject(td_image_name);
	var div_inner = new Element('div', {
		'class':	'blank'
	}).inject(div_outter);
	var a_select = new Element('a', {
		'class':	'modal',
		'title':	'',
		'href':		zj_admin_live_site + 'index.php?option=com_zj_donation&view=imagehandler&tmpl=component&task=selectgallery&image_id=' + id,
		'rel':		'{handler: \'iframe\', size: {x: 650, y: 400}}'
	}).setText('Image').inject(div_inner);
	
	// create the file published table cell
	var td_published = new Element('td', {
		'align':	'center',
		'valign':	'top'
	}).inject(tr);
	// create the published check box
	var chk_published = new Element('input', {
		'type':		'checkbox',
		'class':	'inputbox',
		'value':	'1',
		'checked':	'checked',
		'onclick':	'zj_setImagePublish(' + id + ', this.checked ? 1 : 0);'
	}).inject(td_published);
	// create the published hidden input
	var hidd_published = new Element('input', {
		'type':		'hidden',
		'name':		'gallery[published][]',
		'id':		'zj_image_published-' + id,
		'value':	'1'
	}).inject(td_published);

	// create the remove table cell
	var td_remove = new Element('td', {
		'align':	'center',
		'valign':	'top'
	}).inject(tr);
	// create the a elment
	var a_remove = new Element('a', {
		'href':		'javascript:void(0)',
		'onclick':	'zj_removeImage(' + id + ')'
	}).inject(td_remove);
	// create the image
	var img_remove = new Element('img', {
		'src':		zj_admin_live_site + 'components/com_zj_donation/assets/images/delete.png',
		'alt':		'remove'
	}).inject(a_remove);

	// reload
	zj_reloadJs();
}

// remove image from product
function zj_removeImage(id) {
	if ($('zj_image_row-' + id)) {
		$('zj_image_row-' + id).remove();
	}
}

// set file published status
function zj_setImagePublish(id, value) {
	if ($('zj_image_published-' + id)) {
		$('zj_image_published-' + id).value = value;
	}
}

// select image from modal
function zj_selectImage(id, type, imagename) {
	$('sbox-window').close();

	if (type == 'galleries') {
		$('zj_image_name-' + id).value	= imagename;
		$('zj_image-' + id).value		= imagename;
		$('zj_image_select-' + id).setProperty('title', '<img src="' + zj_live_site + 'media/zj_donation/' + type + '/' + imagename + '" width="200" alt="' + type + '" />');
	} else if (type == 'images') {
		$('zj_campaign_image_name').value	= imagename;
		$('zj_campaign_image').value			= imagename;
		$('zj_campaign_image_select').setProperty('title', '<img src="' + zj_live_site + 'media/zj_donation/images/' + imagename + '" width="200" alt="' + type + '" />');
	}

	zj_reloadJs();
}