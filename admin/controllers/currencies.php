<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * ZJ_Donation Component - Currencies Controller.
 * @package		ZJ_Donation
 * @subpackage	Controller
 */
class ZJ_DonationControllerCurrencies extends ZJ_DonationController {
	
	/**
	 * Constructor
	 */
	function __construct() {
		parent::__construct();
		// register extra tasks
		$this->registerTask('add',		'display');
		$this->registerTask('edit',		'display');
		$this->registerTask('apply',	'save');
	}
	
	/**
	 * Display
	 */
	function display() {
		switch ($this->getTask()) {
			case 'edit':
			case 'add':
				JRequest::setVar('hidemainmenu', 1);
				JRequest::setVar('view', 'currency');
				break;
		}
		
		parent::display();
	}

	/****************************************************************
	 * Currencies View Controller
	 ***************************************************************/

	/**
	 * Publish the currency
	 */
	function publish() {
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');

		$cid = JRequest::getVar('cid', array(), 'post', 'array');
		JArrayHelper::toInteger($cid);

		if (count($cid) < 1) {
			JError::raiseError(500, JText::_('Select an item to publish'));
		}

		$model = &$this->getModel('currencies');

		if ($model->publish($cid, 1)) {
			$msg	= JText::_('Published status changed successfully');
		} else {
			$msg	= JText::_('Error while changing published status');
		}

		$this->setRedirect('index.php?option=com_zj_donation&view=currencies', $msg);
	}

	/**
	 * Unpublish the currency
	 */
	function unpublish() {
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');

		$cid = JRequest::getVar('cid', array(), 'post', 'array');
		JArrayHelper::toInteger($cid);

		if (count($cid) < 1) {
			JError::raiseError(500, JText::_('Select an item to unpublish'));
		}

		$model = &$this->getModel('currencies');

		if ($model->publish($cid, 0)) {
			$msg	= JText::_('Published status changed successfully');
		} else {
			$msg	= JText::_('Error while changing published status');
		}

		$this->setRedirect('index.php?option=com_zj_donation&view=currencies', $msg);
	}

	/**
	 * Remove the currency
	 */
	function remove() {
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');

		$cid = JRequest::getVar('cid', array(), 'post', 'array');
		JArrayHelper::toInteger($cid);

		if (count($cid) < 1) {
			JError::raiseError(500, JText::_('Select an item to delete'));
		}

		$model = &$this->getModel('currencies');

		if ($model->delete($cid)) {
			$msg	= JText::_('Currencies removed');
		} else {
			$msg	= JText::_('Error while removing currencies');
		}

		$this->setRedirect('index.php?option=com_zj_donation&view=currencies', $msg);
	}

	/**
	 * Move the currency to up
	 */
	function orderup() {
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');

		// move the currency
		$model = &$this->getModel('currencies');
		if ($model->move(-1)) {
			$msg = JText::_('Order saved');
		} else {
			$msg = JText::_('Error while saving order');
		}

		$this->setRedirect('index.php?option=com_zj_donation&view=currencies', $msg);
	}

	/**
	 * Move the currency to down
	 */
	function orderdown() {
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');

		// move the currency
		$model = &$this->getModel('currencies');
		if ($model->move(1)) {
			$msg = JText::_('Order saved');
		} else {
			$msg = JText::_('Error while saving order');
		}

		$this->setRedirect('index.php?option=com_zj_donation&view=currencies', $msg);
	}

	/**
	 * Save the order
	 */
	function saveorder() {
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');

		$cid 	= JRequest::getVar('cid', array(), 'post', 'array');
		$order 	= JRequest::getVar('order', array(), 'post', 'array');
		JArrayHelper::toInteger($cid);
		JArrayHelper::toInteger($order);

		$model = &$this->getModel('currencies');
		if ($model->saveorder($cid, $order)) {
			$msg = JText::_('Order saved');
		} else {
			$msg = JText::_('Error while saving order');
		}

		$this->setRedirect('index.php?option=com_zj_donation&view=currencies', $msg);
	}
}
