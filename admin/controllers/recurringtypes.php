<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * ZJ_Donation Component - RecurringTypes Controller.
 * @package		ZJ_Donation
 * @subpackage	Controller
 */
class ZJ_DonationControllerRecurringTypes extends ZJ_DonationController {
	/**
	 * Constructor.
	 */
	function __construct() {
		parent::__construct();
		
		// register extra tasks
		$this->registerTask('add',		'display');
		$this->registerTask('edit',		'display');
	}
	
	/**
	 * Display.
	 */
	function display() {
		switch ($this->getTask()) {
			case 'edit':
			case 'add':
				JRequest::setVar('hidemainmenu', 1);
				JRequest::setVar('view', 'recurringtype');
				break;
		}
		
		parent::display();
	}

	/**
	 * Publish recurringtypes.
	 */
	function publish() {
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');

		$cid = JRequest::getVar('cid', array(), 'post', 'array');
		JArrayHelper::toInteger($cid);

		if (count($cid) < 1) {
			JError::raiseError(500, JText::_('Select a recurring type to publish'));
		}

		$model			= &$this->getModel();
		$affected_rows	= $model->publish($cid, 1);

		if (!$affected_rows === false) {
			$msg = $model->getError();
		} else {
			$msg = JText::sprintf('%d recurring types has been published', $affected_rows);
		}

		$this->setRedirect(JRoute::_('index.php?option=com_zj_donation&view=recurringtypes', false), $msg);
	}

	/**
	 * Unpublish recurringtypes.
	 */
	function unpublish() {
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');

		$cid = JRequest::getVar('cid', array(), 'post', 'array');
		if (count($cid) < 1) {
			JError::raiseError(500, JText::_('Select a recurringtype to unpublish'));
		}

		$model			= &$this->getModel();
		$affected_rows	= $model->publish($cid, 0);

		if ($affected_rows === false) {
			$msg = $model->getError();
		} else {
			$msg = JText::sprintf('%d recurring types has been unpublished', $affected_rows);
		}

		$this->setRedirect(JRoute::_('index.php?option=com_zj_donation&view=recurringtypes', false), $msg);
	}

	/**
	 * Remove recurringtypes.
	 */
	function remove() {
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');

		$cid = JRequest::getVar('cid', array(), 'post', 'array');
		if (count($cid) < 1) {
			JError::raiseError(500, JText::_('Select a recurring type to delete'));
		}

		$model			= &$this->getModel();
		$affected_rows	= $model->delete($cid);

		if ($affected_rows === false) {
			$msg = $model->getError();
		} else {
			$msg = JText::sprintf('%d recurring types has been deleted', $affected_rows);
		}

		$this->setRedirect(JRoute::_('index.php?option=com_zj_donation&view=recurringtypes', false), $msg);
	}

	/**
	 * Move a recurringtype up.
	 */
	function orderup() {
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');

		$cid = JRequest::getVar('cid', array(), 'post', 'array');
		if (count($cid) < 1) {
			JError::raiseError(500, JText::_('Select a recurring type to move up'));
		}

		// move the recurringtype
		$model = &$this->getModel();

		if ($model->move($cid[0], -1)) {
			$msg = JText::_('Recurring type has been moved up');
		} else {
			$msg = $model->getError();
		}

		$this->setRedirect(JRoute::_('index.php?option=com_zj_donation&view=recurringtypes', false), $msg);
	}

	/**
	 * Move a recurringtype down.
	 */
	function orderdown() {
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');

		$cid = JRequest::getVar('cid', array(), 'post', 'array');
		if (count($cid) < 1) {
			JError::raiseError(500, JText::_('Select a recurringtype to move down'));
		}

		// move the recurringtype
		$model = &$this->getModel();

		if ($model->move($cid[0], 1)) {
			$msg = JText::_('Recurring type has been moved down');
		} else {
			$msg = $model->getError();
		}

		$this->setRedirect(JRoute::_('index.php?option=com_zj_donation&view=recurringtypes', false), $msg);
	}

	/**
	 * Save recurringtypes order.
	 */
	function saveorder() {
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');

		$cid	= JRequest::getVar('cid', array(), 'post', 'array');
		$order	= JRequest::getVar('order', array(), 'post', 'array');

		$model	= &$this->getModel();

		if ($model->saveorder($cid, $order)) {
			$msg = JText::_('New ordering saved');
		} else {
			$msg = $model->getError();
		}

		$this->setRedirect(JRoute::_('index.php?option=com_zj_donation&view=recurringtypes', false), $msg);
	}
}
