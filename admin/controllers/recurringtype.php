<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * ZJ_Donation Component - Recuring Type Controller.
 * @package		ZJ_Donation
 * @subpackage	Controller
 */
class ZJ_DonationControllerRecurringType extends ZJ_DonationController {
	/**
	 * Constructor.
	 */
	function __construct() {
		parent::__construct();

		// register extra tasks
		$this->registerTask('apply',	'save');
	}

	/**
	 * Save a recurringtype.
	 */
	function save() {
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');

		$post					= JRequest::get('post');
		$cid					= JRequest::getVar('cid', array(0), 'post', 'array');
		$post['id']				= (int) $cid[0];

		$model	= &$this->getModel();
		$row	= $model->store($post);
		$link	= JRoute::_('index.php?option=com_zj_donation&view=recurringtypes', false);

		if ($row->id) {
			$msg = JText::sprintf('Recurring Type: %s has been saved', $row->title);
			if ($this->getTask() == 'apply') {
				$link = JRoute::_('index.php?option=com_zj_donation&controller=recurringtypes&task=edit&cid[]=' . $row->id, false);
			}
		} else {
			$msg = JText::_('Error saving recurring type');
		}

		// check the table in so it can be edited.
		$model->checkin();

		$this->setRedirect($link, $msg);
	}

	/**
	 * Cancel edit a recurringtype.
	 */
	function cancel() {
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');

		// check in the recurringtype
		$model = &$this->getModel();
		$model->checkin();

		$this->setRedirect(JRoute::_('index.php?option=com_zj_donation&view=recurringtypes', false));
	}
}