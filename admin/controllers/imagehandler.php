
<?php
/**
 * @version		$Id$
 * @author		Joomseller
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 * This file may not be redistributed in whole or significant part.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.filesystem.file');

/**
 * ZJ_Donation Component - Image Handler Controller
 * @package		ZJ_Donation
 * @subpackage	Controller
 * @since		1.0
 *
 */
class ZJ_DonationControllerImageHandler extends ZJ_DonationController {
	
	/**
	 * Constructor.
	 */
	function __construct() {
		parent::__construct();
		
		// register extra tasks
		$this->registerTask('imageup',		'uploadImage');
		$this->registerTask('galleryup',	'uploadImage');
	}
	
	/**
	 * Method to upload image.
	 * @access	public
	 * @since	1.0
	 */
	function uploadImage() {
		global $mainframe;

		$config = &ZJ_DonationFactory::getConfig();
		
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');
		
		$image_id	= JRequest::getInt('image_id');
		
		$file	= JRequest::getVar('zj_image', '', 'files', 'array');
		$task	= $this->getTask();
		
		// Set FTP credentials, if given
		jimport('joomla.client.helper');
		JClientHelper::setCredentialsFromRequest('ftp');
		
		// set the target directory
		if ($task == 'imageup') {
			$folder = 'images';
		} else {
			$folder = 'galleries';
		}
		$base_dir = JPATH_SITE.DS.'media'.DS.'zj_donation'.DS.$folder.DS;
		
		//do we have an upload?
		if (empty($file['name'])) {
			echo "<script> alert('" . JText::_('Please select an image to upload') . "'); window.history.go(-1); </script>\n";
			$mainframe->close();
		}
		
		// check the image
		if (!ZJ_DonationImage::check($file, $config)) {
			$mainframe->redirect($_SERVER['HTTP_REFERER']);
		}
		
		// sanitize the image filename
		$filename = ZJ_DonationImage::sanitize($base_dir, $file['name'], $folder);
		$filepath = $base_dir . $filename;
		
		// upload the image
		if (!JFile::upload($file['tmp_name'], $filepath)) {
			echo "<script> alert('".JText::_('Upload FAILED.')."'); window.history.go(-1); </script>\n";
			$mainframe->close();
		} else {
			// TODO: resize image here...
			if ($folder == 'images') {
				echo "<script> alert('".JText::_('Upload COMPLETE')."'); window.history.go(-1); window.parent.zj_selectImage('$image_id', '$folder', '$filename'); </script>\n";
				$mainframe->close();
			} else {
				echo "<script> alert('".JText::_('Upload COMPLETE')."'); window.history.go(-1); window.parent.zj_selectImage('$image_id', '$folder', '$filename'); </script>\n";
				$mainframe->close();
			}
		}
	}
	
	/**
	 * Method to delete image
	 * @access	public
	 * @since	1.0
	 */
	function delete() {
		global $mainframe;
		
		// Set FTP credentials, if given
		jimport('joomla.client.helper');
		JClientHelper::setCredentialsFromRequest('ftp');
		
		// get data from request
		$images = JRequest::getVar('rm', array(), '', 'array');
		$folder = JRequest::getWord('folder');
		
		// check for valid folder
		$folders = array('images', 'galleries');
		if (!in_array($folder, $folders)) {
			$mainframe->close();
		}
		
		if (count($images)) {
			foreach ($images as $image) {
				if ($image !== JFilterInput::clean($image, 'path')) {
					JError::raiseWarning(100, JText::_('Unable to delete') . ' ' . htmlspecialchars($image, ENT_COMPAT, 'UTF-8'));
					continue;
				}
				
				$fullpath = JPath::clean(JPATH_SITE.DS.'media'.DS.'zj_donation'.DS.$folder.DS.$image);
				if (is_file($fullpath)) {
					JFile::delete($fullpath);
				}
			}
		}
		
		echo "<script>alert('".JText::_('Image has been deleted.')."'); window.history.go(-1); </script>\n";
		$mainframe->close();
	}
}