<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * ZJ_Donation Component - Donates Controller.
 * @package		ZJ_Donation
 * @subpackage	Controller
 */
class ZJ_DonationControllerDonate extends ZJ_DonationController {
	/**
	 * Constructor.
	 */
	function __construct() {
		parent::__construct();
	}

	/**
	 * Cancel edit a donate.
	 */
	function cancel() {
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');
		$this->setRedirect(JRoute::_('index.php?option=com_zj_donation&view=donates', false));
	}
}