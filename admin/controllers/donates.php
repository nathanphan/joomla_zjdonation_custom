<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * ZJ_Donation Component - Donates Controller.
 * @package		ZJ_Donation
 * @subpackage	Controller
 */
class ZJ_DonationControllerDonates extends ZJ_DonationController {
	/**
	 * Constructor.
	 */
	function __construct() {
		parent::__construct();
		
		// register extra tasks
		$this->registerTask('view',		'display');
	}
	
	/**
	 * Display
	 */
	function display() {
		switch ($this->getTask()) {
			case 'view':
				JRequest::setVar('view', 'donate');

				break;
		}

		parent::display();
	}

	/**
	 * Publish donates.
	 */
	function publish() {
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');

		$cid = JRequest::getVar('cid', array(), 'post', 'array');
		JArrayHelper::toInteger($cid);
		
		if (count($cid) < 1) {
			JError::raiseError(500, JText::_('Select a donation to publish'));
		}

		$model			= &$this->getModel();
		$affected_rows	= $model->publish($cid, 1);

		if ($affected_rows === false) {
			$msg = $model->getError();
		} else {
			$msg = JText::sprintf('%d donation has been published', $affected_rows);
		}

		$this->setRedirect(JRoute::_('index.php?option=com_zj_donation&view=donates', false), $msg);
	}

	/**
	 * Unpublish donates.
	 */
	function unpublish() {
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');

		$cid = JRequest::getVar('cid', array(), 'post', 'array');
		if (count($cid) < 1) {
			JError::raiseError(500, JText::_('Select a donation to unpublish'));
		}

		$model			= &$this->getModel();
		$affected_rows	= $model->publish($cid, 0);

		if ($affected_rows === false) {
			$msg = $model->getError();
		} else {
			$msg = JText::sprintf('%d donation has been published', $affected_rows);
		}

		$this->setRedirect(JRoute::_('index.php?option=com_zj_donation&view=donates', false), $msg);
	}

	/**
	 * Remove donates.
	 */
	function remove() {
		// check for request forgeries.
		JRequest::checkToken() or jexit('Invalid Token');

		$cid = JRequest::getVar('cid', array(), 'post', 'array');
		if (count($cid) < 1) {
			JError::raiseError(500, JText::_('Select a donation to delete'));
		}

		$model			= &$this->getModel();
		$affected_rows	= $model->delete($cid);

		if ($affected_rows === false) {
			$msg = $model->getError();
		} else {
			$msg = JText::sprintf('%d donation has been deleted', $affected_rows);

			if ($affected_rows < count($cid)) {
				$msg .= ' ' . JText::_('One or more donation could not be deleted because it is in use.');
			}
		}

		$this->setRedirect(JRoute::_('index.php?option=com_zj_donation&view=donates', false), $msg);
	}
}