<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * ZJ_Donation Component - Currency Controller.
 * @package		ZJ_Donation
 * @subpackage	Controller
 */
class ZJ_DonationControllerCurrency extends ZJ_DonationController {
	/**
	 * Constructor.
	 */
	function __construct() {
		parent::__construct();

		// register extra tasks
		$this->registerTask('apply',	'save');
	}

	/**
	 * Save the currency
	 */
	function save() {
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');

		$post					= JRequest::get('post');
		$cid					= JRequest::getVar('cid', array(0), 'post', 'array');
		$post['id']				= (int) $cid[0];
		$post['description']	= JRequest::getVar('description', '', 'post', 'string', JREQUEST_ALLOWHTML);

		$model	= &$this->getModel('currency');
		$row	= $model->store($post);
		$link	= 'index.php?option=com_zj_donation&view=currencies';
		if ($row->id) {
			$msg = JText::sprintf('Currency: %s has been saved', $row->title);
			if ($this->getTask() == 'apply') {
				$link = JRoute::_('index.php?option=com_zj_donation&controller=currency&task=edit&cid[]=' . $row->id, false);
			}
		} else {
			$msg = JText::_('Error while saving currency');
		}

		// check the table in so it can be edited.
		$model->checkin();

		$this->setRedirect($link, $msg);
	}

	/**
	 * Cancel edit the currency
	 */
	function cancel() {
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');

		// check in the currency
		$model = &$this->getModel('currency');
		$model->checkin();

		$this->setRedirect('index.php?option=com_zj_donation&view=currencies');
	}
}