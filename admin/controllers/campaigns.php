<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * ZJ_Donation Component - Campaigns Controller.
 * @package		ZJ_Donation
 * @subpackage	Controller
 */
class ZJ_DonationControllerCampaigns extends ZJ_DonationController {
	/**
	 * Constructor.
	 */
	function __construct() {
		parent::__construct();
		
		// register extra tasks
		$this->registerTask('add',		'display');
		$this->registerTask('edit',		'display');
	}
	
	/**
	 * Display.
	 */
	function display() {
		switch ($this->getTask()) {
			case 'edit':
			case 'add':
				JRequest::setVar('hidemainmenu', 1);
				JRequest::setVar('view', 'campaign');		
				break;
		}
		
		parent::display();
	}

	/**
	 * Publish campaigns.
	 */
	function publish() {
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');

		$cid = JRequest::getVar('cid', array(), 'post', 'array');
		JArrayHelper::toInteger($cid);

		if (count($cid) < 1) {
			JError::raiseError(500, JText::_('Select a campaign to publish'));
		}

		$model			= &$this->getModel();
		$affected_rows	= $model->publish($cid, 1);

		if (!$affected_rows === false) {
			$msg = $model->getError();
		} else {
			$msg = JText::sprintf('%d campaigns has been published', $affected_rows);
		}

		$this->setRedirect(JRoute::_('index.php?option=com_zj_donation&view=campaigns', false), $msg);
	}

	/**
	 * Unpublish campaigns.
	 */
	function unpublish() {
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');

		$cid = JRequest::getVar('cid', array(), 'post', 'array');
		if (count($cid) < 1) {
			JError::raiseError(500, JText::_('Select a campaign to unpublish'));
		}

		$model			= &$this->getModel();
		$affected_rows	= $model->publish($cid, 0);

		if ($affected_rows === false) {
			$msg = $model->getError();
		} else {
			$msg = JText::sprintf('%d campaigns has been unpublished', $affected_rows);
		}

		$this->setRedirect(JRoute::_('index.php?option=com_zj_donation&view=campaigns', false), $msg);
	}

	/**
	 * Feature campaigns.
	 */
	function feature() {
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');

		$cid = JRequest::getVar('cid', array(), 'post', 'array');
		JArrayHelper::toInteger($cid);

		if (count($cid) < 1) {
			JError::raiseError(500, JText::_('Select a campaign to feature'));
		}

		$model			= &$this->getModel();
		$affected_rows	= $model->feature($cid, 1);

		if (!$affected_rows === false) {
			$msg = $model->getError();
		} else {
			$msg = JText::sprintf('%d campaigns has been add as featured', $affected_rows);
		}

		$this->setRedirect(JRoute::_('index.php?option=com_zj_donation&view=campaigns', false), $msg);
	}

	/**
	 * Not Feature campaigns.
	 */
	function unfeature() {
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');

		$cid = JRequest::getVar('cid', array(), 'post', 'array');
		if (count($cid) < 1) {
			JError::raiseError(500, JText::_('Select a campaign to unfeature'));
		}

		$model			= &$this->getModel();
		$affected_rows	= $model->feature($cid, 0);

		if ($affected_rows === false) {
			$msg = $model->getError();
		} else {
			$msg = JText::sprintf('%d campaigns has been add as not feature', $affected_rows);
		}

		$this->setRedirect(JRoute::_('index.php?option=com_zj_donation&view=campaigns', false), $msg);
	}

	/**
	 * Remove campaigns.
	 */
	function remove() {
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');

		$cid = JRequest::getVar('cid', array(), 'post', 'array');
		if (count($cid) < 1) {
			JError::raiseError(500, JText::_('Select a campaign to delete'));
		}

		$model			= &$this->getModel();
		$affected_rows	= $model->delete($cid);

		if ($affected_rows === false) {
			$msg = $model->getError();
		} else {
			$msg = JText::sprintf('%d campaigns has been deleted', $affected_rows);
		}

		$this->setRedirect(JRoute::_('index.php?option=com_zj_donation&view=campaigns', false), $msg);
	}

	/**
	 * Move a campaign up.
	 */
	function orderup() {
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');

		$cid = JRequest::getVar('cid', array(), 'post', 'array');
		if (count($cid) < 1) {
			JError::raiseError(500, JText::_('Select a campaign to move up'));
		}

		// move the campaign
		$model = &$this->getModel();

		if ($model->move($cid[0], -1)) {
			$msg = JText::_('Campaign has been moved up');
		} else {
			$msg = $model->getError();
		}

		$this->setRedirect(JRoute::_('index.php?option=com_zj_donation&view=campaigns', false), $msg);
	}

	/**
	 * Move a campaign down.
	 */
	function orderdown() {
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');

		$cid = JRequest::getVar('cid', array(), 'post', 'array');
		if (count($cid) < 1) {
			JError::raiseError(500, JText::_('Select a campaign to move down'));
		}

		// move the campaign
		$model = &$this->getModel();

		if ($model->move($cid[0], 1)) {
			$msg = JText::_('Campaign has been moved down');
		} else {
			$msg = $model->getError();
		}

		$this->setRedirect(JRoute::_('index.php?option=com_zj_donation&view=campaigns', false), $msg);
	}

	/**
	 * Save campaigns order.
	 */
	function saveorder() {
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');

		$cid	= JRequest::getVar('cid', array(), 'post', 'array');
		$order	= JRequest::getVar('order', array(), 'post', 'array');

		$model	= &$this->getModel();

		if ($model->saveorder($cid, $order)) {
			$msg = JText::_('New ordering saved');
		} else {
			$msg = $model->getError();
		}

		$this->setRedirect(JRoute::_('index.php?option=com_zj_donation&view=campaigns', false), $msg);
	}
}
