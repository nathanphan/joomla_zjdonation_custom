<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * ZJ_Donation Component - Configs Controller.
 * @package		ZJ_Donation
 * @subpackage	Controller
 */
class ZJ_DonationControllerConfigs extends ZJ_DonationController {
	/**
	 * Constructor
	 */
	function __construct() {
		parent::__construct();
		// register extra tasks
		$this->registerTask('edit',		'display');
		$this->registerTask('apply',	'save');
	}

	/**
	 * Display
	 */
	function display() {
		JRequest::setVar('view', 'configs');
		parent::display();
	}

	/**
	 * Save the config
	 */
	function save() {
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');

		$post		= JRequest::get('post');
		$data		= $post['config'];

		$data['page_thanks_msg']			= @$_POST['config']['page_thanks_msg'];
		$data['page_cancel_msg']			= @$_POST['config']['page_cancel_msg'];
		$data['page_paylater_thanks_msg']	= @$_POST['config']['page_paylater_thanks_msg'];

		$data['field_organization_visible']		= isset($data['field_organization_visible']) ? $data['field_organization_visible'] : 0;
		$data['field_organization_required']	= isset($data['field_organization_required']) ? $data['field_organization_required'] : 0;
		$data['field_website_visible']			= isset($data['field_website_visible']) ? $data['field_website_visible'] : 0;
		$data['field_website_required']			= isset($data['field_website_required']) ? $data['field_website_required'] : 0;
		$data['field_address_visible']			= isset($data['field_address_visible']) ? $data['field_address_visible'] : 0;
		$data['field_address_required']			= isset($data['field_address_required']) ? $data['field_address_required'] : 0;
		$data['field_city_visible']				= isset($data['field_city_visible']) ? $data['field_city_visible'] : 0;
		$data['field_city_required']			= isset($data['field_city_required']) ? $data['field_city_required'] : 0;
		$data['field_state_visible']			= isset($data['field_state_visible']) ? $data['field_state_visible'] : 0;
		$data['field_state_required']			= isset($data['field_state_required']) ? $data['field_state_required'] : 0;
		$data['field_zip_visible']				= isset($data['field_zip_visible']) ? $data['field_zip_visible'] : 0;
		$data['field_zip_required']				= isset($data['field_zip_required']) ? $data['field_zip_required'] : 0;
		$data['field_country_visible']			= isset($data['field_country_visible']) ? $data['field_country_visible'] : 0;
		$data['field_country_required']			= isset($data['field_country_required']) ? $data['field_country_required'] : 0;
		$data['field_phone_visible']			= isset($data['field_phone_visible']) ? $data['field_phone_visible'] : 0;
		$data['field_phone_required']			= isset($data['field_phone_required']) ? $data['field_phone_required'] : 0;

		$model		= &$this->getModel('configs');
		$success	= $model->store($data);
		$link	= 'index.php?option=com_zj_donation';
		if ($success) {
			$msg = JText::_('Config saved');
			if ($this->getTask() == 'apply') {
				$link = 'index.php?option=com_zj_donation&view=configs';
			}
		} else {
			$msg = JText::_('Error when saving config');
		}

		$this->setRedirect($link, $msg);
	}
}
