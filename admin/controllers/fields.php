<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * ZJ_Donation Component - Fields Controller.
 * @package		ZJ_Donation
 * @subpackage	Controller
 */
class ZJ_DonationControllerFields extends ZJ_DonationController {
	
	/**
	 * Constructor.
	 */
	function __construct() {
		parent::__construct();
		
		// register extra tasks
		$this->registerTask('add',		'display');
		$this->registerTask('edit',		'display');
		$this->registerTask('apply',	'save');
	}
	
	/**
	 * Display.
	 */
	function display() {
		switch ($this->getTask()) {
			case 'edit':
			case 'add':
				JRequest::setVar('hidemainmenu', 1);
				JRequest::setVar('view', 'field');
				break;
				break;
		}
		
		parent::display();
	}

	/****************************************************************
	 * Fields View Controller
	 ***************************************************************/

	/**
	 * Remove selected fields
	 *
	 */	
	function remove() {
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');
		
		$cid = JRequest::getVar('cid', null);
		JArrayHelper::toInteger($cid);
		$model = $this->getModel('field');
		$ret = $model->delete($cid);
		if ($ret) {
			$msg = JText::_('Fields removed'); 
		} else {
			$msg = JText::_('Error removing fields');
		}
		$this->setRedirect('index.php?option=com_zj_donation&view=fields', $msg);
	}
	/**
	 * Publish the selected fields
	 *
	 */
	function publish() {
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');
		
		$cid = JRequest::getVar('cid', null) ;
		JArrayHelper::toInteger($cid);
		$model = $this->getModel('field');
		$ret = $model->publish($cid , 1);
		if ($ret) {
			$msg = JText::_('Fields published'); 
		} else {
			$msg = JText::_('Error publishing fields');
		}
		$this->setRedirect('index.php?option=com_zj_donation&view=fields', $msg);
	}
	/**
	 * Unpublish the selected fields
	 *
	 */
	function unpublish() {
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');
		
		$cid = JRequest::getVar('cid', null) ;
		JArrayHelper::toInteger($cid);
		$model = $this->getModel('field') ;
		$ret =  $model->publish($cid, 0);
		if ($ret) {
			$msg = JText::_('Fields unpublished'); 
		} else {
			$msg = JText::_('Error unpublishing fields');
		}
		$this->setRedirect('index.php?option=com_zj_donation&view=fields', $msg);
	}
    
    
	/**
	 * Require the selected fields
	 *
	 */
	function required() {
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');
		
		$cid = JRequest::getVar('cid', null) ;
		JArrayHelper::toInteger($cid);
		$model = $this->getModel('field');
		$ret = $model->required($cid , 1);
		if ($ret) {
			$msg = JText::_('Fields required'); 
		} else {
			$msg = JText::_('Error changing required status');
		}
		$this->setRedirect('index.php?option=com_zj_donation&view=fields', $msg);
	}
	
	/**
	 * Change status to un required
	 *
	 */
	function un_required() {
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');
		
		$cid = JRequest::getVar('cid', null) ;
		JArrayHelper::toInteger($cid);
		$model = $this->getModel('field');
		$ret = $model->required($cid , 0);
		if ($ret) {
			$msg = JText::_('Required unrequired'); 
		} else {
			$msg = JText::_('Error changing required status');
		}
		$this->setRedirect('index.php?option=com_zj_donation&view=fields', $msg);
	}
	
	/**
	 * Save ordering of the selected fields
	 *
	 */
	function save_order() {
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');
		
		$cid = JRequest::getVar('cid', array(), 'post', 'array');
		JArrayHelper::toInteger($cid);
		$order = JRequest::getVar('order', array(), 'post', 'array');
		JArrayHelper::toInteger($order);
		$model = & $this->getModel('field');
		$ret =  $model->saveorder($cid, $order);
		if ($ret) 
			$msg =  JText::_('Ordering updated');
		else 
			$msg = JText::_('Error while updating ordering');
		$url = 'index.php?option=com_zj_donation&view=fields';
		$this->setRedirect($url, $msg);
	}
	/**
	 * Orderdown the field
	 *
	 */
	function orderdown() {
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');
		
		$model =  $this->getModel('field');
		$model->move(1);		
		$msg = JText::_('Ordering updated');		
		$url = 'index.php?option=com_zj_donation&view=fields';
		$this->setRedirect($url, $msg);
	}
	/**
	 * Order up the field
	 *
	 */
	function orderup() {
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');
		
		$model =  $this->getModel('field');
		$model->move(-1);	 				
		$msg = JText::_('Ordering updated');		
		$url = 'index.php?option=com_zj_donation&view=fields';
		$this->setRedirect($url, $msg);
	}
}