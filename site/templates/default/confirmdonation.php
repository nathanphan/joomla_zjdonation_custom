<?php

/**
 * @version		$Id$
 * @author		Joomseller
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 * This file may not be redistributed in whole or significant part.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
// $amount	= (isset($post['amount2']) && $post['amount2'] > 0) ? $post['amount2'] : $post['amount1'];

//$link	= ZJ_DonationRoute::_('index.php?option=com_zj_donation&controller=donate&task=confirm_donation&id='.$row->id);
?>
<form method="post" action="index.php" id="donationForm" name="donationForm" class="form-validate">

<div class="zj_campaign_donors" style="width: 100%">
				<table width="100%" cellspacing="0" cellpadding="0" border="0" class="zj_table">
					<thead>
						<tr align="left" class="zj_table-header">
							<th width="1" align="center">
								#
							</th>
							<th  align="left"><?php echo JText::_('COM_ZJ_DONATION_DONNER_NAME'); ?></th>
							
							<th width="25%" align="center"><?php echo JText::_('COM_ZJ_DONATION_DONATION_AMOUNT'); ?></th>
						</tr>
					</thead>
					<tbody>
					<?php
						$i = 0;
						$total = 0;
						foreach ($cart as $cart_key => $cart_value): //dump($cart_value);
						
					?>
						<tr>
							<td><?php echo ++$i;?></td>
							<td><?php echo $cart_value[$cart_key]->title?></td>
							<td>
								<?php 
									echo ZJ_DonationUtils::formatPrice($cart_value['donate_amount'], null, true);
									$total += 	$cart_value['donate_amount'];
								?>
							</td>
						</tr>
						<?php endforeach;?>
						<tr style="border: 0px">
							<td colspan="3" align="right">&nbsp;</td>
							
						</tr>
						<tr>
							<td colspan="2" align="right">Total Donation Amount</td>
							<td >
								<span id="total" name="total">
									<?php $oper_cost = !empty($post['oper_cost']) ? $post['oper_cost'] : 0;
									
									 echo ZJ_DonationUtils::formatPrice($total + $oper_cost , null, true); 
									
									?>
									<input type="hidden" name="total_amount" value="<?php echo $total ; ?>" />
								</span>
							</td>
						</tr>
					</tbody>
				</table>
			</div>


	<table cellspacing="5" cellpadding="5" border="0" width="100%">
		<tr>
			<td colspan="2">
				<h3 class="donate-form-title">
					<?php echo JText::_('COM_ZJ_DONATION_DONATION_INFORMATION');?>
				</h3>
			</td>
		</tr>
		<tr>
			<td class="title_cell">
				<label for="first_name">
					<strong><?php echo JText::_('COM_ZJ_DONATION_FIRST_NAME'); ?></strong>:
				</label>
			</td>
			<td>
				<?php echo $post['first_name']; ?>
				<input type="hidden" name="first_name" value="<?php echo $post['first_name']; ?>" />
			</td>
		</tr>
		<tr>
			<td class="title_cell">
				<label for="last_name">
					<strong><?php echo JText::_('COM_ZJ_DONATION_LAST_NAME'); ?></strong>:
				</label>
			</td>
			<td>
				<?php echo $post['last_name']; ?>
				<input type="hidden" name="last_name" value="<?php echo $post['last_name']; ?>" />
			</td>
		</tr>
		<tr>
			<td class="title_cell">
				<label for="email">
					<strong><?php echo JText::_('COM_ZJ_DONATION_EMAIL'); ?></strong>:
				</label>
			</td>
			<td>
				<?php echo $post['email']; ?>
				<input type="hidden" name="email" value="<?php echo $post['email']; ?>" />
			</td>
		</tr>

		<?php
		if ($config->get('field_organization_visible')) {
			?>
			<tr>
				<td class="title_cell">
					<label for="organization">
						<strong><?php echo JText::_('COM_ZJ_DONATION_ORGANIZATION'); ?></strong>:
					</label>
				</td>
				<td>
					<?php echo $post['organization']; ?>
					<input type="hidden" name="organization" value="<?php echo $post['organization']; ?>" />
				</td>
			</tr>
		<?php
		}

		if ($config->get('field_website_visible')) {
			?>
			<tr>
				<td class="title_cell">
					<label for="website">
						<strong><?php echo JText::_('COM_ZJ_DONATION_WEBSITE'); ?></strong>:
					</label>
				</td>
				<td>
					<?php echo $post['website']; ?>
					<input type="hidden" name="website" value="<?php echo $post['website']; ?>" />
				</td>
			</tr>
		<?php
		}

		if ($config->get('field_address_visible')) {
			?>
			<tr>
				<td class="title_cell">
					<label for="address">
						<strong><?php echo JText::_('COM_ZJ_DONATION_ADDRESS'); ?></strong>:
					</label>
				</td>
				<td>
					<?php echo $post['address']; ?>
					<input type="hidden" name="address" value="<?php echo $post['address']; ?>" />
				</td>
			</tr>
		<?php
		}

		if ($config->get('field_city_visible')) {
			?>
			<tr>
				<td class="title_cell">
					<label for="city">
						<strong><?php echo JText::_('COM_ZJ_DONATION_CITY'); ?></strong>:
					</label>
				</td>
				<td>
					<?php echo $post['city']; ?>
					<input type="hidden" name="city" value="<?php echo $post['city']; ?>" />
				</td>
			</tr>
		<?php
		}

		if ($config->get('field_state_visible')) {
			?>
			<tr>
				<td class="title_cell">
					<label for="state">
						<strong><?php echo JText::_('COM_ZJ_DONATION_STATE'); ?></strong>:
					</label>
				</td>
				<td>
					<?php echo $post['state']; ?>
					<input type="hidden" name="state" value="<?php echo $post['state']; ?>" />
				</td>
			</tr>
		<?php
		}

		if ($config->get('field_zip_visible')) {
			?>
			<tr>
				<td class="title_cell">
					<label for="zip">
						<strong><?php echo JText::_('COM_ZJ_DONATION_ZIP_CODE'); ?></strong>:
					</label>
				</td>
				<td>
					<?php echo $post['zip']; ?>
					<input type="hidden" name="zip" value="<?php echo $post['zip']; ?>" />
				</td>
			</tr>
		<?php
		}

		if ($config->get('field_country_visible')) {
			?>
			<tr>
				<td class="title_cell">
					<label for="country">
						<strong><?php echo JText::_('COM_ZJ_DONATION_COUNTRY'); ?></strong>:
					</label>
				</td>
				<td>
					<?php echo $post['country']; ?>
					<input type="hidden" name="country" value="<?php echo $post['country']; ?>" />
				</td>
			</tr>
		<?php
		}

		if ($config->get('field_phone_visible')) {
			?>
			<tr>
				<td class="title_cell">
					<label for="phone">
						<strong><?php echo JText::_('COM_ZJ_DONATION_PHONE_NUMBER'); ?></strong>:
					</label>
				</td>
				<td>
					<?php echo $post['phone']; ?>
					<input type="hidden" name="phone" value="<?php echo $post['phone']; ?>" />
				</td>
			</tr>
		<?php
		}
		?>
		<tr>
			<td class="title_cell">
				<label for="phone">
					<strong><?php echo JText::_('COM_ZJ_DONATION_PUBLISH_DONATION'); ?></strong>:
				</label>
			</td>
			<td>
				<?php echo $post['access'] ? JText::_('COM_ZJ_DONATION_YES') : JText::_('COM_ZJ_DONATION_NO'); ?>
				<input type="hidden" name="access" value="<?php echo $post['access']; ?>" />
			</td>
		</tr>
	</table>
	<?php
	//Addition fields confirmation
	if (count($fields)) {
		?>
		<table cellspacing="5" cellpadding="5" border="0" width="100%">
			<tr>
				<td colspan="2">
					<h3 class="donate-form-title">
						<?php echo JText::_('COM_ZJ_DONATION_ADDITION_INFORMATION');?>
					</h3>
				</td>
			</tr>
			<?php echo ZJ_DonationFields::renderConfirmation($fields); ?>
		</table>
		<?php
		echo ZJ_DonationFields::renderHiddenFields($fields);
	}
	?>

	<table cellspacing="5" cellpadding="5" border="0" width="100%">
		<tr>
			<td colspan="2">
				<h3 class="donate-form-title">
					<?php echo JText::_('COM_ZJ_DONATION_PAYMENT_INFORMATION');?>
				</h3>
			</td>
		</tr>
		<!-- <tr>
			<td class="title_cell">
				<?php //echo JText::_('COM_ZJ_DONATION_DONATION_AMOUNT'); ?>:
			</td>
			<td>
				<?php //echo ZJ_DonationUtils::formatPrice($amount) ?>
				<input type="hidden" name="amount" value="<?php //echo $amount; ?>" />
			</td>
		</tr>  -->
		<?php if (isrecurring) //dump($post,'post in confirmdonation.php');
			{ ?>
			<tr>
				<td class="title_cell">
					<label for='recurring'>
						<?php echo JText::_('COM_ZJ_DONATION_DONATION_TYPE'); ?>:
					</label>
				</td>
				<td>
					<?php echo ($post['recurring'])? JText::_('COM_ZJ_DONATION_RECURRING_RECURRING') : JText::_('COM_ZJ_DONATION_RECURRING_ONE_TIME'); ?>
					<input type="hidden" name="recurring" value="<?php echo $post['recurring']; ?>" />
				</td>
			</tr>
			<?php if ($post['recurring']) { ?>
			<tr id="recurringType">
				<td class="title_cell">
					<label for='recurring_id'>
						<?php echo JText::_('COM_ZJ_DONATION_RECURRING_TYPE'); ?>:
					</label>
				</td>
				<td>
					<?php echo $recurring->title; ?>
					<input type="hidden" name="recurring_id" value="<?php echo $post['recurring_id']; ?>" />
				</td>
			</tr>
			<tr id="recurringTimes">
				<td class="title_cell">
					<label for='recurring_times'>
						<?php echo JText::_('COM_ZJ_DONATION_RECURRING_TIMES'); ?>:
					</label>
				</td>
				<td>
					<?php echo $post['recurring_times'] ? (int)$post['recurring_times'] : JText::_('COM_ZJ_DONATION_RECURRING_UNLIMITED');?>
					<input type="hidden" name="recurring_times" value="<?php echo (int)$post['recurring_times']; ?>" />
				</td>
			</tr>
			<?php } ?>
		<?php } ?>
		<tr>
			<td class="title_cell">
				<?php echo JText::_('COM_ZJ_DONATION_PAYMENT_METHOD'); ?>:
			</td>
			<td>
				<?php
				echo $post['payment_method'];
				?>
				<input type="hidden" name="payment_method" value="<?php echo $post['payment_method']; ?>" />
				<input type="hidden" name="oper_cost" value="<?php echo $post['oper_cost']; ?>" />
			</td>
		</tr>
	</table>
	<p>
		<button class="button validate" type="submit"><?php echo JText::_('COM_ZJ_DONATION_CONFIRM'); ?></button>
	</p>
	<input type="hidden" name="option" value="com_zj_donation" />
	<input type="hidden" name="controller" value="donate" />
	<input type="hidden" name="task" value="process_donation" />
	
	
	<?php echo JHTML::_('form.token'); ?>
</form>