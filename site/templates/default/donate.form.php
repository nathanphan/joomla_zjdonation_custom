<?php
/**
 * @version		$Id$
 * @author		Joomseller!
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
?>
<table cellspacing="5" cellpadding="5" border="0" width="100%">
	<tr>
		<td colspan="2">
			<h3 class="donate-form-title">
				<?php echo JText::_('COM_ZJ_DONATION_DONATION_INFORMATION');?>
			</h3>
		</td>
	</tr>
	<tr>
		<td class="title_cell">
			<label for='first_name'>
				<?php echo JText::_('COM_ZJ_DONATION_FIRST_NAME'); ?>:<span class='zj_required'>*</span>
			</label>
		</td>
		<td>
			<input type='text' size="30" class='inputbox required' value='<?php echo isset($post['first_name']) ? $post['first_name'] : '';?>' id='first_name' name='first_name'>
		</td>
	</tr>
	<tr>
		<td class="title_cell">
			<label for='last_name'>
				<?php echo JText::_('COM_ZJ_DONATION_LAST_NAME'); ?>:<span class='zj_required'>*</span>
			</label>
		</td>
		<td>
			<input type='text' size="30" class='inputbox required' value='<?php echo isset($post['last_name']) ? $post['last_name'] : '';?>' id='last_name' name='last_name'>
		</td>
	</tr>
	<tr>
		<td class="title_cell">
			<label for='email'>
				<?php echo JText::_('COM_ZJ_DONATION_EMAIL'); ?>:<span class='zj_required'>*</span>
			</label>
		</td>
		<td>
			<input type='text' size="30" class='inputbox required validate-email' value='<?php echo isset($post['email']) ? $post['email'] : $user->get('email'); ?>' id='email' name='email'>
		</td>
	</tr>
	<tr>
		<td class="title_cell">
			<label for='email_confirm'>
				<?php echo JText::_('COM_ZJ_DONATION_CONFIRM_EMAIL'); ?>:<span class='zj_required'>*</span>
			</label>
		</td>
		<td>
			<input type='text' size="30" class='inputbox required validate-emailverify' value='<?php echo isset($post['email_confirm']) ? $post['email_confirm'] : $user->get('email'); ?>' id='email_confirm' name='email_confirm'>
		</td>
	</tr>
	<?php
	if ( $config->get('field_organization_visible') ) {
		$require = $config->get('field_organization_required') ? '<span class="zj_required">*</span>' : '';
		$require_class = $config->get('field_organization_required') ? 'required' : '';
		$value = isset($post['organization']) ? $post['organization'] : ZJ_DonationUtils::getUserField('organization');
	?>
	<tr>
		<td class="title_cell">
			<label for='organization'>
				<?php echo JText::_('COM_ZJ_DONATION_ORGANIZATION'); ?>:<?php echo $require; ?>
			</label>
		</td>
		<td>
			<input type='text' size="50" class='inputbox' value='' id='organization' name='organization' value='<?php echo $value; ?>'>
		</td>
	</tr>
	<?php }
	if ( $config->get('field_website_visible') ) {
		$require = $config->get('field_website_required') ? '<span class="zj_required">*</span>' : '';
		$require_class = $config->get('field_website_required') ? 'required' : '';
		$value = isset($post['website']) ? $post['website'] : ZJ_DonationUtils::getUserField('website');
	?>
	<tr>
		<td class="title_cell">
			<label for='website'>
				<?php echo JText::_('COM_ZJ_DONATION_WEBSITE'); ?>:<?php echo $require; ?>
			</label>
		</td>
		<td>
			<input type='text' size="50" class='inputbox <?php echo $require_class; ?>' name='website' id='website' value='<?php echo $value; ?>'/>
		</td>
	</tr>
	<?php }
	if ($config->get('field_address_visible')) {
		$require = $config->get('field_address_required') ? '<span class="zj_required">*</span>' : '';
		$require_class = $config->get('field_address_required') ? 'required' : '';
		$value = isset($post['address']) ? $post['address'] : ZJ_DonationUtils::getUserField('address');
	?>
	<tr>
		<td class="title_cell">
			<label for='address'>
				<?php echo JText::_('COM_ZJ_DONATION_ADDRESS'); ?>:<?php echo $require; ?>
			</label>
		</td>
		<td>
			<input type='text' size="50" class='inputbox <?php echo $require_class; ?>' name='address' id='address' value='<?php echo $value; ?>'/>
		</td>
	</tr>
	<?php }
	if ($config->get('field_city_visible')) {
		$require = $config->get('field_city_required') ? '<span class="zj_required">*</span>' : '';
		$require_class = $config->get('field_city_required') ? 'required' : '';
		$value = isset($post['city']) ? $post['city'] : ZJ_DonationUtils::getUserField('city');
	?>
	<tr>
		<td class="title_cell">
			<label for='city'>
				<?php echo JText::_('COM_ZJ_DONATION_CITY'); ?>:<?php echo $require; ?>
			</label>
		</td>
		<td>
			<input type='text' size="30" class='inputbox <?php echo $require_class; ?>' name='city' id='city' value='<?php echo $value; ?>'/>
		</td>
	</tr>
	<?php }
	if ($config->get('field_state_visible')) {
		$require = $config->get('field_state_required') ? '<span class="zj_required">*</span>' : '';
		$require_class = $config->get('field_state_required') ? 'required' : '';
		$value = isset($post['state']) ? $post['state'] : ZJ_DonationUtils::getUserField('state');
	?>
	<tr>
		<td class="title_cell">
			<label for='state'>
				<?php echo JText::_('COM_ZJ_DONATION_STATE'); ?>:<?php echo $require; ?>
			</label>
			</td>
		<td>
			<input type='text' size="30" class='inputbox <?php echo $require_class; ?>' name='state' id='state' value='<?php echo $value; ?>'/>
		</td>
	</tr>
	<?php }
	if ($config->get('field_zip_visible')) {
		$require = $config->get('field_zip_required') ? '<span class="zj_required">*</span>' : '';
		$require_class = $config->get('field_zip_required') ? 'required' : '';
		$value = isset($post['zip']) ? $post['zip'] : ZJ_DonationUtils::getUserField('zip');;
	?>
	<tr>
		<td class="title_cell">
			<label for='zip'>
				<?php echo JText::_('COM_ZJ_DONATION_ZIPCODE'); ?>:<?php echo $require; ?>
			</label>
		</td>
		<td>
			<input type='text' size="30" class='inputbox <?php echo $require_class; ?>' name='zip' id='zip' value='<?php echo $value; ?>'/>
		</td>
	</tr>
	<?php }
	if ($config->get('field_country_visible')) {
		$require = $config->get('field_country_required') ? '<span class="zj_required">*</span>' : '';
		$require_class = $config->get('field_country_required') ? 'required' : '';
	?>
	<tr>
		<td class="title_cell">
			<label for='country'>
				<?php echo JText::_('COM_ZJ_DONATION_COUNTRY'); ?>:<?php echo $require; ?>
			</label>
		</td>
		<td>
			<?php echo JHTML::_('zj_donation.country', 'country');?>
		</td>
	</tr>
	<?php }
	if ($config->get('field_phone_visible')) {
		$require = $config->get('field_phone_required') ? '<span class="zj_required">*</span>' : '';
		$require_class = $config->get('field_phone_required') ? 'required' : '';
		$value = isset($post['phone']) ? $post['phone'] : ZJ_DonationUtils::getUserField('phone');
	?>
	<tr>
		<td class="title_cell">
			<label for='phone'>
				<?php echo JText::_('COM_ZJ_DONATION_PHONE'); ?>:<?php echo $require; ?>
			</label>
		</td>
		<td>
			<input type='text' size="30" class='inputbox <?php echo $require_class; ?>' name='phone' id='phone' value='<?php echo $value; ?>'/>
		</td>
	</tr>	
	<?php }?>
	<tr>
		<td class="title_cell">
			<label>
				<?php echo JText::_('COM_ZJ_DONATION_PUBLISH_DONATION'); ?>:
			</label>
		</td>
		<td>
			<span>
				<?php echo $lists['access']; ?>
			</span>
		</td>
	</tr>
</table>