<?php

/**
 * @version		$Id$
 * @author		Joomseller
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 * This file may not be redistributed in whole or significant part.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
ZJ_DonationTemplate::addStyleSheet('nathan_style');
jimport('joomla.filesystem.file');
?>

<?php if ($params->get( 'show_page_title', 1 )) { ?>
<div class="zj_category-title">
	<h3 class="componentheading"><?php echo $params->get('page_title'); ?></h3>
</div>
<?php } ?>

<div class="zj_list-campaigns">
	<div id="zj_campaigns">
		<ul>
		<?php
		$session 		= JFactory::getSession();
		$cart			= $session->get('DCart');
		
		for ($i = 0, $n = count($campaigns); $i < $n; $i++) {
			$row = &$campaigns[$i];
			
			$link					= ZJ_DonationRoute::_('index.php?option=com_zj_donation&view=campaign&id=' . (int) $row->id);
			
			$donate_form			= '';
			
			if(isset($cart) && !empty($cart))
			{
				$flag = false;
				foreach ($cart as $cart_key => $cart_value){
					if($cart_key == $row->id){
						$flag = true;
					}
				}
				if($flag) 
					$donate_form			= ZJ_DonationRoute::_('index.php?option=com_zj_donation&controller=donate&task=checkout&id=' . (int) $row->id);
				else 
					$donate_form			= ZJ_DonationRoute::_('index.php?option=com_zj_donation&controller=donate&task=donation&id=' . (int) $row->id);
			}
			else{
				$donate_form			= ZJ_DonationRoute::_('index.php?option=com_zj_donation&controller=donate&task=donation&id=' . (int) $row->id);
			}
			
			
			?>
			<li class="zj_campaign">
				<div class="zj_thumb">
					<img src="<?php echo ZJ_DONATION_IMG; ?>?width=100&height=150&image=/images/<?php echo $row->image; ?>" alt="Image" />
				</div>
				<div class="zj_details">
					<h4>
						<a href="<?php echo $link; ?>">
							<?php echo $row->title; ?>
						</a>
					</h4>
					<dl class="zj_props">
						<div>
							<dt>
								<?php echo JText::_('COM_ZJ_DONATION_CAMPAIGN_START_DATE'); ?>:
							</dt>
							<dd>
								<?php echo JHTML::_('date', $row->published_up, JText::_('DATE_FORMAT_LC')); ?>
							</dd>
						</div>
						<div>
							<dt>
								<?php echo JText::_('COM_ZJ_DONATION_CAMPAIGN_END_DATE'); ?>:
							</dt>
							<dd>
								<?php echo JHTML::_('date', $row->published_down, JText::_('DATE_FORMAT_LC')); ?>
							</dd>
						</div>
						<div>
							<dt>
								<?php echo JText::_('COM_ZJ_DONATION_CAMPAIGN_GOAL_AMOUNT'); ?>:
							</dt>
							<dd>
								<strong class="price_entry"><?php echo ZJ_DonationUtils::formatPrice($row->goal, null, true); ?></strong>
							</dd>
						</div>
						<div class="zj_prop">
				<?php 
						$goalAmount = $row->goal;
						$donatedAmount = $row->donated_amount;
						$percent = 0;
						if($goalAmount != 0)
							$percent = ($donatedAmount * 100) / $goalAmount;
						$percent = round($percent);
					?>
					<dt>
						<div class="percentRaised fundRaising">
							
						</div> 
					</dt>
					<dd>
					
						<div class="fundRaisingMeter  meter">
							<div id="bar" style="width: <?php echo $percent ?>%;" >&nbsp;</div>
						</div>
						<span class="number">
								<?php echo $percent ?>%</span>&nbsp;&nbsp;raised&nbsp;&nbsp;
					</dd>
				</div>
					</dl>
					<div class="zj_desc">
						<?php echo $row->description; ?>
					</div>
					<div class="zj_clear"></div>
					<div class="zj_buttons">
						
						
						<a class="zj_button" href="<?php echo $donate_form; ?>">
							
							<?php 
									if($flag) echo JText::_('Checkout');
									else echo JText::_('COM_ZJ_DONATION_DONATE_BUTTON');
								
							 ?>
						
						</a>
						<a class="zj_button" href="<?php echo $link; ?>">
							<?php echo JText::_('COM_ZJ_DONATION_READMORE'); ?>
						</a>
					</div>
				</div>
			</li>
		<?php
		}
		?>
		</ul>
	</div>
	<?php if ($params->get('show_pagination', 1)) { ?>
	<div id="zj_campaign-pagination">
		<?php echo $pagination->getPagesLinks(); ?>
	</div>
	<?php } ?>
</div>
