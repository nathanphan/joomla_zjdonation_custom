<?php
/**
 * @version		$Id$
 * @author		Joomseller!
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

?>
<table cellspacing="5" cellpadding="5" border="0" width="100%">
	<tr>
		<td colspan="2">
			<h3 class="donate-form-title">
				<?php echo JText::_('COM_ZJ_DONATION_PAYMENT_INFORMATION');?>
			</h3>
		</td>
	</tr>
	
	<?php if ($isRecurring == true) { ?>
	<tr>
		<td class="title_cell">
			<label for='recurring'>
				<?php echo JText::_('COM_ZJ_DONATION_DONATION_TYPE'); ?>:
			</label>
		</td>
		<td>
			<?php echo $lists['recurring']; ?>
		</td>
	</tr>
	<tr id="recurringType" class="hide">
		<td class="title_cell">
			<label for='recurring_id'>
				<?php echo JText::_('COM_ZJ_DONATION_RECURRING_TYPE'); ?>:
			</label>
		</td>
		<td>
			<?php echo $lists['recurringtype']; ?>
		</td>
	</tr>
	<tr id="recurringTimes" class="hide">
		<td class="title_cell">
			<label for='recurring_times'>
				<?php echo JText::_('COM_ZJ_DONATION_RECURRING_TIMES'); ?>:
			</label>
		</td>
		<td>
			<input type="text" class="inputbox" size="10" name="recurring_times" id="recurring_times" value="<?php echo isset($post['recurring_times']) ? $post['recurring_times'] : '';?>"/>
			&nbsp;&nbsp;<em>(<?php echo JText::_('COM_ZJ_DONATION_LEAVE_BLANK_FOR_UNLIMITED'); ?>)</em>
		</td>
	</tr>
	<?php } ?>
	<tr>
		<td class="title_cell">
			<?php echo JText::_('COM_ZJ_DONATION_PAYMENT_METHOD'); ?>:<span class="zj_required">*</span>
		</td>
		<td>
			<?php
			echo ZJ_DonationFactory::getPayments('payment_method');
			?>
		</td>
	</tr>
</table>
<script type="text/javascript">
	function changeRecurring(form) {
		var radioObj		= form.recurring;
		var	recurringVal	= 0;
		var recurringType	= $('recurringType');
		var recurringTimes	= $('recurringTimes');

		if(!radioObj) { return false; }
		var radioLength = radioObj.length;
		if(radioLength == undefined) {
			if(radioObj.checked) {
				recurringVal = radioObj.value;
			}
		}
			
		for(var i = 0; i < radioLength; i++) {
			if(radioObj[i].checked) {
				recurringVal = radioObj[i].value;
			}
		}

		if (recurringVal == 1) {
			recurringType.removeClass('hide');
			recurringTimes.removeClass('hide');
		} else {
			recurringType.addClass('hide');
			recurringTimes.addClass('hide');
		}
	}

	window.addEvent('domready', function() {
		changeRecurring(document.donationForm);
	});
</script>