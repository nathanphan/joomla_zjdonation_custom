<?php

/**
 * @version		$Id$
 * @author		Joomseller
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 * This file may not be redistributed in whole or significant part.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.html.pane');
$tabs = &JPane::getInstance();
	
// multibox
JHTML::_('behavior.mootools');
ZJ_DonationTemplate::addScript('slimbox');
ZJ_DonationTemplate::addStyleSheet('slimbox');
ZJ_DonationTemplate::addStyleSheet('nathan_style');

$js = '
window.addEvent(\'domready\', function(){
	var box = new MultiBox(\'zj_gallery_image\', {
		useOverlay: true,
		opac: \'0.7\',
		ocolor: \'#000\'
	});
});
';

//$donate_form	= ZJ_DonationRoute::_('index.php?option=com_zj_donation&controller=donate&task=donation&id=' . (int) $row->id);
?>

<div class="zj_campaign-title">
	<h1 class="componentheading"><?php echo $campaign->title; ?></h1>
</div>

<div class="zj_campaign-details">
	<div id="zj_campaign" class="clearfix">
		<div class="zj_thumb">
			<img src="<?php echo ZJ_DONATION_IMG; ?>?width=175&height=250&image=/images/<?php echo $campaign->image; ?>" alt="Image" rel="lightbox-atomium" />
			
			<div class="zj_buttons">
				<form name="donate_campaign" action="<?php echo JUri::base();?>" method="post">
				<?php if(isset($action)):?>
					<p align="center">
						<input type="submit" class="zj_button" value="Check Out" />
					</p>
					<input type="hidden" name="task" value="checkout" />
					<br />
					<a href="<?php echo JRoute::_('index.php?option=com_zj_donation&view=campaigns&layout=table')?>" class="zj_button">Continue</a>
				<?php else: ?>
					<p align="center">
						<input type="submit" class="zj_button" value="<?php echo 'Donate'; ?>" />
					</p>
					<input type="hidden" name="task" value="donation" />
				<?php endif;?>
					
					<input type="hidden" name="option" value="com_zj_donation" />
					<input type="hidden" name="controller" value="donate" />
					
					<input type="hidden" name="id" value="<?php echo $campaign->id; ?>" />
				</form>
			</div>
		</div>
		<div class="zj_details">
			<dl class="zj_props">
				<div class="zj_prop">
					<dt>
						<?php echo JText::_('COM_ZJ_DONATION_CAMPAIGN_START_DATE'); ?>:
					</dt>
					<dd>
						<?php echo JHTML::_('date', $campaign->published_up, JText::_('DATE_FORMAT_LC')); ?>
					</dd>
				</div>
				<div class="zj_prop">
					<dt>
						<?php echo JText::_('COM_ZJ_DONATION_CAMPAIGN_END_DATE'); ?>:
					</dt>
					<dd>
						<?php echo JHTML::_('date', $campaign->published_down, JText::_('DATE_FORMAT_LC')); ?>
					</dd>
				</div>
				<div class="zj_prop">
					<dt>
						<?php echo JText::_('COM_ZJ_DONATION_CAMPAIGN_GOAL_AMOUNT'); ?>:
					</dt>
					<dd>
						<strong class="price_entry"><?php echo ZJ_DonationUtils::formatPrice($campaign->goal, null, true); ?></strong>
					</dd>
				</div>
				<div class="zj_prop">
					<dt>
						<?php echo JText::_('COM_ZJ_DONATION_CAMPAIGN_DONATED_AMOUNT'); ?>:
					</dt>
					<dd>
						<strong class="price_entry"><?php echo ZJ_DonationUtils::formatPrice($campaign->donated_amount, null, true); ?></strong>
					</dd>
				</div>
				<div class="zj_prop">
					<dt>
						<?php echo JText::_('COM_ZJ_DONATION_DONORS'); ?>:
					</dt>
					<dd>
						<?php echo $count; ?>
					</dd>
				</div>
				<div class="zj_prop">
				<?php 
						$goalAmount = $campaign->goal;
						$donatedAmount = $campaign->donated_amount;
						$percent = 0;
						if($goalAmount != 0)
							$percent = ($donatedAmount * 100) / $goalAmount;
						$percent = round($percent);
					?>
					<dt>
						<div class="percentRaised fundRaising">
							
						</div> 
					</dt>
					<dd>
					
						<div class="fundRaisingMeter  meter">
							<div id="bar" style="width: <?php echo $percent ?>%;" >&nbsp;</div>
						</div>
						<span class="number">
								<?php echo $percent ?>%</span>&nbsp;&nbsp;raised&nbsp;&nbsp;
					</dd>
				</div>
			</dl>
			<?php if ($config->get('social_share')) { ?>
				<div class="zj_social_share" style="padding-top: 5px;">
					<?php echo ZJ_DonationFactory::renderSocialButtons(); ?>
				</div>
			<?php } ?>
			<div class="zj_desc">
				<?php echo $campaign->description; ?>
			</div>
		</div>
	</div>
	<div class="zj_campaign-info">
		<?php
		echo $tabs->startPane('campaign_info');
		if ($campaign->addition) {
			echo $tabs->startPanel(JText::_('COM_ZJ_DONATION_ADDITION_INFORMATION'), 'addic_info');
			?>
			<div class="zj_campaign_addic">
				<?php echo $campaign->addition; ?>
			</div>
			<?php
			echo $tabs->endPanel();
		}

		if (count($images)) {
			echo $tabs->startPanel(JText::sprintf('COM_ZJ_DONATION_IMAGES_COUNT', count($images->title)), 'zj_gallery');
			?>
			<div class="zj_campaign_gallery">
				<?php
				for ($i = 0, $n = count($images->title); $i < $n; $i++) {
					?>
					<a class="zj_gallery_image" rel="lightbox-atomium" title="<?php echo $images->title[$i]; ?>" href="<?php echo ZJ_DONATION_IMG; ?>?width=700&height=700&image=/galleries/<?php echo $images->name[$i]; ?>">
						<img border="0" src="<?php echo ZJ_DONATION_IMG; ?>?width=180&height=180&image=/galleries/<?php echo $images->name[$i]; ?>" alt="image" />
					</a>
					<?php
				}
				?>
			</div>
			<?php
			echo $tabs->endPanel();
		}
				
		if (count($donors)) {
			echo $tabs->startPanel(JText::sprintf('COM_ZJ_DONATION_DONORS_COUNT', count($donors)), 'zj_donors');
			?>
			<div class="zj_campaign_donors">
				<table width="100%" cellspacing="0" cellpadding="0" border="0" class="zj_table">
					<thead>
						<tr align="left" class="zj_table-header">
							<th width="1" align="center">
								#
							</th>
							<th width="25%" align="center"><?php echo JText::_('COM_ZJ_DONATION_DONNER_NAME'); ?></th>
							<th width="15%" align="center"><?php echo JText::_('COM_ZJ_DONATION_DONNER_AMOUNT'); ?></th>
							<th width="10%" nowrap="nowrap" align="center"><?php echo JText::_('COM_ZJ_DONATION_DONNER_CITY'); ?></th>
							<th width="15%" nowrap="nowrap" align="center"><?php echo JText::_('COM_ZJ_DONATION_DONNER_COUNTRY'); ?></th>
							<th width="5%" align="center"><?php echo JText::_('COM_ZJ_DONATION_DONNER_RECURRING'); ?></th>
							<th align="center"><?php echo JText::_('COM_ZJ_DONATION_DONNER_DATE'); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php
						for ($i = 0, $n = count($donors); $i < $n; $i++) {
							$donor	= $donors[$i];
							?>
						<tr>
							<td>
								<?php echo $i+1; ?>
							</td>
							<td>
								<?php echo $donor->first_name. ' '. $donor->last_name;?>
							</td>
							<td>
								<?php echo ZJ_DonationUtils::formatPrice($donor->amount);?>
							</td>
							<td>
								<?php echo $donor->city;?>
							</td>
							<td>
								<?php echo $donor->country;?>
							</td>
							<td align="center">
								<?php //echo JHTML::_('zj_donation.icon', $donor->recurring);?>
								<?php echo ($donor->recurring) ? JText::_('COM_ZJ_DONATION_YES') : JText::_('COM_ZJ_DONATION_NO'); ?>
							</td>
							<td>
								<?php echo JHTML::_('date', $campaign->created_date, JText::_('DATE_FORMAT_LC2')); ?>
							</td>
						</tr>
							<?php
						}
						?>
					</tbody>
				</table>
			</div>
			<?php
			echo $tabs->endPanel();
		}
		echo $tabs->endPane();
		?>
	</div>
</div>