<?php
/**
 * @version		$Id$
 * @author		Joomseller
 * @package		Joomla!
 * @subpackage	ZJ_FileSeller
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 * This file may not be redistributed in whole or significant part.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.filesystem.file');
ZJ_DonationTemplate::addStyleSheet('nathan_style');

$count_campaigns = count($campaigns);
?>

<?php if ($params->get( 'show_page_title', 1 )) { ?>
<div class="zj_category-title">
	<h3 class="componentheading"><?php echo $params->get('page_title'); ?></h3>
</div>
<?php } ?>

<div class="zj_list-campaigns-blog">
	<?php if ($params->get('num_leading_campaigns', 0)) { ?>
	<div class="zj_campaign-leading">
		<div id="zj_campaigns">
			<ul>
				<?php
				$leading = $params->get('num_leading_campaigns');
				$leading = $leading <= $count_campaigns ? $leading : $count_campaigns;
				for ($i = 0; $i < $leading; $i++) {
					$row = &$campaigns[$i];

					$link					= ZJ_DonationRoute::_('index.php?option=com_zj_donation&view=campaign&id=' . (int) $row->id);
					$donate_form			= ZJ_DonationRoute::_('index.php?option=com_zj_donation&controller=donate&task=donation&id=' . (int) $row->id);
					?>
					<li class="zj_campaign">
						<div class="zj_thumb">
							<img src="<?php echo ZJ_DONATION_IMG; ?>?width=120&height=200&image=/images/<?php echo $row->image; ?>" alt="Image" />
						</div>
						<div class="zj_details">
							<h4>
								<a href="<?php echo $link; ?>">
									<?php echo $row->title; ?>
								</a>
							</h4>
							<dl class="zj_props">
								<div>
									<dt>
										<?php echo JText::_('COM_ZJ_DONATION_CAMPAIGN_START_DATE'); ?>:
									</dt>
									<dd>
										<?php echo JHTML::_('date', $row->published_up, JText::_('DATE_FORMAT_LC')); ?>
									</dd>
								</div>
								<div>
									<dt>
										<?php echo JText::_('COM_ZJ_DONATION_CAMPAIGN_END_DATE'); ?>:
									</dt>
									<dd>
										<?php echo JHTML::_('date', $row->published_down, JText::_('DATE_FORMAT_LC')); ?>
									</dd>
								</div>
								<div>
									<dt>
										<?php echo JText::_('COM_ZJ_DONATION_CAMPAIGN_GOAL_AMOUNT'); ?>:
									</dt>
									<dd>
										<strong class="price_entry"><?php echo ZJ_DonationUtils::formatPrice($row->goal, null, true); ?></strong>
									</dd>
								</div>
								<div class="zj_prop">
				<?php 
						$goalAmount = $row->goal;
						$donatedAmount = $row->donated_amount;
						$percent = 0;
						if($goalAmount != 0)
							$percent = ($donatedAmount * 100) / $goalAmount;
						$percent = round($percent);
					?>
					<dt>
						<div class="percentRaised fundRaising">
							
						</div> 
					</dt>
					<dd>
					
						<div class="fundRaisingMeter  meter">
							<div id="bar" style="width: <?php echo $percent ?>%;" >&nbsp;</div>
						</div>
						<span class="number">
								<?php echo $percent ?>%</span>&nbsp;&nbsp;raised&nbsp;&nbsp;
					</dd>
				</div>
							</dl>
							<div class="zj_desc">
								<?php echo $row->description; ?>
							</div>
							<div class="zj_buttons">
								<a class="zj_button" href="<?php echo $donate_form; ?>">
									<?php echo JText::_('COM_ZJ_DONATION_DONATE_BUTTON'); ?>
								</a>
								<a class="zj_button" href="<?php echo $link; ?>">
									<?php echo JText::_('COM_ZJ_DONATION_READMORE'); ?>
								</a>
							</div>
						</div>
					</li>
					<?php
				}
				?>
			</ul>
		</div>
	</div>
	<?php } ?>
	<?php
	$introstart		= $params->get('num_leading_campaigns', 0);
	$column			= $params->get('num_columns', 2);
	$column			= $column != 0 ? $column : 1;
	$column_width	= (int) (100 / $column) - 1;

	for ($i = $introstart; $i < $count_campaigns; $i++) {
		$row = &$campaigns[$i];

		$link			= ZJ_DonationRoute::_('index.php?option=com_zj_donation&view=campaign&id=' . (int) $row->id);
		$donate_form	= ZJ_DonationRoute::_('index.php?option=com_zj_donation&controller=donate&task=donation&id=' . (int) $row->id);
	?>
	<?php if (($i - $introstart) % $column == 0) { ?>
	<div class="zj_campaign-row zj_campaign-cols<?php echo $column; ?>">
	<?php } ?>
		<div class="zj_campaign-column column<?php echo ($i - $introstart) % $column + 1; ?>">
			<div class="zj_campaign-intro-content">
				<h4>
					<a href="<?php echo $link; ?>">
						<?php echo $row->title; ?>
					</a>
				</h4>
				<div class="zj_desc">
					<div class="zj_thumb">
						<img src="<?php echo ZJ_DONATION_IMG; ?>?width=80&height=120&image=/images/<?php echo $row->image; ?>" alt="Image" />
					</div>
					<?php echo $row->description; ?>
				</div>
				<div class="zj_buttons">
					<a class="zj_button" href="<?php echo $donate_form; ?>">
						<?php echo JText::_('COM_ZJ_DONATION_DONATE_BUTTON'); ?>
					</a>
					<a class="zj_button" href="<?php echo $link; ?>">
						<?php echo JText::_('COM_ZJ_DONATION_READMORE'); ?>
					</a>
				</div>
				<div class="zj_clear"></div>
			</div>
		</div>
	<?php if (($i - $introstart + 1) % $column == 0 || $i == $count_campaigns - 1) { ?>
		<div class="zj_clear"></div>
	</div>
	<?php } ?>
	<?php
	}
	?>
	<?php if ($params->get('show_pagination', 1)) { ?>
	<div id="zj_campaign-pagination">
		<?php echo $pagination->getPagesLinks(); ?>
	</div>
	<?php } ?>
</div>