<?php

/**
 * @package		checkout.php
 * @author 		Nathan
 * @copyright 	2012 NathanPhan	.
 * @license 	GNU/GPL v2 http://www.gnu.org/licenses/gpl-2.0.html
 *
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.html.pane');
$tabs = &JPane::getInstance();

// multibox
JHTML::_('behavior.mootools');
ZJ_DonationTemplate::addScript('slimbox');
ZJ_DonationTemplate::addStyleSheet('slimbox');
ZJ_DonationTemplate::addStyleSheet('nathan_style');

$js = '
window.addEvent(\'domready\', function(){
	var box = new MultiBox(\'zj_gallery_image\', {
		useOverlay: true,
		opac: \'0.7\',
		ocolor: \'#000\'
	});
});
';

$host		= JURI::base();

//$donate_form	= ZJ_DonationRoute::_('index.php?option=com_zj_donation&controller=donate&task=donation&id=' . (int) $row->id);
?>
<script type="text/javascript">
	function getTotal(){
		
	
	}
</script>
<form method="post" action="index.php" id="donationForm" name="donationForm" class="form-validate">
	<div class="zj_campaign-info">
		
			<div class="zj_campaign_donors" style="width: 100%">
				<table width="100%" cellspacing="0" cellpadding="0" border="0" class="zj_table">
					<thead>
						<tr align="left" class="zj_table-header">
							<th width="1" align="center">
								#
							</th>
							<th  align="left"><?php echo JText::_('COM_ZJ_DONATION_DONNER_NAME'); ?></th>
							
							<th width="50%" align="center"><?php echo JText::_('COM_ZJ_DONATION_DONATION_AMOUNT'); ?></th>
							
						</tr>
					</thead>
					<tbody>
					<?php
					$i = 0;
					foreach ($cart as $cart_key => $cart_value): //dump($cart_value);?>
						<tr>
							<td>
								<a href = "<?php echo JRoute::_('index.php?option=com_zj_donation&view=checkout&controller=donate&task=removeFromCart&iId='.$cart_key)?>"
										 onclick="return confirm('Really remove?')">
									<img src="<?php echo JRoute::_($host.'components/com_zj_donation/templates/default/images/b_cancel.png' )?>"
									alt="Click here to remove" />
								</a>
							</td>
							<td><?php echo $cart_value[$cart_key]->title?></td>
							<td>
								<span>
									<?php  //dump($cart_value['lists']['amount1'])  ; 
											echo $cart_value['lists']['amount1_'.$cart_key];?>
								</span>
								<?php if ($cart_value[$cart_key]->custom_amount) { ?>
									<span>
										&nbsp;&nbsp;<?php echo JText::_('COM_ZJ_DONATION_ENTER_AMOUNT');?>
									</span>
									<span>
										<?php echo $config->get('currency_sign');?>
										<input type="text" class="inputbox" size="6" name="amount2_<?php echo $cart_key?>"  id="amount2_<?php echo $cart_key?>" onBlur="" />
									</span>
								<?php } ?>
							</td>
						</tr>
						<?php endforeach;?>
						<tr style="border: 0px">
							<td colspan="3" align="right">&nbsp;</td>
							
						</tr>
						
							
						</tr>
						 <tr>
							<td colspan="2" align="right">Optional Operation Costs <?php echo $config->get('currency_sign');?>: </td>
							<td >
								<input type="text" name="oper_cost" id="oper_cost" class="inputbox" size="10" value="5" />
							</td>
						</tr> 
						
						<tr style="border: 0px">
							<td colspan="3" align="right">&nbsp;</td>
							
						</tr>
						
						<!-- <tr>
							<td colspan="2" align="right">Total Donation Amount</td>
							<td >
								<input type="text" name="total" id="total" class="inputbox" size="10" readonly="readonly"/>
							</td>
						</tr> -->
					</tbody>
				</table>
				<br />
				<div class="cart-button cB">
					<a href="<?php echo JRoute::_('index.php?option=com_zj_donation&view=campaigns&layout=table')?>"
									 class="button bigger">
						More Campaigns
					</a>				
				</div>
			</div>
			
	</div>
	
	<?php
	$this->load('donate.form');
	
	if (count($fields)) {
		$this->load('donate.fields');
	}
	
	$this->load('donate.payment');
	?>
	<p>
		<button class="button validate" type="submit"><?php echo JText::_('COM_ZJ_DONATION_DONATE_NOW'); ?></button>
	</p>
	<input type="hidden" name="option" value="com_zj_donation" />
	<input type="hidden" name="controller" value="donate" />
	<input type="hidden" name="task" value="confirm_donation" />
	
	<?php echo JHTML::_('form.token'); ?>
</form>

