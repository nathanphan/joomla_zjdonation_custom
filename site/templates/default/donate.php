<?php

/**
 * @version		$Id$
 * @author		Joomseller
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 * This file may not be redistributed in whole or significant part.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
// Load the form validation behavior
JHTML::_('behavior.formvalidation');

$link	= ZJ_DonationRoute::_('index.php?option=com_zj_donation&controller=donate&task=confirm_donation&id='.$row->id);
?>
<script type="text/javascript">
<!--
	Window.onDomReady(function(){
		document.formvalidator.setHandler('emailverify', function (value) { return ($('email').value == value); }	);
	});
// -->
</script>

<table cellspacing="5" cellpadding="5" border="0" width="100%">
	<tr>
		<td colspan="2">
			<h3 class="donate-form-title">
				<?php echo JText::_('COM_ZJ_DONATION_CAMPAIGN_INFORMATION');?>
			</h3>
		</td>
	</tr>
	<tr>
		<td class="title_cell" align="right">
			<?php echo JText::_('COM_ZJ_DONATION_CAMPAIGN'); ?>:
		</td>
		<td><?php echo $row->title;?></td>
	</tr>
	<tr>
		<td class="title_cell">
			<?php echo JText::_('COM_ZJ_DONATION_CAMPAIGN_START_DATE'); ?>:
		</td>
		<td>
			<?php echo JHTML::_('date', $row->published_up, JText::_('DATE_FORMAT_LC')); ?>
		</td>
	</tr>
	<tr>
		<td class="title_cell">
			<?php echo JText::_('COM_ZJ_DONATION_CAMPAIGN_END_DATE'); ?>:
		</td>
		<td>
			<?php echo JHTML::_('date', $row->published_down, JText::_('DATE_FORMAT_LC')); ?>
		</td>
	</tr>
	<tr>
		<td class="title_cell">
			<?php echo JText::_('COM_ZJ_DONATION_CAMPAIGN_GOAL_AMOUNT'); ?>:
		</td>
		<td><?php echo ZJ_DonationUtils::formatPrice($row->goal);?></td>
	</tr>
	<tr>
		<td class="title_cell">
			<?php echo JText::_('COM_ZJ_DONATION_CAMPAIGN_DONATED_AMOUNT'); ?>:
		</td>
		<td>
			<?php echo ZJ_DonationUtils::formatPrice($row->donated_amount, null, true); ?>
		</td>
	</tr>
</table>

<form method="post" action="index.php" id="donationForm" name="donationForm" class="form-validate">
	<?php
	$this->load('donate.form');
	
	if (count($fields)) {
		$this->load('donate.fields');
	}
	
	$this->load('donate.payment');
	?>
	<p>
		<button class="button validate" type="submit"><?php echo JText::_('COM_ZJ_DONATION_DONATE_NOW'); ?></button>
	</p>
	<input type="hidden" name="option" value="com_zj_donation" />
	<input type="hidden" name="controller" value="donate" />
	<input type="hidden" name="task" value="confirm_donation" />
	<input type="hidden" name="id" value="<?php echo $row->id;?>" />
	<?php echo JHTML::_('form.token'); ?>
</form>