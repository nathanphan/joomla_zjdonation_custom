<?php
/**
 * @version		$Id$
 * @author		Joomseller
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 * This file may not be redistributed in whole or significant part.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.filesystem.file');
?>

<?php if ($params->get( 'show_page_title', 1 )) { ?>
<div class="zj_category-title">
	<h3 class="componentheading"><?php echo $params->get('page_title'); ?></h3>
</div>
<?php } ?>

<div class="zj_category-mydonations-table">
	<table width="100%" class="zj_table" cellpadding="0" cellspacing="0" border="0">
		<thead>
			<tr align="left" class="zj_table-header">
				<th width="5">
				</th>
				<th>
					<?php echo JText::_('COM_ZJ_DONATION_CAMPAIGN_INFORMATION'); ?>
				</th>
				<th>
					<?php echo JText::_('COM_ZJ_DONATION_DONNER_AMOUNT'); ?>
				</th>
				<th>
					<?php echo JText::_('COM_ZJ_DONATION_DONNER_RECURRING'); ?>
				</th>
				<th>
					<?php echo JText::_('COM_ZJ_DONATION_DONATED'); ?>
				</th>
				<th>
					<?php echo JText::_('COM_ZJ_DONATION_SUBSCRIBE_ID'); ?>
				</th>
				<th>
					<?php echo JText::_('COM_ZJ_DONATION_DONNER_PAYMENT_METHOD'); ?>
				</th>
				<th>
					<?php echo JText::_('COM_ZJ_DONATION_DATE'); ?>
				</th>
				<th width="10%" align="center" nowrap="nowrap">
					<?php echo JText::_('COM_ZJ_DONATION_DETAILS'); ?>
				</th>
			</tr>
		</thead>
		<tbody>
		<?php
		if (!count($rows)) {
		?>
			<tr>
				<td colspan="9">
					<?php echo JText::_('COM_ZJ_DONATION_MY_DONATIONS_EMPTY'); ?>
				</td>
			</tr>
		<?php
		} else {
			$limitstart	= JRequest::getInt('limitstart', 0);
			$k			= 0;
			for ($i = 0, $n = count($rows); $i < $n; $i++) {
				$row			= &$rows[$i];
				$link			= ZJ_DonationRoute::_('index.php?option=com_zj_donation&view=campaign&id=' . (int) $row->campaign_id);
				$mydonation		= ZJ_DonationRoute::_('index.php?option=com_zj_donation&view=mydonation&id=' . (int) $row->id);
				?>
				<tr class="zj_table-entry<?php echo $k + 1; ?>" valign="top">
					<td>
						<?php echo $i + $limitstart + 1; ?>
					</td>
					<td>
						<a href="<?php echo $link; ?>">
							<?php echo $row->title; ?>
						</a>
						<br/>
						<?php echo '<strong>'.JText::_('COM_ZJ_DONATION_CAMPAIGN_GOAL'). ':</strong>: '. ZJ_DonationUtils::formatPrice($row->goal);?>
						<br/>
						<?php echo '<strong>'.JText::_('COM_ZJ_DONATION_CAMPAIGN_DONATED_AMOUNT'). ':</strong> '. ZJ_DonationUtils::formatPrice($row->donated_amount);?>
						<br/>
						<?php echo '<strong>'.JText::_('COM_ZJ_DONATION_DONORS'). ':</strong> '. $row->donors;?>
					</td>
					<td>
						<?php echo ZJ_DonationUtils::formatPrice($row->amount); ?>
					</td>
					<td>
						<?php echo ($row->recurring) ? $row->recurring_title : JText::_('COM_ZJ_DONATION_RECURRING_ONE_TIME'); ?>
					</td>
					<td align="center">
						<?php $recurring_times	= ($row->recurring_times) ? $row->recurring_times : JText::_('COM_ZJ_DONATION_RECURRING_UNLIMITED'); ?>
						<?php echo ($row->recurring) ? $row->donated_times . '/'. $recurring_times : '1'; ?>
					</td>
					<td>
						<?php echo $row->subscribe_id; ?>
					</td>
					<td align="center">
						<?php echo strtoupper($row->payment_method); ?>
					</td>
					<td>
						<?php echo JHTML::_('date', $row->created_date, JText::_('DATE_FORMAT_LC4')); ?>
					</td>
					<td align="center" nowrap="nowrap">
						<a href="<?php echo $mydonation; ?>" title="<?php echo JText::_('COM_ZJ_DONATION_VIEW_DONATION'); ?>">
							<?php echo JText::_('COM_ZJ_DONATION_VIEW_DONATION'); ?>
						</a>
					</td>
				</tr>
				<?php
				$k = 1 - $k;
			}
			?>
		</tbody>
		<?php
		}
		?>
	</table>
	<?php if ($params->get('show_pagination', 1)) { ?>
	<div id="zj_mydonation-pagination">
		<?php echo $pagination->getPagesLinks(); ?>
	</div>
	<?php } ?>
</div>