<?php
/**
 * @version		$Id$
 * @author		Joomseller!
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
?>
<table cellspacing="5" cellpadding="5" border="0" width="100%">
	<tr>
		<td colspan="2">
			<h3 class="donate-form-title">
				<?php echo JText::_('COM_ZJ_DONATION_ADDITION_INFORMATION');?>
			</h3>
		</td>
	</tr>
	<?php echo ZJ_DonationFields::renderCustomFields($fields); ?>
</table>