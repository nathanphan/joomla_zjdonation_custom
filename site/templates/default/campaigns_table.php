<?php
/**
 * @version		$Id$
 * @author		Joomseller
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 * This file may not be redistributed in whole or significant part.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
ZJ_DonationTemplate::addStyleSheet('nathan_style');
jimport('joomla.filesystem.file');
?>

<?php if ($params->get( 'show_page_title', 1 )) { ?>
<div class="zj_category-title">
	<h3 class="componentheading"><?php echo $params->get('page_title'); ?></h3>
</div>
<?php } ?>

<div class="zj_category-campaigns-table">
	<table width="100%" class="zj_table" cellpadding="0" cellspacing="0" border="0">
		<thead>
			<tr align="left" class="zj_table-header">
				<th width="70px">
				</th>
				<th>
					<?php echo JText::_('COM_ZJ_DONATION_CAMPAIGN_TITLE'); ?>
				</th>
				<th>
					<?php echo JText::_('COM_ZJ_DONATION_CAMPAIGN_START_DATE'); ?>
				</th>
				<th>
					<?php echo JText::_('COM_ZJ_DONATION_CAMPAIGN_END_DATE'); ?>
				</th>
				<th>
					<?php echo JText::_('COM_ZJ_DONATION_CAMPAIGN_GOAL_AMOUNT'); ?>
				</th>
				<th>
					<?php echo JText::_('COM_ZJ_DONATION_CAMPAIGN_DONATED_AMOUNT'); ?>
				</th>
				<th width="10%" align="center" nowrap="nowrap">
					<?php echo JText::_('COM_ZJ_DONATION_OPERATIONS'); ?>
				</th>
			</tr>
		</thead>
		<tbody>
		<?php
		if (!count($campaigns)) {
		?>
			<tr>
				<td colspan="4">
					<?php echo JText::_('COM_ZJ_DONATION_CAMPAIGN_EMPTY'); ?>
				</td>
			</tr>
		<?php
		} else {
			$k = 0;
			$session 		= JFactory::getSession();
			$cart			= $session->get('DCart');
			for ($i = 0, $n = count($campaigns); $i < $n; $i++) {
				$row			= &$campaigns[$i];
				$link			= ZJ_DonationRoute::_('index.php?option=com_zj_donation&view=campaign&id=' . (int) $row->id);
				
				$donate_form			= '';
			
			if(isset($cart) && !empty($cart))
			{
				$flag = false;
				foreach ($cart as $cart_key => $cart_value){
					if($cart_key == $row->id){
						$flag = true;
					}
				}
				if($flag) 
					$donate_form			= ZJ_DonationRoute::_('index.php?option=com_zj_donation&controller=donate&task=checkout&id=' . (int) $row->id);
				else 
					$donate_form			= ZJ_DonationRoute::_('index.php?option=com_zj_donation&controller=donate&task=donation&id=' . (int) $row->id);
			}
			else{
				$donate_form			= ZJ_DonationRoute::_('index.php?option=com_zj_donation&controller=donate&task=donation&id=' . (int) $row->id);
			}
				?>
				<tr class="zj_table-entry<?php echo $k + 1; ?>" valign="top">
					<td>
						<a href="<?php echo $link; ?>">
							<img src="<?php echo ZJ_DONATION_IMG; ?>?width=75&height=100&image=/images/<?php echo $row->image; ?>" alt="Image" />
						</a>
					</td>
					<td>
						<a href="<?php echo $link; ?>">
							<?php echo $row->title; ?>
						</a>
						<br/>
						<?php echo strip_tags($row->description); ?>
					</td>
					<td>
						<?php echo JHTML::_('date', $row->published_up, JText::_('DATE_FORMAT_LC4')); ?>
					</td>
					<td>
						<?php echo JHTML::_('date', $row->published_down, JText::_('DATE_FORMAT_LC4')); ?>
					</td>
					<td align="center">
						<strong class="price_entry"><?php echo ZJ_DonationUtils::formatPrice($row->goal, null, true); ?></strong>
						<div class="zj_prop">
				<?php 
						$goalAmount = $row->goal;
						$donatedAmount = $row->donated_amount;
						$percent = 0;
						if($goalAmount != 0)
							$percent = ($donatedAmount * 100) / $goalAmount;
						$percent = round($percent);
					?>
					
						<div class="percentRaised fundRaising">
							<span class="number">
								<?php echo $percent ?>%</span>&nbsp;&nbsp;raised&nbsp;&nbsp;
						</div> 
					
					
						<div class="fundRaisingMeter  meter">
							<div id="bar" style="width: <?php echo $percent ?>%;" >&nbsp;</div>
						</div>
					
					</td>
					<td align="center">
						<strong class="price_entry"><?php echo ZJ_DonationUtils::formatPrice($row->donated_amount, null, true); ?></strong>
					</td>
					<td align="center" nowrap="nowrap">
						<a href="<?php echo $donate_form; ?>" title="<?php echo JText::_('COM_ZJ_DONATION_DONATE_THIS_CAMPAIGN'); ?>">
							<img src="<?php echo $live_site; ?>components/com_zj_donation/templates/default/images/donate.png" alt="<?php echo JText::_('COM_ZJ_DONATION_DONATE_BUTTON'); ?>" />
						</a>
						<br />
						
						<a href="<?php echo $donate_form; ?>" title="<?php echo JText::_('COM_ZJ_DONATION_DONATE_THIS_CAMPAIGN'); ?>" class="zj_button" >
							
							<?php 
									if($flag) echo JText::_('Checkout');
									else echo 'Donate';
								
							 ?>
						</a>
						
					</td>
				</tr>
				<?php
				$k = 1 - $k;
			}
			?>
		</tbody>
		<?php
		}
		?>
	</table>
	<?php if ($params->get('show_pagination', 1)) { ?>
	<div id="zj_campaign-pagination">
		<?php echo $pagination->getPagesLinks(); ?>
	</div>
	<?php } ?>
</div>