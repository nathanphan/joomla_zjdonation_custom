<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

$db			= JFactory::getDBO();
$nullDate	= $db->getNullDate();

?>
<style type="text/css">
	table.mydonation td.title_cell {
		width: 30%;
	}
</style>
<div class="zj_donation-title">
	<h1 class="componentheading"><?php echo $params->get('page_title'); ?></h1>
</div>

<table cellspacing="0" cellpadding="0" border="0" width="100%" class="mydonation">
	<tr>
		<td valign="top" width="45%" align="left">
				<h3><?php echo JText::_('COM_ZJ_DONATION_DONOR_INFORMATION'); ?></h3>
				<table cellpadding="2" cellspacing="2" border="0" width="100%">
					<tbody>
						<tr>
							<td class="title_cell">
								<?php echo JText::_('COM_ZJ_DONATION_FIRST_NAME'); ?>
							</td>
							<td>
								<?php echo $row->first_name; ?>
							</td>
						</tr>
						<tr>
							<td class="title_cell">
								<?php echo JText::_('COM_ZJ_DONATION_LAST_NAME'); ?>
							</td>
							<td>
								<?php echo $row->last_name; ?>
							</td>
						</tr>
						<tr>
							<td class="title_cell">
								<?php echo JText::_('COM_ZJ_DONATION_EMAIL'); ?>
							</td>
							<td>
								<?php echo $row->email; ?>
							</td>
						</tr>
						<tr>
							<td class="title_cell">
								<?php echo JText::_('COM_ZJ_DONATION_ORGANIZATION'); ?>
							</td>
							<td>
								<?php echo $row->organization; ?>
							</td>
						</tr>
						<tr>
							<td class="title_cell">
								<?php echo JText::_('COM_ZJ_DONATION_WEBSITE'); ?>
							</td>
							<td>
								<?php echo $row->website; ?>
							</td>
						</tr>
						<tr>
							<td class="title_cell">
								<?php echo JText::_('COM_ZJ_DONATION_ADDRESS'); ?>
							</td>
							<td>
								<?php echo $row->address; ?>
							</td>
						</tr>
						<tr>
							<td class="title_cell">
								<?php echo JText::_('COM_ZJ_DONATION_CITY'); ?>
							</td>
							<td>
								<?php echo $row->city; ?>
							</td>
						</tr>
						<tr>
							<td class="title_cell">
								<?php echo JText::_('COM_ZJ_DONATION_STATE'); ?>
							</td>
							<td>
								<?php echo $row->state; ?>
							</td>
						</tr>
						<tr>
							<td class="title_cell">
								<?php echo JText::_('COM_ZJ_DONATION_ZIPCODE'); ?>
							</td>
							<td>
								<?php echo $row->zip; ?>
							</td>
						</tr>
						<tr>
							<td class="title_cell">
								<?php echo JText::_('COM_ZJ_DONATION_COUNTRY'); ?>
							</td>
							<td>
								<?php echo $row->country; ?>
							</td>
						</tr>
						<tr>
							<td class="title_cell">
								<?php echo JText::_('COM_ZJ_DONATION_PHONE_NUMBER'); ?>
							</td>
							<td>
								<?php echo $row->phone; ?>
							</td>
						</tr>
					</tbody>
				</table>
			<?php
			if (count($fields)) {
				?>
				<h3><?php echo JText::_('COM_ZJ_DONATION_ADDITION_INFORMATION'); ?></h3>
				<table cellpadding="2" cellspacing="2" border="0" width="100%">
					<tbody>
						<?php
						for ($i = 0, $n = count($fields); $i < $n; $i++) {
							$field	= $fields[$i];
							?>
							<tr>
								<td class="title_cell">
									<?php echo $field->title; ?>
								</td>
								<td>
									<?php echo $field->field_value; ?>
								</td>
							</tr>
							<?php
						}
						?>
					</tbody>
				</table>
				<?php
			}
			?>
		</td>
		<td width="45%" valign="top">
			<h3><?php echo JText::_('COM_ZJ_DONATION_DONATION_INFORMATION'); ?></h3>
			<table cellpadding="2" cellspacing="2" border="0" width="100%">
				<tbody>
					<tr>
						<td class="title_cell">
							<?php echo JText::_('COM_ZJ_DONATION_ID'); ?>
						</td>
						<td>
							<?php echo $row->id; ?>
						</td>
					</tr>
					<tr>
						<td class="title_cell">
							<?php echo JText::_('COM_ZJ_DONATION_USER_NAME'); ?>
						</td>
						<td>
							<?php echo $row->username; ?>
						</td>
					</tr>
					<tr>
						<td class="title_cell">
							<?php echo JText::_('COM_ZJ_DONATION_DONATION_AMOUNT'); ?>
						</td>
						<td>
							<?php echo ZJ_DonationUtils::formatPrice($row->amount); ?>
						</td>
					</tr>
					<tr>
						<td class="title_cell">
							<?php echo JText::_('COM_ZJ_DONATION_PAYMENT_METHOD'); ?>
						</td>
						<td>
							<?php echo $row->payment_method; ?>
						</td>
					</tr>
					<tr>
						<td class="title_cell">
							<?php echo JText::_('COM_ZJ_DONATION_TRANSACTION_ID'); ?>
						</td>
						<td>
							<?php echo $row->transaction_id; ?>
						</td>
					</tr>
					<tr>
						<td class="title_cell">
							<?php echo JText::_('COM_ZJ_DONATION_PUBLISHED'); ?>
						</td>
						<td style="color: red;">
							<?php echo $row->published ? JText::_('COM_ZJ_DONATION_YES') : JText::_('COM_ZJ_DONATION_NO'); ?>
						</td>
					</tr>
					<tr>
						<td class="title_cell">
							<?php echo JText::_('COM_ZJ_DONATION_CREATED_DATE'); ?>
						</td>
						<td>
							<?php echo JHTML::_('date', $row->created_date, JText::_('DATE_FORMAT_LC2')); ?>
						</td>
					</tr>
					<tr>
						<td class="title_cell">
							<?php echo JText::_('COM_ZJ_DONATION_PAYMENT_DATE'); ?>
						</td>
						<td>
							<?php echo $row->payment_date == $nullDate ? JText::_('COM_ZJ_DONATION_NEVER') : JHTML::_('date', $row->payment_date, JText::_('DATE_FORMAT_LC2')); ?>
						</td>
					</tr>
					<tr>
						<td class="title_cell">
							<?php echo JText::_('COM_ZJ_DONATION_DONATION_TYPE'); ?>
						</td>
						<td>
							<?php echo $row->recurring ? $row->recurring_title : JText::_('COM_ZJ_DONATION_RECURRING_ONE_TIME'); ?>
						</td>
					</tr>
					<tr>
						<td class="title_cell">
							<?php echo JText::_('COM_ZJ_DONATION_RECURRING_TIMES'); ?>
						</td>
						<td>
							<?php echo ($row->recurring) ? (int)$row->donated_times.'/'.(int)$row->recurring_times : '0'; ?>
						</td>
					</tr>
				</tbody>
			</table>
			<h3><?php echo JText::_('COM_ZJ_DONATION_CAMPAIGN_INFORMATION'); ?></h3>
			<table cellpadding="2" cellspacing="2" border="0" width="100%">
				<thead>
					<tr class="title">
						<th width="1">
							<?php echo JText::_('ID'); ?>
						</th>
						<th class="title">
							<?php echo JText::_('COM_ZJ_DONATION_CAMPAIGN_TITLE'); ?>
						</th>
						<th class="title" width="20%">
							<?php echo JText::_('COM_ZJ_DONATION_CAMPAIGN_GOAL'); ?>
						</th>
						<th class="title" width="20%">
							<?php echo JText::_('COM_ZJ_DONATION_CAMPAIGN_DONATED_AMOUNT'); ?>
						</th>
						<th class="title">
							<?php echo JText::_('COM_ZJ_DONATION_DONORS'); ?>
						</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$campaign_link = ZJ_DonationRoute::_('index.php?option=com_zj_donation&view=campaign&id=' . $campaign->id);
					?>
					<tr>
						<td>
							<?php echo $campaign->id; ?>
						</td>
						<td>
							<a href="<?php echo $campaign_link ; ?>">
								<?php echo $campaign->title; ?>
							</a>
						</td>
						<td align="center">
							<?php echo ZJ_DonationUtils::formatPrice($campaign->goal); ?>
						</td>
						<td align="center">
							<?php echo ZJ_DonationUtils::formatPrice($campaign->donated_amount); ?>
						</td>
						<td align="center">
							<?php echo $campaign->donors; ?>
						</td>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>
</table>
