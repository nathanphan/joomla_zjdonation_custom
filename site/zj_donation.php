<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

require_once(JPATH_COMPONENT.DS.'defines.php');

// add assets
ZJ_DonationTemplate::addStyleSheet('zj_donation');
ZJ_DonationTemplate::addScript('zj_donation');

// set the table directory
JTable::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR.DS.'tables');
JHTML::addIncludePath(JPATH_COMPONENT.DS.'helpers');

// require the base controller
require_once(JPATH_COMPONENT.DS.'controller.php');

// require specific controller
$controller = JRequest::getWord('controller', 'default');
if ($controller) {
	$path = JPATH_COMPONENT.DS.'controllers'.DS.$controller.'.php';
	if (file_exists($path)) {
		require_once($path);
	} else {
		$controller = '';
	}
}

// create the controller
$classname	= 'ZJ_DonationController' . ucfirst($controller);
$controller	= new $classname();

// perform the request task
$controller->execute(JRequest::getVar('task', null, 'default', 'cmd'));

// redirect if set by the controller
$controller->redirect();
