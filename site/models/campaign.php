<?php
/**
 * @version		$Id$
 * @author		Joomseller
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 * This file may not be redistributed in whole or significant part.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * ZJ_Donation Component - Campaign Model
 * @package		ZJ_Donation
 * @subpackage	Model
 */
class ZJ_DonationModelCampaign extends JModel {
	/** @var int Campaign id */
	var $_id					= null;
	/** @var object Campaign object */
	var $_data					= null;
	/** @var object Donors object */
	var $_donors				= null;

	/**
	 * Constructor.
	 */
	function __construct() {
		parent::__construct();

		// get default campaign id
		$this->_id = JRequest::getInt('id', 0);
	}

	/****************************************************************
	 * GET DATA FOR PERSENTATION.
	 ****************************************************************/

	/**
	 * Get campaign data.
	 */
	function getData($id = null) {
		
		$id = isset($id) ? $id : $this->_id;
		
 		if (empty($this->_data)) {
			$query = 'SELECT a.*, sum(d.amount) AS `donated_amount`, count(d.id) AS `donors`'
				. ' FROM #__zj_donation_campaigns AS a'
				. ' LEFT JOIN #__zj_donation_donates AS d ON d.campaign_id = a.id AND d.donated = 1'
				. ' WHERE a.id = ' . $id
				. ' GROUP BY a.id'
			;
			$this->_db->setQuery($query);
			$this->_data = $this->_db->loadObject();
 		}
		// set error if no campaign found
		if (empty($this->_data)) {
			$this->setError('COM_ZJ_DONATION_CAMPAIGN_NOT_FOUND');
		}
		
		return $this->_data;
	}
	
	/**
	* Get campaign data for checkout.
	*/
	function getData4Checkout($id = null) {
	
		$id = isset($id) ? $id : $this->_id;
		//dump($id,'id in model');
	
	
		// 		if (empty($this->_data)) {
		$query = 'SELECT a.*, sum(d.amount) AS `donated_amount`, count(d.id) AS `donors`'
		. ' FROM #__zj_donation_campaigns AS a'
		. ' LEFT JOIN #__zj_donation_donates AS d ON d.campaign_id = a.id AND d.donated = 1'
		. ' WHERE a.id = ' . $id
		. ' GROUP BY a.id'
		;
		$this->_db->setQuery($query);
		$this->_data = $this->_db->loadObject();
		// 		}
		// set error if no campaign found
		if (empty($this->_data)) {
			$this->setError('COM_ZJ_DONATION_CAMPAIGN_NOT_FOUND');
		}
	
		//dump($this->_data,'data');
	
		return $this->_data;
	}

	/**
	 * Get donors data
	 */
	function getDonors($id = null) {
		$id = isset($id) ? $id : $this->_id;
		if (empty($this->_donors)) {
			$query 	= ' SELECT a.*'
					. ' FROM #__zj_donation_donates AS a'
					. ' WHERE a.campaign_id = ' . $id
					. ' AND a.donated = 1'
					. ' AND a.access = 1'
					. ' ORDER BY a.amount DESC'
					;
			$this->_db->setQuery($query);
			$this->_donors = $this->_db->loadObjectList();
		}
		
		return $this->_donors;
	}
	
	function getCount($id = null) {
		$id 	= isset($id) ? $id : $this->_id;		
		$query 	= ' SELECT COUNT(*)'
				. ' FROM #__zj_donation_donates AS a'
				. ' WHERE a.campaign_id = ' . $id
				. ' AND a.donated = 1'				
				;
				
		$this->_db->setQuery($query);		
		
		return $this->_db->loadResult();
	}
}