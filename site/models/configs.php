<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

/**
 * ZJ_Donation Component - Configs Model.
 * @package		ZJ_Donation
 * @subpackage	Model
 */

class ZJ_DonationModelConfigs extends JModel {
	/** @var array {table} data array */
	var $_data					= null;
	/** @var object */
	var $_community_components	= null;
	/** @var object */
	var $_donate_user_types		= null;

	/**
	 * Constructor
	 */
	function __construct() {
		parent::__construct();
	}

	/**
	 * Logic for the settings edit screen.
	 * @access public
	 * @return array
	 */
	function getData() {
		// let load the content if it doesn't already exist
		if (empty($this->_data)) {
			$query = 'SELECT *'
				. ' FROM #__zj_donation_configs'
			;
			$this->_db->setQuery($query);
			$rows = $this->_db->loadObjectList();

			$config = new JObject();

			for ($i = 0, $n = count($rows); $i < $n; $i++) {
				$row	= $rows[$i];
				$key	= $row->key;
				$value	= $row->value;

				$config->set($key, $value);
			}

			// get currency config
			$query = 'SELECT a.*'
				. ' FROM #__zj_donation_currencies AS a'
				. ' WHERE a.id = ' . (int) $config->get('currency')
				. ' UNION (SELECT a.*'
				. ' FROM #__zj_donation_currencies AS a'
				. ' ORDER BY a.ordering'
				. ' LIMIT 1)'
			;
			$this->_db->setQuery($query);
			$row = $this->_db->loadObject();

			$config->set('currency_code', $row->code);
			$config->set('currency_sign', $row->sign);
			$config->set('currency_position', $row->position);

			//
			$this->_data = $config;
		}
		return $this->_data;
	}
}