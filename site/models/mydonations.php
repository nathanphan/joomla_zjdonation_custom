<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

/**
 * ZJ_Donation Component - MyDonations Model
 * @package		ZJ_Donation
 * @subpackage	Model
 */
class ZJ_DonationModelMyDonations extends JModel {

	/** @var int Total of documents */
	var $_total				= null;
	/** @var object Pagination object */
	var $_pagination		= null;
	/** @var string */
	var $nullDate			= null;
	/** @var string */
	var $now				= null;
	/** @var int */
	var $_id				= null;
	/** @var array items data */
	var $_data				= null;

	/**
	 * Constructor.
	 */
	function __construct() {
		global $mainframe, $option;
		parent::__construct();

		$id	= JRequest::getInt('id', 0);
		$this->setId($id);
	}

	function setId($id) {
		$this->_id		= $id;
		$this->_data	= null;
	}


	function getData() {
		if (empty($this->_data)) {
			$limit		= JRequest::getVar('limit', 0, '', 'int');
			$limitstart	= JRequest::getVar('limitstart', 0, '', 'int');

			$this->setState('limit', $limit);
			$this->setState('limitstart', $limitstart);

			$query = $this->_buildQuery();
			$this->_data = $this->_getList($query, $limitstart, $limit);
		}
		return $this->_data;
	}

	function getTotal() {
		if (empty($this->_total)) {
			$query = $this->_buildQuery();
			$this->_total = $this->_getListCount($query);
		}
		return $this->_total;
	}

	function getPagination() {
		if (empty($this->_pagination)) {
			jimport('joomla.html.pagination');
			$this->_pagination = new JPagination( $this->getTotal(), $this->getState('limitstart'), $this->getState('limit') );
		}
		return $this->_pagination;
	}


	function _buildQuery(){
		global $mainframe;
		// get the WHERE and ORDER BY clauses for the query
		$where		= $this->_buildQueryWhere();
		$orderby	= $this->_buildQueryOrderBy();

		$query	= 'SELECT a.*, c.title, c.goal, SUM(d.amount) AS `donated_amount`, count(d.id) AS `donors`, r.title AS `recurring_title`'
				. ' FROM #__zj_donation_donates as a'
				. ' INNER JOIN #__zj_donation_campaigns AS c ON c.id = a.campaign_id'
				. ' INNER JOIN #__users AS u ON u.id = a.user_id'
				. ' LEFT JOIN #__zj_donation_donates AS d ON d.campaign_id = c.id AND d.donated = 1'
				. ' LEFT JOIN #__zj_donation_recurring_types AS r ON r.id = a.recurring_id'
				. $where
				. ' GROUP BY a.id, c.id'
				. $orderby
				;
		return $query;
	}

	function _buildQueryWhere() {
		global $mainframe, $option;
		$user		= JFactory::getUser();
		
		$where		= array();
		$where[]	= 'a.donated = 1';
		$where[]	= 'a.user_id = '. (int)$user->get('id');
		// build the WHERE string
		$where		= count($where) ? ' WHERE ' . implode(' AND ', $where) : '';

		return $where;
	}

	function _buildQueryOrderBy() {
		global $mainframe, $option;
		$params = &$mainframe->getParams();
		$order_by	= ' ORDER BY ';
		$order		= JRequest::getCmd('orderby_sec',$params->get('orderby_sec'));
		switch ($order) {
			case 'date':
				$order_by .= 'a.created_date';
				break;
			case 'rdate':
				$order_by .= 'a.created_date DESC';
				break;
				break;
			case 'amount':
				$order_by .= 'a.amount';
				break;
			case 'ramount':
				$order_by .= 'a.amount DESC';
				break;
			default:
				$order_by .= 'a.created_date DESC';
		}
		return $order_by;
	}
}