<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

/**
 * ZJ_Donation Component - MyDonation Model.
 * @package		ZJ_Donation
 * @subpackage	Model
 */
class ZJ_DonationModelMyDonation extends JModel {
	/** @var int Id of donate */
	var $_id				= null;
	/** @var object donate object */
	var $_data				= null;
	/** @var object fields object */
	var $_fields			= null;

	/**
	 * Constructor.
	 */
	function __construct() {
		// call parent constructor
		parent::__construct();

		$id		= JRequest::getInt('id', 0);
		$this->setId($id);
	}

	/****************************************************************
	 * GET DATA FOR PRESENTATION.
	 ***************************************************************/

	/**
	 * Set donate id.
	 */
	function setId($id) {
		if (!isset($this->_id) || $this->_id != $id) {
			$this->_id		= (int) $id;
			$this->_data	= null;
		}
	}

	/**
	 * Get or init donate for edit screen.
	 */
	function getData() {
		$this->_loadData();

		return $this->_data;
	}

	/**
	 * Load donate data.
	 */
	function _loadData() {
		if (empty($this->_data)) {
			$user	= JFactory::getUser();
			$query	= 'SELECT a.*, u.username AS username, b.title AS `recurring_title`'
					. ' FROM #__zj_donation_donates AS a'
					. ' INNER JOIN #__users AS u ON a.user_id = u.id'
					. ' LEFT JOIN #__zj_donation_recurring_types AS b ON b.id = a.recurring_id'
					. ' WHERE a.id = ' . $this->_id. ' AND a.donated = 1 AND u.id = '. $user->get('id')
					;
			$this->_db->setQuery($query);
			$this->_data = $this->_db->loadObject();

			return (boolean) $this->_data;
		}

		return true;
	}

	/**
	 * Load extra fields
	 */
	function getFields() {
		if (empty($this->_fields)) {
			$query = 'SELECT a.field_value, b.title'
				. ' FROM #__zj_donation_field_values AS a'
				. ' INNER JOIN #__zj_donation_fields AS b ON a.field_id = b.id'
				. ' WHERE a.donate_id = ' . $this->_id
			;
			$this->_db->setQuery($query);
			$this->_fields = $this->_db->loadObjectList();

			return $this->_fields;
		}

		return true;
	}
}