<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

/**
 * ZJ_Donation Component - Campaigns Model
 * @package		ZJ_Donation
 * @subpackage	Model
 */
class ZJ_DonationModelCampaigns extends JModel {

	/** @var int Total of documents */
	var $_total				= null;
	/** @var object Pagination object */
	var $_pagination		= null;
	/** @var string */
	var $nullDate			= null;
	/** @var string */
	var $now				= null;
	/** @var int */
	var $_id				= null;
	/** @var array items data */
	var $_data				= null;

	/**
	 * Constructor.
	 */
	function __construct() {
		global $mainframe, $option;
		parent::__construct();

		$id	= JRequest::getInt('id', 0);
		$this->setId($id);
	}

	function setId($id) {
		$this->_id		= $id;
		$this->_data	= null;
	}


	function getData() {
		if (empty($this->_data)) {
			$limit		= JRequest::getVar('limit', 0, '', 'int');
			$limitstart	= JRequest::getVar('limitstart', 0, '', 'int');

			$this->setState('limit', $limit);
			$this->setState('limitstart', $limitstart);
			
			$query = $this->_buildQuery();
			$this->_data = $this->_getList($query, $limitstart, $limit);
		}
		return $this->_data;
	}

	function getTotal() {
		if (empty($this->_total)) {
			$query = $this->_buildQuery();
			$this->_total = $this->_getListCount($query);
		}
		return $this->_total;
	}

	function getPagination() {
		if (empty($this->_pagination)) {
			jimport('joomla.html.pagination');
			$this->_pagination = new JPagination( $this->getTotal(), $this->getState('limitstart'), $this->getState('limit') );
		}
		return $this->_pagination;
	}


	function _buildQuery(){
		global $mainframe;
		// get the WHERE and ORDER BY clauses for the query
		$where		= $this->_buildQueryWhere();
		$orderby	= $this->_buildQueryOrderBy();

		$query	= 'SELECT a.*, sum(d.amount) AS `donated_amount`, count(d.id) AS `donors`'
				. ' FROM #__zj_donation_campaigns as a'
				. ' LEFT JOIN #__zj_donation_donates AS d ON d.campaign_id = a.id AND d.donated = 1'
				. $where
				. ' GROUP BY a.id'
				. $orderby
				;
		return $query;
	}

	function _buildQueryWhere() {
		global $mainframe, $option;
		$search				= JRequest::getVar('qsearch');
		$search				= JString::strtolower($search);
		
		//get country_id
		$country_id = JRequest::getInt('country_id');
		
		
		$where		= array();

		$where[]	= 'a.published = 1';
		if($country_id){
			$where[]	= 'a.country_id = '. $country_id ;
		}

		// build the WHERE string
		$where		= count($where) ? ' WHERE ' . implode(' AND ', $where) : '';

		return $where;
	}

	function _buildQueryOrderBy() {
		global $mainframe, $option;
		$params = &$mainframe->getParams();
		$orderby_pri	= $params->def('orderby_pri', '');
		$primary		= $this->orderbyPrimary($orderby_pri);
		$order_by	= ' ORDER BY ';
		$order		= JRequest::getCmd('orderby_sec',$params->get('orderby_sec'));
		switch ($order) {
			case 'date':
				$order_by .= $primary.'a.created';
				break;
			case 'rdate':
				$order_by .= $primary.'a.created DESC';
				break;
				break;
			case 'alpha':
				$order_by .= $primary.'a.title';
				break;
			case 'ralpha':
				$order_by .= $primary.'a.title DESC';
				break;
			case 'author':
				$order_by .= $primary.'a.created_by DESC';
				break;
			case 'rauthor':
				$order_by .= $primary.'a.created_by';
				break;
			case 'order':
				$order_by .= $primary.'a.ordering';
				break;
			case 'rorder':
				$order_by .= $primary.'a.ordering DESC';
				break;
			default:
				$order_by .= $primary.'a.ordering DESC';
		}
		return $order_by;
	}


	function orderbyPrimary($orderby){
		switch ($orderby){
			case 'alpha' :
				$orderby = 'a.title, ';
				break;

			case 'ralpha' :
				$orderby = 'a.title DESC, ';
				break;

			case 'order' :
				$orderby = 'a.ordering, ';
				break;

			default :
				$orderby = '';
				break;
		}

		return $orderby;
	}
}
