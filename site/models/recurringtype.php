<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

/**
 * ZJ_Donation Component - RecurringType Model.
 * @package		ZJ_Donation
 * @subpackage	Model
 */
class ZJ_DonationModelRecurringType extends JModel {
	/** @var int Id of recurring type */
	var $_id				= null;
	/** @var object recurring type object */
	var $_data				= null;
	
	/**
	 * Constructor.
	 */
	function __construct() {
		// call parent constructor
		parent::__construct();
		
		$cid = JRequest::getVar('cid', array(0), '', 'array');
		$this->setId((int) $cid[0]);
	}

	/****************************************************************
	 * GET DATA FOR PRESENTATION.
	 ***************************************************************/
	
	/**
	 * Set recurring type id.
	 */
	function setId($id) {
		if (!isset($this->_id) || $this->_id != $id) {
			$this->_id		= (int) $id;
			$this->_data	= null;
		}
	}

	/**
	 * Get or init recurring type for edit screen.
	 */
	function getData() {
		if ($this->_loadData()) {

		} else {
			$this->_initData();
		}

		return $this->_data;
	}

	/**
	 * Load recurring type data.
	 */
	function _loadData() {
		if (empty($this->_data)) {
			$query = 'SELECT a.*'
				. ' FROM #__zj_donation_recurring_types AS a'
				. ' WHERE a.id = ' . $this->_id
			;
			$this->_db->setQuery($query);
			$this->_data = $this->_db->loadObject();

			return (boolean) $this->_data;
		}

		return true;
	}

	/**
	 * Initialize recurring type data.
	 */
	function _initData() {
		if (empty($this->_data)) {
			$user	= &JFactory::getUser();
			$date	= &JFactory::getDate();
			$row	= new stdClass();

			$row->id				= 0;
			$row->title				= null;
			$row->description		= null;
			$row->type				= null;
			$row->value				= 1;
			$row->published			= 0;
			$row->ordering			= 0;
			$row->checked_out		= 0;
			$row->checked_out_time	= null;

			$this->_data = $row;

			return (boolean) $this->_data;
		}

		return true;
	}

	/**
	 * Test if recurring type is checked out.
	 */
	function isCheckedOut($uid = 0) {
		if ($this->_loadData()) {
			if ($uid) {
				return ($this->_data->checked_out && $this->_data->checked_out != $uid);
			} else {
				return $this->_data->checked_out;
			}
		}

		return false;
	}

	/****************************************************************
	 * MANIPULATE DATA.
	 ***************************************************************/
	
	/**
	 * Checkin/unlock the recurring type.
	 */
	function checkin() {
		if ($this->_id) {
			$row = $this->getTable();
			if (!$row->checkin($this->_id)) {
				$this->setError($this->_db->getErrorMsg());
				return false;
			}
			return true;
		}

		return false;
	}

	/**
	 * Checkout/lock recurring type.
	 */
	function checkout($uid = null) {
		if ($this->_id) {
			if (is_null($uid)) {
				$user	= &JFactory::getUser();
				$uid	= $user->get('id');
			}

			$row = &$this->getTable();
			if (!$row->checkout($uid, $this->_id)) {
				$this->setError($this->_db->getErrorMsg());
				return false;
			}

			return true;
		}

		return false;
	}
	
	/**
	 * Save recurring type to database.
	 */
	function store($data) {
		$row = &$this->getTable();

		// bind the form fields to the recurring types table
		if (!$row->bind($data)) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		if (!$row->id) {
			$row->ordering = $row->getNextOrder();
		}

		// make sure the recurring type is valid
		if (!$row->check()) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		// store the recurring type to the database
		if (!$row->store()) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		// unlock the table
		$row->checkin();

		return $row;
	}
}