<?php
/**
 * @version		$Id$
 * @author		Joomseller
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 * This file may not be redistributed in whole or significant part.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * ZJ_Donation Component - Donate Model
 * @package		ZJ_Donation
 * @subpackage	Model
 */
class ZJ_DonationModelDonate extends JModel {
	/** @var int Product id */
	var $_id					= null;
	/** @var object Product object */
	var $_data					= null;


	/**
	 * Constructor.
	 */
	function __construct() {
		parent::__construct();
		$this->_id = JRequest::getInt('id');
	}

	/**
	 * Get product object.
	 */
	function getData($id = null) {
		$id = $id ? $id : $this->_id;

		if (empty($this->_data[$id])) {
			$row = &$this->getTable('donate');
			$row->load($id);
			$this->_data[$id] = $row;
		}
		return $this->_data[$id];
	}

	/**
	 * Load extra fields
	 */
	function getFields($id) {
		$query = 'SELECT a.field_value, b.title'
			. ' FROM #__zj_donation_field_values AS a'
			. ' INNER JOIN #__zj_donation_fields AS b ON a.field_id = b.id'
			. ' WHERE a.donate_id = ' . (int)$id
		;
		$this->_db->setQuery($query);
		return $this->_db->loadObjectList();
	}

	/**
	 * Save the order.
	 */
	function store($post) {
		
		// get order table instance
		$row = &$this->getTable('donate');

		// bind the form fields to the version table
		if (!$row->bind($post)) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		$user	= &JFactory::getUser();
		$date	= &JFactory::getDate();

		$row->id			= 0;
		$row->user_id		= $user->get('id');
		$row->created_date	= $date->toMySQL();
		$row->token			= JUtility::getHash($row->created_date);
		$row->donated		= 0;

		// make sure the order is valid
		
		if (!$row->check()) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		// store the currency table to the database
		if (!$row->store()) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		//Store addition fields
		ZJ_DonationFields::saveFieldValues($row->id, $post);

		return $row;
	}

	/**
	 * Approve the order.
	 */
	function approve($data) {
		$row = &$this->getTable('donate');
		$row->load($data['order_id']);

		if ($row->id == 0) {
			$this->setError(JText::_('COM_ZJ_DONATION_DONATION_NOT_FOUND'));
			return false;
		}
		
		$date = &JFactory::getDate();
		$row->payment_date		= $date->toMySQL();
		$row->transaction_id	= ($row->transaction_id != '') ? $row->transaction_id : $data['transaction_id'];
		$row->donated			= 1;

		if ($row->recurring) {
			$row->subscribe_id = $data['subscribe_id'];
			$row->donated_times	+= 1; //counting recurring
			if ($row->donated_times > 1) {	//subscribe payment
				$row->amount	+= $data['amount'];
			}
		}

		if (!$row->store()) {
			$this->setError(JText::_('COM_ZJ_DONATION_ERROR_WHILE_SAVING_DONATION'));
			return false;
		}

		return $row;
	}
	
	/**
	* Approve the order. by NathanPhan
	*/
	function approveNew($data,$transaction) {
		$row = &$this->getTable('donate');
		$row->load($data->id);
		
		if ($row->id == 0) {
			$this->setError(JText::_('COM_ZJ_DONATION_DONATION_NOT_FOUND'));
			return false;
		}
	
		$date = &JFactory::getDate();
		$row->payment_date		= $date->toMySQL();
		$row->transaction_id	= ($row->transaction_id != '') ? $row->transaction_id : $transaction;
		$row->donated			= 1;
	
		if ($row->recurring) {
			$row->subscribe_id = $data['subscribe_id'];
			$row->donated_times	+= 1; //counting recurring
			if ($row->donated_times > 1) {
				//subscribe payment
				$row->amount	+= $data['amount'];
			}
		}
	
		if (!$row->store()) {
			$this->setError(JText::_('COM_ZJ_DONATION_ERROR_WHILE_SAVING_DONATION'));
			return false;
		}
	
		return $row;
	}
	
	public function getListByOrderId($order_id){
		return $this->_getList ( $this->_buildQuery () . " WHERE order_id = {$order_id}" );
	}
	
	private function _buildQuery() {
		$query = "SELECT SQL_CALC_FOUND_ROWS * FROM #__zj_donation_donates";
		return $query;
	}
}
?>