<?php
/**
 * @version		$Id$
 * @author		Joomseller
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 * This file may not be redistributed in whole or significant part.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * ZJ_Donation Component - Factory Libraries.
 * @package		ZJ_Donation
 * @subpackage	Library
 */
class ZJ_DonationFactory {
	/**
	 * Get model for the component.
	 */
	function &getModel($type, $prefix = 'ZJ_DonationModel', $config = array()) {
		static $instance;

		if (!isset($instance[$type]) || !is_object($instance[$type])) {
			$instance[$type] = JModel::getInstance($type, $prefix, $config);
		}

		return $instance[$type];
	}

	/**
	 * Get component config.
	 */
	function getConfig() {
		$config_model	= &ZJ_DonationFactory::getModel('configs');
		$config			= &$config_model->getData();

		return $config;
	}

	/**
	 * Get all payment plugins
	 */
	function getPayments($name = 'payment_method') {
		$name		= ($name != '') ? trim($name) : JRequest::getVar('payment_method', '');
		$jspayments = JPluginHelper::getPlugin('zjdonation');
		$dispatcher	= &JDispatcher::getInstance();
		$post		= &JRequest::get('post');
		JPluginHelper::importPlugin('zjdonation');
		$out	= '';
		if(count($jspayments)){
			$data			= $dispatcher->trigger('onPaymentInfo');
			for ($i = 0, $n = count($data); $i < $n; $i++) {
				$row	= $data[$i];
				$checked	= (isset($post['payment_method']) && $post['payment_method'] == $row['code']) ? ' checked' : '';
				if (!$i && !$checked) {
					$checked	= ' checked';
				}
				$out	.= '<input type="radio" id="'.$row['code'].'_payment" name="'.$name.'" value="'.$row['code'].'" '.$checked.'>'
						. ' <label for="'.$row['code'].'_payment"><span>'.$row['name']
						;
					if ($row['image'] != '') {
						$out	.= '<span><img src="'.$row['image'].'" width="149" height="100" align="middle"></span>';
					}
					$out	.= '</span></label>';
			}
		}
		return $out;
	}

	function getDeliciousButton($title, $link){
        $img_url = JUri::base()."media/zj_donation/socialbtn/delicious.png";

        return '<a href="http://del.icio.us/post?url=' . $link . '&amp;title=' . $title . '" title="' . JText::sprintf('COM_ZJ_DONATION_SHARE_SOCIAL_TITLE', "Delicious") . '" target="_blank" >
        <img src="' . $img_url . '" alt="' . JText::sprintf('COM_ZJ_DONATION_SHARE_SOCIAL_TITLE', 'Delicious') . '" />
        </a>';
    }

    function getDiggButton($title, $link){
        $img_url = JUri::base()."media/zj_donation/socialbtn/digg.png";

        return '<a href="http://digg.com/submit?url=' . $link . '&amp;title=' . $title . '" title="' . JText::sprintf('COM_ZJ_DONATION_SHARE_SOCIAL_TITLE', "Digg") . '" target="_blank" >
        <img src="' . $img_url . '" alt="' . JText::sprintf('COM_ZJ_DONATION_SHARE_SOCIAL_TITLE', 'Digg') . '" />
        </a>';
    }

    function getFacebookButton($title, $link){
        $img_url = JUri::base()."media/zj_donation/socialbtn/facebook.png";

        return '<a href="http://www.facebook.com/sharer.php?u=' . $link . '&amp;t=' . $title . '" title="' . JText::sprintf('COM_ZJ_DONATION_SHARE_SOCIAL_TITLE', "Facebook") . '" target="_blank" >
        <img src="' . $img_url . '" alt="' . JText::_('COM_ZJ_DONATION_SHARE_SOCIAL_TITLE', 'Facebook') . '" />
        </a>';
    }

    function getGoogleButton($title, $link){
        $img_url = JUri::base()."media/zj_donation/socialbtn/google.png";

        return '<a href="http://www.google.com/bookmarks/mark?op=edit&amp;bkmk=' . $link . '" title="' . JText::sprintf('COM_ZJ_DONATION_SHARE_SOCIAL_TITLE', "Google Bookmarks") . '" target="_blank" >
        <img src="' . $img_url . '" alt="' . JText::sprintf('COM_ZJ_DONATION_SHARE_SOCIAL_TITLE', 'Google Bookmarks') . '" />
        </a>';
    }

    function getStumbleuponButton($title, $link){
        $img_url = JUri::base()."media/zj_donation/socialbtn/stumbleupon.png";

        return '<a href="http://www.stumbleupon.com/submit?url=' . $link . '&amp;title=' . $title . '" title="' . JText::sprintf('COM_ZJ_DONATION_SHARE_SOCIAL_TITLE', "Stumbleupon") . '" target="_blank" >
        <img src="' . $img_url . '" alt="' . JText::sprintf('COM_ZJ_DONATION_SHARE_SOCIAL_TITLE', 'Stumbleupon') . '" />
        </a>';
    }

    function getTechnoratiButton($title, $link){
        $img_url = JUri::base()."media/zj_donation/socialbtn/technorati.png";

        return '<a href="http://technorati.com/faves?add=' . $link . '" title="' . JText::sprintf('COM_ZJ_DONATION_SHARE_SOCIAL_TITLE', "Technorati") . '" target="_blank" >
        <img src="' . $img_url . '" alt="' . JText::sprintf('COM_ZJ_DONATION_SHARE_SOCIAL_TITLE', 'Technorati') . '" />
        </a>';
    }

    function getTwitterButton($title, $link){
        $img_url = JUri::base()."media/zj_donation/socialbtn/twitter.png";

        return '<a href="http://twitter.com/share?text=' . $title . "&amp;url=" . $link . '" title="' . JText::sprintf('COM_ZJ_DONATION_SHARE_SOCIAL_TITLE', "Twitter") . '" target="_blank" >
        <img src="' . $img_url . '" alt="' . JText::sprintf('COM_ZJ_DONATION_SHARE_SOCIAL_TITLE', 'Twitter') . '" />
        </a>';
    }

    function getLinkedInButton($title, $link){
        $img_url = JUri::base()."media/zj_donation/socialbtn/linkedin.png";

        return '<a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=' . $link .'&amp;title=' . $title . '" title="' . JText::sprintf('COM_ZJ_DONATION_SHARE_SOCIAL_TITLE', "LinkedIn") . '" target="_blank" >
        <img src="' . $img_url . '" alt="' . JText::sprintf('COM_ZJ_DONATION_SHARE_SOCIAL_TITLE', 'LinkedIn') . '" />
        </a>';
    }

	function renderSocialButtons() {
		$document	= JFactory::getDocument();
		$link       = JURI::getInstance();
		$link       = $link->toString();
		$title      = $document->getTitle();

		$title      = rawurlencode($title);
		$link       = rawurlencode($link);

		return ZJ_DonationFactory::getDeliciousButton($title, $link)
				. ZJ_DonationFactory::getDiggButton($title, $link)
				. ZJ_DonationFactory::getFacebookButton($title, $link)
				. ZJ_DonationFactory::getGoogleButton($title, $link)
				. ZJ_DonationFactory::getStumbleuponButton($title, $link)
				. ZJ_DonationFactory::getTechnoratiButton($title, $link)
				. ZJ_DonationFactory::getTwitterButton($title, $link)
				. ZJ_DonationFactory::getLinkedInButton($title, $link)
				;
	}

	function renderMetaData($item) {
		$document	= JFactory::getDocument();
		// set meta keyworks
		if ($item->meta_keywords) {
			$document->setMetaData('keywords', $item->meta_keywords);
		}
		// set meta description
		if ($item->meta_desc) {
			$document->setDescription($item->meta_keywords);
		}

		//Set Facebook Metadata
		$document->addCustomTag( '<meta property="og:title" content="' . $item->title . '" />' );
		$meta_desc	= $item->meta_desc;
		if (!$meta_desc) {
			$meta_desc	= strip_tags($item->description);
		}
		$document->addCustomTag( '<meta property="og:description" content="'. $meta_desc. '" />' );
		$gconfig	= &JFactory::getConfig();
		$document->addCustomTag( '<meta property="og:site_name" content="' . $gconfig->getValue('sitename') . '" />' );
		$document->addCustomTag( '<meta property="og:image" content="' . JURI::base() . 'media/zj_donation/'.$item->images . '" />' );
	}
}