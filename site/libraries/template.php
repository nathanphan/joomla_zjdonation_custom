<?php
/**
 * @version		$Id$
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 * This file may not be redistributed in whole or significant part.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * ZJ_Donation Component - Templating System Library.
 * @package		ZJ_Donation
 * @subpackage	Library
 */
class ZJ_DonationTemplate extends JObject {
	/** @var array Variables of template */
	var $_vars			= null;
	/** @var string Template file */
	var $_file			= null;
	/** @var string Template layout */
	var $_layout		= null;

	/**
	 * Constructor
	 */
	function __construct($file = null, $layout = null) {
		$this->_file	= $file;
		$this->_layout	= $layout;
	}

	/**
	 * Method to get template full path name.
	 */
	function _getTemplateFullPath($file, $layout = null) {
		$mainframe	= &JFactory::getApplication();
		$config		= &ZJ_DonationFactory::getConfig();

		jimport('joomla.filesystem.file');
		jimport('joomla.filesystem.folder');

		$file_name		= $file;

		if (isset($layout) && $layout != 'default') {
			$arr		= explode('.', $file_name);
			$arr[0]		.= '_' . $layout;
			$file_name	= implode('.', $arr);
		}
		// test if template override exists in joomla's template folder
		$override_path		= JPATH_ROOT.DS.'templates'.DS.$mainframe->getTemplate().DS.'html';
		$override_exists	= JFolder::exists($override_path.DS.'com_zj_donation');
		$template			= ZJ_DONATION_COM_PATH.DS.'templates'.DS.$config->get('template').DS.$file_name.'.php';

		// test override path first
		if (JFile::exists($override_path.DS.'com_zj_donation'.DS.$file_name.'.php')) {
			// load the override template
			$file = $override_path.DS.'com_zj_donation'.DS.$file_name.'.php';
		} else if (JFile::exists($template) && !$override_exists) {
			// if override fails, try the template set in config
			$file = $template;
		} else {
			// we assume to use the default template
			$file = ZJ_DONATION_COM_PATH.DS.'templates'.DS.'default'.DS.$file_name.'.php';
		}

		return $file;
	}

	/**
	 * Method to set template variable.
	 */
	function set($name, $value) {
		$this->_vars[$name] = $value;
	}

	/**
	 * Method to set template variable by reference.
	 */
	function setRef($name, &$value) {
		$this->_vars[$name] = &$value;
	}

	/**
	 * Method to add template style sheet.
	 */
	function addStyleSheet($file) {
		$mainframe	= &JFactory::getApplication();
		$config		= &ZJ_DonationFactory::getConfig();

		if (!JString::strpos($file, '.css')) {
			jimport('joomla.filesystem.file');
			jimport('joomla.filesystem.folder');

			$file_name		= $file;
			// test if template override exists in joomla's template folder
			$override_path		= JPATH_ROOT.DS.'templates'.DS.$mainframe->getTemplate().DS.'html';
			$override_exists	= JFolder::exists($override_path.DS.'com_zj_donation');
			$template			= ZJ_DONATION_COM_PATH.DS.'templates'.DS.$config->get('template').DS.'css'.DS.$file_name.'.css';

			// test override path first
			if (JFile::exists($override_path.DS.'com_zj_donation'.DS.$file_name.'.css')) {
				// load the override template
				$file = '/templates/' . $mainframe->getTemplate() . '/html/com_zj_donation/css/' . $file_name . '.css';
			} else if (JFile::exists($template) && !$override_exists) {
				// if override fails, try the template set in config
				$file = '/components/com_zj_donation/templates/' . $config->get('template') . '/css/' . $file_name . '.css';
			} else {
				// we assume to use the default template
				$file = '/components/com_zj_donation/templates/default/css/' . $file_name . '.css';
			}
		}

		self::attachAssets($file, 'css', rtrim(JURI::root(), '/'));
	}

	/**
	 * Method to add template style sheet.
	 */
	function addScript($file) {
		$mainframe	= &JFactory::getApplication();
		$config		= &ZJ_DonationFactory::getConfig();

		if (!JString::strpos($file, '.js')) {
			jimport('joomla.filesystem.file');
			jimport('joomla.filesystem.folder');

			$file_name		= $file;
			// test if template override exists in joomla's template folder
			$override_path		= JPATH_ROOT.DS.'templates'.DS.$mainframe->getTemplate().DS.'html';
			$override_exists	= JFolder::exists($override_path.DS.'com_zj_donation');
			$template			= ZJ_DONATION_COM_PATH.DS.'templates'.DS.$config->get('template').DS.'js'.DS.$file_name.'.js';

			// test override path first
			if (JFile::exists($override_path.DS.'com_zj_donation'.DS.$file_name.'.js')) {
				// load the override template
				$file = '/templates/' . $mainframe->getTemplate() . '/html/com_zj_donation/js/' . $file_name . '.js';
			} else if (JFile::exists($template) && !$override_exists) {
				// if override fails, try the template set in config
				$file = '/components/com_zj_donation/templates/' . $config->get('template') . '/js/' . $file_name . '.js';
			} else {
				// we assume to use the default template
				$file = '/components/com_zj_donation/templates/default/js/' . $file_name . '.js';
			}
		}

		self::attachAssets($file, 'js', rtrim(JURI::root(), '/'));
	}

	/**
	 * Method to attach component assets
	 */
	function attachAssets($path, $type , $asset_path = '') {
		$document = &JFactory::getDocument();

		if ($document->getType() != 'html') {
			return;
		}

		if (!empty($asset_path)) {
			$path = $asset_path . $path;
		} else {
			$path = JURI::root() . 'components/com_zj_donation' . $path;
		}

		if (!defined('ZJ_DONATION_ASSET_' . md5($path))) {
			define('ZJ_DONATION_ASSET_' . md5($path), 1);

			switch ($type) {
				case 'js':
					$document->addScript($path);
					break;
				case 'css':
					$document->addStyleSheet($path);
					break;
			}
		}
	}

	/**
	 * Method to allow a temmplate include other template and inherit all the variables.
	 */
	function load($file, $layout = null) {
		if ($this->_vars) {
			extract($this->_vars, EXTR_REFS);
		}
		
		$file = $this->_getTemplateFullPath($file, $layout);
		include($file);
		
		return $this;
	}

	/**
	 * Method to open, parse, and return the template file.
	 */
	function fetch($file = null, $layout = null) {
		$file_layout = $this->_getTemplateFullPath($file, $layout);

		if (!$file_layout || !JFile::exists($file_layout)) {
			$file = $this->_getTemplateFullPath($file);
		} else {
			$file = $file_layout;
		}

		if (!$file) {
			$file = $this->_file;
		}

		if (!isset($this->_vars['config']) || empty($this->_vars['config'])) {
			$this->_vars['config'] = &ZJ_DonationFactory::getConfig();
		}

		if (!isset($this->_vars['user']) || empty($this->_vars['user'])) {
			$this->_vars['user'] = &JFactory::getUser();
		}

		if (!isset($this->_vars['live_site']) || empty($this->_vars['live_site'])) {
			$this->_vars['live_site'] = JURI::root();
		}

		if ($this->_vars) {
			// extract the variables to local namespace
			extract($this->_vars, EXTR_REFS);
		}

		ob_start();
		require($file);
		$contents = ob_get_contents();
		ob_end_clean();

		return $contents;
	}
	
}