<?php
/**
 * @version		$Id$
 * @author		Joomseller
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * ZJ_Donation Component - Route Library
 * @package		ZJ_Donation
 * @subpackage	Library
 */
class ZJ_DonationRoute {
	/**
	 * Get Itemid for component.
	 */
	function getItemid($view = null, $id = null, $url = null) {
		$route_model = ZJ_DonationFactory::getModel('route');
		return $route_model->getItemid($view, $id, $url);
	}

	/**
	 * Translates an internal Joomla URL to a humanly readible URL.
	 */
	function _($url, $xhtml = true, $ssl = null) {
		// parse the url to get the view parameter
		parse_str($url);
		
		$view	= !empty($view) ? $view : null;
		$id		= !empty($id) ? $id : null;
		
		// get Itemid by view
		$Itemid = ZJ_DonationRoute::getItemid($view, $id, $url);
		// insert Itemid to url
		$pos = JString::strpos($url, '#');
		if ($pos === false) {
			if (isset($Itemid)) {
				$url .= '&Itemid=' . $Itemid;
			}
		} else {
			if (isset($Itemid)) {
				$url = JString::str_ireplace('#', '&Itemid=' . $Itemid . '#', $url);
			}
		}

		return JRoute::_($url, $xhtml, $ssl);
	}

	/**
	 * Get current uri.
	 */
	function getURI($encode = true) {
		// get request uri
		$uri = JRequest::getVar('REQUEST_URI', '', 'server', 'string');
		// encode the uri
		$uri = !$encode ? $uri : base64_encode($uri);

		return $uri;
	}
}