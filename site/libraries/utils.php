<?php
/**
 * @version		$Id$
 * @author		Joomseller
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 * This file may not be redistributed in whole or significant part.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * ZJ_Donation Component - Utility Library.
 * @package		ZJ_Donation
 * @subpackage	Library
 */
class ZJ_DonationUtils {

	/**
	 * Format price.
	 */
	function formatPrice($price, $display = null, $number_only = false) {
		if ($price == 0 && $number_only == false) {
			return JText::_('COM_ZJ_DONATION_FREE');
		}

		$config	= &ZJ_DonationFactory::getConfig();
		$result	= sprintf('%01.02f', $price);

		$currency_display = isset($display) ? $display : $config->get('currency_display');

		if ($currency_display == 0) {
			$result = $result . ' ' . $config->get('currency_code');
		} else {
			if ($config->get('currency_position') == 0) {
				$result = $config->get('currency_sign') . $result;
			} else {
				$result = $result . ' ' . $config->get('currency_sign');
			}
		}
		
		return $result;
	}

	/**
	 * Get user field value.
	 */
	function getUserField($field_name, $user_id = null) {
		static $fields;

		$user		= JFactory::getUser($user_id);
		$user_id	= $user->get('id');

		if (empty($fields[$user_id][$field_name])) {
			if ($user_id != 0) {
				$config = &ZJ_DonationFactory::getConfig();
				if ($config->get('community_integrate') == 'jomsocial') {
					// require joomla library
					if (file_exists(JPATH_ROOT.DS.'components'.DS.'com_community'.DS.'libraries'.DS.'core.php')) {
						require_once(JPATH_ROOT.DS.'components'.DS.'com_community'.DS.'libraries'.DS.'core.php');

						$user		= &CFactory::getUser($user_id);
						$field		= 'field_' . $field_name . '_js';
						$field_code	= $config->get($field);
						$value		= $user->getInfo($field_code);
						$fields[$user_id][$field_name] = $value;
					} else {
						$fields[$user_id][$field_name] = '';
					}
				} else {
					$fields[$user_id][$field_name] = '';
				}
			} else {
				$fields[$user_id][$field_name] = '';
			}
		}

		return $fields[$user_id][$field_name];
	}
}