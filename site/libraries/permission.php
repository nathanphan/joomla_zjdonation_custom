<?php
/**
 * @version		$Id$
 * @author		Joomseller
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 * This file may not be redistributed in whole or significant part.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * ZJ_Donation Component - Permission Library.
 * @package		ZJ_Donation
 * @subpackage	Library
 */
class ZJ_DonationPermission {

	/**
	 * Check if user can perform checkout.
	 */
	function userCanDonate($user_id = null) {
		$user	= &JFactory::getUser($user_id);
		$config = &ZJ_DonationFactory::getConfig();

		if ($config->get('donate_permission') && $user->get('id') == 0) {
			return ZJ_DONATION_PERM_GUEST;
		}

		return ZJ_DONATION_PERM_SUCCESS;
	}
}