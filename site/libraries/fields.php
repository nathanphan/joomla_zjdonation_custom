<?php
/**
 * @version		$Id$
 * @author		Joomseller!
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 */

// no direct access
defined('_JEXEC') or die('Restricted access');


define('NUMBER_OPTION_PER_LINE', 1) ;
define('FIELD_TYPE_TEXTBOX', 0);
define('FIELD_TYPE_TEXTAREA', 1);
define('FIELD_TYPE_DROPDOWN', 2) ;
define('FIELD_TYPE_CHECKBOXLIST', 3) ;
define('FIELD_TYPE_RADIOLIST', 4) ;
define('FIELD_TYPE_DATETIME', 5) ;
define('FIELD_TYPE_HEADING', 6) ;
define('FIELD_TYPE_MESSAGE', 7) ;
define('FIELD_TYPE_MULTISELECT', 8) ;

/**
 * ZJ_Donation Component - Fields Helper
 * @package		ZJ_Donation
 * @subpackage	Helper
 * @since		1.5
 */

class ZJ_DonationFields {

	/**
	 * get all campaign custom fields
	 */
	function _getFields($ids) {
		$ids	= implode(',', $ids);
		//Load all custom field
		$db		= &JFactory::getDBO() ;
		$sql	= 'SELECT * FROM #__zj_donation_fields WHERE published = 1 AND id IN ('.$ids.') ORDER BY ordering';
		$db->setQuery($sql);
		return $db->loadObjectList();
	}


	/**
	 * Render a textbox
	 *
	 * @param object $row
	 */
	function _renderTextBox($row) {
		$postedValue = JRequest::getVar($row->name,  $row->default_values) ;
		?>
		<tr>
			<td class="title_cell">
				<label for="<?php echo $row->name; ?>">
					<strong><?php echo  JText::_($row->title); ?></strong>:
					<?php
					if ($row->required)
						echo '<span class="zj_required">*</span>';
					?>
				</label>
			</td>
			<td>
				<input type="text" id="<?php echo $row->name ; ?>" name="<?php echo $row->name ; ?>" class="<?php echo $row->css_class; ?> <?php echo ($row->required)? 'required':''; ?>" size="<?php echo $row->size ; ?>" value="<?php echo JText::_($postedValue) ; ?>" />
			</td>
		</tr>
		<?php
	}

	/**
	 * Output values which users entered in the textbox field
	 *
	 * @param object $row
	 */

	function _renderTextboxConfirmation($row) {
		?>
		<tr>
			<td class="title_cell">
				<?php echo JText::_($row->title); ?>
			</td>
			<td>
				<?php
					$postedValue = JRequest::getVar($row->name, '', 'post') ;
					echo $postedValue ;
				?>
			</td>
		</tr>
		<?php
	}
	/**
	 * Render hidden field for textbox
	 *
	 * @param object $row
	 */
	function _renderTextboxHidden($row) {
		$postedValue = JRequest::getVar($row->name, '', 'post') ;
		?>
		<input type="hidden" name="<?php echo $row->name ; ?>" value="<?php echo $postedValue; ?>" />
		<?php
	}


	/**
	 * Render textarea object
	 *
	 * @param object $row
	 */
	function _renderTextarea($row) {
		$postedValue = JRequest::getVar($row->name,  $row->default_values) ;
		?>
		<tr>
			<td class="title_cell">
				<label for="<?php echo $row->name; ?>">
					<strong><?php echo  JText::_($row->title); ?></strong>:
					<?php
					if ($row->required)
						echo '<span class="zj_required">*</span>';
					?>
				</label>
			</td>
			<td>
				<textarea id="<?php echo $row->name ; ?>" name="<?php echo $row->name ; ?>" rows="<?php echo $row->rows; ?>" cols="<?php echo $row->cols ; ?>" class="inputbox <?php echo $row->css_class; ?> <?php echo ($row->required)? 'required':''; ?>"><?php echo $postedValue; ?></textarea>
			</td>
		</tr>
		<?php
	}
	/**
	 * Output values which users entered in the textarea field
	 *
	 * @param object $row
	 */
	function _renderTextAreaConfirmation($row) {
		$postedValue = JRequest::getVar($row->name, '', 'post') ;
		?>
		<tr>
			<td class="title_cell">
				<?php echo JText::_($row->title); ?>
			</td>
			<td>
				<?php echo $postedValue ; ?>
			</td>
		</tr>
		<?php
	}
	/**
	 * Render hidden field for textarea
	 *
	 * @param object $row
	 */
	function _renderTextAreaHidden($row) {
		$postedValue = JRequest::getVar($row->name, '', 'post') ;
		?>
		<input type="hidden" name="<?php echo $row->name ; ?>" value="<?php echo $postedValue; ?>" />
		<?php
	}
	/**
	 * Render dropdown field type
	 *
	 * @param object $row
	 */
	function _renderDropdown($row) {
		$postedValue = JRequest::getVar($row->name, $row->default_values, 'post') ;
		$options = array() ;
		$options[] = JHTML::_('select.option', '', JText::_('COM_ZJ_DONATION_SELECT_A_VALUE'));
		$values = explode("\r\n", $row->values) ;
		for ($i = 0 , $n = count($values) ; $i < $n ; $i++) {
			$options[] = JHTML::_('select.option', $values[$i], JText::_($values[$i])) ;
		}
		?>
		<tr>
			<td class="title_cell">
				<label for="<?php echo $row->name; ?>">
					<strong><?php echo  JText::_($row->title); ?></strong>:
					<?php
					if ($row->required)
						echo '<span class="zj_required">*</span>';
					?>
				</label>
			</td>
			<td>
				<?php
					$required	= ($row->required) ? ' required' : '';
					$class		= 'id="'.$row->name.'" class="inputbox '. $row->css_class. $required. '"';
					echo JHTML::_('select.genericlist', $options, $row->name, $class, 'value', 'text', $postedValue);
				?>
			</td>
		</tr>
		<?php
	}

	/**
	 * Output values which users choosed in the dropdown
	 *
	 * @param object $row
	 */
	function _renderDropDownConfirmation($row) {
		$postedValue = JRequest::getVar($row->name, '', 'post') ;
		?>
		<tr>
			<td class="title_cell">
				<?php echo JText::_($row->title); ?>
			</td>
			<td class="field_cell">
				<?php echo $postedValue ; ?>
			</td>
		</tr>
		<?php
	}
	/**
	 * Render hidden field for textbox
	 *
	 * @param object $row
	 */
	function _renderDropdownHidden($row) {
		$postedValue = JRequest::getVar($row->name, '', 'post') ;
		?>
		<input type="hidden" name="<?php echo $row->name ; ?>" value="<?php echo $postedValue; ?>" />
		<?php
	}



	/**
	 * Render dropdown field type
	 *
	 * @param object $row
	 */
	function _renderMultiSelect($row) {
		if (isset($_POST[$row->name])) {
			$selectedValues = $_POST[$row->name] ;
		} else {
			$selectedValues = explode("\r\n", $row->default_values) ;
		}
		$options = array() ;
		$values = explode("\r\n", $row->values) ;
		for ($i = 0 , $n = count($values) ; $i < $n ; $i++) {
			$options[] = JHTML::_('select.option', $values[$i], JText::_($values[$i])) ;
		}
		$selectedOptions = array() ;
		for ($i = 0 , $n = count($selectedValues); $i < $n; $i++) {
			$selectedOptions[] = JHTML::_('select.option', $selectedValues[$i], $selectedValues[$i]) ;
		}
		?>
		<tr>
			<td class="title_cell">
				<label for="<?php echo $row->name; ?>">
					<strong><?php echo  JText::_($row->title); ?></strong>:
					<?php
					if ($row->required)
						echo '<span class="zj_required">*</span>';
					?>
				</label>
			</td>
			<td>
				<?php
					$required	= ($row->required) ? ' required' : '';
					$class		= 'id="'.$row->name.'" class="inputbox '. $row->css_class. $required. '"';
					echo JHTML::_('select.genericlist', $options, $row->name.'[]', ' multiple="multiple" size="4" '. $class, 'value', 'text', $selectedValues);
				?>
			</td>
		</tr>
		<?php
	}

	/**
	 * Output values which users choosed in the dropdown
	 *
	 * @param object $row
	 */
	function _renderMultiSelectConfirmation($row) {
		$postedValue = JRequest::getVar($row->name, '', 'post') ;
		$postedValue = implode(' -+- ', $postedValue);
		?>
		<tr>
			<td class="title_cell">
				<?php echo JText::_($row->title); ?>
			</td>
			<td class="field_cell">
				<?php echo $postedValue ; ?>
			</td>
		</tr>
		<?php
	}
	/**
	 * Render hidden field for textbox
	 *
	 * @param object $row
	 */
	function _renderMultiSelectHidden($row) {
		$postedValue = JRequest::getVar($row->name, '', 'post') ;
		for ($i = 0 , $n = count($postedValue) ; $i < $n ; $i++) {
			?>
			<input type="hidden" name="<?php echo $row->name ; ?>[]" value="<?php echo $postedValue[$i]; ?>" />
			<?php
		}
	}
	/**
	 * Render checkbox list
	 *
	 * @param object $row
	 */
	function _renderCheckboxList($row) {
		$values = explode("\r\n", $row->values);
		if (isset($_POST[$row->name])) {
			$defaultValues = $_POST[$row->name] ;
		} else {
			$defaultValues = explode("\r\n", $row->default_values) ;
		}
		?>
			<tr>
				<td class="title_cell">
					<label for="<?php echo $row->name; ?>">
						<strong><?php echo  JText::_($row->title); ?></strong>:
						<?php
						if ($row->required)
							echo '<span class="zj_required">*</span>';
						?>
					</label>
				</td>
				<td>
					<?php
					$required	= ($row->required) ? ' required' : '';
					$class		= 'id="'.$row->name.'" class="inputbox '. $row->css_class. $required. '"';
					?>
					<table cellspacing="3" cellpadding="3" width="100%">
					<?php
						for ($i = 0 , $n = count($values) ; $i < $n ; $i++) {
							$value = $values[$i] ;
							if ($i % NUMBER_OPTION_PER_LINE == 0) {
								?>
								<tr>
								<?php
							}
							?>
								<td>
									<input class="inputbox" value="<?php echo $value; ?>" type="checkbox" name="<?php echo $row->name; ?>[]" <?php if (in_array($value, $defaultValues)) echo ' checked="checked" ' ; ?>><?php echo JText::_($value);?>
								</td>
							<?php
							if (($i+1) % NUMBER_OPTION_PER_LINE == 0) {
								echo '</tr>';
							}
						}
						if ($i % NUMBER_OPTION_PER_LINE != 0) {
							$colspan = NUMBER_OPTION_PER_LINE - $i % NUMBER_OPTION_PER_LINE ;
							?>
								<td colspan="<?php echo $colspan; ?>">&nbsp;</td>
							</tr>
							<?php
						}
					?>
					</table>
				</td>
			</tr>
	<?php
	}

	/**
	 * Output values which users selectd in the checkboxlist
	 *
	 * @param object $row
	 */
	function _renderCheckBoxListConfirmation($row) {
		$postedValue = JRequest::getVar($row->name, array(), 'post');
		?>
		<tr>
			<td class="title_cell">
				<?php echo JText::_($row->title); ?>
			</td>
			<td>
				<?php
				echo implode(' -+- ',  $postedValue);
				?>
			</td>
		</tr>
		<?php
	}
	/**
	 * Render hidden field for textbox
	 *
	 * @param object $row
	 */
	function _renderCheckBoxListHidden($row) {
		$postedValue = JRequest::getVar($row->name, array(), 'post') ;
		for ($i = 0 , $n = count($postedValue) ; $i < $n ; $i++) {
			$value = $postedValue[$i];
		?>
			<input type="hidden" name="<?php echo $row->name ; ?>[]" value="<?php echo $value; ?>" />
		<?php
		}
	}
	/**
	 * Reder radio list
	 *
	 * @param object $row
	 */
	function _renderRadioList(&$row) {
		$postedValue = JRequest::getVar($row->name, $row->default_values, 'post') ;
		$values = explode("\r\n",  $row->values);
		$options = array();
		for ($i = 0 , $n = count($values) ; $i < $n; $i++) {
			$options[] = JHTML::_('select.option', $values[$i] , JText::_($values[$i])) ;
		}
		?>
		<tr>
			<td class="title_cell">
				<label for="<?php echo $row->name; ?>">
					<strong><?php echo  JText::_($row->title); ?></strong>:
					<?php
					if ($row->required)
						echo '<span class="zj_required">*</span>';
					?>
				</label>
			</td>
			<td>
				<?php
				$class		= 'id="'.$row->name.'" class="inputbox '. $row->css_class. '"';
				echo JHTML::_('select.radiolist', $options, $row->name , $class, 'value', 'text', $postedValue);
				?>
			</td>
		</tr>
		<?php
	}

	/**
	 * Output values which users entered in the textarea field
	 *
	 * @param object $row
	 */
	function _renderRadioListConfirmation($row) {
		$postedValue = JRequest::getVar($row->name, '', 'post') ;
		?>
		<tr>
			<td class="title_cell">
				<?php echo JText::_($row->title); ?>
			</td>
			<td class="field_cell">
				<?php echo $postedValue; ?>
			</td>
		</tr>
		<?php
	}
	/**
	 * Render hidden tag for radio list
	 *
	 * @param object $row
	 */
	function _renderRadioListHidden($row) {
		$postedValue = JRequest::getVar($row->name, '', 'post') ;
		?>
		<input type="hidden" name="<?php echo $row->name ; ?>" value="<?php echo $postedValue; ?>" />
		<?php
	}
	/**
	 *
	 *
	 * @param string $row
	 */
	function _renderDateTime(&$row) {
		$config			= ZJ_DonationFactory::getConfig();
		$dateFormat		= '%d-%m-%Y';
		$postedValue	= JRequest::getVar($row->name, $row->default_values) ;
		?>
		<tr>
			<td class="title_cell">
				<label for="<?php echo $row->name; ?>">
					<strong><?php echo  JText::_($row->title); ?></strong>:
					<?php
					if ($row->required)
						echo '<span class="zj_required">*</span>';
					?>
				</label>
			</td>
			<td class="field_cell">
				<?php
				$required	= ($row->required) ? ' required' : '';
				$class		= 'id="'.$row->name.'" class="inputbox '. $row->css_class. $required. '"';
				echo JHTML::_('calendar', $postedValue, $row->name, $row->name, $dateFormat, $class) ;
				?>
			</td>
		</tr>
		<?php
	}

	/**
	 * Output values which users entered in the textarea field
	 *
	 * @param object $row
	 */
	function _renderDateTimeConfirmation($row) {
		$postedValue = JRequest::getVar($row->name, '', 'post') ;
		?>
		<tr>
			<td class="title_cell">
				<?php echo JText::_($row->title); ?>
			</td>
			<td class="field_cell">
				<?php echo $postedValue ; ?>
			</td>
		</tr>
		<?php
	}
	/**
	 * Render hidden tag for radio list
	 *
	 * @param object $row
	 */
	function _renderDateTimeHidden($row) {
		$postedValue = JRequest::getVar($row->name, '', 'post') ;
		?>
			<input type="hidden" name="<?php echo $row->name ; ?>" value="<?php echo $postedValue; ?>" />
		<?php
	}


	/**
	 * Render output in the confirmation page
	 *
	 */
	function renderConfirmation($ids) {
		$fields	= ZJ_DonationFields::_getFields($ids);
		ob_start();
		for ($i = 0 , $n = count($fields) ; $i < $n ; $i++) {
			$row = $fields[$i];
			switch ($row->field_type) {
				case FIELD_TYPE_HEADING :
				?>
					<tr>
						<td class="heading" colspan="2">
							<?php echo JText::_($row->title); ?>
						</td>
					</tr>
				<?php
					break ;
				case FIELD_TYPE_TEXTBOX :
					ZJ_DonationFields::_renderTextboxConfirmation($row);
					break ;
				case FIELD_TYPE_TEXTAREA :
					ZJ_DonationFields::_renderTextAreaConfirmation($row);
					break ;
				case FIELD_TYPE_DROPDOWN :
					ZJ_DonationFields::_renderDropDownConfirmation($row);
					break ;
				case FIELD_TYPE_CHECKBOXLIST :
					ZJ_DonationFields::_renderCheckBoxListConfirmation($row);
					break ;
				case FIELD_TYPE_RADIOLIST :
					ZJ_DonationFields::_renderRadioListConfirmation($row);
					break ;
				case FIELD_TYPE_DATETIME :
					ZJ_DonationFields::_renderDateTimeConfirmation($row);
					break ;
				case FIELD_TYPE_MULTISELECT :
					ZJ_DonationFields::_renderMultiSelectConfirmation($row);
					break ;
			}
		}
		$output = ob_get_contents() ;
		ob_end_clean() ;
		return $output ;
	}
	/**
	 * Render published custom fields
	 *
	 */
	function renderCustomFields($ids) {
		$fields	= ZJ_DonationFields::_getFields($ids);
		ob_start();
		for ($i = 0 , $n = count($fields) ; $i < $n ; $i++) {
			$row = $fields[$i];
			switch ($row->field_type) {
				case FIELD_TYPE_HEADING :
					?>
						<tr>
							<td colspan="2" class="heading"><?php echo JText::_($row->title); ?></td>
						</tr>
					<?php
					break ;
				case FIELD_TYPE_MESSAGE :
					?>
						<tr>
							<td colspan="2" class="message">
								<?php echo $row->description ; ?>
							</td>
						</tr>
					<?php
					break ;
				 case FIELD_TYPE_TEXTBOX :
					ZJ_DonationFields::_renderTextBox($row);
					break ;
				 case FIELD_TYPE_TEXTAREA :
					ZJ_DonationFields::_renderTextarea($row);
					break ;
				 case FIELD_TYPE_DROPDOWN :
					ZJ_DonationFields::_renderDropdown($row);
					break ;
				 case FIELD_TYPE_CHECKBOXLIST :
					ZJ_DonationFields::_renderCheckboxList($row);
					break ;
				 case FIELD_TYPE_RADIOLIST :
					ZJ_DonationFields::_renderRadioList($row);
					break ;
				 case FIELD_TYPE_DATETIME :
					ZJ_DonationFields::_renderDateTime($row);
					break ;
				 case FIELD_TYPE_MULTISELECT :
				 	ZJ_DonationFields::_renderMultiSelect($row);
				 	break ;
			}
			?>
		<?php
		}
		$output = ob_get_contents() ;
		ob_end_clean();
		return $output ;
	}

	/**
	 * Render hidden fields to pass to the next form
	 *
	 */
	function renderHiddenFields($ids) {
		$fields	= ZJ_DonationFields::_getFields($ids);
		ob_start();
		for ($i = 0 , $n = count($fields) ; $i < $n ; $i++) {
			$row = $fields[$i];
			switch ($row->field_type) {
				case FIELD_TYPE_TEXTBOX :
					ZJ_DonationFields::_renderTextboxHidden($row);
					break ;
				case FIELD_TYPE_TEXTAREA :
					ZJ_DonationFields::_renderTextAreaHidden($row);
					break ;
				case FIELD_TYPE_DROPDOWN :
					ZJ_DonationFields::_renderDropdownHidden($row);
					break ;
				case FIELD_TYPE_CHECKBOXLIST :
					ZJ_DonationFields::_renderCheckBoxListHidden($row);
					break ;
				case FIELD_TYPE_RADIOLIST :
					ZJ_DonationFields::_renderRadioListHidden($row);
					break ;
				case FIELD_TYPE_DATETIME :
					ZJ_DonationFields::_renderDateTimeHidden($row);
					break ;
				case FIELD_TYPE_MULTISELECT :
					ZJ_DonationFields::_renderMultiSelectHidden($row);
					break ;
			}
		}
		$output = ob_get_contents();
		ob_end_clean();
		return $output ;
	}
	/**
	 * Save Field Value
	 *
	 * @param int $id
	 * @return boolean
	 */
	function saveFieldValues($id, $data = array()) {
		if (count($data)) {
			$db	= &JFactory::getDBO();
			foreach ($data as $key=>$value) {
				$sql	= 'SELECT id FROM #__zj_donation_fields where `name` LIKE '.$db->Quote($key);
				$db->setQuery($sql);
				$field_id	= $db->loadResult();
				if ($field_id) {
					if (is_array($value)) {
						$value = implode('|*|', $value);
					}
					$value = $db->Quote($value);
					//Store new fields data
					$sql	= 'INSERT INTO #__zj_donation_field_values(`field_id`, `donate_id`, `field_value`)'
							. " VALUES($field_id, $id, $value)"
							;
					$db->setQuery($sql);
					$db->query();
				}
			}
		}
		return true;
	}
}
?>