<?php
/**
 * @version		$Id$
 * @author		Joomseller
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 * This file may not be redistributed in whole or significant part.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

/**
 * ZJ_Donation Component - MyDonation View
 * @package		ZJ_Donation
 * @subpackage	View
 */
class ZJ_DonationViewMyDonation extends JView {
	/**
	 * Display.
	 */
	function display($tpl = null) {
		global $mainframe;

		// get data from model
		$row			= &$this->get('Data');
		$fields			= &$this->get('Fields');

		//Get campaign
		$campaign_model	= ZJ_DonationFactory::getModel('campaign');
		$campaign		= $campaign_model->getData($row->campaign_id);

		// raise error if mydonation not found
		if (!isset($row->id)) {
			$mainframe->redirect(ZJ_DonationRoute::_('index.php?option=com_zj_donation&view=mydonations'), JText::_('COM_ZJ_DONATION_DONATION_NOT_FOUND'));
		}

		// handle the metadata
		$document	= &JFactory::getDocument();
		$pathway	= &$mainframe->getPathway();
		$params		= &$mainframe->getParams('com_zj_donation');
		$menus		= &JSite::getMenu();
		$menu		= $menus->getActive();
		
		$title		= JText::_('COM_ZJ_DONATION_DONATION'). ' #'. $row->id;

		if (is_object($menu) && isset($menu->query['option']) && $menu->query['option'] == 'com_zj_donation' && isset($menu->query['view']) && $menu->query['view'] == 'mydonation' && isset($menu->query['id']) && $menu->query['id'] == $row->id) {
			$menu_params = new JParameter($menu->params);
			if (!$menu_params->get('page_title')) {
				$params->set('page_title',	$title);
			} else {
				$params->set('page_title',	$menu_params->get('page_title'));
			}
		} else {
			$params->set('page_title',	$title);
		}
		$document->setTitle($params->get('page_title'));

		// add pathway
		if (is_object($menu) && isset($menu->query['option']) && $menu->query['option'] == 'com_zj_donation' && isset($menu->query['view']) && $menu->query['view'] == 'row' || isset($menu->query['id']) && $menu->query['id'] == $row->id ) {
			$pathway->addItem( $menu->name ? JText::_($menu->name) : $title);
		} else {
			$pathway->addItem($title);
		}

		$tmpl	= new ZJ_DonationTemplate();

		$tmpl->set('row',			$row);
		$tmpl->set('document',		$document);
		$tmpl->set('fields',		$fields);
		$tmpl->set('campaign',		$campaign);
		$tmpl->set('params',		$params);

		echo $tmpl->fetch('mydonation', $tpl);
	}
}