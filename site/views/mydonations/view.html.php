<?php
/**
 * @version		$Id$
 * @author		Joomseller
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 * This file may not be redistributed in whole or significant part.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

/**
 * ZJ_Donation Component - MyDonations View
 * @package		ZJ_Donation
 * @subpackage	View
 */
class ZJ_DonationViewMyDonations extends JView {
	/**
	 * Display.
	 */
	function display($tpl = null) {
		global $mainframe;

		$config 	= ZJ_DonationFactory::getConfig();
		// get page layout
		$layout = $this->getLayout();

		// get the page/component configuration
		$params = clone($mainframe->getParams('com_zj_donation'));

		// parameters
		$params->def('orderby_sec',				'');
		$params->def('show_pagination',			2);
		$limit	= $params->def('display_num',	10);

		JRequest::setVar('limit', (int) $limit);

		$orderby_sec = $params->get('orderby_sec');
		JRequest::setVar('orderby_sec', $orderby_sec);

		// get data from model
		$rows		= &$this->get('Data');
		$total		= &$this->get('Total');
		$pagination	= &$this->get('Pagination');

		// handle the metadata
		$document	= &JFactory::getDocument();
		$pathway	= &$mainframe->getPathway();
		$params		= &$mainframe->getParams('com_zj_donation');
		$menus		= &JSite::getMenu();
		$menu		= $menus->getActive();

		if (is_object($menu) && isset($menu->query['option']) && $menu->query['option'] == 'com_zj_donation' && isset($menu->query['view']) && $menu->query['view'] == 'mydonations') {
			$menu_params = new JParameter($menu->params);
			if (!$menu_params->get('page_title')) {
				$params->set('page_title',	JText::_('COM_ZJ_DONATION_MY_DONATIONS'));
			} else {
				$params->set('page_title',	$menu_params->get('page_title'));
			}
		} else {
			$params->set('page_title',	JText::_('COM_ZJ_DONATION_MY_DONATIONS'));
		}
		$document->setTitle($params->get('page_title'));

		$tmpl = new ZJ_DonationTemplate();

		$tmpl->set('params',		$params);
		$tmpl->set('rows',			$rows);
		$tmpl->set('pagination',	$pagination);

		echo $tmpl->fetch('mydonations', $layout);
	}
}