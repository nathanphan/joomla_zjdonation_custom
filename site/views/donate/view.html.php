<?php
/**
 * @version		$Id$
 * @author		Joomseller!
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

/**
 * ZJ_Donation Component - Donate View
 * @package		ZJ_Donation
 * @subpackage	View
 */
class ZJ_DonationViewDonate extends JView {

	
	/**
	 * Display.
	 */
// 	function display($tpl = null) {
// 		$task = JRequest::getVar('task');
// 		switch ($task){
// 			case 'donation':
// 				$this->_displayForm($tpl);
// 				break;
// 			case 'confirm_donation':
// 				$this->_confirmDonation($tpl);
// 				break;
// 			default:
// 				break;
// 		}
// 	}

	
	/**
	* Display by Nathan Phan.
	*/
		function display($tpl = null) {
			$task = JRequest::getVar('task');
			switch ($task){
				case 'donation':
						$this->_addDonateToCart($tpl);
					break;
				case 'confirm_donation':
					$this->_confirmDonation($tpl);
					break;
				case 'checkout':
					$this->_checkout($tpl);
					break;
				default:
					
					break;
			}
		}
		
	function _addDonateToCart($tpl = null){
		global $mainframe,$option;
		
		$config		= &ZJ_DonationFactory::getConfig();
		// handle the metadata
		$pathway	= &$mainframe->getPathway();
		$menus		= &JSite::getMenu();
		$menu		= $menus->getActive();
		$document	= JFactory::getDocument();
		$post		= &JRequest::get('post');
		
		
	}
	
	function _checkout($tpl = null){
		global $mainframe,$option;
		
		$config		= &ZJ_DonationFactory::getConfig();
		// handle the metadata
		$pathway	= &$mainframe->getPathway();
		$menus		= &JSite::getMenu();
		$menu		= $menus->getActive();
		$document	= JFactory::getDocument();
		$post		= &JRequest::get('post');
		$session = &JFactory::getSession();
		$cart = $session->get('DCart');
		
		//dump($cart,'cart in checkout');
		
		// get page layout
		$layout = $this->getLayout();
		$tmpl	= new ZJ_DonationTemplate();
		
		// get the page/component configuration
		$params 	= &$mainframe->getParams('com_zj_donation');
		
		$campaign_model		= ZJ_DonationFactory::getModel('campaign');
		
		foreach ($cart as $cart_key => $cart_value){
			//refresh
 			unset($cart_value['lists']); 
			unset($options);
			
			
			$row				= $campaign_model->getData4Checkout($cart_key);
			
			// build amount list
			$amounts	= explode(',', $row->amounts);
			$options[]	= JHTML::_('select.option', 0, JText::_('COM_ZJ_DONATION_SELECT')) ;
			for ($i = 0, $n = count(@$amounts); $i < $n; $i++) {
				$options[]		= array(
													'value'	=> $amounts[$i],
													'text'	=> ZJ_DonationUtils::formatPrice($amounts[$i])
				)
				;
			}
			$lists['amount1_'.$cart_key]	= JHTML::_('select.genericlist', $options, 'amount1_'.$cart_key, 'class="inputbox" onChange="" ', 'value', 'text');
					
			
			//add $lists to $cart
			$cart_value['lists'] = $lists;
						
			
			$cart[$cart_key] = $cart_value;			
			//dump($cart,'cart in checkout after');
		}
		//add new $cart to session
		$session->set('DCart',$cart);
		//dump($cart,'cart in checkout after');
		//load it again
		$cart = $session->get('DCart');
		
		
// 		if( !$row->id ) {
// 			$mainframe->redirect(ZJ_DonationRoute::_('index.php?option=com_zj_donation', false), JText::_('COM_ZJ_DONATION_CAMPAIGN_NOT_FOUND'));
// 			return;
// 		}
		
		// build recurring grid
		$lists['access']	= JHTML::_('select.booleanlist', 'access', 'class="inputbox"', 1, JText::_('COM_ZJ_DONATION_YES'), JText::_('COM_ZJ_DONATION_NO'));
		
		// build recurring grid
		$lists['recurring']	= JHTML::_('select.booleanlist', 'recurring', 'class="inputbox" onclick="changeRecurring(this.form);"', @$post['recurring'], JText::_('COM_ZJ_DONATION_RECURRING_RECURRING'), JText::_('COM_ZJ_DONATION_RECURRING_ONE_TIME'));
		
		// build recurring types list
		$lists['recurringtype']	= JHTML::_('zj_donation.recurringtype', 'recurring_id', @$post['recurring_id']);
		
		if (is_object($menu) && isset($menu->query['option']) && $menu->query['option'] == 'com_zj_donation' && isset($menu->query['view']) && $menu->query['view'] == 'form' && isset($menu->query['id']) && $menu->query['id'] == $row->id) {
			$menu_params = new JParameter($menu->params);
			
			$params->set('page_title',	$menu_params->get('page_title'));
			
		} else {
			$params->set('page_title',	'Checkout');
		}
		$document->setTitle($params->get('page_title'));
		
		// add pathway
		$pathway->addItem('Checkout Cart');
		
		$extra		= json_decode($row->fields);
		$fields		= array();
		for ($i = 0, $n = count(@$extra->id); $i < $n; $i++) {
			if ($extra->published[$i]) {
				$fields[]	= $extra->id[$i];
			}
		}
		
		//check if only one item in cart => can be recurring.
		
		if(count($cart) == 1 )
			$isrecurring = true;
		else 
			$isrecurring = false;
		
		
		
		$tmpl->set('params',			$params);
// 		$tmpl->set('row',				$row);
		$tmpl->set('config',			$config);
 		$tmpl->set('lists',				$lists);
		$tmpl->set('fields',			$fields);
		$tmpl->set('post',				$post);
		$tmpl->set('cart',				$cart);
		$tmpl->set('isRecurring',		$isrecurring);
		
		echo $tmpl->fetch('checkout',	$layout);
	}

	
	function _confirmDonation($tpl = null) {
		global $mainframe,$option;
		
		$post		= JRequest::get('post');
		
		$config		= &ZJ_DonationFactory::getConfig();
		// handle the metadata
		$pathway	= &$mainframe->getPathway();
		$menus		= &JSite::getMenu();
		$menu		= $menus->getActive();
		$document	= JFactory::getDocument();

		// get page layout
		$layout = $this->getLayout();
		$tmpl	= new ZJ_DonationTemplate();

		// get the page/component configuration
		$params 	= &$mainframe->getParams('com_zj_donation');

// 		$campaign_model		= ZJ_DonationFactory::getModel('campaign');
// 		$row				= $campaign_model->getData($post['id']);
		
		$session = &JFactory::getSession();
		$cart = $session->get('DCart');
		
		$campaign_model		= ZJ_DonationFactory::getModel('campaign');
		
		foreach ($cart as $cart_key => $cart_value){

			//indentify donate amount
			$donate_amount 	= !$post['amount2_'.$cart_key] ? $post['amount1_'.$cart_key] : $post['amount2_'.$cart_key];
			
			
			//add $lists to $cart
			$cart_value['donate_amount'] = $donate_amount;
		
				
			$cart[$cart_key] = $cart_value;
			//dump($cart,'cart in checkout after');
		}
		//add new $cart to session
		$session->set('DCart',$cart);
		
		//load it again
		$cart = $session->get('DCart');
		//dump($cart,'cart in checkout after');

// 		if( !$row->id ) {
// 			$mainframe->redirect(ZJ_DonationRoute::_('index.php?option=com_zj_donation', false), JText::_('COM_ZJ_DONATION_CAMPAIGN_NOT_FOUND'));
// 			return;
// 		}

// 		if (is_object($menu) && isset($menu->query['option']) && $menu->query['option'] == 'com_zj_donation' && isset($menu->query['view']) && $menu->query['view'] == 'form' && isset($menu->query['id']) && $menu->query['id'] == $row->id) {
// 			$menu_params = new JParameter($menu->params);
// 			$params->set('page_title',	'Confirm Checkout');
// 		} else {
			$params->set('page_title',	'Confirm Checkout');
// 		}
		$document->setTitle($params->get('page_title'));

		// add pathway
// 		if (is_object($menu) && isset($menu->query['option']) && $menu->query['option'] == 'com_zj_donation' && isset($menu->query['view']) && $menu->query['view'] == 'form' || isset($menu->query['id']) && $menu->query['id'] == $row->id ) {
// 			$pathway->addItem( $menu->name ? JText::_($menu->name) : $row->title);
// 		} else {
// 			$pathway->addItem($row->title);
// 		}

		$pathway->addItem('Confirm Checkout');

// 		$extra		= json_decode($row->fields);
// 		$fields		= array();
// 		for ($i = 0, $n = count(@$extra->id); $i < $n; $i++) {
// 			if ($extra->published[$i]) {
// 				$fields[]	= $extra->id[$i];
// 			}
// 		}
		
		$recurring_model	= ZJ_DonationFactory::getModel('recurringtype');
		$recurring_model->setId($post['recurring_id']);
		$recurring			= $recurring_model->getData();
		$isRecurring = false;
		if($recurring) $isRecurring = true;
		
		$tmpl->set('params',			$params);
// 		$tmpl->set('row',				$row);
		$tmpl->set('config',			$config);
		$tmpl->set('fields',			$fields);
		$tmpl->set('post',				$post);
		$tmpl->set('cart',				$cart);
 		$tmpl->set('recurring',			$recurring);
 		$tmpl->set('isrecurring',			$isRecurring);
		
		echo $tmpl->fetch('confirmdonation',	$layout);
	}
	
	function _displayForm($tpl = null) {
		global $mainframe,$option;
	
		$config		= &ZJ_DonationFactory::getConfig();
		// handle the metadata
		$pathway	= &$mainframe->getPathway();
		$menus		= &JSite::getMenu();
		$menu		= $menus->getActive();
		$document	= JFactory::getDocument();
		$post		= &JRequest::get('post');
	
		// get page layout
		$layout = $this->getLayout();
		$tmpl	= new ZJ_DonationTemplate();
	
		// get the page/component configuration
		$params 	= &$mainframe->getParams('com_zj_donation');
	
		$campaign_model		= ZJ_DonationFactory::getModel('campaign');
		$row				= $campaign_model->getData();
	
		if( !$row->id ) {
			$mainframe->redirect(ZJ_DonationRoute::_('index.php?option=com_zj_donation', false), JText::_('COM_ZJ_DONATION_CAMPAIGN_NOT_FOUND'));
			return;
		}
	
		if (is_object($menu) && isset($menu->query['option']) && $menu->query['option'] == 'com_zj_donation' && isset($menu->query['view']) && $menu->query['view'] == 'form' && isset($menu->query['id']) && $menu->query['id'] == $row->id) {
			$menu_params = new JParameter($menu->params);
			if (!$menu_params->get('page_title')) {
				$params->set('page_title',	JText::sprintf('COM_ZJ_DONATION_DONATE_FOR_CAMPAIGN', $row->title));
			} else {
				$params->set('page_title',	$menu_params->get('page_title'));
			}
		} else {
			$params->set('page_title',	JText::sprintf('COM_ZJ_DONATION_DONATE_FOR_CAMPAIGN', $row->title));
		}
		$document->setTitle($params->get('page_title'));
	
		// add pathway
		if (is_object($menu) && isset($menu->query['option']) && $menu->query['option'] == 'com_zj_donation' && isset($menu->query['view']) && $menu->query['view'] == 'form' || isset($menu->query['id']) && $menu->query['id'] == $row->id ) {
			$pathway->addItem( $menu->name ? JText::_($menu->name) : $row->title);
		} else {
			$pathway->addItem($row->title);
		}
	
		$extra		= json_decode($row->fields);
		$fields		= array();
		for ($i = 0, $n = count(@$extra->id); $i < $n; $i++) {
			if ($extra->published[$i]) {
				$fields[]	= $extra->id[$i];
			}
		}
	
		// build amount list
		$amounts	= explode(',', $row->amounts);
		$options[]	= JHTML::_('select.option', 0, JText::_('COM_ZJ_DONATION_SELECT')) ;
		for ($i = 0, $n = count(@$amounts); $i < $n; $i++) {
			$options[]		= array(
									'value'	=> $amounts[$i],
									'text'	=> ZJ_DonationUtils::formatPrice($amounts[$i])
			)
			;
		}
		$lists['amount1']	= JHTML::_('select.genericlist', $options, 'amount1', 'class="inputbox"', 'value', 'text', @$post['amount1']);
	
		// build recurring grid
		$lists['recurring']	= JHTML::_('select.booleanlist', 'recurring', 'class="inputbox" onclick="changeRecurring(this.form);"', @$post['recurring'], JText::_('COM_ZJ_DONATION_RECURRING_RECURRING'), JText::_('COM_ZJ_DONATION_RECURRING_ONE_TIME'));
	
		// build recurring grid
		$lists['access']	= JHTML::_('select.booleanlist', 'access', 'class="inputbox"', 1, JText::_('COM_ZJ_DONATION_YES'), JText::_('COM_ZJ_DONATION_NO'));
	
		// build recurring types list
		$lists['recurringtype']	= JHTML::_('zj_donation.recurringtype', 'recurring_id', @$post['recurring_id']);
	
	
		$tmpl->set('params',			$params);
		$tmpl->set('row',				$row);
		$tmpl->set('config',			$config);
		$tmpl->set('lists',				$lists);
		$tmpl->set('fields',			$fields);
		$tmpl->set('post',				$post);
	
		echo $tmpl->fetch('donate',	$layout);
	}
	
}
?>