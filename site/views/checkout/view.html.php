<?php
/**
 * @version		$Id$
 * @author		Joomseller
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 * This file may not be redistributed in whole or significant part.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

/**
 * ZJ_Donation Component - Product View
 * @package		ZJ_Donation
 * @subpackage	View
 */
class ZJ_DonationViewCheckout extends JView {
	/**
	 * Display.
	 */
	function display($tpl = null) {
		global $mainframe;
// 		dump(JRequest::get('post'));
		// get data from model
		$campaign		= &$this->get('Data');
		$donors			= &$this->get('Donors');
		$count			= &$this->get('Count');
		
		// raise error if product not found
		if (!isset($campaign->id)) {
			$mainframe->redirect(ZJ_DonationRoute::_('index.php?option=com_zj_donation&view=campaigns'), JText::_('COM_ZJ_DONATION_CAMPAIGN_NOT_FOUND'));
		}

		// handle the metadata
		$document	= &JFactory::getDocument();
		$pathway	= &$mainframe->getPathway();
		$params		= &$mainframe->getParams('com_zj_donation');
		$menus		= &JSite::getMenu();
		$menu		= $menus->getActive();

		if (is_object($menu) && isset($menu->query['option']) && $menu->query['option'] == 'com_zj_donation' && isset($menu->query['view']) && $menu->query['view'] == 'campaign' && isset($menu->query['id']) && $menu->query['id'] == $campaign->id) {
			$menu_params = new JParameter($menu->params);
			if (!$menu_params->get('page_title')) {
				$params->set('page_title',	$campaign->title);
			} else {
				$params->set('page_title',	$menu_params->get('page_title'));
			}
		} else {
			$params->set('page_title',	$campaign->title);
		}
		$document->setTitle($params->get('page_title'));

		// add pathway
		if (is_object($menu) && isset($menu->query['option']) && $menu->query['option'] == 'com_zj_donation' && isset($menu->query['view']) && $menu->query['view'] == 'campaign' || isset($menu->query['id']) && $menu->query['id'] == $campaign->id ) {
			$pathway->addItem( $menu->name ? JText::_($menu->name) : $campaign->title);
		} else {
			$pathway->addItem($campaign->title);
		}

		// set meta keyworks
		if ($campaign->meta_keywords) {
			$document->setMetaData('keywords', $campaign->meta_keywords);
		}

		// set meta description
		if ($campaign->meta_description) {
			$document->setDescription($campaign->meta_description);
		}


		//Set Facebook Metadata
		$document->addCustomTag( '<meta property="og:title" content="' . $campaign->title . '" />' );
		$meta_desc	= $campaign->meta_description;
		if (!$meta_desc) {
			$meta_desc	= strip_tags($campaign->description);
		}
		$document->addCustomTag( '<meta property="og:description" content="'. $meta_desc. '" />' );
		$gconfig	= &JFactory::getConfig();
		$document->addCustomTag( '<meta property="og:site_name" content="' . $gconfig->getValue('sitename') . '" />' );
		$document->addCustomTag( '<meta property="og:image" content="' . JURI::root() . 'media/zj_donation/images/'.$campaign->image . '" />' );

		$images	= json_decode($campaign->gallery);
		$tmpl	= new ZJ_DonationTemplate();

		$tmpl->set('campaign',		$campaign);
		$tmpl->set('images',		$images);
		$tmpl->set('document',		$document);
		$tmpl->set('donors',		$donors);
		$tmpl->set('count',			$count);
		$tmpl->set('action', JRequest::getVar('action'));

		echo $tmpl->fetch('campaign', $tpl);
	}
}