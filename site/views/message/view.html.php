<?php
/**
 * @version		$Id$
 * @author		Joomseller
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 * This file may not be redistributed in whole or significant part.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

/**
 * ZJ_Donation Component - Message View
 * @package		ZJ_Donation
 * @subpackage	View
 */
class ZJ_DonationViewMessage extends JView {
	/**
	 * Display.
	 */
	function display($tpl = null) {
		// get data from model

		// @todo: set page title here...

		// @todo: set pathway here...

		$type		= JRequest::getCmd('type');
		$config		= &ZJ_DonationFactory::getConfig();

		if ($type == 'cancel') {
			$title		= $config->get('page_cancel_title');
			$content	= JRequest::getString('page_cancel_msg');
		} else if ($type == 'thankyou') {
			$title		= $config->get('page_thanks_title');
			$content	= JRequest::getString('page_thanks_msg');
		}

		$tmpl = new ZJ_DonationTemplate();

		$tmpl->set('title',		$title);
		$tmpl->set('content',	$content);

		echo $tmpl->fetch('message', $tpl);
	}

	function getThanksMessage() {
		$user	= JFactory::getUser();
		$thanks_message	= array();
		
		$ret['search']	= array('{name}');
		$ret['replace']	= array($name);
		
		$thanks_message[$user->get('id')]	= $ret;
		return $thanks_message[$user->get('id')];
	}
}