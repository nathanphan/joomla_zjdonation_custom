<?php
/**
 * @version		$Id$
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 * This file may not be redistributed in whole or significant part.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/********************************************************************
 * DEFINE CONSTANTS.
 *******************************************************************/

define('ZJ_DONATION_COM_PATH',	JPATH_ROOT.DS.'components'.DS.'com_zj_donation');
define('ZJ_DONATION_IMG',			JURI::base() . 'components/com_zj_donation/libraries/image.php');

// permission return code
define('ZJ_DONATION_PERM_SUCCESS',	0);
define('ZJ_DONATION_PERM_DONE',		1);
define('ZJ_DONATION_PERM_GUEST',		2);
define('ZJ_DONATION_PERM_FAIL',		3);


/********************************************************************
 * LOAD LIBRARIES.
 *******************************************************************/

require_once(ZJ_DONATION_COM_PATH.DS.'libraries'.DS.'factory.php');
require_once(ZJ_DONATION_COM_PATH.DS.'libraries'.DS.'fields.php');
require_once(ZJ_DONATION_COM_PATH.DS.'libraries'.DS.'permission.php');
require_once(ZJ_DONATION_COM_PATH.DS.'libraries'.DS.'route.php');
require_once(ZJ_DONATION_COM_PATH.DS.'libraries'.DS.'template.php');
require_once(ZJ_DONATION_COM_PATH.DS.'libraries'.DS.'utils.php');

jimport('joomla.utilities.utility');
jimport('joomla.application.component.model');
JModel::addIncludePath(ZJ_DONATION_COM_PATH.DS.'models');
