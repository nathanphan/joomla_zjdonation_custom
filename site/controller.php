<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');

/**
 * ZJ_Donation Component - Base Controller.
 * @package		ZJ_Donation
 * @subpackage	Controller
 */
class ZJ_DonationController extends JController {
	
	/**
	 * default display
	 */
	function display() {
		// set a default view if none exists
		if (!JRequest::getCmd('view') && !JRequest::getCmd('task')) {
			JRequest::setVar('view', 'campaigns');
		}
		parent::display();
	}	
}
