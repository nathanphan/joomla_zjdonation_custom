<?php
/**
 * @version		$Id$
 * @author		Joomseller
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 * This file may not be redistributed in whole or significant part.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * ZJ_Donation Component - Donate Controller
 * @package		ZJ_Donation
 * @subpackage	Controller
 */
class ZJ_DonationControllerDonate extends ZJ_DonationController {
	var $_format_email			= null;
	var $_thanks_message		= null;

	/**
	 * Check donation action permission.
	 */
	function _checkPermission() {

		$permission = ZJ_DonationPermission::userCanDonate();

		if ($permission != ZJ_DONATION_PERM_SUCCESS) {
			if ($permission == ZJ_DONATION_PERM_GUEST) {
				$url = ZJ_DonationRoute::getURI();
				$this->setRedirect(JRoute::_('index.php?option=com_user&view=login&return=' . $url), JText::_('COM_ZJ_DONATION_PLEASE_LOGIN_TO_PERFORM_THIS_TASK'));
				return false;
			} else if ($permission == ZJ_DONATION_PERM_FAIL) {
				$this->setRedirect(JRoute::_('index.php?option=com_zj_donation'), JText::_('COM_ZJ_DONATION_YOU_DONT_HAVE_PERMISSION_TO_PERFORM_THIS_TASK'));
				return false;
			} else {
				$this->setRedirect(JRoute::_('index.php?option=com_zj_donation'), JText::_('COM_ZJ_DONATION_UNKNOWN_ERROR'));
				return false;
			}
		} else {
			return true;
		}
	}

	/**
	 * Validate donation form data
	 */
	function _formValidate($post) {
		global $mainframe;

		$success = true;
		$config = &ZJ_DonationFactory::getConfig();

		// check for first name
		if (!isset($post['first_name']) || trim($post['first_name'] == '')) {
			$mainframe->enqueueMessage(JText::_('COM_ZJ_DONATION_PLEASE_ENTER_YOUR_FIRST_NAME'), 'error');
			$success = false;
		}

		// check for last name
		if (!isset($post['last_name']) || trim($post['last_name'] == '')) {
			$mainframe->enqueueMessage(JText::_('COM_ZJ_DONATION_PLEASE_ENTER_YOUR_LAST_NAME'), 'error');
			$success = false;
		}

		// check for email
		jimport('joomla.mail.helper');
		if (!isset($post['email']) || !JMailHelper::isEmailAddress($post['email'])) {
			$mainframe->enqueueMessage(JText::_('COM_ZJ_DONATION_PLEASE_ENTER_YOUR_VALID_EMAIL_ADDRESS'), 'error');
			$success = false;
		} else {
			if (isset($post['task']) && $post['task'] == 'confirm_donation' && (!isset($post['email_confirm']) || $post['email'] != $post['email_confirm'])) {
				$mainframe->enqueueMessage(JText::_('COM_ZJ_DONATION_PLEASE_CHECK_YOUR_EMAIL_THE_CONFIRMATION_ENTRY_DOES_NOT_MATCH'), 'error');
				$success = false;
			}
		}

		// check for organization
		if ($config->get('field_organization_visible') && $config->get('field_organization_required')) {
			if (!isset($post['organization']) || trim($post['organization'] == '')) {
				$mainframe->enqueueMessage(JText::_('COM_ZJ_DONATION_PLEASE_ENTER_YOUR_ORGANIZATION'), 'error');
				$success = false;
			}
		}

		// check for website
		if ($config->get('field_website_visible') && $config->get('field_website_required')) {
			if (!isset($post['website']) || trim($post['website'] == '')) {
				$mainframe->enqueueMessage(JText::_('COM_ZJ_DONATION_PLEASE_ENTER_YOUR_WEBSITE'), 'error');
				$success = false;
			}
		}

		// check for address
		if ($config->get('field_address_visible') && $config->get('field_address_required')) {
			if (!isset($post['address']) || trim($post['address'] == '')) {
				$mainframe->enqueueMessage(JText::_('COM_ZJ_DONATION_PLEASE_ENTER_YOUR_ADDRESS'), 'error');
				$success = false;
			}
		}

		// check for city
		if ($config->get('field_city_visible') && $config->get('field_city_required')) {
			if (!isset($post['city']) || trim($post['city'] == '')) {
				$mainframe->enqueueMessage(JText::_('COM_ZJ_DONATION_PLEASE_ENTER_YOUR_CITY'), 'error');
				$success = false;
			}
		}

		// check for state
		if ($config->get('field_state_visible') && $config->get('field_state_required')) {
			if (!isset($post['state']) || trim($post['state'] == '')) {
				$mainframe->enqueueMessage(JText::_('COM_ZJ_DONATION_PLEASE_ENTER_YOUR_STATE'), 'error');
				$success = false;
			}
		}

		// check for zip
		if ($config->get('field_zip_visible') && $config->get('field_zip_required')) {
			if (!isset($post['zip']) || trim($post['zip'] == '')) {
				$mainframe->enqueueMessage(JText::_('COM_ZJ_DONATION_PLEASE_ENTER_YOUR_ZIP'), 'error');
				$success = false;
			}
		}

		// check for country
		if ($config->get('field_country_visible') && $config->get('field_country_required')) {
			if (!isset($post['country']) || trim($post['country'] == '')) {
				$mainframe->enqueueMessage(JText::_('COM_ZJ_DONATION_PLEASE_ENTER_YOUR_COUNTRY'), 'error');
				$success = false;
			}
		}

		// check for phone
		if ($config->get('field_phone_visible') && $config->get('field_phone_required')) {
			if (!isset($post['phone']) || trim($post['phone'] == '')) {
				$mainframe->enqueueMessage(JText::_('COM_ZJ_DONATION_PLEASE_ENTER_YOUR_PHONE'), 'error');
				$success = false;
			}
		}

		// check amount
		$task = JRequest::getVar('task');
		if ($task == 'confirm_donation') {
			
			$session = &JFactory::getSession();
			$cart = $session->get('DCart');
			
			foreach ($cart as $cart_key => $cart_value): 
					
					if (!$post['amount1_'.$cart_key] && !$post['amount2_'.$cart_key] ) {
				
						$mainframe->enqueueMessage(JText::_('COM_ZJ_DONATION_PLEASE_ENTER_YOUR_DONATION_AMOUNT'), 'error');
						$success = false;
					}
				
			endforeach;
			
		}

		// check payment method
		if (!isset($post['payment_method'])) {
			$mainframe->enqueueMessage(JText::_('COM_ZJ_DONATION_PLEASE_CHOOSE_A_PAYMENT_METHOD'), 'error');
			$success = false;
		}
		
		return $success;
	}

	/**
	 * Get search and replace string for email sending.
	 */
	function _getFormatEmail($donation_id) {
		if (empty($this->_format_email[$donation_id])) {
			// get the donation info
			$donation_model	= &ZJ_DonationFactory::getModel('donate');
			unset($donation_model->_data);
			$donation		= $donation_model->getData($donation_id);
			$config			= &ZJ_DonationFactory::getConfig();

			$campaign_model	= &ZJ_DonationFactory::getModel('campaign');
			$campaign		= $campaign_model->getData($donation->campaign_id);

			$name			= $donation->first_name . ' ' . $donation->last_name;
			$email			= $donation->email;
			$fields			= $donation_model->getFields($donation_id);
			$extra_info		= '';

			// organization
			if ($config->get('field_organization_visible')) {
				$extra_info .= JText::_('COM_ZJ_DONATION_ORGANIZATION'). ': ' . $donation->organization . '<br />';
			}
			// website
			if ($config->get('field_website_visible')) {
				$extra_info .= JText::_('COM_ZJ_DONATION_WEBSITE'). ': ' . $donation->website . '<br />';
			}
			// address
			if ($config->get('field_address_visible')) {
				$extra_info .= JText::_('COM_ZJ_DONATION_ADDRESS'). ': ' . $donation->address . '<br />';
			}
			// city
			if ($config->get('field_city_visible')) {
				$extra_info .= JText::_('COM_ZJ_DONATION_CITY'). ': ' . $donation->city . '<br />';
			}
			// state
			if ($config->get('field_state_visible')) {
				$extra_info .= JText::_('COM_ZJ_DONATION_STATE'). ': ' . $donation->state . '<br />';
			}
			// zip code
			if ($config->get('field_zip_visible')) {
				$extra_info .= JText::_('COM_ZJ_DONATION_ZIP_CODE'). ': ' . $donation->zip . '<br />';
			}
			// country
			if ($config->get('field_country_visible')) {
				$extra_info .= JText::_('COM_ZJ_DONATION_COUNTRY'). ': ' . $donation->country . '<br />';
			}
			// phone
			if ($config->get('field_phone_visible')) {
				$extra_info .= JText::_('COM_ZJ_DONATION_PHONE'). ': ' . $donation->phone . '<br />';
			}

			$full_info		= JText::_('COM_ZJ_DONATION_NAME'). ': ' . $name . '<br />'
							. JText::_('COM_ZJ_DONATION_EMAIL'). ': ' . $email . '<br />'
							. $extra_info
			;

			$donation_id		= $donation->id;
			$donation_date		= JHTML::_('date', $donation->created_date);
			$payment_date		= JHTML::_('date', $donation->payment_date);
			$transaction_id		= $donation->transaction_id;
			$campaign_title		= $campaign->title;
			$campaign_desc		= $campaign->description;
			$campaign_link		= ZJ_DonationRoute::_('index.php?option=com_zj_donation&view=campaign&id='.$campaign->id);
			$donor_link			= ZJ_DonationRoute::_('index.php?option=com_zj_donation&view=mydonation&id='.$donation->user_id);

			//Recurring information
			$recurring			= ($donation->recurring) ? JText::_('COM_ZJ_DONATION_YES') : JText::_('COM_ZJ_DONATION_NO');
			$recurring			= JText::_('COM_ZJ_DONATION_RECURRING_RECURRING'). ': ' . $recurring . '<br />';

			$recurring_info		= '';
			if ($donation->recurring) {				
				$recurring_model	= ZJ_DonationFactory::getModel('recurringtype');
				$recurring_model->setId($donation->recurring_id);
				$recurring_data		= $recurring_model->getData();

				$recurring_times	= ($donation->recurring_times) ? $donation->recurring_times : JText::_('COM_ZJ_DONATION_RECURRING_UNLIMITED');
				
				$recurring_info	.= '<br/><strong>'. JText::_('COM_ZJ_DONATION_RECURRING_INFORMATION'). '</strong><br/>';
				$recurring_info	.= JText::_('COM_ZJ_DONATION_SUBSCRIBE_ID'). ': '. $donation->subscribe_id. '<br/>';
				$recurring_info	.= JText::_('COM_ZJ_DONATION_RECURRING_TYPE'). ': '. $recurring_data->title. '<br/>';
				$recurring_info	.= JText::_('COM_ZJ_DONATION_RECURRING_TIMES'). ': '. $recurring_times . '<br/>';
				if ($donation->donated) {
					$recurring_info	.= JText::_('COM_ZJ_DONATION_DONATED_TIMES'). ': '. $donation->donated_times. '<br/>';
					$recurring_info	.= JText::_('COM_ZJ_DONATION_CAMPAIGN_DONATED_AMOUNT'). ': '. ZJ_DonationUtils::formatPrice($donation->amount, null, true). '<br/>';

					$donation_amount	= $donation->amount / $donation->donated_times;
					$donation_amount	= ZJ_DonationUtils::formatPrice($donation_amount, null, true);
				} else {
					$donation_amount	= ZJ_DonationUtils::formatPrice($donation->amount, null, true);
				}
			} else {
				$donation_amount	= ZJ_DonationUtils::formatPrice($donation->amount, null, true);
			}

			//Addition Information
			$addition_info	= '';
			if (count($fields)) {
				$addition_info	.= '<br/><strong>'. JText::_('COM_ZJ_DONATION_ADDITION_INFORMATION'). '</strong><br/>';
				foreach ($fields as $field) {
					$addition_info	.= $field->title. ': '. $field->field_value. '<br/>';
				}
			}

			$ret['search']	= array('{name}', '{email}', '{full_info}', '{donation_id}', '{donation_amount}', '{donation_date}', '{recurring}', '{recurring_info}', '{payment_date}', '{campaign_link}', '{campaign_title}', '{campaign_desc}', '{transaction_id}', '{donor_link}', '{addition_info}');
			$ret['replace']	= array($name, $email, $full_info, $donation_id, $donation_amount, $donation_date, $recurring, $recurring_info, $payment_date, $campaign_link, $campaign_title, $campaign_desc, $transaction_id, $donor_link, $addition_info);

			$this->_format_email[$donation_id] = $ret;
		}

		return $this->_format_email[$donation_id];
	}

	/**
	 * Display donation form.
	 */
// 	function donation() {
// 		// check permission
// 		if (!$this->_checkPermission()) {
// 			return false;
// 		}

// 		JRequest::setVar('task', 'donation');
// 		JRequest::setVar('view', 'donate');

// 		parent::display();
// 	}
	/**
	* Display donation form by Nathan Phan.
	*/
		function donation() {
			// check permission
			if (!$this->_checkPermission()) {
				return false;
			}
			$this->addToCart();
			
			JRequest::setVar('action','donated');
			JRequest::setVar('view', 'campaign');			
		
			parent::display();
		}
		
		function addToCart(){
			$session	=	& JFactory::getSession();
			
			$campaign_model		= ZJ_DonationFactory::getModel('campaign');
			$row				= $campaign_model->getData(JRequest::getInt('id'));
			
			
			$cart 	=	$session->get('DCart');
			
			if(isset($cart) && !empty($cart)){
				$cart[$row->id]	=	array($row->id => $row);
				
			}
			else{
				$cart[$row->id]	=	array($row->id => $row);
				
			}
			
			$session->set('DCart',$cart);
			
			
			//$session->set('DCart',$cart);
			//$cart = $session->get('DCart');
			//dump($session->get('DCart'));
			
		}
		
		function removeFromCart(){
			$session	=	& JFactory::getSession();
			$cart 	=	$session->get('DCart');
			
			$remove_id = JRequest::getInt('iId');
			//dump($remove_id);
			if(isset($cart) && !empty($cart)){
				foreach ($cart as $cart_key => $cart_value){
					if($cart_key == $remove_id){
						unset($cart[$cart_key]);
						$session->set('DCart',$cart);
					}
				}
			}
			
			$this->execute('checkout');
		}
		
		/**
		 * check out
		 */
		function checkout(){
			// check permission
			if (!$this->_checkPermission()) {
				return false;
			}
			
			JRequest::setVar('task','checkout');
			JRequest::setVar('view', 'donate');
			
			parent::display();
		}

	/**
	 * Confirm donation.
	 */
	function confirm_donation() {
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');
		
		// check permission
		if (!$this->_checkPermission()) {
			return false;
		}

		// check form validation
		$post	= &JRequest::get('post');
		
		if ($this->_formValidate($post)) {
			JRequest::setVar('view', 'donate');
			JRequest::setVar('task', 'confirm_donation');
			parent::display();
		} else {
			$this->execute('checkout');
		}
	}

	/**
	 * Process donation.
	 */
	function process_donation() {
		global $mainframe;
		
		// check for request forgeries
		JRequest::checkToken() or jexit('Invalid Token');

		// check permission
		if (!$this->_checkPermission()) {
			return false;
		}

		$config = &ZJ_DonationFactory::getConfig();

		// check form validation
		$post	= &JRequest::get('post');

		if ($this->_formValidate($post)) {			
			$order_model		= &ZJ_DonationFactory::getModel('order');
			$order	= $order_model->store( );
			
			//get order id for order_detail
			$order_id = $order->id;
			
			JRequest::setVar('order_id',$order_id,'post');
			
			//get campaign and its donated amount in Cart session
			$session = &JFactory::getSession();
			$cart = $session->get('DCart');
			
			$order_detail_model		= &ZJ_DonationFactory::getModel('order_detail');
			
			$donate_model			= &ZJ_DonationFactory::getModel('donate');
			$donation_recurring = '';
			foreach ($cart as $cart_key => $cart_value){
				
				JRequest::setVar('campaign_id',$cart_key,'post',true);
				JRequest::setVar('amount',$cart_value['donate_amount'],'post',true);
				
				$order_detail_model->store( );

				//insert to donate table
				$post = JRequest::get('post');
				$donation_recurring = $donate_model->store( $post );
			}
			

			//Send confirmation email
// 			$campaign_model	= &ZJ_DonationFactory::getModel('campaign');
// 			$campaign		= $campaign_model->getData($donation->campaign_id);
// 			$messages		= json_decode($campaign->messages);
// 			if ($messages->notify_new_donation) {
// 				$this->sendConfirmationEmail($order->id, $messages, $order->email);
// 			}
			
			$user			= &JFactory::getUser();
// 			if($config->get('js_donate')) {
// 				$this->pushDonateNotification($user->get('id'), $donation->id);
// 			}			
			
// 			if ($donation->amount != 0) {
				//Process donation  here
				$dispatcher				= &JDispatcher::getInstance();
				$config					= ZJ_DonationFactory::getConfig();
				$order->payment_method	= $post['payment_method'];
				//$order->return_url		= ZJ_DonationRoute::_(JURI::base().'index.php?option=com_zj_donation&controller=donate&task=payment_return&order_id=' . $order_id . '&token='. $order->token);
				$order->return_url		= ZJ_DonationRoute::_(JURI::base().'index.php?option=com_zj_donation&controller=donate&task=payment_notify&payment_method='.$post['payment_method']);
				//$donation->cancel_url		= ZJ_DonationRoute::_(JURI::base().'index.php?option=com_zj_donation&controller=donate&task=payment_cancel&id=' . $donation->id . '&token=' . $donation->token);
				$order->notify_url		= ZJ_DonationRoute::_(JURI::base().'index.php?option=com_zj_donation&controller=donate&task=payment_notify&payment_method='.$post['payment_method']);
				$order->invoice			= $order->token;
				
				$order->currency_code	= $config->get('currency_code');
				$order->recurring		= false;
				//dump($post);
				//recurring information
				$recurring_id				= (int)@$post['recurring_id'];
				$recurring_model			= ZJ_DonationFactory::getModel('recurringtype');
				$recurring_model->setId($recurring_id);
				$recurring					= $recurring_model->getData();
				if ($recurring->id) {
					$order->recurring_value	= $recurring->value;
					$order->recurring_type	= strtoupper($recurring->type);
					$order->recurring_times	= $post['recurring_times'];
					$order->recurring		= true;
				}
			//	dump($order,'order');
				JPluginHelper::importPlugin('zjdonation',$post['payment_method']);
				if ($order->recurring) {
					
					$dispatcher->trigger('onProcessRecurringPaymentNew',array ($order,$cart));
				} else {
					
					$dispatcher->trigger('onProcessPaymentCart',array ($order,$cart));
				}
// 			}
		} else {
			$this->execute('checkout1');
		}
	}

	/**
	 * User cancel payment.
	 */
	function payment_cancel() {
		// check if donation is valid
		$id		= JRequest::getInt('id');
		$token	= JRequest::getString('token');

		$donation_model	= &ZJ_DonationFactory::getModel('donate');
		$donation		= &$donation_model->getData($id);

		if ($donation->token != $token || $donation->donated != 0) {
			$this->setRedirect(ZJ_DonationRoute::_('index.php?option=com_zj_donation'), JText::_('COM_ZJ_DONATION_INVALID_DONATION'));
			return false;
		}

		// display cancel page
		$config = &ZJ_DonationFactory::getConfig();

		if ($config->get('page_cancel_type')) {
			$this->setRedirect(JRoute::_($config->get('page_cancel_url')));
		} else {
			// redirect to cancel page			
			$format				= $this->_getThanksMessage($id);
			$page_cancel_msg	= str_replace($format['search'], $format['replace'], $config->get('page_cancel_msg'));
			JRequest::setVar('view', 'message');
			JRequest::setVar('type', 'cancel');
			JRequest::setVar('page_cancel_msg', $page_cancel_msg);
			parent::display();
		}
	}

	/**
	 * Payment complete.
	 */
	function payment_return() {
		global $mainframe;

		// check if donation is valid
		$id		= JRequest::getInt('id');
		$token	= JRequest::getString('token');
		
		//$donation_model	= &ZJ_DonationFactory::getModel('donate');
		$order_model		= &ZJ_DonationFactory::getModel('order');
		$order				= $order_model->getData( );
		
		if ($order->token != $token) {
			$this->setRedirect(ZJ_DonationRoute::_('index.php?option=com_zj_donation'), JText::_('COM_ZJ_DONATION_INVALID_DONATION'));
			return false;
		}

		$config = &ZJ_DonationFactory::getConfig();
		
		$session = &JFactory::getSession();		
		$session->destroy();
		
		// display thank you page
		if ($config->get('page_thanks_type')) {
			$this->setRedirect(JRoute::_($config->get('page_thanks_url')));
		} else {
			$format				= $this->_getThanksMessage($id);
			$page_thanks_msg	= str_replace($format['search'], $format['replace'], $config->get('page_thanks_msg'));
			JRequest::setVar('view', 'message');
			JRequest::setVar('type', 'thankyou');
			JRequest::setVar('page_thanks_msg', $page_thanks_msg);
			parent::display();
		}
	}

	function _getThanksMessage($id) {
		$donation_model	= &ZJ_DonationFactory::getModel('donate');
		$donation		= &$donation_model->getData($id);

		$name			= $donation->first_name . ' ' . $donation->last_name;				

		$ret['search']	= array('{name}');
		$ret['replace']	= array($name);

		$this->_thanks_message[$id]	= $ret;
		
		return $this->_thanks_message[$id];
	}

	/**
	 * Payment complete notify.
	 */
	function payment_notify() {
		global $mainframe;
		
		$dispatcher		= &JDispatcher::getInstance();
		$payment_method = JRequest::getString('payment_method');
		
		JPluginHelper::importPlugin('zjdonation',$payment_method);
		$post	= JRequest::get('post');
		$config = &ZJ_DonationFactory::getConfig();
	//	dump($post,'post in notity');
		
		if( count($post) ){
			$data		= $dispatcher->trigger('onPaymentNotify',array ($payment_method));
			//dump($data,'data in notify');
			if (count($data)) {
				$data	= $data[0];
				
				
			} else {
				return;
			}
			
 			$donation_model	= &ZJ_DonationFactory::getModel('donate');
// 			$donation		= &$donation_model->getData($data['order_id']);
			
			JRequest::setVar('order_id', $data['order_id'],'post');
			
			$order_model		= &ZJ_DonationFactory::getModel('order');
			$order				= $order_model->getData( );
			$order_detail_model		= &ZJ_DonationFactory::getModel('order_detail');
			

			if ($order->recurring) {
				if ($data['transaction_type'] != 'subscr_payment') {
					return;
				} elseif ($data['transaction_id'] == $order->transaction_id) {
					//ensure new transaction ID
					return;
				}
			} 
// 			else {
// 				if ($order->transaction_id != '') {
// 					//ensure new transaction ID
// 					return;
// 				}
// 			}
			

			$order->currency_code	= $config->get('currency_code');
			$order->payment_method	= $payment_method;
			$result 	= $dispatcher->trigger('onVerifyPayment',array ($order));
			
			if (count($result)) {
				$result	= $result[0];
				
			} else {
				return;
			}

			if ($result == 'COMPLETED'){
				$order_detail_list = $donation_model->getListByOrderId($data['order_id']);     
				
				foreach ($order_detail_list as $each) {
					
					$donation		= $donation_model->approveNew($each,$data['transaction_id']);
				}                             
				

// 				if (!$donation->id) {
// 					return false;
// 				}
				//Send notification email
// 				$campaign_model	= &ZJ_DonationFactory::getModel('campaign');
// 				$campaign		= $campaign_model->getData($donation->campaign_id);
// 				$messages		= json_decode($campaign->messages);
// 				if ($messages->notify_donation_actived) {
// 					$this->sendNotificationEmail($donation->id, $messages, $donation->email);
// 				}
			}
		}
		JRequest::setVar('token',$order->token);
		$this->payment_return($data['order_id'], $order->token);
	}

	/**
	 * Send donation email
	 */
	function sendNotificationEmail($id, $messages, $donation_email) {
		global $mainframe;

		$format		= $this->_getFormatEmail($id);
		$config		= &ZJ_DonationFactory::getConfig();
		
		// send donation active email
		$from		= $config->get('store_email', $mainframe->getCfg('mailfrom'));
		$fromname	= $config->get('store_email_name', $mainframe->getCfg('fromname'));
		$recipient	= $donation_email;
		$subject	= str_replace($format['search'], $format['replace'], $messages->email_donation_actived_subject);
		$body		= str_replace($format['search'], $format['replace'], $messages->email_donation_actived_body);
		JUtility::sendMail($from, $fromname, $recipient, $subject, $body, 1);

		// send notify email to admin
		$from		= $config->get('store_email', $mainframe->getCfg('mailfrom'));
		$fromname	= $config->get('store_email_name', $mainframe->getCfg('fromname'));
		$recipient	= $messages->notify_email;
		$subject	= str_replace($format['search'], $format['replace'], $messages->email_admin_donation_actived_subject);
		$body		= str_replace($format['search'], $format['replace'], $messages->email_admin_donation_actived_body);
		JUtility::sendMail($from, $fromname, $recipient, $subject, $body, 1);
	}

	/**
	 * send email notification
	 */
	function sendConfirmationEmail($id, $messages, $donation_email) {
		global $mainframe;
		
		$format		= $this->_getFormatEmail($id);
		$config		= &ZJ_DonationFactory::getConfig();
		
		// send notify email to admin
		$from		= $config->get('store_email', $mainframe->getCfg('mailfrom'));
		$fromname	= $config->get('store_email_name', $mainframe->getCfg('fromname'));
		$recipient	= $messages->notify_email;
		$subject	= str_replace($format['search'], $format['replace'], $messages->email_admin_donation_created_subject);
		$body		= str_replace($format['search'], $format['replace'], $messages->email_admin_donation_created_body);
		JUtility::sendMail($from, $fromname, $recipient, $subject, $body, 1);

		//Send email to donor
		$from		= $config->get('store_email', $mainframe->getCfg('mailfrom'));
		$fromname	= $config->get('store_email_name', $mainframe->getCfg('fromname'));
		$recipient	= $donation_email;
		$subject	= str_replace($format['search'], $format['replace'], $messages->email_thanks_subject);
		$body		= str_replace($format['search'], $format['replace'], $messages->email_thanks_body);
		JUtility::sendMail($from, $fromname, $recipient, $subject, $body, 1);
	}
	
	/**
	 * Post an notification on user jomsocial Wall when donate
	 */
	function pushDonateNotification($userid = null, $donateid	= null) {
		if ($userid && $donateid) {
			$model			= ZJ_DonationFactory::getModel('campaign');
			$donate			= $model->getData($donateid);

			$link		    = ZJ_DonationRoute::_('index.php?option=com_zj_donation&view=campaign&id='.$donate->id);			
            $link		    = '<span><strong style=\'color:blue;\'><a href=\''. $link. '\'>'.$donate->title.'</a></strong></span>';

			$act->cmd		= 'Donation.donate';
			$act->actor		= $userid;
			$act->target	= 0; // no target            
			$act->title		= JText::sprintf('COM_ZJ_DONATION_JUST_DONATED_FOR_CAMPAIGN', '{actor}', $link);			
			$act->content	= '';
			$act->app		= 'wall';
			$act->cid		= 0;
			$act->params	= '';

			$this->AddActivity($act);
		}
	}
	
	function AddActivity($activity) {        
        $db     = JFactory::getDBO();
        $date   = JFactory::getDate();

		$sql	= ' SELECT id'
				. ' FROM #__community_activities'
				. ' ORDER BY id DESC'
				. ' LIMIT 0,1'
				;
		$db->setQuery($sql);
		$activity_id	= $db->loadResult() + 1;
        
        $cmd            = !empty($activity->cmd) ? $activity->cmd : '';             
        $actor		    = !empty($activity->actor) ? $activity->actor 	: '';
		$target 	    = !empty($activity->target) ? $activity->target : 0;
		$title		    = !empty($activity->title) ? $activity->title 	: '';
		$content	    = !empty($activity->content) ? $activity->content : '';
		$appname	    = !empty($activity->app) ? $activity->app : '';
		$cid		    = !empty($activity->cid) ? $activity->cid : 0;
		$params         = !empty($activity->params) ? $activity->params : '';		
		$points		    = 1;
		$access		    = 0;
		$location	    = '';		
		$comment_id	    = $activity_id;
		$comment_type	= 'profile.status';
		$like_id		= $activity_id;
		$like_type		= 'profile.status';
        $created        = $db->Quote($date->toMySQL());        
        $archived       = 0;
        
        $sql    = ' INSERT INTO #__community_activities(actor,target,title,content,app,'
                . ' cid,created,access,params,points,archived,location,comment_id,'
                . ' comment_type,like_id,like_type)'
                . ' VALUES('.$actor.','.$target.',"'.$title.'","'.$content.'","'.$appname.'",'.$cid
                . ' ,'.$created.','.$access.',"'.$params.'",'.$points.','.$archived.',"'.$location.'",'.$comment_id
                . ' ,"'.$comment_type.'",'.$like_id.',"'.$like_type.'")'
                ;
        
        $db->setQuery($sql);
        $db->Query();                                        
    }
}