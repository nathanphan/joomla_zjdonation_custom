<?php
/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	ZJ_Donation
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Utility class for creating HTML lists for ZJ_Donation component
 * @package		ZJ_Donation
 * @subpackage	Helper
 */
class JHTMLZJ_Donation {
	/**
	 * Display (Un) published icon
	 * @return string
	 */
	function icon($value, $text1 = 'Recurring', $text2 = 'Not Recurring', $imgY = 'tick.png', $imgX = 'publish_x.png') {
		$img	= $value ? $imgY : $imgX;
		$alt 	= $value ? $text1 : $text2;
		$href = '<img src="images/'. $img .'" border="0" alt="'. $alt .'" title="'. $alt .'" />';
		return $href;
	}

	/**
	 * Display the countries listing
	 * @param $name Name of the list
	 * @param $active Active list item
	 * @param $javascript Javascript of the list
	 * @param $size Size of the list
	 * @return string
	 */
	function country($name = 'country', $active = 0, $javascript = '', $size = 1) {

		$db		= &JFactory::getDBO();
		$query	= 'SELECT `name` AS value, `name` AS text'
				. ' FROM #__zj_donation_countries AS a'
				. ' WHERE a.published = 1'
				. ' ORDER BY a.id'
				;
		$db->setQuery($query);
		$rows = $db->loadObjectList();
		return JHTML::_('select.genericlist', $rows, $name, 'class="inputbox" size="' . $size . '" ' . $javascript, 'value', 'text', $active);
	}

	/**
	 * Display the recurring listing
	 * @param $name Name of the list
	 * @param $active Active list item
	 * @param $javascript Javascript of the list
	 * @param $size Size of the list
	 * @return string
	 */
	function recurringtype($name = 'recurring_id', $active = 0, $javascript = '', $size = 1) {

		$db		= &JFactory::getDBO();
		$query	= 'SELECT `id` AS value, `title` AS text'
				. ' FROM #__zj_donation_recurring_types AS a'
				. ' WHERE a.published = 1'
				. ' ORDER BY a.ordering'
				;
		$db->setQuery($query);
		$rows = $db->loadObjectList();
		return JHTML::_('select.genericlist', $rows, $name, 'class="inputbox" size="' . $size . '" ' . $javascript, 'value', 'text', $active);
	}
}